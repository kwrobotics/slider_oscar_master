#include <iostream>
#include <chrono>
#include <cmath>
#include <Eigen/Dense>

#include "ros/ros.h"
#include <std_msgs/Float64.h>
#include <std_msgs/Float64MultiArray.h>

using namespace Eigen;
using namespace std;

const double m = 11.5;    // body mass
const double m_s = 0.3;   // swing leg mass
const double L = 1.2;     // leg length
const double h = 0.6;     // body height
const double g = 9.81;    // gravity
const double omega = sqrt(g / h);  // omega

double weight_x = 1.0;
double weight_y = 1.0;

const double T_end = 100;
const double dt = 0.001;
//const double vmax = 2.0;              // m/s
const double step_time = 0.4;
int flag = 0;
int support_foot_flag = -1;
Vector2d x_dot_ref(0.0, 0.0);     // m/s
// Parameters subscribed from the slider_controller node
Vector2d x_0(0.0, 0.0);
Vector2d x_dot_0(0.0, 0.0);
Vector4d t_s(0, 0.2, 0.45, 0.70);
Matrix<double,2,3> u_s((Matrix<double,2,3>() << 0.0, 0.0,  0.02, 0.15, -0.21924587,  0.03075412).finished());
Vector4d t_s_rk4;
Matrix<double,2,3> u_s_rk4;


// Eigen::Matrix3d A((Eigen::Matrix3d() << 1, 2, 3, 4, 5, 6, 7, 8, 9).finished());

double left_x = 0.0;
double left_y = 0.15;
double right_x = 0.0;
double right_y = -0.15;
int foot_flag = -1;

//////////////// GOOD FOR LATERAL WALKING ///////////////////////////
// step duration penalties
const double kdt0 = 0.3;
const double kdt1 = 0.3;
const double kdt2 = 0.3;
const double kdt0_ = 20;
const double kdt1_ = 20;
const double kdt2_ = 20;
// leg length penalties
//const double lmax = 0.5*sqrt(pow(L, 2.0) - pow(h, 2.0));
const double k_t_len = 1.00;
const double k_ux_len = 1.000;
const double k_uy_len = 1.000;
const double klmax = 10.0;
// foot radius penalty
//const double rfoot = 0.23;
const double k_foot = 1.0;// step duration penalties
const double k_foot_ = 50.0;
// vmax penalties
const double vmax = 2.5;              // m/s
const double kvmax = 2.0;
const double kdtvmax = 2.00;
const double kuxvmax = 2.00;
const double kuyvmax = 2.00;

// step duration penalties
//const double kdt0 = 1.0;
//const double kdt1 = 0.5;
//const double kdt2 = 0.05;
//// leg length penalties
//const double k_t_len = 0.05;
//const double k_ux_len = 0.005;
//const double k_uy_len = 0.005;
//// foot radius penalty
//const double k_foot = 0.5;// step duration penalties
//
//// vmax penalties
//const double kdtvmax = 0.05;
//const double kuxvmax = 0.01;
//const double kuyvmax = 0.01;

//const double kdt0 = 0.01;
//const double kdt1 = 0.005;
//const double kdt2 = 0.0005;
//// leg length penalties
//const double k_t_len = 0.005;
//const double k_ux_len = 0.005;
//const double k_uy_len = 0.005;
//// foot radius penalty
//const double k_foot = 0.5;

double swing_x = 0.0;
double swing_z = 0.0;
double swing_y = -0.15;

const double max_iter = 100;
int n_repeats = 1;

// constraints
double lmax = 0.5*sqrt(pow(L, 2.0) - pow(h, 2.0));
const double rfoot = 0.23;
const double maxfoot = 0.55;

const double eta_t = 0.0;
const double eta_u = 0.0;
//const double eta_t = 1e-7;
//const double eta_u = 2e-6;

double current_time = 0.0;

double dxdot_dt(Vector4d t_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s, int kx, int kt);
double dx_dt(Vector4d t_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s, int kx, int kt);
double dxdot_du(Vector4d t_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s, int kx, int ku);
double dx_du(Vector4d t_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s, int kx, int ku);


struct optimal{
    Vector4d time;
    Matrix<double,2,3> u_s;
};


double x(double dt, double u, double x_prev, double x_dot_prev){
	double x_ = 0.5 * (x_prev - u + x_dot_prev / omega) * exp(omega * dt) + 0.5 * (x_prev - u - x_dot_prev / omega) * exp(-omega * dt) + u;
    return x_;
}

double xdot(double dt, double u, double x_prev, double x_dot_prev){
	double xdot_ = 0.5 * omega * (x_prev - u + x_dot_prev / omega) * exp(omega * dt) - 0.5 * omega * (x_prev - u - x_dot_prev / omega) * exp(-omega * dt);
    return xdot_;
}

double xddot(double dt, double u, double x_prev, double x_dot_prev){
	double xddot_ = 0.5 * pow(omega, 2) * (x_prev - u + x_dot_prev / omega) * exp(omega * dt) + 0.5 * pow(omega, 2) * (x_prev - u - x_dot_prev / omega) * exp(-omega * dt);
    return xddot_;
}

double dxdot_dt(Vector4d t_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s, int kx, int kt){
	double dxdot_dt_ = 0.0;
	if (kx == kt){
		dxdot_dt_ = xddot_s[kx];
	}else if (kx > kt){
		dxdot_dt_ = 0.5 * dx_dt(t_s, u_s, x_s, xdot_s, xddot_s, kx-1, kt) * omega * (exp(omega * (t_s[kx] - t_s[kx-1]))
		+ exp(-omega * (t_s[kx] - t_s[kx-1]))) - 0.5 * dxdot_dt(t_s, u_s, x_s, xdot_s, xddot_s, kx-1, kt) * (exp(omega * (t_s[kx] - t_s[kx-1])) - exp(-omega * (t_s[kx] - t_s[kx-1])));
	}
	return dxdot_dt_;
}

double dx_dt(Vector4d t_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s, int kx, int kt){
    double dx_dt_ = 0.0;
	if (kx == kt){
		dx_dt_ = xdot_s[kx];
	}else if (kx > kt){
		dx_dt_ = 0.5 * dx_dt(t_s, u_s, x_s, xdot_s, xddot_s, kx-1, kt) * (exp(omega * (t_s[kx] - t_s[kx-1])) + exp(-omega * (t_s[kx] - t_s[kx-1]))) + 0.5 * dxdot_dt(t_s, u_s, x_s, xdot_s, xddot_s, kx-1, kt) * (exp(omega * (t_s[kx] - t_s[kx-1])) - exp(-omega * (t_s[kx] - t_s[kx-1]))) / omega;
	}
	return dx_dt_;
}

double dx_du(Vector4d t_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s, int kx, int ku){
    double dx_du_ = 0.0;
	if (kx == ku + 1){
		dx_du_ = -0.5 * (exp(omega * (t_s[kx-1] - t_s[kx-2])) + exp(-omega * (t_s[kx-1] - t_s[kx-2])) + 1);
	}else if (kx > ku + 1){
		dx_du_ = 0.5 * dx_du(t_s, u_s, x_s, xdot_s, xddot_s, kx-1, ku) * (exp(omega * (t_s[kx-1] - t_s[kx-2])) + exp(-omega * (t_s[kx-1] - t_s[kx-2]))) + 0.5 * dxdot_du(t_s, u_s, x_s, xdot_s, xddot_s, kx-1, ku) * (exp(omega * (t_s[kx-1] - t_s[kx-2])) - exp(-omega * (t_s[kx-1] - t_s[kx-2]))) / omega;
	}
	return dx_du_;
}

double dxdot_du(Vector4d t_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s, int kx, int ku){
    double dxdot_du_ = 0.0;
	if (kx == ku + 1){
		dxdot_du_ = -0.5 * omega * (exp(omega * (t_s[kx-1] - t_s[kx-2])) - exp(-omega * (t_s[kx-1] - t_s[kx-2])));
	}else if (kx > ku + 1){
		dxdot_du_ = 0.5 * dx_du(t_s, u_s, x_s, xdot_s, xddot_s, kx-1, ku) * omega * (exp(omega * (t_s[kx-1] - t_s[kx-2])) - exp(-omega * (t_s[kx-1] - t_s[kx-2]))) + 0.5 * dxdot_du(t_s, u_s, x_s, xdot_s, xddot_s, kx-1, ku) * (exp(omega * (t_s[kx-1] - t_s[kx-2])) + exp(-omega * (t_s[kx-1] - t_s[kx-2])));
	}
	return dxdot_du_;
}

double dJ_dux1(Vector4d t_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s){
    double grad = 0;
    for(int j=2; j<4; j++){
        grad += 2 * dxdot_du(t_s, u_s, x_s, xdot_s, xddot_s, j, 1) * (xdot_s[j] - x_dot_ref[0]);}
    return grad;
}

double dJ_dux2(Vector4d t_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s){
    double grad = 0;
    for(int j=3; j<4; j++){
        grad += 2 * dxdot_du(t_s, u_s, x_s, xdot_s, xddot_s, j, 2) * (xdot_s[j] - x_dot_ref[0]);}
    return grad;
}

double dJ_duy1(Vector4d t_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s){
    double grad = 0;
    for(int j=2; j<4; j++){
        grad += 2 * dxdot_du(t_s, u_s, x_s, xdot_s, xddot_s, j, 1) * (xdot_s[j] - x_dot_ref[1]);}
    return grad;
}

double dJ_duy2(Vector4d t_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s){
    double grad = 0;
    for(int j=3; j<4; j++){
        grad += 2 * dxdot_du(t_s, u_s, x_s, xdot_s, xddot_s, j, 2) * (xdot_s[j] - x_dot_ref[1]);}
    return grad;
}

double dJ_dt0(Vector4d t_s, Matrix<double,2, 3> u_s, Matrix<double,2, 4> x_s, Matrix<double,2, 4> xdot_s, Matrix<double, 2, 4> xddot_s){
    double grad = 0.0;
	Vector3d u_x = u_s.row(0);
	Vector3d u_y = u_s.row(1);
	Vector4d x_x = x_s.row(0);
	Vector4d x_y = x_s.row(1);
	Vector4d xdot_x = xdot_s.row(0);
	Vector4d xdot_y = xdot_s.row(1);
	Vector4d xddot_x = xddot_s.row(0);
	Vector4d xddot_y = xddot_s.row(1);
	
    for(int j=1; j<4; j++){
        grad += 2 * weight_x * dxdot_dt(t_s, u_x, x_x, xdot_x, xddot_x, j, 0) * (xdot_x[j] - x_dot_ref[0]);
        grad += 2 * weight_y * dxdot_dt(t_s, u_y, x_y, xdot_y, xddot_y, j, 0) * (xdot_y[j] - x_dot_ref[1]);
    }
    return grad;
}

double dJ_dt1(Vector4d t_s, Matrix<double,2, 3> u_s, Matrix<double,2, 4> x_s, Matrix<double,2, 4> xdot_s, Matrix<double, 2, 4> xddot_s){
    double grad = 0.0;
	Vector3d u_x = u_s.row(0);
	Vector3d u_y = u_s.row(1);
	Vector4d x_x = x_s.row(0);
	Vector4d x_y = x_s.row(1);
	Vector4d xdot_x = xdot_s.row(0);
	Vector4d xdot_y = xdot_s.row(1);
	Vector4d xddot_x = xddot_s.row(0);
	Vector4d xddot_y = xddot_s.row(1);
	
    for(int j=1; j<4; j++){
        grad += 2 * weight_x * dxdot_dt(t_s, u_x, x_x, xdot_x, xddot_x, j, 1) * (xdot_x[j] - x_dot_ref[0]);
        grad += 2 * weight_y * dxdot_dt(t_s, u_y, x_y, xdot_y, xddot_y, j, 1) * (xdot_y[j] - x_dot_ref[1]);
    }
    return grad;
}

double dJ_dt2(Vector4d t_s, Matrix<double,2, 3> u_s, Matrix<double,2, 4> x_s, Matrix<double,2, 4> xdot_s, Matrix<double, 2, 4> xddot_s){
    double grad = 0.0;
	Vector3d u_x = u_s.row(0);
	Vector3d u_y = u_s.row(1);
	Vector4d x_x = x_s.row(0);
	Vector4d x_y = x_s.row(1);
	Vector4d xdot_x = xdot_s.row(0);
	Vector4d xdot_y = xdot_s.row(1);
	Vector4d xddot_x = xddot_s.row(0);
	Vector4d xddot_y = xddot_s.row(1);
	
    for(int j=1; j<4; j++){
        grad += 2 * weight_x * dxdot_dt(t_s, u_x, x_x, xdot_x, xddot_x, j, 2) * (xdot_x[j] - x_dot_ref[0]);
        grad += 2 * weight_y * dxdot_dt(t_s, u_y, x_y, xdot_y, xddot_y, j, 2) * (xdot_y[j] - x_dot_ref[1]);
    }
    return grad;
}

double gaussian_penalty(Vector2d x, Vector2d mean, double std){
    Vector2d var = x-mean;
    double square_var = var.transpose() * var;
    double penalty = exp(- square_var/ pow(std, 2));
    return penalty;
}


double inverse_gaussian_penalty(Vector2d x, Vector2d mean, double std){
    Vector2d var = x-mean;
    double square_var = var.transpose() * var;
    double penalty = exp(square_var / pow(std, 2));
    return penalty;
}

Vector2d dJ_dux(Vector4d t_s, Matrix<double,2, 3> u_s, Matrix<double,2, 4> x_s, Matrix<double,2, 4> xdot_s, Matrix<double, 2, 4> xddot_s){
	Vector3d u_x = u_s.row(0);
	Vector3d u_y = u_s.row(1);
	Vector4d x_x = x_s.row(0);
	Vector4d x_y = x_s.row(1);
	Vector4d xdot_x = xdot_s.row(0);
	Vector4d xdot_y = xdot_s.row(1);
	Vector4d xddot_x = xddot_s.row(0);
	Vector4d xddot_y = xddot_s.row(1);


    double dt0 = t_s[1] - t_s[0];
    double pvmax = exp(kvmax * ((pow(u_s(0, 1) - swing_x, 2) + pow(u_s(1, 1) - swing_y, 2))/(pow(vmax * dt0, 2)) - 1));

    double dpvmax_dux1 = 2 * kuxvmax * (u_s(0, 1) - swing_x) / pow(vmax * dt0, 2) * pvmax;
//	cout << "velmax penalty with x" << dpvmax_dux1 << endl;
//    double px1u1 = inverse_gaussian_penalty(x_s.col(1), u_s.col(1), lmax);
    double px1u1 = exp(klmax * ((pow(x_s(0,1) - u_s(0,1), 2) + pow(x_s(1,1) - u_s(1,1), 2))/(pow(lmax,2)) - 1));
//    double px2u2 = inverse_gaussian_penalty(x_s.col(2), u_s.col(2), lmax);
    double px2u2 = exp(klmax * ((pow(x_s(0,2) - u_s(0,2), 2) + pow(x_s(1,2) - u_s(1,2), 2))/(pow(lmax,2)) - 1));

    double dpx1u1_dux1 = -(x_s(0, 1) - u_s(0, 1)) * px1u1;

    double dpx2u2_dux1 = (dx_du(t_s, u_x, x_x, xdot_x, xddot_x, 2, 1)) * (x_s(0, 2) - u_s(0, 1)) * px2u2;

    double dpx2u2_dux2 = -(x_s(0, 2) - u_s(0, 2)) * px2u2;


    double px2u1 = exp(klmax * ((pow(x_s(0,2) - u_s(0,1), 2) + pow(x_s(1,2) - u_s(1,1), 2))/(pow(lmax,2)) - 1));
//    double px2u2 = inverse_gaussian_penalty(x_s.col(2), u_s.col(2), lmax);
    double px3u2 = exp(klmax * ((pow(x_s(0,3) - u_s(0,2), 2) + pow(x_s(1,3) - u_s(1,2), 2))/(pow(lmax,2)) - 1));

    double dpx2u1_dux1 = (dx_du(t_s, u_x, x_x, xdot_x, xddot_x, 2, 1) - 1) * (x_s(0, 2) - u_s(0, 1)) * px2u1;

    double dpx3u2_dux2 = (dx_du(t_s, u_x, x_x, xdot_x, xddot_x, 3, 2) - 1) * (x_s(0, 3) - u_s(0, 2)) * px3u2;

    double dpx3u2_dux1 = (dx_du(t_s, u_x, x_x, xdot_x, xddot_x, 3, 1)) * (x_s(0, 3) - u_s(0, 2)) * px3u2;

    /* double pu1u0 = gaussian_penalty(u_s.col(1), u_s.col(0), rfoot);
    double pu2u1 = gaussian_penalty(u_s.col(2), u_s.col(1), rfoot);

    double dpu1u0_dux1 = -(u_s(0, 1) - u_s(0, 0)) * pu1u0;

    double dpu2u1_dux1 = (u_s(0, 2) - u_s(0, 1)) * pu2u1;

    double dpu2u1_dux2 = -(u_s(0, 2) - u_s(0, 1)) * pu2u1; */

    Vector2d dJ_dux_;
    dJ_dux_ << 2 * weight_x * (dJ_dux1(t_s, u_x, x_x, xdot_x, xddot_x) + dpvmax_dux1 + k_ux_len * (dpx1u1_dux1 + dpx2u2_dux1 + dpx2u1_dux1 + dpx3u2_dux1)), //+ k_foot * (dpu1u0_dux1 + dpu2u1_dux1)),
               2 * weight_x * (dJ_dux2(t_s, u_x, x_x, xdot_x, xddot_x) + k_ux_len * (dpx2u2_dux2 + dpx3u2_dux2)); //+ k_foot * (dpu2u1_dux2));
    return dJ_dux_;
}

Vector2d dJ_duy(Vector4d t_s, Matrix<double,2, 3> u_s, Matrix<double,2, 4> x_s, Matrix<double,2, 4> x_dot_s, Matrix<double, 2, 4> xddot_s){
	Vector3d u_x = u_s.row(0);
	Vector3d u_y = u_s.row(1);
	Vector4d x_x = x_s.row(0);
	Vector4d x_y = x_s.row(1);
	Vector4d xdot_x = x_dot_s.row(0);
	Vector4d xdot_y = x_dot_s.row(1);
	Vector4d xddot_x = xddot_s.row(0);
	Vector4d xddot_y = xddot_s.row(1);

    double dt0 = t_s[1] - t_s[0];
    double pvmax = exp(kvmax * ((pow(u_s(0, 1) - swing_x, 2) + pow(u_s(1, 1) - swing_y, 2))/(pow(vmax * dt0, 2)) - 1));
    double dpvmax_duy1 = 2 * kuyvmax * (u_s(1, 1) - swing_y) / pow(vmax * dt0, 2) * pvmax;

//    cout << "velmax penalty" << pvmax << endl;
//    cout << "swing foot distance, y" << u_s(1, 1) - swing_y << endl;
//    cout << "swing foot distance, x" << u_s(0, 1) - swing_x << endl;
//    cout << "linear distance, vmax * dt0" << vmax * dt0 << endl;

//    double px1u1 = inverse_gaussian_penalty(x_s.col(1), u_s.col(1), lmax);
//    double px2u2 = inverse_gaussian_penalty(x_s.col(2), u_s.col(2), lmax);
    double px1u1 = exp(klmax * ((pow(x_s(0,1) - u_s(0,1), 2) + pow(x_s(1,1) - u_s(1,1), 2))/(pow(lmax,2)) - 1));
//    double px2u2 = inverse_gaussian_penalty(x_s.col(2), u_s.col(2), lmax);
    double px2u2 = exp(klmax * ((pow(x_s(0,2) - u_s(0,2), 2) + pow(x_s(1,2) - u_s(1,2), 2))/(pow(lmax,2)) - 1));

    double dpx1u1_duy1 = -(x_s(1, 1) - u_s(1, 1)) * px1u1;

    double dpx2u2_duy1 = (dx_du(t_s, u_y, x_y, xdot_y, xddot_y, 2, 1)) * (x_s(1, 2) - u_s(1, 1)) * px2u2;

    double dpx2u2_duy2 = -(x_s(1, 2) - u_s(1, 2)) * px2u2;

//    double px2u1 = inverse_gaussian_penalty(x_s.col(2), u_s.col(1), lmax);
//    double px3u2 = inverse_gaussian_penalty(x_s.col(3), u_s.col(2), lmax);
    double px2u1 = exp(klmax * ((pow(x_s(0,2) - u_s(0,1), 2) + pow(x_s(1,2) - u_s(1,1), 2))/(pow(lmax,2)) - 1));
//    double px2u2 = inverse_gaussian_penalty(x_s.col(2), u_s.col(2), lmax);
    double px3u2 = exp(klmax * ((pow(x_s(0,3) - u_s(0,2), 2) + pow(x_s(1,3) - u_s(1,2), 2))/(pow(lmax,2)) - 1));

    double dpx2u1_duy1 = (dx_du(t_s, u_y, x_y, xdot_y, xddot_y, 2, 1) - 1) * (x_s(1, 2) - u_s(1, 1)) * px2u1;

    double dpx3u2_duy2 = (dx_du(t_s, u_y, x_y, xdot_y, xddot_y, 3, 2) - 1) * (x_s(1, 3) - u_s(1, 2)) * px3u2;

    double dpx3u2_duy1 = (dx_du(t_s, u_y, x_y, xdot_y, xddot_y, 3, 1)) * (x_s(1, 3) - u_s(1, 2)) * px3u2;

    /* double pu1u0 = gaussian_penalty(u_s.col(1), u_s.col(0), rfoot);
    double pu2u1 = gaussian_penalty(u_s.col(2), u_s.col(1), rfoot);*/
	double pu1u0 = exp(-k_foot_ * (foot_flag * (u_s(1, 1) - u_s(1, 0))/rfoot - 1));
	double pu2u1 = exp(-k_foot_ * (-foot_flag * (u_s(1, 2) - u_s(1, 1))/rfoot - 1));
    double dpu1u0_duy1 = - foot_flag * pu1u0;
    double dpu2u1_duy1 = - foot_flag * pu2u1;
    double dpu2u1_duy2 = foot_flag * pu2u1;

	double pu1u0_high = exp(k_foot_ * (foot_flag * (u_s(1, 1) - u_s(1, 0))/maxfoot - 1));
	double pu2u1_high = exp(k_foot_ * (-foot_flag * (u_s(1, 2) - u_s(1, 1))/maxfoot - 1));
    double dpu1u0_duy1_high = - foot_flag * pu1u0_high;
    double dpu2u1_duy1_high = - foot_flag * pu2u1_high;
    double dpu2u1_duy2_high = foot_flag * pu2u1_high;
//	    cout << "px1u1" << px1u1 << endl;
//	    cout << "px2u2" << px2u2 << endl;
//	    cout << "px2u1" << px2u1 << endl;
//	    cout << "px3u2" << px3u2 << endl;
//	    cout << "pu1u0" << pu1u0 << endl;
//	    cout << "pu2u1" << pu2u1 << endl;

    Vector2d dJ_duy_;
    dJ_duy_ << 2 * weight_y * (dJ_duy1(t_s, u_y, x_y, xdot_y, xddot_y) + dpvmax_duy1 + k_uy_len * (dpx1u1_duy1 + dpx2u2_duy1 + dpx2u1_duy1 + dpx3u2_duy1) + k_foot * (dpu1u0_duy1 + dpu2u1_duy1 + pu1u0_high)),
               2 * weight_y * (dJ_duy2(t_s, u_y, x_y, xdot_y, xddot_y) + k_uy_len * (dpx2u2_duy2 + dpx3u2_duy2 + dpu2u1_duy1_high) + k_foot * (dpu2u1_duy2 + dpu2u1_duy2_high));
    return dJ_duy_;
}

Vector3d dJ_dt(Vector4d t_s, Matrix<double,2, 3> u_s, Matrix<double,2, 4> x_s, Matrix<double,2, 4> xdot_s, Matrix<double, 2, 4> xddot_s){
    double dt0 = t_s[1] - t_s[0];
    // dt_0_low = np.sqrt((u_s(0, 1) - swing_x)**2 + (u_s(1, 1) - swing_y)**2) / vmax
    double pvmax = exp(kvmax * ((pow(u_s(0, 1) - swing_x, 2) + pow(u_s(1, 1) - swing_y, 2))/(pow(vmax * dt0, 2)) - 1));
    double dt_0_low = max(0.2 - current_time, 0.001);
    double dt_0_high = 1.0;
    double dt1 = t_s[2] - t_s[1];
    // dt_1_low = np.sqrt((u_s(0, 2) - u_s(0, 0))**2 + (u_s(1, 2) - u_s(1, 0))**2) / vmax

    double dpvmax_dt0 = kdtvmax * (- 2 / pow(dt0, 3)) * (pow(u_s(0, 1) - swing_x, 2) + pow(u_s(1, 1) - swing_y, 2))/pow(vmax, 2) * pvmax;

    double dt_1_low = 0.4;
    double dt_1_high = 1.0;
    double dt2 = t_s[3] - t_s[2];
    double dt_2_low = 0.4;
    double dt_2_high = 1.0;

    double dpt_dt0 = kdt0 * (exp(kdt0_ * (dt0 / dt_0_high - 1)) - 10 * exp(- (kdt0_ * (dt0 / dt_0_low - 1))));
    double dpt_dt1 = kdt1 * (exp(kdt1_ * (dt1 / dt_1_high - 1)) - 5 *  exp(- (kdt1_ * (dt1 / dt_1_low - 1))));
    double dpt_dt2 = kdt2 * (exp(kdt2_ * (dt2 / dt_2_high - 1)) - 25 * exp(- (kdt2_ * (dt2 / dt_2_low - 1))));

//    double px1u1 = inverse_gaussian_penalty(x_s.col(1), u_s.col(1), lmax);
//    double px2u2 = inverse_gaussian_penalty(x_s.col(2), u_s.col(2), lmax);
    double px1u1 = exp(klmax * ((pow(x_s(0,1) - u_s(0,1), 2) + pow(x_s(1,1) - u_s(1,1), 2))/(pow(lmax,2)) - 1));
//    double px2u2 = inverse_gaussian_penalty(x_s.col(2), u_s.col(2), lmax);
    double px2u2 = exp(klmax * ((pow(x_s(0,2) - u_s(0,2), 2) + pow(x_s(1,2) - u_s(1,2), 2))/(pow(lmax,2)) - 1));

    Vector3d u_x = u_s.row(0);
	Vector3d u_y = u_s.row(1);
	Vector4d x_x = x_s.row(0);
	Vector4d x_y = x_s.row(1);
	Vector4d xdot_x = xdot_s.row(0);
	Vector4d xdot_y = xdot_s.row(1);
	Vector4d xddot_x = xddot_s.row(0);
	Vector4d xddot_y = xddot_s.row(1);

    double dpx1u1_dt0 = (dx_dt(t_s, u_x, x_x, xdot_x, xddot_x, 1, 0) * (x_s(0, 1) - u_s(0, 1)) +
						 dx_dt(t_s, u_y, x_y, xdot_y, xddot_y, 1, 0) * (x_s(1, 1) - u_s(1, 1))) * px1u1;

    double dpx2u2_dt0 = (dx_dt(t_s, u_x, x_x, xdot_x, xddot_x, 2, 0) * (x_s(0, 2) - u_s(0, 2)) +
						 dx_dt(t_s, u_y, x_y, xdot_y, xddot_y, 2, 0) * (x_s(1, 2) - u_s(1, 2))) * px2u2;

    double dpx2u2_dt1 = (dx_dt(t_s, u_x, x_x, xdot_x, xddot_x, 2, 1) * (x_s(0, 2) - u_s(0, 2)) +
						 dx_dt(t_s, u_y, x_y, xdot_y, xddot_y, 2, 1) * (x_s(1, 2) - u_s(1, 2))) * px2u2;

//    double px2u1 = inverse_gaussian_penalty(x_s.col(1), u_s.col(1), lmax);
//    double px3u2 = inverse_gaussian_penalty(x_s.col(2), u_s.col(2), lmax);
    double px2u1 = exp(klmax * ((pow(x_s(0,2) - u_s(0,1), 2) + pow(x_s(1,2) - u_s(1,1), 2))/(pow(lmax,2)) - 1));
//    double px2u2 = inverse_gaussian_penalty(x_s.col(2), u_s.col(2), lmax);
    double px3u2 = exp(klmax * ((pow(x_s(0,3) - u_s(0,2), 2) + pow(x_s(1,3) - u_s(1,2), 2))/(pow(lmax,2)) - 1));

    double dpx2u1_dt0 = (dx_dt(t_s, u_x, x_x, xdot_x, xddot_x, 2, 0) * (x_s(0, 2) - u_s(0, 1)) +
						 dx_dt(t_s, u_y, x_y, xdot_y, xddot_y, 2, 0) * (x_s(1, 2) - u_s(1, 1))) * px2u1;

    double dpx3u2_dt0 = (dx_dt(t_s, u_x, x_x, xdot_x, xddot_x, 3, 0) * (x_s(0, 3) - u_s(0, 2)) +
						 dx_dt(t_s, u_y, x_y, xdot_y, xddot_y, 3, 0) * (x_s(1, 3) - u_s(1, 2))) * px3u2;

    double dpx2u1_dt1 = (dx_dt(t_s, u_x, x_x, xdot_x, xddot_x, 2, 1) * (x_s(0, 2) - u_s(0, 1)) +
						 dx_dt(t_s, u_y, x_y, xdot_y, xddot_y, 2, 1) * (x_s(1, 2) - u_s(1, 1))) * px2u1;

    double dpx3u2_dt1 = (dx_dt(t_s, u_x, x_x, xdot_x, xddot_x, 3, 1) * (x_s(0, 3) - u_s(0, 2)) +
						 dx_dt(t_s, u_y, x_y, xdot_y, xddot_y, 3, 1) * (x_s(1, 3) - u_s(1, 2))) * px3u2;

    double dpx3u2_dt2 = (dx_dt(t_s, u_x, x_x, xdot_x, xddot_x, 3, 2) * (x_s(0, 3) - u_s(0, 2)) +
						 dx_dt(t_s, u_y, x_y, xdot_y, xddot_y, 3, 2) * (x_s(1, 3) - u_s(1, 2))) * px3u2;
    Vector3d dJ_dt_;

    dJ_dt_ << 2 * (dJ_dt0(t_s, u_s, x_s, xdot_s, xddot_s) + dpvmax_dt0 + dpt_dt0 + k_t_len * (dpx1u1_dt0 + dpx2u2_dt0 + dpx2u1_dt0 + dpx3u2_dt0)),
              2 * (dJ_dt1(t_s, u_s, x_s, xdot_s, xddot_s) + dpt_dt1 + k_t_len * (dpx2u2_dt1 + dpx2u1_dt1 + dpx3u2_dt1)),
              2 * (dJ_dt2(t_s, u_s, x_s, xdot_s, xddot_s) + dpt_dt2 + k_t_len * (dpx3u2_dt2));

    return dJ_dt_;
}

double J(Vector4d t_s, Matrix<double,2, 3> u_s, Matrix<double,2, 4> x_s, Matrix<double,2, 4> xdot_s, Matrix<double, 2, 4> xddot_s){
    double J = 0.0;
    for(int i=1; i<u_s.size()/2+1; i++){
        J += pow(xdot_s(0, i)-x_dot_ref[0],2) + pow(xdot_s(1, i)-x_dot_ref[1],2);
    }
    return J;
 }

optimal gradient_descent(Vector4d& t_s, Matrix<double,2, 3>& u_s, int iter=max_iter, bool verbose=true){
    double update = 1.0;

    int i = 0;
    Matrix<double,2, 4> x_s, xdot_s, xddot_s;

    if (foot_flag == -1){
	    swing_x = right_x;
	    swing_y = right_y;
	} else if (foot_flag == 1){
	    swing_x = left_x;
	    swing_y = left_y;
	}
	
	// build position, velocity, acceleration vectors
	x_s(0, 0) = x_0(0);
	x_s(1, 0) = x_0(1);
	xdot_s(0, 0) = x_dot_0(0);
	xdot_s(1, 0) = x_dot_0(1);
	xddot_s(0, 0) = xddot(0.0, u_s(0, 0), x_s(0, 0), xdot_s(0, 0));
	xddot_s(1, 0) = xddot(0.0, u_s(1, 0), x_s(1, 0), xdot_s(1, 0));
    for(int k=1; k<4; k++){
        x_s(0,k) = x(t_s[k] - t_s[k-1], u_s(0, k-1), x_s(0, k-1), xdot_s(0, k-1));
        x_s(1,k) = x(t_s[k] - t_s[k-1], u_s(0, k-1), x_s(0, k-1), xdot_s(0, k-1));
		xdot_s(0,k) = xdot(t_s[k] - t_s[k-1], u_s(0, k-1), x_s(0, k-1), xdot_s(0, k-1));
        xdot_s(1,k) = xdot(t_s[k] - t_s[k-1], u_s(0, k-1), x_s(0, k-1), xdot_s(0, k-1));
		xddot_s(0,k) = xddot(t_s[k] - t_s[k-1], u_s(0, k-1), x_s(0, k-1), xdot_s(0, k-1));
        xddot_s(1,k) = xddot(t_s[k] - t_s[k-1], u_s(1, k-1), x_s(1, k-1), xdot_s(1, k-1));
	}
	
	// calculate initial gradients
    Vector3d grad_t = dJ_dt(t_s, u_s, x_s, xdot_s, xddot_s).array().min(100).max(-100);
    Vector2d grad_ux = dJ_dux(t_s, u_s, x_s, xdot_s, xddot_s).array().min(100).max(-100);
    Vector2d grad_uy = dJ_duy(t_s, u_s, x_s, xdot_s, xddot_s).array().min(100).max(-100);
//    cout << "-------- gradients" << endl;
//    cout << "t  grad \n" << grad_t << endl;
//    cout << "ux grad \n" << grad_ux << endl;
//    cout << "uy grad \n" << grad_uy << endl;

//    cout << "--------- penalties" << endl;

    double dt0 = t_s[1] - t_s[0];
    double pvmax = exp(kvmax * ((pow(u_s(0, 1) - swing_x, 2) + pow(u_s(1, 1) - swing_y, 2))/(pow(vmax * dt0, 2)) - 1));
    double dpvmax_dux1 = 2 * kuxvmax * (u_s(0, 1) - swing_x) / pow(vmax * dt0, 2) * pvmax;
    double dpvmax_duy1 = 2 * kuyvmax * (u_s(1, 1) - swing_y) / pow(vmax * dt0, 2) * pvmax;
    double dpvmax_dt0 = kdtvmax * (- 2 / pow(dt0, 3)) * (pow(u_s(0, 1) - swing_x, 2) + pow(u_s(1, 1) - swing_y, 2))/pow(vmax, 2) * pvmax;

//    cout << "dpvmax_dux1 penalty \t" << dpvmax_dux1 << endl;
//    cout << "dpvmax_duy1 penalty \t" << dpvmax_duy1 << endl;
//    cout << "dpvmax_dt0 penalty \t" << dpvmax_dt0 << endl;
    // dt_0_low = np.sqrt((u_s(0, 1) - swing_x)**2 + (u_s(1, 1) - swing_y)**2) / vmax
    double dt_0_low = max(0.2 - current_time, 0.001);
    double dt_0_high = 1.0;
    double dt1 = t_s[2] - t_s[1];
    double dt_1_low = 0.4;
    double dt_1_high = 1.0;
    double dt2 = t_s[3] - t_s[2];
    double dt_2_low = 0.4;
    double dt_2_high = 1.0;

    double dpt_dt0_high = kdt0 * exp(kdt0_ * (dt0 / dt_0_high - 1));
    double dpt_dt0_low = - kdt0 * 10 * exp(- (kdt0_ * (dt0 / dt_0_low - 1)));
    double dpt_dt1_high = kdt1 * exp(kdt1_ * (dt1 / dt_1_high - 1));
    double dpt_dt1_low = - kdt1 * 5 *  exp(- (kdt1_ * (dt1 / dt_1_low - 1)));
    double dpt_dt2_high = kdt2 * exp(kdt2_ * (dt2 / dt_2_high - 1));
    double dpt_dt2_low = - kdt2 * 25 * exp(- (kdt2_ * (dt2 / dt_2_low - 1)));

//    cout << "dt0 low penalty \t" << dpt_dt0_low << endl;
//    cout << "dt0 high penalty \t" << dpt_dt0_high << endl;
//    cout << "dt1 low penalty \t" << dpt_dt1_low << endl;
//    cout << "dt1 high penalty \t" << dpt_dt1_high << endl;
//    cout << "dt2 low penalty \t" << dpt_dt2_low << endl;
//    cout << "dt2 high penalty \t" << dpt_dt2_high << endl;

	double pu1u0 = exp(-k_foot_ * (foot_flag * (u_s(1, 1) - u_s(1, 0))/rfoot - 1));
	double pu2u1 = exp(-k_foot_ * (-foot_flag * (u_s(1, 2) - u_s(1, 1))/rfoot - 1));
    double dpu1u0_duy1 = - foot_flag * pu1u0;
    double dpu2u1_duy1 = - foot_flag * pu2u1;
    double dpu2u1_duy2 = foot_flag * pu2u1;
//    cout << "penalty u1u0y1 with rFoot \t" << dpu1u0_duy1 << endl;
//    cout << "penalty u2u1y1 with rFoot \t" << dpu2u1_duy1 << endl;
//    cout << "penalty u2u1y2 with rFoot \t" << dpu2u1_duy2 << endl;

    double pu1u0_high = exp(k_foot_ * (foot_flag * (u_s(1, 1) - u_s(1, 0))/maxfoot - 1));
	double pu2u1_high = exp(k_foot_ * (-foot_flag * (u_s(1, 2) - u_s(1, 1))/maxfoot - 1));
    double dpu1u0_duy1_high = - foot_flag * pu1u0_high;
    double dpu2u1_duy1_high = - foot_flag * pu2u1_high;
    double dpu2u1_duy2_high = foot_flag * pu2u1_high;
//    cout << "penalty dpu1u0_duy1_high with rFoot max \t" << dpu1u0_duy1_high << endl;
//    cout << "penalty dpu2u1_duy1_high with rFoot max \t" << dpu2u1_duy1_high << endl;
//    cout << "penalty dpu2u1_duy2_high with rFoot max \t" << dpu2u1_duy2_high << endl;

    double px1u1 = exp(klmax * ((pow(x_s(0,1) - u_s(0,1), 2) + pow(x_s(1,1) - u_s(1,1), 2))/(pow(lmax,2)) - 1));
//    double px2u2 = inverse_gaussian_penalty(x_s.col(2), u_s.col(2), lmax);
    double px2u2 = exp(klmax * ((pow(x_s(0,2) - u_s(0,2), 2) + pow(x_s(1,2) - u_s(1,2), 2))/(pow(lmax,2)) - 1));
    double px2u1 = exp(klmax * ((pow(x_s(0,2) - u_s(0,1), 2) + pow(x_s(1,2) - u_s(1,1), 2))/(pow(lmax,2)) - 1));
//    double px2u2 = inverse_gaussian_penalty(x_s.col(2), u_s.col(2), lmax);
    double px3u2 = exp(klmax * ((pow(x_s(0,3) - u_s(0,2), 2) + pow(x_s(1,3) - u_s(1,2), 2))/(pow(lmax,2)) - 1));
//    cout << "penalty px1u1 with lmax \t" << px1u1 << endl;
//    cout << "penalty px2u2 with lmax \t" << px2u2 << endl;
//    cout << "penalty px2u1 with lmax \t" << px2u1 << endl;
//    cout << "penalty px3u2 with lmax \t" << px3u2 << endl;
//    cout << "velmax penalty gradient with x \t" << dpvmax_dux1 << endl;
//    cout << "velmax penalty gradient with y \t" << dpvmax_duy1 << endl;
//    cout << "velmax penalty gradient with t \t" << dpvmax_dt0 << endl;
//    cout << "velmax penalty \t" << pvmax << endl;
//    cout << "swing foot distance, x \t" << u_s(0, 1) - swing_x << endl;
//    cout << "swing foot distance, y \t" << u_s(1, 1) - swing_y << endl;
//    cout << "linear distance, vmax * dt0 \t" << vmax * dt0 << endl;

//    if ( 1e15 > abs(grad_t.sum()) > 1e5 || 1e15 >abs(grad_ux.sum()) > 1e6 || 1e15 > abs(grad_uy.sum()) > 1e6){
////        optimal result;
////        result.time = t_s; result.u_s = u_s;
////        return result;
////        iter = 25;
//        eta_u *= 2;
//        eta_t *= 2;
//    }
//    if (abs(grad_t.sum()) < 5e2 && abs(grad_ux.sum()) < 5e2 && abs(grad_uy.sum()) < 5e2){
////        optimal result;
////        result.time = t_s; result.u_s = u_s;
////        return result;
////        iter = 25;
//        eta_u /= 10;
//        eta_t /= 10;
//    }
	
	// begin the gradient descent loop
    while (update > 0.5 * (eta_t + eta_u)){
		// build position, velocity, acceleration vectors
        x_s(0, 0) = x_0(0);
		x_s(1, 0) = x_0(1);
		xdot_s(0, 0) = x_dot_0(0);
		xdot_s(1, 0) = x_dot_0(1);
		xddot_s(0, 0) = xddot(0.0, u_s(0, 0), x_s(0, 0), xdot_s(0, 0));
		xddot_s(1, 0) = xddot(0.0, u_s(1, 0), x_s(1, 0), xdot_s(1, 0));
		for(int k=1; k<4; k++){
			x_s(0,k) = x(t_s[k] - t_s[k-1], u_s(0, k-1), x_s(0, k-1), xdot_s(0, k-1));
			x_s(1,k) = x(t_s[k] - t_s[k-1], u_s(1, k-1), x_s(1, k-1), xdot_s(1, k-1));
			xdot_s(0,k) = xdot(t_s[k] - t_s[k-1], u_s(0, k-1), x_s(0, k-1), xdot_s(0, k-1));
			xdot_s(1,k) = xdot(t_s[k] - t_s[k-1], u_s(1, k-1), x_s(1, k-1), xdot_s(1, k-1));
			xddot_s(0,k) = xddot(t_s[k] - t_s[k-1], u_s(0, k-1), x_s(0, k-1), xdot_s(0, k-1));
			xddot_s(1,k) = xddot(t_s[k] - t_s[k-1], u_s(1, k-1), x_s(1, k-1), xdot_s(1, k-1));
		}
		// calculate gradients
        grad_t = dJ_dt(t_s, u_s, x_s, xdot_s, xddot_s).array().min(100).max(-100);
        grad_ux = dJ_dux(t_s, u_s, x_s, xdot_s, xddot_s).array().min(100).max(-100);
        grad_uy = dJ_duy(t_s, u_s, x_s, xdot_s, xddot_s).array().min(100).max(-100);
		
		// update step times and locations
        t_s.segment(1,3) -= eta_t * grad_t;
        u_s.block(0,1,1,2) -= eta_u * grad_ux.transpose();
        u_s.block(1,1,1,2) -= eta_u * grad_uy.transpose();
		
		// calculate update size to check for convergence
		VectorXd grad(7);
        grad.segment(0,3) = grad_t * eta_t;
        grad.segment(3,2)  = grad_ux * eta_u;
        grad.segment(5,2) = grad_uy * eta_u;
        grad = grad.cwiseAbs();
        update = grad.sum();
		
		// if maximum iterations has passed, stop
        i += 1;
        if (i > iter) {break;}
    }
    // print(i)
//    cout<<"iteration is :" << i << endl;
    optimal result;
    result.time = t_s; result.u_s = u_s;
    return result;
}

// Callback function for receiving the planner outputs
void footstepPlanCallbackRK4(const std_msgs::Float64MultiArray::ConstPtr& msg)
{
    u_s_rk4(0,1) = msg->data[0];
    u_s_rk4(1,1) = msg->data[1];
    u_s_rk4(0,2) = msg->data[2];
    u_s_rk4(1,2) = msg->data[3];
    t_s_rk4(1) = msg->data[4];
    t_s_rk4(2) = msg->data[5];
    t_s_rk4(3) = msg->data[6];

    u_s = u_s_rk4;
    t_s = t_s_rk4;
    support_foot_flag = msg->data[7];
//    flag = -1;
//    cout<< "received u \n" << u_s << endl;
//    cout<< "received t \n" << t_s << endl;
}

void plannerInputCallback(const std_msgs::Float64MultiArray::ConstPtr& msg)
{
//    x, x_dot, y, y_dot = msg.data[:4]
    x_0(0) = msg->data[0];
    x_dot_0(0) = msg->data[1];
    x_0(1) = msg->data[2];
    x_dot_0(1) = msg->data[3];
    left_x  = msg->data[6];
    left_y  = msg->data[7];
    right_x = msg->data[8];
    right_y = msg->data[9];
    foot_flag = msg->data[10];
    current_time = msg->data[11];
}

void velocityCallback(const std_msgs::Float64MultiArray::ConstPtr& msg)
{
    x_dot_ref(0) = msg->data[0];
    x_dot_ref(1) = msg->data[1];
//    cout<<"---------------------------------------------------" << endl;
//    cout<<"---------------------------------------------------" << endl;
//    cout<<"---------------------------------------------------" << endl;
//    cout<<"x_dot_ref is\n" << x_dot_ref << endl;
//    cout<<"---------------------------------------------------" << endl;
//    cout<<"---------------------------------------------------" << endl;
}

bool check_constraints(Vector4d t_s, Matrix<double,2, 3> u_s){
    Matrix<double,2, 4> x_s, xdot_s, xddot_s;
    x_s(0, 0) = x_0(0);
    x_s(1, 0) = x_0(1);
    xdot_s(0, 0) = x_dot_0(0);
    xdot_s(1, 0) = x_dot_0(1);
    xddot_s(0, 0) = xddot(0.0, u_s(0, 0), x_s(0, 0), xdot_s(0, 0));
    xddot_s(1, 0) = xddot(0.0, u_s(1, 0), x_s(1, 0), xdot_s(1, 0));
    for(int k=1; k<4; k++){
        x_s(0,k) = x(t_s[k] - t_s[k-1], u_s(0, k-1), x_s(0, k-1), xdot_s(0, k-1));
        x_s(1,k) = x(t_s[k] - t_s[k-1], u_s(1, k-1), x_s(1, k-1), xdot_s(1, k-1));
        xdot_s(0,k) = xdot(t_s[k] - t_s[k-1], u_s(0, k-1), x_s(0, k-1), xdot_s(0, k-1));
        xdot_s(1,k) = xdot(t_s[k] - t_s[k-1], u_s(1, k-1), x_s(1, k-1), xdot_s(1, k-1));
        xddot_s(0,k) = xddot(t_s[k] - t_s[k-1], u_s(0, k-1), x_s(0, k-1), xdot_s(0, k-1));
        xddot_s(1,k) = xddot(t_s[k] - t_s[k-1], u_s(1, k-1), x_s(1, k-1), xdot_s(1, k-1));
    }
    double dt0 = t_s(1) - t_s(0) + current_time;
    double dt1 = t_s(2) - t_s(1);
    double dt2 = t_s(3) - t_s(2);
    if (dt0 < 0.20 || dt1 < 0.25 || dt2 < 0.25){
        cout << "dt violated" << endl;
        cout << dt0 << endl;
        cout << dt1 << endl;
        cout << dt2 << endl;
        return true;
    }
    if (abs(u_s(1, 1) - u_s(1, 0)) < rfoot - 0.05 || abs(u_s(1, 2) - u_s(1, 1)) < rfoot - 0.05){
        cout << "rfoot violated" << endl;
        cout << rfoot << endl;
        cout << abs(u_s(1, 1) - u_s(1, 0)) << endl;
        cout << abs(u_s(1, 2) - u_s(1, 1)) << endl;
        return true;
    }
    return false;
}

int main(int argc, char ** argv){
    // Vector4d t_s(0.0, 0.5, 1.0, 1.5);      // initial guess
    // Vector3d u_s(0.0, -0.01, 0.02/0);   // step locations
//    double x_0 = 0.0;                      // initial COM position
//    double x_dot_0 = 0.0;                  //initial COM velocity
    Vector4d x_opt,y_opt;
    optimal opt;
    const double LOOP_RATE = 250;
    int iter_main = 0;
    ros::init(argc, argv, "gradient_descent_footPlanner_node");
    ros::NodeHandle n;
    ros::Rate loop_rate(LOOP_RATE);
    ros::Publisher planner_output_pub = n.advertise<std_msgs::Float64MultiArray>("/time_slider_gazebo/footstep_plan", 1);
    ros::Subscriber sub_planner_input = n.subscribe<std_msgs::Float64MultiArray>("/time_slider_gazebo/planner_input", '1', plannerInputCallback);
    ros::Subscriber sub_RK4_planner_input = n.subscribe<std_msgs::Float64MultiArray>("/time_slider_gazebo/footstep_plan_rk4", '1', footstepPlanCallbackRK4);
    ros::Subscriber sub_velocity_input = n.subscribe<std_msgs::Float64MultiArray>("/time_slider_gazebo/reference_velocity", '1', velocityCallback);
    support_foot_flag = foot_flag;
    Matrix<double,2, 4> x_s, xdot_s, xddot_s;
    while (ros::ok()){

        // for (int i=0; i <max_iter; i+=2){
//        cout << "-------- initial" << endl;
//        cout << "u ini is \n" << u_s << endl;
//        cout << "t ini is \n" << t_s.array() + current_time << endl;
//        cout << "current robot COM pos is \t" << x_0 << endl;
//        cout << "current robot COM vel is \t" << x_dot_0 << endl;
        ////////////// Gradient Descent ////////////////////////////////
        auto t1 = std::chrono::high_resolution_clock::now();
        if (foot_flag == -1){
            u_s(0,0) = left_x;
            u_s(1,0) = left_y;
            u_s_rk4(0, 0) = left_x;
            u_s_rk4(1, 0) = left_y;
        }
        else if (foot_flag == 1){
            u_s(0,0) = right_x;
            u_s(1,0) = right_y;
            u_s_rk4(0, 0) = right_x;
            u_s_rk4(1, 0) = right_y;
        }
        if (t_s(1) - t_s (0) > 1/LOOP_RATE){
            opt = gradient_descent(t_s, u_s);
        }
        // build position, velocity, acceleration vectors
        x_s(0, 0) = x_0(0);
        x_s(1, 0) = x_0(1);
        xdot_s(0, 0) = x_dot_0(0);
        xdot_s(1, 0) = x_dot_0(1);
        xddot_s(0, 0) = xddot(0.0, u_s(0, 0), x_s(0, 0), xdot_s(0, 0));
        xddot_s(1, 0) = xddot(0.0, u_s(1, 0), x_s(1, 0), xdot_s(1, 0));
        for(int k=1; k<4; k++){
            x_s(0,k) = x(t_s[k] - t_s[k-1], u_s(0, k-1), x_s(0, k-1), xdot_s(0, k-1));
            x_s(1,k) = x(t_s[k] - t_s[k-1], u_s(1, k-1), x_s(1, k-1), xdot_s(1, k-1));
            xdot_s(0,k) = xdot(t_s[k] - t_s[k-1], u_s(0, k-1), x_s(0, k-1), xdot_s(0, k-1));
            xdot_s(1,k) = xdot(t_s[k] - t_s[k-1], u_s(1, k-1), x_s(1, k-1), xdot_s(1, k-1));
            xddot_s(0,k) = xddot(t_s[k] - t_s[k-1], u_s(0, k-1), x_s(0, k-1), xdot_s(0, k-1));
            xddot_s(1,k) = xddot(t_s[k] - t_s[k-1], u_s(1, k-1), x_s(1, k-1), xdot_s(1, k-1));
        }

        // }
        auto t2 = chrono::high_resolution_clock::now();
        auto duration = chrono::duration_cast<chrono::microseconds>( t2 - t1 ).count();

//        bool violated = check_constraints(opt.time, opt.u_s);
        bool violated = false;

//        cout << "-------- gradient descent statistics" << endl;
//        cout << "Duration: \n" << duration << " microseconds" << endl;
//        cout<< "Support foot " << support_foot_flag << endl;
//        cout << "Foot flag" << foot_flag << endl;
//        cout << "support foot position \t" << u_s(0, 0) << "\t" << u_s(1, 0) << endl;
//        cout << "swing foot position \t" << swing_x << "\t" << swing_y << endl;
//        cout << "reference_velocity \t" << x_dot_ref(0) << "\t" << x_dot_ref(1) << endl;
//        cout << "violated constraints? \t" << violated << endl;
//
//        cout << "-------- state prediction" << endl;
//        cout << "x 0123 \t" << x_s(0, 0) << "  " << x_s(0, 1) << "  " << x_s(0, 2) << "  " << x_s(0, 3) << endl;
//        cout << "y 0123 \t" << x_s(1, 0) << "  " << x_s(1, 1) << "  " << x_s(1, 2) << "  " << x_s(1, 3) << endl;
//        cout << "dx 0123 \t" << xdot_s(0, 0) << "  " << xdot_s(0, 1) << "  " << xdot_s(0, 2) << "  " << xdot_s(0, 3) << endl;
//        cout << "dy 0123 \t" << xdot_s(1, 0) << "  " << xdot_s(1, 1) << "  " << xdot_s(1, 2) << "  " << xdot_s(1, 3) << endl;
//        cout << "ddx 0123 \t" << xddot_s(0, 0) << "  " << xddot_s(0, 1) << "  " << xddot_s(0, 2) << "  " << xddot_s(0, 3) << endl;
//        cout << "ddy 0123 \t" << xddot_s(1, 0) << "  " << xddot_s(1, 1) << "  " << xddot_s(1, 2) << "  " << xddot_s(1, 3) << endl;

        // publish
        if(support_foot_flag == foot_flag && t_s(1) - t_s(0) > 1/LOOP_RATE && !opt.time.hasNaN() && !opt.u_s.hasNaN() && !violated){
//            cout << "Publish Plan !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << endl;
            t_s = opt.time;
            u_s = opt.u_s;
            std_msgs::Float64MultiArray footstep_plan;
            footstep_plan.data.push_back(u_s(0,1));
            footstep_plan.data.push_back(u_s(1,1));
            footstep_plan.data.push_back(u_s(0,2));
            footstep_plan.data.push_back(u_s(1,2));
            footstep_plan.data.push_back(t_s(1)-t_s(0));
            footstep_plan.data.push_back(support_foot_flag);
            footstep_plan.data.push_back(t_s(2)-t_s(1));
            footstep_plan.data.push_back(t_s(3)-t_s(2));
            footstep_plan.data.push_back(x_s(0, 0));    // 8
            footstep_plan.data.push_back(x_s(1, 0));
            footstep_plan.data.push_back(x_s(0, 1));
            footstep_plan.data.push_back(x_s(1, 1));
            footstep_plan.data.push_back(x_s(0, 2));
            footstep_plan.data.push_back(x_s(1, 2));
            footstep_plan.data.push_back(x_s(0, 3));
            footstep_plan.data.push_back(x_s(1, 3));
            planner_output_pub.publish(footstep_plan);
            if(flag==0){
                t_s(1) -= 1/LOOP_RATE;
                t_s(2) -= 1/LOOP_RATE;
                t_s(3) -= 1/LOOP_RATE;
            }
            else if (flag==1){flag = 0;}
        } else if (opt.time.hasNaN() || opt.u_s.hasNaN()){
            cout << "Publishing RK4 -------------------" << endl;
            t_s = t_s_rk4;
            u_s = u_s_rk4;
            std_msgs::Float64MultiArray footstep_plan;
            footstep_plan.data.push_back(u_s(0,1));
            footstep_plan.data.push_back(u_s(1,1));
            footstep_plan.data.push_back(u_s(0,2));
            footstep_plan.data.push_back(u_s(1,2));
            footstep_plan.data.push_back(t_s(1)-t_s(0));
            footstep_plan.data.push_back(support_foot_flag);
            planner_output_pub.publish(footstep_plan);
        }else
        {
            cout << "Not publishing -------------------" << endl;
            if(flag==0){
                t_s(1) -= 1/LOOP_RATE;
                t_s(2) -= 1/LOOP_RATE;
                t_s(3) -= 1/LOOP_RATE;
            }
        }
        if(support_foot_flag != foot_flag){
            t_s(1) = opt.time(2);
            t_s(2) = opt.time(3);
            t_s(3) = opt.time(3) + 0.4;//opt.time(3) - opt.time(2);
            u_s(0,0) = opt.u_s(0,1);
            u_s(1,0) = opt.u_s(1,1);
            u_s(0,1) = opt.u_s(0,2);
            u_s(1,1) = opt.u_s(1,2);
            u_s(0, 2) = opt.u_s(0,2) + (opt.u_s(0,2) - opt.u_s(0, 1));
            u_s(1, 2) = opt.u_s(1,2) + (opt.u_s(1,1) - opt.u_s(1, 2));
            current_time = 0.0;
            support_foot_flag = foot_flag;
            cout << "switching !!!!!!!!!!!!!!!!!!!!!!!!!!1!!!!!!!!!!!!" << endl;
        }
        // assign prev optimal as initial guess of next iteration

//        cout << "planned foot \t" << support_foot_flag << endl;
//        cout << "real foot \t" << foot_flag << endl;
//        cout << "current time is \t"<< current_time << endl;

//        for (int i = 0; i < 4; i++)
//        {
//            x_opt[i] = x(opt.time, opt.u_s, i);
//            y_opt[i] = y(opt.time, opt.u_s, i);
//        }
        // cout << "x_opt is \n" << x_opt << endl;
        // cout << "y_opt is \n" << y_opt << endl;
        iter_main += 1;
//        cout << "iteration in main loop is \t" << iter_main << endl;
//        cout << "-------- final" << endl;
//        cout << "u opt is \n" << opt.u_s << endl;
//        cout << "t opt is \n" << opt.time.array() + current_time << endl;

        ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;}

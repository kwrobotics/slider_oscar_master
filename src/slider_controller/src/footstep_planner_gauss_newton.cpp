#include <iostream>
#include <chrono>
#include <cmath>
#include <Eigen/Dense>

#include "ros/ros.h"
#include <std_msgs/Float64.h>
#include <std_msgs/Float64MultiArray.h>

using namespace Eigen;
using namespace std;

const double m = 15.0;    // body mass
const double m_s = 0.3;   // swing leg mass
const double L = 1.2;     // leg length
const double h = 0.7;     // body height
const double g = 9.81;    // gravity
const double omega = sqrt(g / h);  // omega

// penalty weight
const double T_end = 100;
const double dt = 0.001;
const double vmax = 5.0;              // m/s
const double step_time = 0.7;
int flag = 0;
int support_foot_flag = -1;
Vector2d x_dot_ref(0.05, 0.00);     // m/s
double eta_t = 0.0;
double eta_u = 0.0;
// Parameters subscribed from the slider_controller node
Vector2d x_0(0.0, 0.0);
Vector2d x_dot_0(0.0, 0.0);
Vector2d x_dot_dot_0(0.0, 0.0);
Vector3d dt_s(0.4, 0.4, 0.4);
Matrix<double,2,3> u_s((Matrix<double,2,3>() << 0.0, 0.0,  0.02, 0.15, -0.21924587,  0.03075412).finished());
// Eigen::Matrix3d A((Eigen::Matrix3d() << 1, 2, 3, 4, 5, 6, 7, 8, 9).finished());

double left_x = 0.0;
double left_y = 0.15;
double right_x = 0.0;
double right_y = -0.15;
int foot_flag = -1;
// penalty weights
const int n_weights = 12;
const double w[n_weights] = { 0 };

double a0x = 0.5 * (x_0[0] - u_s(0, 0) + x_dot_0[0] / omega);
double b0x = 0.5 * (x_0[0] - u_s(0, 0)- x_dot_0[0] / omega);

double a0y = 0.5 * (x_0[1] - u_s(1, 0) + x_dot_0[1] / omega);
double b0y = 0.5 * (x_0[1] - u_s(1, 0) - x_dot_0[1] / omega);

double swing_x = 0.0;
double swing_z = 0.0;
double swing_y = -0.15;

const double max_iter = 200;
int n_repeats = 1;

// constraints
const double lmax = 2*sqrt(pow(L, 2.0) - pow(h, 2.0));
const double rfoot = 0.3;
const double dt0_min = 0.2;
const double dt_min = 0.4;
const double dt_max = 1.2;

double current_time = 0.0;

double dxdot_dt(Vector3d t_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s, int kx, int kt);
double dx_dt(Vector3d t_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s, int kx, int kt);
double dxdot_du(Vector3d t_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s, int kx, int ku);
double dx_du(Vector3d t_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s, int kx, int ku);


struct optimal{
    Vector3d time;
    Matrix<double,2,3> u_s;
};

double x(double dt, double u, double x_prev, double x_dot_prev){
	double x_ = (x_prev - u + x_dot_prev / omega) * exp(omega * dt) + (x_prev - u - x_dot_prev / omega) * exp(-omega * dt) + u;
    return x_;
}

double xdot(double dt, double u, double x_prev, double x_dot_prev){
	double xdot_ = omega * (x_prev - u + x_dot_prev / omega) * exp(omega * dt) - omega * (x_prev - u - x_dot_prev / omega) * exp(-omega * dt);
    return xdot_;
}

double xddot(double dt, double u, double x_prev, double x_dot_prev){
	double xddot_ = pow(omega, 2) * (x_prev - u + x_dot_prev / omega) * exp(omega * dt) + pow(omega, 2) * (x_prev - u - x_dot_prev / omega) * exp(-omega * dt);
    return xddot_;
}

double dxdot_dt(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s, int kx=1, int kt=0){
	double dxdot_dt_ = 0.0;
	if (kx == kt){
		dxdot_dt_ = xddot_s[kx];
	}else if (kx > kt){
		dxdot_dt_ = 0.5 * dx_dt(dt_s, u_s, x_s, xdot_s, xddot_s, kx-1, kt) * omega * (exp(omega * dt_s[kx]) + exp(-omega * dt_s[kx])) - 0.5 * dxdot_dt(dt_s, u_s, x_s, xdot_s, xddot_s, kx-1, kt) * (exp(omega * dt_s[kx]) - exp(-omega * dt_s[kx]));
	}
	return dxdot_dt_;
}

double dx_dt(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s, int kx=1, int kt=0){
    double dx_dt_ = 0.0;
	if (kx == kt){
		dx_dt_ = xdot_s[kx];
	}else if (kx > kt){
		dx_dt_ = 0.5 * dx_dt(dt_s, u_s, x_s, xdot_s, xddot_s, kx-1, kt) * (exp(omega * dt_s[kx]) + exp(-omega * dt_s[kx])) + 0.5 * dxdot_dt(dt_s, u_s, x_s, xdot_s, xddot_s, kx-1, kt) * (exp(omega * dt_s[kx]) - exp(-omega * dt_s[kx])) / omega;
	}
	return dx_dt_;
}

double dx_du(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s, int kx=1, int ku=1){
    double dx_du_ = 0.0;
	if (kx == ku + 1){
		dx_du_ = -0.5 * (exp(omega * dt_s[kx-2]) + exp(-omega * dt_s[kx-2])) + 1;
	}else if (kx > ku + 1){
		dx_du_ = 0.5 * dx_du(dt_s, u_s, x_s, xdot_s, xddot_s, kx-1, ku) * (exp(omega * dt_s[kx-1]) + exp(-omega * dt_s[kx-1])) + 0.5 * dxdot_du(dt_s, u_s, x_s, xdot_s, xddot_s, kx-1, ku) * (exp(omega * dt_s[kx-1]) - exp(-omega * dt_s[kx-1])) / omega;
	}
	return dx_du_;
}

double dxdot_du(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s, int kx=1, int ku=1){
    double dxdot_du_ = 0.0;
	if (kx == ku + 1){
		dxdot_du_ = -0.5 * omega * (exp(omega * dt_s[kx-1]) - exp(-omega * dt_s[kx-1]));
	}else if (kx > ku + 1){
		dxdot_du_ = 0.5 * dx_du(dt_s, u_s, x_s, xdot_s, xddot_s, kx-1, ku) * omega * (exp(omega * dt_s[kx-1]) - exp(-omega * dt_s[kx-1])) + 0.5 * dxdot_du(dt_s, u_s, x_s, xdot_s, xddot_s, kx-1, ku) * (exp(omega * dt_s[kx-1]) + exp(-omega * dt_s[kx-1]));
	}
	return dxdot_du_;
}

double dJ_dux1(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s){
    double grad = 0;
    for(int j=2; j<4; j++){
        grad += 2 * dxdot_du(dt_s, u_s, x_s, xdot_s, xddot_s, j, 1) * (xdot_s[j] - x_dot_ref[0]);}
    return grad;
}

double dJ_dux2(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s){
    double grad = 0;
    for(int j=3; j<4; j++){
        grad += 2 * dxdot_du(dt_s, u_s, x_s, xdot_s, xddot_s, j, 2) * (xdot_s[j] - x_dot_ref[0]);}
    return grad;
}

double dJ_duy1(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s){
    double grad = 0;
    for(int j=2; j<4; j++){
        grad += 2 * dxdot_du(dt_s, u_s, x_s, xdot_s, xddot_s, j, 1) * (xdot_s[j] - x_dot_ref[1]);}
    return grad;
}

double dJ_duy2(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s){
    double grad = 0;
    for(int j=3; j<4; j++){
        grad += 2 * dxdot_du(dt_s, u_s, x_s, xdot_s, xddot_s, j, 2) * (xdot_s[j] - x_dot_ref[1]);}
    return grad;
}

double dJ_dt0(Vector3d dt_s, Matrix<double,2, 3> u_s, Matrix<double,2, 4> x_s, Matrix<double,2, 4> xdot_s, Matrix<double, 2, 4> xddot_s){
    double grad = 0.0;
	Vector3d u_x = u_s.row(0);
	Vector3d u_y = u_s.row(1);
	Vector4d x_x = x_s.row(0);
	Vector4d x_y = x_s.row(1);
	Vector4d xdot_x = xdot_s.row(0);
	Vector4d xdot_y = xdot_s.row(1);
	Vector4d xddot_x = xddot_s.row(0);
	Vector4d xddot_y = xddot_s.row(1);
	
    for(int j=1; j<4; j++){
        grad += 2 * dxdot_dt(dt_s, u_x, x_x, xdot_x, xddot_x, j, 0) * (xdot_x[j] - x_dot_ref[0]);
        grad += 2 * dxdot_dt(dt_s, u_y, x_y, xdot_y, xddot_y, j, 0) * (xdot_y[j] - x_dot_ref[1]);
    }
    return grad;
}

double dJ_dt1(Vector3d dt_s, Matrix<double,2, 3> u_s, Matrix<double,2, 4> x_s, Matrix<double,2, 4> xdot_s, Matrix<double, 2, 4> xddot_s){
    double grad = 0.0;
	Vector3d u_x = u_s.row(0);
	Vector3d u_y = u_s.row(1);
	Vector4d x_x = x_s.row(0);
	Vector4d x_y = x_s.row(1);
	Vector4d xdot_x = xdot_s.row(0);
	Vector4d xdot_y = xdot_s.row(1);
	Vector4d xddot_x = xddot_s.row(0);
	Vector4d xddot_y = xddot_s.row(1);
	
    for(int j=1; j<4; j++){
        grad += 2 * dxdot_dt(dt_s, u_x, x_x, xdot_x, xddot_x, j, 1) * (xdot_x[j] - x_dot_ref[0]);
        grad += 2 * dxdot_dt(dt_s, u_y, x_y, xdot_y, xddot_y, j, 1) * (xdot_y[j] - x_dot_ref[1]);
    }
    return grad;
}

double dJ_dt2(Vector3d dt_s, Matrix<double,2, 3> u_s, Matrix<double,2, 4> x_s, Matrix<double,2, 4> xdot_s, Matrix<double, 2, 4> xddot_s){
    double grad = 0.0;
	Vector3d u_x = u_s.row(0);
	Vector3d u_y = u_s.row(1);
	Vector4d x_x = x_s.row(0);
	Vector4d x_y = x_s.row(1);
	Vector4d xdot_x = xdot_s.row(0);
	Vector4d xdot_y = xdot_s.row(1);
	Vector4d xddot_x = xddot_s.row(0);
	Vector4d xddot_y = xddot_s.row(1);
	
    for(int j=1; j<4; j++){
        grad += 2 * dxdot_dt(dt_s, u_x, x_x, xdot_x, xddot_x, j, 2) * (xdot_x[j] - x_dot_ref[0]);
        grad += 2 * dxdot_dt(dt_s, u_y, x_y, xdot_y, xddot_y, j, 2) * (xdot_y[j] - x_dot_ref[1]);
    }
    return grad;
}

Vector2d dJ_dux(Vector3d dt_s, Matrix<double,2, 3> u_s, Matrix<double,2, 4> x_s, Matrix<double,2, 4> xdot_s, Matrix<double, 2, 4> xddot_s){
	Vector3d u_x = u_s.row(0);
	Vector3d u_y = u_s.row(1);
	Vector4d x_x = x_s.row(0);
	Vector4d x_y = x_s.row(1);
	Vector4d xdot_x = xdot_s.row(0);
	Vector4d xdot_y = xdot_s.row(1);
	Vector4d xddot_x = xddot_s.row(0);
	Vector4d xddot_y = xddot_s.row(1);
	
	double px1u1 = w[6] * ((x_s.col(1) - u_s.col(1)).transpose() * (x_s.col(1) - u_s.col(1)) - lmax);
	double px2u2 = w[7] * ((x_s.col(2) - u_s.col(2)).transpose() * (x_s.col(2) - u_s.col(2)) - lmax);

	double dpx1u1_dux1 = -2 * (x_s(0, 1) - u_s(0, 1)) * w[6];

	double dpx2u2_dux1 = 2 * (dx_du(dt_s, u_x, x_x, xdot_x, xddot_x, 2, 1)) * (x_s(0, 2) - u_s(0, 1)) * w[7];

	double dpx2u2_dux2 = -2 * (x_s(1, 2) - u_s(1, 2)) * w[7];

	double px2u1 = w[8] * ((x_s.col(2) - u_s.col(1)).transpose() * (x_s.col(2) - u_s.col(1)) - lmax);
	double px3u2 = w[9] * ((x_s.col(3) - u_s.col(2)).transpose() * (x_s.col(3) - u_s.col(2)) - lmax);

	double dpx2u1_dux1 = 2 * (dx_du(dt_s, u_x, x_x, xdot_x, xddot_x, 2, 1) - 1) * (x_s(0, 2) - u_s(0, 1)) * w[8];

	double dpx3u2_dux2 = 2 * (dx_du(dt_s, u_x, x_x, xdot_x, xddot_x, 3, 2) - 1) * (x_s(0, 3) - u_s(0, 2)) * w[9];

	double dpx3u2_dux1 = 2 * (dx_du(dt_s, u_x, x_x, xdot_x, xddot_x, 3, 1)) * (x_s(0, 3) - u_s(0, 2)) * w[9];

    Vector2d dJ_dux_;
    dJ_dux_ << 2 * dJ_dux1(dt_s, u_x, x_x, xdot_x, xddot_x) + dpx1u1_dux1 + dpx2u2_dux1 + dpx2u1_dux1 + dpx3u2_dux1,
               2 * dJ_dux2(dt_s, u_x, x_x, xdot_x, xddot_x) + dpx2u2_dux2 + dpx3u2_dux2;
    return dJ_dux_;
}


Vector2d dJ_duy(Vector3d dt_s, Matrix<double,2, 3> u_s, Matrix<double,2, 4> x_s, Matrix<double,2, 4> x_dot_s, Matrix<double,2, 4> xd_dot_s){
	Vector3d u_x = u_s.row(0);
	Vector3d u_y = u_s.row(1);
	Vector4d x_x = x_s.row(0);
	Vector4d x_y = x_s.row(1);
	Vector4d xdot_x = x_dot_s.row(0);
	Vector4d xdot_y = x_dot_s.row(1);
	Vector4d xddot_x = xd_dot_s.row(0);
	Vector4d xddot_y = xd_dot_s.row(1);
	
	double px1u1 = w[6] * ((x_s.col(1) - u_s.col(1)).transpose() * (x_s.col(1) - u_s.col(1)) - lmax);
	double px2u2 = w[7] * ((x_s.col(2) - u_s.col(2)).transpose() * (x_s.col(2) - u_s.col(2)) - lmax);

    double dpx1u1_duy1 = -2 * (x_s(1, 1) - u_s(1, 1)) * w[6];

    double dpx2u2_duy1 = 2 * (dx_du(dt_s, u_y, x_y, xdot_y, xddot_y, 2, 1)) * (x_s(1, 2) - u_s(1, 1)) * w[7];

    double dpx2u2_duy2 = -2 * (x_s(1, 2) - u_s(1, 2)) * w[7];

	double px2u1 = w[8] * ((x_s.col(2) - u_s.col(1)).transpose() * (x_s.col(2) - u_s.col(1)) - lmax);
	double px3u2 = w[9] * ((x_s.col(3) - u_s.col(2)).transpose() * (x_s.col(3) - u_s.col(2)) - lmax);

    double dpx2u1_duy1 = 2 * (dx_du(dt_s, u_y, x_y, xdot_y, xddot_y, 2, 1) - 1) * (x_s(1, 2) - u_s(1, 1)) * w[8];

    double dpx3u2_duy2 = 2 * (dx_du(dt_s, u_y, x_y, xdot_y, xddot_y, 3, 2) - 1) * (x_s(1, 3) - u_s(1, 2)) * w[9];

    double dpx3u2_duy1 = 2 * (dx_du(dt_s, u_y, x_y, xdot_y, xddot_y, 3, 1)) * (x_s(1, 3) - u_s(1, 2)) * w[9];
	
	double pu1u0 = w[10] * (foot_flag * (u_s(1, 1) - u_s(1, 0)) - rfoot);
	double pu2u1 = w[11] * (-foot_flag * (u_s(1, 2) - u_s(1, 1)) - rfoot);

	double dpu1u0_duy1 = foot_flag * w[10];

    double dpu2u1_duy1 = - foot_flag * w[11];

    double dpu2u1_duy2 = foot_flag * w[11];

    Vector2d dJ_duy_;
    dJ_duy_ << 2 * dJ_duy1(dt_s, u_y, x_y, xdot_y, xddot_y) + dpx1u1_duy1 + dpx2u2_duy1 + dpx2u1_duy1 + dpx3u2_duy1 + dpu1u0_duy1 + dpu2u1_duy1,
               2 * dJ_duy2(dt_s, u_y, x_y, xdot_y, xddot_y) + dpx2u2_duy2 + dpx3u2_duy2 + dpu2u1_duy2;
    return dJ_duy_;
}

Vector3d dJ_dt(Vector3d dt_s, Matrix<double,2, 3> u_s, Matrix<double,2, 4> x_s, Matrix<double,2, 4> xdot_s, Matrix<double, 2, 4> xddot_s){
    double dt0 = dt_s[0];
    double dt0_low = max(dt0_min - current_time, 0.001);
    double dt0_high = dt_max;
    double dt1 = dt_s[1];
    double dt1_low = dt_min;
    double dt1_high = dt_max;
    double dt2 = dt_s[2];
    double dt2_low = dt_min;
    double dt2_high = dt_max;

	Vector3d u_x = u_s.row(0);
	Vector3d u_y = u_s.row(1);
	Vector4d x_x = x_s.row(0);
	Vector4d x_y = x_s.row(1);
	Vector4d xdot_x = xdot_s.row(0);
	Vector4d xdot_y = xdot_s.row(1);
	Vector4d xddot_x = xddot_s.row(0);
	Vector4d xddot_y = xddot_s.row(1);

	double p_dt0 = w[0] * (dt0_low - dt0) + w[1] * (dt0 - dt0_high);
	double p_dt1 = w[2] * (dt1_low - dt1) + w[3] * (dt1 - dt1_high);
	double p_dt2 = w[4] * (dt2_low - dt2) + w[5] * (dt2 - dt2_high);

	double dpt_dt0 = w[0] - w[1];
    double dpt_dt1 = w[2] - w[3];
    double dpt_dt2 = w[4] - w[5];

	// make sure the next step isn't too far away
	double px1u1 = w[6] * ((x_s.col(1) - u_s.col(1)).transpose() * (x_s.col(1) - u_s.col(1)) - lmax);
	double px2u2 = w[7] * ((x_s.col(2) - u_s.col(2)).transpose() * (x_s.col(2) - u_s.col(2)) - lmax);

	VectorXd v1(2); v1 <<	dx_dt(dt_s, u_x, x_x, xdot_x, xddot_x, 1, 0) * (x_s(0, 1) - u_s(0, 1)),
							dx_dt(dt_s, u_y, x_y, xdot_y, xddot_y, 1, 0) * (x_s(1, 1) - u_s(1, 1));
    double dpx1u1_dt0 = 2 * v1.transpose() * (x_s.col(1) - u_s.col(1));

	VectorXd v2(2); v2 <<	dx_dt(dt_s, u_x, x_x, xdot_x, xddot_x, 2, 0) * (x_s(0, 2) - u_s(0, 2)),
							dx_dt(dt_s, u_y, x_y, xdot_y, xddot_y, 2, 0)* (x_s(1, 2) - u_s(1, 2));
    double dpx2u2_dt0 = 2 * v2.transpose() * (x_s.col(2) - u_s.col(2));

	VectorXd v3(2); v3 <<	dx_dt(dt_s, u_x, x_x, xdot_x, xddot_x, 2, 1) * (x_s(0, 2) - u_s(0, 2)),
							dx_dt(dt_s, u_y, x_y, xdot_y, xddot_y, 2, 1)* (x_s(1, 2) - u_s(1, 2));
    double dpx2u2_dt1 = 2 * v3.transpose() * (x_s.col(2) - u_s.col(2));

	// make sure the current step isn't too far
	double px2u1 = w[8] * ((x_s.col(2) - u_s.col(1)).transpose() * (x_s.col(2) - u_s.col(1)) - lmax);
	double px3u2 = w[9] * ((x_s.col(3) - u_s.col(2)).transpose() * (x_s.col(3) - u_s.col(2)) - lmax);

	VectorXd v4(2); v4 <<	dx_dt(dt_s, u_x, x_x, xdot_x, xddot_x, 2, 0) * (x_s(0, 2) - u_s(0, 1)),
							dx_dt(dt_s, u_y, x_y, xdot_y, xddot_y, 2, 0)* (x_s(1, 2) - u_s(1, 1));
    double dpx2u1_dt0 = 2 * v4.transpose() * (x_s.col(2) - u_s.col(1));

	VectorXd v5(2); v5 <<	dx_dt(dt_s, u_x, x_x, xdot_x, xddot_x, 3, 0) * (x_s(0, 3) - u_s(0, 2)),
							dx_dt(dt_s, u_y, x_y, xdot_y, xddot_y, 3, 0)* (x_s(1, 3) - u_s(1, 2));
    double dpx3u2_dt0 = 2 * v5.transpose() * (x_s.col(3) - u_s.col(2));
	
	VectorXd v6(2); v6 <<	dx_dt(dt_s, u_x, x_x, xdot_x, xddot_x, 2, 1) * (x_s(0, 2) - u_s(0, 1)),
							dx_dt(dt_s, u_y, x_y, xdot_y, xddot_y, 2, 1)* (x_s(1, 2) - u_s(1, 1));
    double dpx2u1_dt1 = 2 * v6.transpose() * (x_s.col(2) - u_s.col(1));

	VectorXd v7(2); v7 <<	dx_dt(dt_s, u_x, x_x, xdot_x, xddot_x, 3, 1) * (x_s(0, 3) - u_s(0, 2)),
							dx_dt(dt_s, u_y, x_y, xdot_y, xddot_y, 3, 1)* (x_s(1, 3) - u_s(1, 2));
    double dpx3u2_dt1 = 2 * v7.transpose() * (x_s.col(3) - u_s.col(2));

	VectorXd v8(2); v8 << dx_dt(dt_s, u_x, x_x, xdot_x, xddot_x, 3, 2) * (x_s(0, 3) - u_s(0, 2)), dx_dt(dt_s, u_y, x_y, xdot_y, xddot_y, 3, 2)* (x_s(1, 3) - u_s(1, 2));
    double dpx3u2_dt2 = 2 * v8.transpose() * (x_s.col(3) - u_s.col(2));
    Vector3d dJ_dt_;

    dJ_dt_ << 2 * dJ_dt0(dt_s, u_s, x_s, xdot_s, xddot_s) + dpt_dt0 + dpx1u1_dt0 + dpx2u2_dt0 + dpx2u1_dt0 + dpx3u2_dt0,
              2 * dJ_dt1(dt_s, u_s, x_s, xdot_s, xddot_s) + dpt_dt1 + dpx2u2_dt1 + dpx2u1_dt1 + dpx3u2_dt1,
              2 * dJ_dt2(dt_s, u_s, x_s, xdot_s, xddot_s) + dpt_dt2 + dpx3u2_dt2;

    return dJ_dt_;
}

double J(Vector3d dt_s, Matrix<double,2, 3> u_s, Matrix<double,2, 4> x_s, Matrix<double,2, 4> xdot_s, Matrix<double, 2, 4> xddot_s){
    double J = 0.0;
    for(int i=1; i<u_s.size()/2+1; i++){
        J += pow(xdot_s(0, i)-x_dot_ref[0],2) + pow(xdot_s(1, i)-x_dot_ref[1],2);
    }
    return J;
 }

optimal gradient_descent(Vector3d& dt_s, Matrix<double,2, 3>& u_s, int iter=max_iter, bool verbose=true){
    double update = 1.0;
    int i = 0;
    Matrix<double,2, 4> x_s, xdot_s, xddot_s;
	
	// build position, velocity, acceleration vectors
	x_s(0, 0) = x_0(0);
	x_s(1, 0) = x_0(1);
	xdot_s(0, 0) = x_dot_0(0);
	xdot_s(1, 0) = x_dot_0(1);
	xddot_s(0, 0) = xddot(0.0, u_s(0, 0), x_s(0, 0), xdot_s(0, 0));
	xddot_s(1, 0) = xddot(0.0, u_s(1, 0), x_s(1, 0), xdot_s(1, 0));
    for(int k=0; k<3; k++){
        x_s(0,k) = x(dt_s[k], u_s(0, k), x_s(0, k), xdot_s(0, k));
        x_s(1,k) = x(dt_s[k], u_s(0, k), x_s(0, k), xdot_s(0, k));
		xdot_s(0,k) = xdot(dt_s[k], u_s(0, k), x_s(0, k), xdot_s(0, k));
        xdot_s(1,k) = xdot(dt_s[k], u_s(0, k), x_s(0, k), xdot_s(0, k));
		xddot_s(0,k) = xddot(dt_s[k], u_s(0, k), x_s(0, k), xdot_s(0, k));
        xddot_s(1,k) = xddot(dt_s[k], u_s(1, k), x_s(1, k), xdot_s(1, k));
	}
	
	// calculate initial gradients
    Vector3d grad_t = dJ_dt(dt_s, u_s, x_s, xdot_s, xddot_s);
    Vector2d grad_ux = dJ_dux(dt_s, u_s, x_s, xdot_s, xddot_s);
    Vector2d grad_uy = dJ_duy(dt_s, u_s, x_s, xdot_s, xddot_s);
    cout << "t  grad" << grad_t << endl;
    cout << "ux grad" << grad_ux << endl;
    cout << "uy grad" << grad_uy << endl;
	
	// begin the gradient descent loop
    while (update > 0.5 * (eta_u + eta_t)){	
		// build position, velocity, acceleration vectors
        x_s(0, 0) = x_0(0);
		x_s(1, 0) = x_0(1);
		xdot_s(0, 0) = x_dot_0(0);
		xdot_s(1, 0) = x_dot_0(1);
		xddot_s(0, 0) = xddot(0.0, u_s(0, 0), x_s(0, 0), xdot_s(0, 0));
		xddot_s(1, 0) = xddot(0.0, u_s(1, 0), x_s(1, 0), xdot_s(1, 0));
        for(int k=0; k<3; k++){
            x_s(0,k+1) = x(dt_s[k], u_s(0, k), x_s(0, k), xdot_s(0, k));
            x_s(1,k+1) = x(dt_s[k], u_s(1, k), x_s(1, k), xdot_s(1, k));
            xdot_s(0,k+1) = xdot(dt_s[k], u_s(0, k), x_s(0, k), xdot_s(0, k));
            xdot_s(1,k+1) = xdot(dt_s[k], u_s(1, k), x_s(1, k), xdot_s(1, k));
            xddot_s(0,k+1) = xddot(dt_s[k], u_s(0, k), x_s(0, k), xdot_s(0, k));
            xddot_s(1,k+1) = xddot(dt_s[k], u_s(1, k), x_s(1, k), xdot_s(1, k));
        }
		// calculate gradients
        grad_t = dJ_dt(dt_s, u_s, x_s, xdot_s, xddot_s);
        grad_ux = dJ_dux(dt_s, u_s, x_s, xdot_s, xddot_s);
        grad_uy = dJ_duy(dt_s, u_s, x_s, xdot_s, xddot_s);
		
		// update step times and locations
        dt_s -= eta_t * grad_t;
        u_s.block(0,1,1,2) -= eta_u * grad_ux.transpose();
        u_s.block(1,1,1,2) -= eta_u * grad_uy.transpose();
		
		// calculate update size to check for convergence
		VectorXd grad(7);
        grad.segment(0, 3) = grad_t * eta_t;
        grad.segment(3,2)  = grad_ux * eta_u;
        grad.segment(5,2) = grad_uy * eta_u;
        grad = grad.cwiseAbs();
        update = grad.sum();
		
		// if maximum iterations has passed, stop
        i += 1;
        if (i > iter) {break;}
    }
    // print(i)
    cout<<"iteration is :" << i << endl;
    optimal result;
    result.time = dt_s; result.u_s = u_s;
    return result;
}

// Callback function for receiving the planner outputs
void footstepPlanCallbackRK4(const std_msgs::Float64MultiArray::ConstPtr& msg)
{
    u_s(0,1) = msg->data[0];
    u_s(1,1) = msg->data[1];
    u_s(0,2) = msg->data[2];
    u_s(1,2) = msg->data[3];
    dt_s[0] = msg->data[4];
    dt_s[1] = msg->data[5];
    dt_s[2] = msg->data[6];
//    support_foot_flag = msg->data[7];
    flag = 1;
    cout<< "received u \n" << u_s << endl;
    cout<< "received t \n" << dt_s << endl;
}

void plannerInputCallback(const std_msgs::Float64MultiArray::ConstPtr& msg)
{
//    x, x_dot, y, y_dot = msg.data[:4]
    x_0(0) = msg->data[0];
    x_dot_0(0) = msg->data[1];
    x_dot_dot_0(0) = msg->data[2];
    x_0(1) = msg->data[3];
    x_dot_0(1) = msg->data[4];
    x_dot_dot_0(1) = msg->data[5];
    left_x  = msg->data[6];
    left_y  = msg->data[7];
    right_x = msg->data[8];
    right_y = msg->data[9];
    foot_flag = msg->data[10];
    current_time = msg->data[11];
}

int main(int argc, char ** argv){
    // Vector4d t_s(0.0, 0.5, 1.0, 1.5);      // initial guess
    // Vector3d u_s(0.0, -0.01, 0.02/0);   // step locations
//    double x_0 = 0.0;                      // initial COM position
//    double x_dot_0 = 0.0;                  //initial COM velocity
    Vector4d x_opt,y_opt;
    optimal opt;
    const double LOOP_RATE = 100;
    int iter_main = 0;
    ros::init(argc, argv, "gradient_descent_footPlanner_node");
    ros::NodeHandle n;
    ros::Rate loop_rate(LOOP_RATE);
    ros::Publisher planner_output_pub = n.advertise<std_msgs::Float64MultiArray>("/time_slider_gazebo/footstep_plan", 1);
    ros::Subscriber sub_planner_input = n.subscribe<std_msgs::Float64MultiArray>("/time_slider_gazebo/planner_input", 'None', plannerInputCallback);
    ros::Subscriber sub_RK4_planner_input = n.subscribe<std_msgs::Float64MultiArray>("/time_slider_gazebo/footstep_plan_rk4", 'None', footstepPlanCallbackRK4);
    support_foot_flag = foot_flag;

    while (ros::ok()){
        auto t1 = std::chrono::high_resolution_clock::now();
        // for (int i=0; i <max_iter; i+=2){
        cout << "u ini is \t" << u_s << endl;
        cout << "t ini is \t" << dt_s << endl;
        cout << "current robot COM pos is \t" << x_0 << endl;
        cout << "current robot COM vel is \t" << x_dot_0 << endl;
        ////////////// Gradient Descent ////////////////////////////////
        opt = gradient_descent(dt_s, u_s);
        if (foot_flag == -1){
            u_s(0,0) = left_x;
            u_s(1,0) = left_y;
        }
        else if (foot_flag == 1){
            u_s(0,0) = right_x;
            u_s(1,0) = right_y;
        }
        // }
        auto t2 = chrono::high_resolution_clock::now();
        auto duration = chrono::duration_cast<chrono::microseconds>( t2 - t1 ).count();
        cout << "Duration: \n" << duration << " microseconds" << endl;

        // publish
        if(support_foot_flag == foot_flag && opt.time(1) >= 1/LOOP_RATE){
            dt_s = opt.time; // PROBLEM!
            u_s = opt.u_s;
            std_msgs::Float64MultiArray footstep_plan;
            footstep_plan.data.push_back(u_s(0,1));
            footstep_plan.data.push_back(u_s(1,1));
            footstep_plan.data.push_back(u_s(0,2));
            footstep_plan.data.push_back(u_s(1,2));
            footstep_plan.data.push_back(dt_s(0));
            footstep_plan.data.push_back(dt_s(1));
            footstep_plan.data.push_back(dt_s(2));
            footstep_plan.data.push_back(support_foot_flag);
            planner_output_pub.publish(footstep_plan);
            if(flag==0){
                dt_s(0) -= 1/LOOP_RATE;
            }
            else if (flag==1){flag = 0;}
        } else {
            cout << "Not publishing -------------------" << endl;
        }
        if(support_foot_flag != foot_flag){
            dt_s(0) = opt.time(1);
            dt_s(1) = opt.time(2);
            u_s(0,0) = opt.u_s(0,1);
            u_s(1,0) = opt.u_s(1,1);
            u_s(0,1) = opt.u_s(0,2);
            u_s(1,1) = opt.u_s(1,2);
            u_s(0, 2) = opt.u_s(0,2) + (opt.u_s(0,2) - opt.u_s(0, 1));
            u_s(1, 2) = opt.u_s(1,2) + (opt.u_s(1,1) - opt.u_s(1, 2));
            current_time = 0.0;
            support_foot_flag = foot_flag;
            cout << "switching !!!!!!!!!!!!!!!!!!!!!!!!!!1!!!!!!!!!!!!" << endl;
        }
        // assign prev optimal as initial guess of next iteration


        cout << "planned foot \t" << support_foot_flag << endl;
        cout << "real foot \t" << foot_flag << endl;
        cout << "current time is \t"<< current_time << endl;

//        for (int i = 0; i < 4; i++)
//        {
//            x_opt[i] = x(opt.time, opt.u_s, i);
//            y_opt[i] = y(opt.time, opt.u_s, i);
//        }
        // cout << "x_opt is \n" << x_opt << endl;
        // cout << "y_opt is \n" << y_opt << endl;
        iter_main += 1;
        cout << "iteration in main loop is \t" << iter_main << endl;
        cout << "u opt is \t" << opt.u_s << endl;
        cout << "t opt is \t" << opt.time << endl;

        ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;}

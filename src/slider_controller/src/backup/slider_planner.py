#!/usr/bin/env python
import rospy
from planner import *
from z_planner import SLIP
import time
# from slider_controller.msg import FloatArray
from std_msgs.msg import Float64MultiArray

# Parameters setting of LIP model
g = 9.8
Zc = 0.7 # CoM height
Ts = 0.1 # sampling time

# Parameters setting of walking 
forward_velocity = 0.2
lateral_velocity = 0.0
# foot_length = 0.205
# foot_width = 0.15
step_time = 0.7
step_length = forward_velocity*step_time
inter_feet_clearance = 0.28

# Parameters of MPC planner
N_steps = 5 # number of walking steps in the prediction horizon
Qx = np.diag(np.array([0,3])) # weighting matrix on the CoM state
Qy = np.diag(np.array([0,3]))*2  #5 # weighting matrix on the CoM state
Wx = np.array([15]) # weighting matrix on tracking the difference of consecutive footsteps
Wy = np.array([15]) # weighting matrix on tracking the difference of consecutive footsteps
Rx = 100 # weighting matix on the ZMP tracking
Ry = 100 # weighting matix on the ZMP tracking

# 0.6 step time: 5,10,50
# 0.8 step time: 3,15,100

########### Parameters subscribed from the slider_controller node #############################
x_hat = np.array([[0.0],[0.0]]) # estimated current CoM position and velocity in the x direction
y_hat = np.array([[0.0],[0.0]])   # estimated current CoM position and velocity in the y direction
z_hat = np.array([[0.0],[0.0]])   # estimated current CoM position and velocity in the z direction

remaining_time = step_time # remaining time for the current step
p0_x = 0.0 # current foot position x direction
p0_y = inter_feet_clearance/2 # current foot position y direction
support_foot = -1 # right 1, left -1
###############################################################################################

# Z = SLIP()


def callback(msg):
    global x_hat, y_hat, remaining_time, p0_x, p0_y, support_foot
    x_hat = np.array([[msg.data[0]],[msg.data[1]]]) # estimated current CoM position and velocity in the x direction
    y_hat = np.array([[msg.data[2]],[msg.data[3]]])   # estimated current CoM position and velocity in the y direction

    remaining_time = msg.data[4] # remaining time for the current step
    p0_x = msg.data[5] # current foot position x direction
    p0_y = msg.data[6] # current foot position y direction
    support_foot = msg.data[7] # support foot


def sliderPlanner():
    # Initialize this node
    rospy.init_node('ros_SLIDER_planner_node', anonymous=True)

    pub = rospy.Publisher('/slider_gazebo/zmp_foothold', Float64MultiArray, queue_size=1)
    sub = rospy.Subscriber('/slider_gazebo/planner_input', Float64MultiArray, callback)

    rate = rospy.Rate(1000)

    opt = Float64MultiArray()

    while not rospy.is_shutdown():   
        # start = time.time()     

        Ns = int(step_time/Ts) # number of samples per step
        Nr = int(remaining_time/Ts)# number of remaining samples for the current step
        N = Nr+N_steps*Ns # horizon length (measured by the number of samples)

        # Run MPC planner
        Uopt_x, Uopt_y = mpcPlanner(x_hat,y_hat,p0_x,p0_y,support_foot,Ns,Nr,N_steps,N,inter_feet_clearance,forward_velocity,lateral_velocity,A,B,Qx,Qy,Rx,Ry,Wx,Wy,step_length)
        #
        # print('Uopt is ', Uopt_x, Uopt_y)
        # print('Prediction length is ', N)
        # current_zmp_x, current_zmp_y, next_foothold_x, next_foothold_y
        opt.data = [Uopt_x[0], Uopt_y[0], (p0_x+Uopt_x[N]), (p0_y+Uopt_y[N])]

        # end = time.time()

        # print "-----------optimal solution-------------"
        # print "support: ", support_foot
        # print "p0_x: ", p0_x
        # print "current foot y: ", p0_y
        # print "next foot x: ", p0_x+Uopt_x[N]
        # print "next foot y: ", p0_y+Uopt_y[N]
        # print "----------------------------------------"
        # print "time: ", end-start

        pub.publish(opt)

        rate.sleep()


if __name__ == '__main__':
    # Generate offline matrices
    A,B = genLIPM(g,Zc,Ts)
    try:
        sliderPlanner()
    except rospy.ROSInterruptException:
        pass
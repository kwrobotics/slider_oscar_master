#include <iostream>
#include <chrono>
#include <cmath>
#include <Eigen/Dense>

#include "ros/ros.h"
#include <std_msgs/Float64.h>
#include <std_msgs/Float64MultiArray.h>

using namespace Eigen;
using namespace std;

const double m = 15.0;    // body mass
const double m_s = 0.3;   // swing leg mass
const double L = 1.2;     // leg length
const double h = 0.7;     // body height
const double g = 9.81;    // gravity
const double omega = sqrt(g / h);  // omega

// penalty weight
const double T_end = 100;
const double dt = 0.001;
const double vmax = 5.0;              // m/s
const double step_time = 0.7;
int flag = 0;
int support_foot_flag = -1;
Vector2d x_dot_ref(0.05, 0.00);     // m/s

// Parameters subscribed from the slider_controller node
Vector2d x_0(0.0, 0.0);
Vector2d x_dot_0(0.0, 0.0);
Vector2d x_dot_dot_0(0.0, 0.0);
Vector3d dt_s(0.4, 0.4, 0.4);
Matrix<double,2,3> u_s((Matrix<double,2,3>() << 0.0, 0.0,  0.02, 0.15, -0.21924587,  0.03075412).finished());
// Eigen::Matrix3d A((Eigen::Matrix3d() << 1, 2, 3, 4, 5, 6, 7, 8, 9).finished());

double left_x = 0.0;
double left_y = 0.15;
double right_x = 0.0;
double right_y = -0.15;
int foot_flag = -1;
// penalty weights
const int n_weights = 12;
double w[n_weights] = { 0 };

double a0x = 0.5 * (x_0[0] - u_s(0, 0) + x_dot_0[0] / omega);
double b0x = 0.5 * (x_0[0] - u_s(0, 0)- x_dot_0[0] / omega);

double a0y = 0.5 * (x_0[1] - u_s(1, 0) + x_dot_0[1] / omega);
double b0y = 0.5 * (x_0[1] - u_s(1, 0) - x_dot_0[1] / omega);

double swing_x = 0.0;
double swing_z = 0.0;
double swing_y = -0.15;

const double max_iter = 200;
int n_repeats = 1;
bool rk4_received = false;

// constraints
// line up with Python
const double lmax = 2*sqrt(pow(L, 2.0) - pow(h, 2.0));
const double rfoot = 0.3;
const double dt0_min = 0.2;
const double dt_min = 0.4;
const double dt_max = 1.2;

double current_time = 0.0;

double dxdot_dt(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s, int kx, int kt);
double dx_dt(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s, int kx, int kt);
double dxdot_du(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s, int kx, int ku);
double dx_du(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s, int kx, int ku);

struct bfgs {
	VectorXd x_k;		// parameters (dt, u_x, u_y, at step k)
	VectorXd delta_x_k;	// parameter update at step k
	VectorXd y_k;		// change in gradient at step k
	MatrixXd B_k;		// Hessian approximation at step k
	MatrixXd H_k;		// inverse Hessian approximation at step k
};

//VectorXd x_k_init(7);
//x_k_init[0] = 0.4;
////x_k_init  = VectorXd::Zero(7);
//
////x_k_init << 0.4, 0.4, 0.4, 0.0, 0.0, -0.15, 0.15;
////x_k_init(7) << 0.4, 0.4, 0.4, 0.0, 0.0, -0.15, 0.15;
////x_k_init << 0.4, 0.4, 0.4, 0.0, 0.0, -0.15, 0.15;

bfgs solution = {
	VectorXd::Zero(7),
	VectorXd::Zero(7),
	VectorXd::Zero(7),
	Matrix<double, 7, 7>::Identity(),
	Matrix<double, 7, 7>::Identity(),
};

double x(double dt, double u, double x_prev, double x_dot_prev){
	double x_ = (x_prev - u + x_dot_prev / omega) * exp(omega * dt) + (x_prev - u - x_dot_prev / omega) * exp(-omega * dt) + u;
    return x_;
}

double xdot(double dt, double u, double x_prev, double x_dot_prev){
	double xdot_ = omega * (x_prev - u + x_dot_prev / omega) * exp(omega * dt) - omega * (x_prev - u - x_dot_prev / omega) * exp(-omega * dt);
    return xdot_;
}

double xddot(double dt, double u, double x_prev, double x_dot_prev){
	double xddot_ = pow(omega, 2) * (x_prev - u + x_dot_prev / omega) * exp(omega * dt) + pow(omega, 2) * (x_prev - u - x_dot_prev / omega) * exp(-omega * dt);
    return xddot_;
}

double dxdot_dt(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s, int kx=1, int kt=0){
	double dxdot_dt_ = 0.0;
	if (kx == kt){
		dxdot_dt_ = xddot_s[kx]; // question, will be -1?
	}else if (kx > kt){
		dxdot_dt_ = 0.5 * dx_dt(dt_s, u_s, x_s, xdot_s, xddot_s, kx-1, kt) * omega * (exp(omega * dt_s[kx-1]) + exp(-omega * dt_s[kx-1])) - 0.5 * dxdot_dt(dt_s, u_s, x_s, xdot_s, xddot_s, kx-1, kt) * (exp(omega * dt_s[kx-1]) - exp(-omega * dt_s[kx-1]));
	}
	return dxdot_dt_;
}

double dx_dt(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s, int kx=1, int kt=0){
    double dx_dt_ = 0.0;
	if (kx == kt){dxdot_dt
		dx_dt_ = xdot_s[kx];
	}else if (kx > kt){
		dx_dt_ = 0.5 * dx_dt(dt_s, u_s, x_s, xdot_s, xddot_s, kx-1, kt) * (exp(omega * dt_s[kx-1]) + exp(-omega * dt_s[kx-1])) + 0.5 * dxdot_dt(dt_s, u_s, x_s, xdot_s, xddot_s, kx-1, kt) * (exp(omega * dt_s[kx-1]) - exp(-omega * dt_s[kx-1])) / omega;
	}
	return dx_dt_;
}

double dx_du(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s, int kx=1, int ku=1){
    double dx_du_ = 0.0;
	if (kx == ku + 1){
		dx_du_ = -0.5 * (exp(omega * dt_s[kx-2]) + exp(-omega * dt_s[kx-2])) + 1;
	}else if (kx > ku + 1){
		dx_du_ = 0.5 * dx_du(dt_s, u_s, x_s, xdot_s, xddot_s, kx-1, ku) * (exp(omega * dt_s[kx-1]) + exp(-omega * dt_s[kx-1])) + 0.5 * dxdot_du(dt_s, u_s, x_s, xdot_s, xddot_s, kx-1, ku) * (exp(omega * dt_s[kx-1]) - exp(-omega * dt_s[kx-1])) / omega;
	}
	return dx_du_;
}

double dxdot_du(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s, int kx=1, int ku=1){
    double dxdot_du_ = 0.0;
	if (kx == ku + 1){
		dxdot_du_ = -0.5 * omega * (exp(omega * dt_s[kx-1]) - exp(-omega * dt_s[kx-1]));
	}else if (kx > ku + 1){
		dxdot_du_ = 0.5 * dx_du(dt_s, u_s, x_s, xdot_s, xddot_s, kx-1, ku) * omega * (exp(omega * dt_s[kx-1]) - exp(-omega * dt_s[kx-1])) + 0.5 * dxdot_du(dt_s, u_s, x_s, xdot_s, xddot_s, kx-1, ku) * (exp(omega * dt_s[kx-1]) + exp(-omega * dt_s[kx-1]));
	}
	return dxdot_du_;
}

double dJ_dux1(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s){
    double grad = 0;
    for(int j=2; j<4; j++){
        grad += 2 * dxdot_du(dt_s, u_s, x_s, xdot_s, xddot_s, j, 1) * (xdot_s[j] - x_dot_ref[0]);}
    return grad;
}

double dJ_dux2(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s){
    double grad = 0;
    for(int j=3; j<4; j++){
        grad += 2 * dxdot_du(dt_s, u_s, x_s, xdot_s, xddot_s, j, 2) * (xdot_s[j] - x_dot_ref[0]);}
    return grad;
}

double dJ_duy1(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s){
    double grad = 0;
    for(int j=2; j<4; j++){
        grad += 2 * dxdot_du(dt_s, u_s, x_s, xdot_s, xddot_s, j, 1) * (xdot_s[j] - x_dot_ref[1]);}
    return grad;
}

double dJ_duy2(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s){
    double grad = 0;
    for(int j=3; j<4; j++){
        grad += 2 * dxdot_du(dt_s, u_s, x_s, xdot_s, xddot_s, j, 2) * (xdot_s[j] - x_dot_ref[1]);}
    return grad;
}

double dJ_dt0(Vector3d dt_s, Matrix<double,2, 3> u_s, Matrix<double,2, 4> x_s, Matrix<double,2, 4> xdot_s, Matrix<double, 2, 4> xddot_s){
    double grad = 0.0;
	Vector3d u_x = u_s.row(0);
	Vector3d u_y = u_s.row(1);
	Vector4d x_x = x_s.row(0);
	Vector4d x_y = x_s.row(1);
	Vector4d xdot_x = xdot_s.row(0);
	Vector4d xdot_y = xdot_s.row(1);
	Vector4d xddot_x = xddot_s.row(0);
	Vector4d xddot_y = xddot_s.row(1);
	
    for(int j=1; j<4; j++){
        grad += 2 * dxdot_dt(dt_s, u_x, x_x, xdot_x, xddot_x, j, 0) * (xdot_x[j] - x_dot_ref[0]);
        grad += 2 * dxdot_dt(dt_s, u_y, x_y, xdot_y, xddot_y, j, 0) * (xdot_y[j] - x_dot_ref[1]);
    }
    return grad;
}

double dJ_dt1(Vector3d dt_s, Matrix<double,2, 3> u_s, Matrix<double,2, 4> x_s, Matrix<double,2, 4> xdot_s, Matrix<double, 2, 4> xddot_s){
    double grad = 0.0;
	Vector3d u_x = u_s.row(0);
	Vector3d u_y = u_s.row(1);
	Vector4d x_x = x_s.row(0);
	Vector4d x_y = x_s.row(1);
	Vector4d xdot_x = xdot_s.row(0);
	Vector4d xdot_y = xdot_s.row(1);
	Vector4d xddot_x = xddot_s.row(0);
	Vector4d xddot_y = xddot_s.row(1);
	
    for(int j=1; j<4; j++){
        grad += 2 * dxdot_dt(dt_s, u_x, x_x, xdot_x, xddot_x, j, 1) * (xdot_x[j] - x_dot_ref[0]);
        grad += 2 * dxdot_dt(dt_s, u_y, x_y, xdot_y, xddot_y, j, 1) * (xdot_y[j] - x_dot_ref[1]);
    }
    return grad;
}

double dJ_dt2(Vector3d dt_s, Matrix<double,2, 3> u_s, Matrix<double,2, 4> x_s, Matrix<double,2, 4> xdot_s, Matrix<double, 2, 4> xddot_s){
    double grad = 0.0;
	Vector3d u_x = u_s.row(0);
	Vector3d u_y = u_s.row(1);
	Vector4d x_x = x_s.row(0);
	Vector4d x_y = x_s.row(1);
	Vector4d xdot_x = xdot_s.row(0);
	Vector4d xdot_y = xdot_s.row(1);
	Vector4d xddot_x = xddot_s.row(0);
	Vector4d xddot_y = xddot_s.row(1);
	
    for(int j=1; j<4; j++){
        grad += 2 * dxdot_dt(dt_s, u_x, x_x, xdot_x, xddot_x, j, 2) * (xdot_x[j] - x_dot_ref[0]);
        grad += 2 * dxdot_dt(dt_s, u_y, x_y, xdot_y, xddot_y, j, 2) * (xdot_y[j] - x_dot_ref[1]);
    }
    return grad;
}

Vector2d dJ_dux(Vector3d dt_s, Matrix<double,2, 3> u_s, Matrix<double,2, 4> x_s, Matrix<double,2, 4> xdot_s, Matrix<double, 2, 4> xddot_s){
	Vector3d u_x = u_s.row(0);
	Vector3d u_y = u_s.row(1);
	Vector4d x_x = x_s.row(0);
	Vector4d x_y = x_s.row(1);
	Vector4d xdot_x = xdot_s.row(0);
	Vector4d xdot_y = xdot_s.row(1);
	Vector4d xddot_x = xddot_s.row(0);
	Vector4d xddot_y = xddot_s.row(1);
	
	double px1u1 = w[6] * ((x_s.col(1) - u_s.col(1)).transpose() * (x_s.col(1) - u_s.col(1)) - lmax);
	double px2u2 = w[7] * ((x_s.col(2) - u_s.col(2)).transpose() * (x_s.col(2) - u_s.col(2)) - lmax);

	double dpx1u1_dux1 = -2 * (x_s(0, 1) - u_s(0, 1)) * w[6];

	double dpx2u2_dux1 = 2 * (dx_du(dt_s, u_x, x_x, xdot_x, xddot_x, 2, 1)) * (x_s(0, 2) - u_s(0, 1)) * w[7];

	double dpx2u2_dux2 = -2 * (x_s(1, 2) - u_s(1, 2)) * w[7];

	double px2u1 = w[8] * ((x_s.col(2) - u_s.col(1)).transpose() * (x_s.col(2) - u_s.col(1)) - lmax);
	double px3u2 = w[9] * ((x_s.col(3) - u_s.col(2)).transpose() * (x_s.col(3) - u_s.col(2)) - lmax);

	double dpx2u1_dux1 = 2 * (dx_du(dt_s, u_x, x_x, xdot_x, xddot_x, 2, 1) - 1) * (x_s(0, 2) - u_s(0, 1)) * w[8];

	double dpx3u2_dux2 = 2 * (dx_du(dt_s, u_x, x_x, xdot_x, xddot_x, 3, 2) - 1) * (x_s(0, 3) - u_s(0, 2)) * w[9];

	double dpx3u2_dux1 = 2 * (dx_du(dt_s, u_x, x_x, xdot_x, xddot_x, 3, 1)) * (x_s(0, 3) - u_s(0, 2)) * w[9];

    Vector2d dJ_dux_;
    dJ_dux_ << 2 * dJ_dux1(dt_s, u_x, x_x, xdot_x, xddot_x) + dpx1u1_dux1 + dpx2u2_dux1 + dpx2u1_dux1 + dpx3u2_dux1,
               2 * dJ_dux2(dt_s, u_x, x_x, xdot_x, xddot_x) + dpx2u2_dux2 + dpx3u2_dux2;
    return dJ_dux_;
}


Vector2d dJ_duy(Vector3d dt_s, Matrix<double,2, 3> u_s, Matrix<double,2, 4> x_s, Matrix<double,2, 4> x_dot_s, Matrix<double,2, 4> xd_dot_s){
	Vector3d u_x = u_s.row(0);
	Vector3d u_y = u_s.row(1);
	Vector4d x_x = x_s.row(0);
	Vector4d x_y = x_s.row(1);
	Vector4d xdot_x = x_dot_s.row(0);
	Vector4d xdot_y = x_dot_s.row(1);
	Vector4d xddot_x = xd_dot_s.row(0);
	Vector4d xddot_y = xd_dot_s.row(1);
	
	double px1u1 = w[6] * ((x_s.col(1) - u_s.col(1)).transpose() * (x_s.col(1) - u_s.col(1)) - lmax);
	double px2u2 = w[7] * ((x_s.col(2) - u_s.col(2)).transpose() * (x_s.col(2) - u_s.col(2)) - lmax);

    double dpx1u1_duy1 = -2 * (x_s(1, 1) - u_s(1, 1)) * w[6];

    double dpx2u2_duy1 = 2 * (dx_du(dt_s, u_y, x_y, xdot_y, xddot_y, 2, 1)) * (x_s(1, 2) - u_s(1, 1)) * w[7];

    double dpx2u2_duy2 = -2 * (x_s(1, 2) - u_s(1, 2)) * w[7];

	double px2u1 = w[8] * ((x_s.col(2) - u_s.col(1)).transpose() * (x_s.col(2) - u_s.col(1)) - lmax);
	double px3u2 = w[9] * ((x_s.col(3) - u_s.col(2)).transpose() * (x_s.col(3) - u_s.col(2)) - lmax);

    double dpx2u1_duy1 = 2 * (dx_du(dt_s, u_y, x_y, xdot_y, xddot_y, 2, 1) - 1) * (x_s(1, 2) - u_s(1, 1)) * w[8];

    double dpx3u2_duy2 = 2 * (dx_du(dt_s, u_y, x_y, xdot_y, xddot_y, 3, 2) - 1) * (x_s(1, 3) - u_s(1, 2)) * w[9];

    double dpx3u2_duy1 = 2 * (dx_du(dt_s, u_y, x_y, xdot_y, xddot_y, 3, 1)) * (x_s(1, 3) - u_s(1, 2)) * w[9];
	
	double pu1u0 = w[10] * (foot_flag * (u_s(1, 1) - u_s(1, 0)) - rfoot);
	double pu2u1 = w[11] * (-foot_flag * (u_s(1, 2) - u_s(1, 1)) - rfoot);

	double dpu1u0_duy1 = foot_flag * w[10];

    double dpu2u1_duy1 = - foot_flag * w[11];

    double dpu2u1_duy2 = foot_flag * w[11];

    Vector2d dJ_duy_;
    dJ_duy_ << 2 * dJ_duy1(dt_s, u_y, x_y, xdot_y, xddot_y) + dpx1u1_duy1 + dpx2u2_duy1 + dpx2u1_duy1 + dpx3u2_duy1 + dpu1u0_duy1 + dpu2u1_duy1,
               2 * dJ_duy2(dt_s, u_y, x_y, xdot_y, xddot_y) + dpx2u2_duy2 + dpx3u2_duy2 + dpu2u1_duy2;
    return dJ_duy_;
}

Vector3d dJ_dt(Vector3d dt_s, Matrix<double,2, 3> u_s, Matrix<double,2, 4> x_s, Matrix<double,2, 4> xdot_s, Matrix<double, 2, 4> xddot_s){
    double dt0 = dt_s[0];
    double dt0_low = max(dt0_min - current_time, 0.001);
    double dt0_high = dt_max;
    double dt1 = dt_s[1];
    double dt1_low = dt_min;
    double dt1_high = dt_max;
    double dt2 = dt_s[2];
    double dt2_low = dt_min;
    double dt2_high = dt_max;

	Vector3d u_x = u_s.row(0);
	Vector3d u_y = u_s.row(1);
	Vector4d x_x = x_s.row(0);
	Vector4d x_y = x_s.row(1);
	Vector4d xdot_x = xdot_s.row(0);
	Vector4d xdot_y = xdot_s.row(1);
	Vector4d xddot_x = xddot_s.row(0);
	Vector4d xddot_y = xddot_s.row(1);

//	double p_dt0 = w[0] * (dt0_low - dt0) + w[1] * (dt0 - dt0_high);
//	double p_dt1 = w[2] * (dt1_low - dt1) + w[3] * (dt1 - dt1_high);
//	double p_dt2 = w[4] * (dt2_low - dt2) + w[5] * (dt2 - dt2_high);

	double dpt_dt0 = w[0] - w[1];
    double dpt_dt1 = w[2] - w[3];
    double dpt_dt2 = w[4] - w[5];

	// make sure the next step isn't too far away
	double px1u1 = w[6] * ((x_s.col(1) - u_s.col(1)).transpose() * (x_s.col(1) - u_s.col(1)) - lmax);
	double px2u2 = w[7] * ((x_s.col(2) - u_s.col(2)).transpose() * (x_s.col(2) - u_s.col(2)) - lmax);

	VectorXd v1(2); v1 <<	dx_dt(dt_s, u_x, x_x, xdot_x, xddot_x, 1, 0) * (x_s(0, 1) - u_s(0, 1)),
							dx_dt(dt_s, u_y, x_y, xdot_y, xddot_y, 1, 0) * (x_s(1, 1) - u_s(1, 1));
    double dpx1u1_dt0 = 2 * w[6] * v1.transpose() * (x_s.col(1) - u_s.col(1));

	VectorXd v2(2); v2 <<	dx_dt(dt_s, u_x, x_x, xdot_x, xddot_x, 2, 0) * (x_s(0, 2) - u_s(0, 2)),
							dx_dt(dt_s, u_y, x_y, xdot_y, xddot_y, 2, 0)* (x_s(1, 2) - u_s(1, 2));
    double dpx2u2_dt0 = 2 * w[7] * v2.transpose() * (x_s.col(2) - u_s.col(2));

	VectorXd v3(2); v3 <<	dx_dt(dt_s, u_x, x_x, xdot_x, xddot_x, 2, 1) * (x_s(0, 2) - u_s(0, 2)),
							dx_dt(dt_s, u_y, x_y, xdot_y, xddot_y, 2, 1)* (x_s(1, 2) - u_s(1, 2));
    double dpx2u2_dt1 = 2 * w[7] * v3.transpose() * (x_s.col(2) - u_s.col(2));

	// make sure the current step isn't too far
	double px2u1 = w[8] * ((x_s.col(2) - u_s.col(1)).transpose() * (x_s.col(2) - u_s.col(1)) - lmax);
	double px3u2 = w[9] * ((x_s.col(3) - u_s.col(2)).transpose() * (x_s.col(3) - u_s.col(2)) - lmax);

	VectorXd v4(2); v4 <<	dx_dt(dt_s, u_x, x_x, xdot_x, xddot_x, 2, 0) * (x_s(0, 2) - u_s(0, 1)),
							dx_dt(dt_s, u_y, x_y, xdot_y, xddot_y, 2, 0)* (x_s(1, 2) - u_s(1, 1));
    double dpx2u1_dt0 = 2 * w[8] * v4.transpose() * (x_s.col(2) - u_s.col(1));

	VectorXd v5(2); v5 <<	dx_dt(dt_s, u_x, x_x, xdot_x, xddot_x, 3, 0) * (x_s(0, 3) - u_s(0, 2)),
							dx_dt(dt_s, u_y, x_y, xdot_y, xddot_y, 3, 0)* (x_s(1, 3) - u_s(1, 2));
    double dpx3u2_dt0 = 2 * w[9] * v5.transpose() * (x_s.col(3) - u_s.col(2));

	
	VectorXd v6(2); v6 <<	dx_dt(dt_s, u_x, x_x, xdot_x, xddot_x, 2, 1) * (x_s(0, 2) - u_s(0, 1)),
							dx_dt(dt_s, u_y, x_y, xdot_y, xddot_y, 2, 1)* (x_s(1, 2) - u_s(1, 1));
    double dpx2u1_dt1 = 2 * w[8] * v6.transpose() * (x_s.col(2) - u_s.col(1));

	VectorXd v7(2); v7 <<	dx_dt(dt_s, u_x, x_x, xdot_x, xddot_x, 3, 1) * (x_s(0, 3) - u_s(0, 2)),
							dx_dt(dt_s, u_y, x_y, xdot_y, xddot_y, 3, 1)* (x_s(1, 3) - u_s(1, 2));
    double dpx3u2_dt1 = 2 * w[9] * v7.transpose() * (x_s.col(3) - u_s.col(2));


	VectorXd v8(2); v8 << dx_dt(dt_s, u_x, x_x, xdot_x, xddot_x, 3, 2) * (x_s(0, 3) - u_s(0, 2)), dx_dt(dt_s, u_y, x_y, xdot_y, xddot_y, 3, 2)* (x_s(1, 3) - u_s(1, 2));
    double dpx3u2_dt2 = 2 * w[9] * v8.transpose() * (x_s.col(3) - u_s.col(2));


    Vector3d dJ_dt_;

    dJ_dt_ << 2 * dJ_dt0(dt_s, u_s, x_s, xdot_s, xddot_s) + dpt_dt0 + dpx1u1_dt0 + dpx2u2_dt0 + dpx2u1_dt0 + dpx3u2_dt0,
              2 * dJ_dt1(dt_s, u_s, x_s, xdot_s, xddot_s) + dpt_dt1 + dpx2u2_dt1 + dpx2u1_dt1 + dpx3u2_dt1,
              2 * dJ_dt2(dt_s, u_s, x_s, xdot_s, xddot_s) + dpt_dt2 + dpx3u2_dt2;

    return dJ_dt_;
}

double J(Vector3d dt_s, Matrix<double,2, 3> u_s, Matrix<double,2, 4> x_s, Matrix<double,2, 4> xdot_s, Matrix<double, 2, 4> xddot_s){
    double J = 0.0;
    for(int i=1; i<u_s.size()/2+1; i++){
        J += pow(xdot_s(0, i)-x_dot_ref[0],2) + pow(xdot_s(1, i)-x_dot_ref[1],2);
    }
    return J;
 }


bfgs BFGS(bfgs& prev_result, bool verbose = true) {
	// previous solution
	VectorXd x_k = prev_result.x_k;
	VectorXd delta_x_k = prev_result.delta_x_k;
//	VectorXd y_k = prev_result.y_k;x_k
	MatrixXd B_k = prev_result.B_k;
	MatrixXd H_k = prev_result.H_k;
	MatrixXd I = MatrixXd::Identity(7,7);

	// full dynamic model
	// build position, velocity, acceleration vectors
	MatrixXd x_s, xdot_s, xddot_s;
	x_s = MatrixXd::Zero(2, 4);
	xdot_s = MatrixXd::Zero(2, 4);
	xddot_s = MatrixXd::Zero(2, 4);
	x_s(0, 0) = x_0(0);
	x_s(1, 0) = x_0(1);
	xdot_s(0, 0) = x_dot_0(0);
	xdot_s(1, 0) = x_dot_0(1);
	xddot_s(0, 0) = xddot(0.0, u_s(0, 0), x_s(0, 0), xdot_s(0, 0));
	xddot_s(1, 0) = xddot(0.0, u_s(1, 0), x_s(1, 0), xdot_s(1, 0));
	for (int k = 0; k < 3; k++) {
		x_s(0, k + 1) = x(dt_s[k], u_s(0, k), x_s(0, k), xdot_s(0, k));
		x_s(1, k + 1) = x(dt_s[k], u_s(1, k), x_s(1, k), xdot_s(1, k));
		xdot_s(0, k + 1) = xdot(dt_s[k], u_s(0, k), x_s(0, k), xdot_s(0, k));
		xdot_s(1, k + 1) = xdot(dt_s[k], u_s(1, k), x_s(1, k), xdot_s(1, k));
		xddot_s(0, k + 1) = xddot(dt_s[k], u_s(0, k), x_s(0, k), xdot_s(0, k));
		xddot_s(1, k + 1) = xddot(dt_s[k], u_s(1, k), x_s(1, k), xdot_s(1, k));
	}

//	cout << "Previous solution: \t" << x_k << endl;

	// calculate gradients at step k
	Vector3d grad_t = dJ_dt(dt_s, u_s, x_s, xdot_s, xddot_s);
//	cout<< "calculate dJ_dt, first iteration" << endl;
	Vector2d grad_ux = dJ_dux(dt_s, u_s, x_s, xdot_s, xddot_s);
//	cout<< "calculate dJ_dux, first iteration" << endl;
	Vector2d grad_uy = dJ_duy(dt_s, u_s, x_s, xdot_s, xddot_s);
//	cout<< "calculate dJ_duy, first iteration" << endl;
	VectorXd grad_k(7);
	grad_k << grad_t, grad_ux, grad_uy;
//	cout<<"grad_k is " << grad_k << endl;

	// line search to find update size
	double alpha_k = 1e-7;
	double best_cost = J(dt_s, u_s, x_s, xdot_s, xddot_s);
//    cout<< "line search " << endl;
	for (int i = 0; i < 100; i++) {
		double alpha_int = 1e-6 / (i+1);								// intermediate value of alpha
		// get intermediate dt_int and u_int
		VectorXd x_k_int = x_k - alpha_int * H_k * grad_k;		// intermediate parameter valu
//		cout<< "x_k_int is " << x_k_int << endl;
		Vector3d dt_int = x_k_int.segment(0, 3);				// get dts
		MatrixXd u_int = u_s;						// get us, (2, 3)
		u_int.block(0, 1, 1, 2) = x_k_int.segment(3, 2).transpose(); // x_k_int Vector(7)
		u_int.block(1, 1, 1, 2) = x_k_int.segment(5, 2).transpose();
//		cout<< "line search 0.1 " << endl;

		// use full dynamic model to evaluate state with dt_int and u_int
		Matrix<double, 2, 4> x_int, xdot_int, xddot_int;
		x_int(0, 0) = x_0(0);
		x_int(1, 0) = x_0(1);
		xdot_int(0, 0) = x_dot_0(0);
		xdot_int(1, 0) = x_dot_0(1);
		xddot_int(0, 0) = xddot(0.0, u_int(0, 0), x_int(0, 0), xdot_int(0, 0));
		xddot_int(1, 0) = xddot(0.0, u_int(1, 0), x_int(1, 0), xdot_int(1, 0));
		for (int k = 0; k < 3; k++) {
			x_int(0, k + 1) = x(dt_int[k], u_int(0, k), x_int(0, k), xdot_int(0, k));
			x_int(1, k + 1) = x(dt_int[k], u_int(0, k), x_int(0, k), xdot_int(0, k));
			xdot_int(0, k + 1) = xdot(dt_int[k], u_int(0, k), x_int(0, k), xdot_int(0, k));
			xdot_int(1, k + 1) = xdot(dt_int[k], u_int(1, k), x_int(1, k), xdot_int(1, k));
			xddot_int(0, k + 1) = xddot(dt_int[k], u_int(0, k), x_int(0, k), xdot_int(0, k));
			xddot_int(1, k + 1) = xddot(dt_int[k], u_int(1, k), x_int(1, k), xdot_int(1, k));
		}

		// calculate cost with intermediate values
		double cost = J(dt_int, u_int, x_int, xdot_int, xddot_int);		// calculate cost
		if (cost < best_cost) {											// if cost has improved, keep corresponding best alpha
			alpha_k = alpha_int;
			best_cost = cost;
		}
//		cout << "alpha is " << alpha_int << endl;
//		cout << "cost is " << cost << endl;
	}
	cout << "final value of alpha " << alpha_k << endl;
    delta_x_k = - alpha_k * H_k * grad_k;
	VectorXd x_k_new(7);
	x_k_new << x_k + delta_x_k;
//	cout << "delta_x_k" << delta_x_k << endl;
//    cout << "new solution" << x_k_new << endl;
	Vector3d dt_new = x_k_new.segment(0, 3);				// get dts
	Matrix<double, 2, 3>& u_new = u_s;						// get us
	u_new.block(0, 1, 1, 2) = x_k_new.segment(3, 2).transpose();
	u_new.block(1, 1, 1, 2) = x_k_new.segment(5, 2).transpose();

	MatrixXd x_new, xdot_new, xddot_new;
	x_new = MatrixXd::Zero(2, 4);
	xdot_new = MatrixXd::Zero(2, 4);
	xddot_new = MatrixXd::Zero(2, 4);
	x_new(0, 0) = x_0(0);
	x_new(1, 0) = x_0(1);
	xdot_new(0, 0) = x_dot_0(0);
	xdot_new(1, 0) = x_dot_0(1);
	xddot_new(0, 0) = xddot(0.0, u_new(0, 0), x_new(0, 0), xdot_new(0, 0));
	xddot_new(1, 0) = xddot(0.0, u_new(1, 0), x_new(1, 0), xdot_new(1, 0));
	for (int k = 0; k < 3; k++) {
		x_new(0, k + 1) = x(dt_new[k], u_new(0, k), x_new(0, k), xdot_new(0, k));
		x_new(1, k + 1) = x(dt_new[k], u_new(0, k), x_new(0, k), xdot_new(0, k));
		xdot_new(0, k + 1) = xdot(dt_new[k], u_new(0, k), x_new(0, k), xdot_new(0, k));
		xdot_new(1, k + 1) = xdot(dt_new[k], u_new(1, k), x_new(1, k), xdot_new(1, k));
		xddot_new(0, k + 1) = xddot(dt_new[k], u_new(0, k), x_new(0, k), xdot_new(0, k));
		xddot_new(1, k + 1) = xddot(dt_new[k], u_new(1, k), x_new(1, k), xdot_new(1, k));
	}
	// calculate gradients at step k
	Vector3d grad_t_new = dJ_dt(dt_new, u_new, x_new, xdot_new, xddot_new);
	Vector2d grad_ux_new = dJ_dux(dt_new, u_new, x_new, xdot_new, xddot_new);
	Vector2d grad_uy_new = dJ_duy(dt_new, u_new, x_new, xdot_new, xddot_new);
	VectorXd grad_k_new(7);
	grad_k_new << grad_t_new, grad_ux_new, grad_uy_new;

	VectorXd y_k(7);
	y_k << grad_k_new - grad_k;
//    cout << "y_k" << y_k << endl;
	MatrixXd B_k_new(7,7);
	B_k_new<< B_k + y_k * y_k.transpose() / (y_k.transpose() * delta_x_k) - B_k * delta_x_k * (B_k * delta_x_k).transpose() / (delta_x_k.transpose() * B_k * delta_x_k);
	MatrixXd H_k_new(7,7);
	H_k_new << (I - delta_x_k * y_k.transpose() / (y_k.transpose() * delta_x_k)) * H_k * (I - y_k * delta_x_k.transpose() / (y_k.transpose() * delta_x_k)) + delta_x_k * delta_x_k.transpose() / (y_k.transpose() * delta_x_k);

	bfgs result;
	result.x_k = x_k_new;
	result.delta_x_k = delta_x_k;
	result.y_k = y_k;
	result.B_k = B_k_new;
	result.H_k = H_k_new;
	return result;
}

// Callback function for receiving the planner outputs
void footstepPlanCallbackRK4(const std_msgs::Float64MultiArray::ConstPtr& msg)
{
    u_s(0,1) = msg->data[0];
    u_s(1,1) = msg->data[1];
    u_s(0,2) = msg->data[2];
    u_s(1,2) = msg->data[3];
    dt_s(0) = msg->data[4];
    dt_s(1) = msg->data[5];
    dt_s(2) = msg->data[6];
    support_foot_flag = msg->data[7];
//    flag = 1;
    cout << "--------------------------------" << endl;
    cout<< "received u \n" << u_s << endl;
    cout<< "received t \n" << dt_s << endl;

	solution.x_k.segment(0, 3) = dt_s.transpose();
	solution.x_k.segment(3, 2) = u_s.block(0, 1, 1, 2).transpose();
	solution.x_k.segment(5, 2) = u_s.block(1, 1, 1, 2).transpose();
	solution.delta_x_k = VectorXd::Zero(7);
	solution.y_k = VectorXd::Zero(7);
	solution.B_k = Matrix<double, 7, 7>::Identity();
	solution.H_k = Matrix<double, 7, 7>::Identity();
	rk4_received = true;
}

void plannerInputCallback(const std_msgs::Float64MultiArray::ConstPtr& msg)
{
//    x, x_dot, y, y_dot = msg.data[:4]
    x_0(0) = msg->data[0];
    x_dot_0(0) = msg->data[1];
    x_dot_dot_0(0) = msg->data[2];
    x_0(1) = msg->data[3];
    x_dot_0(1) = msg->data[4];
    x_dot_dot_0(1) = msg->data[5];
    left_x  = msg->data[6];
    left_y  = msg->data[7];
    right_x = msg->data[8];
    right_y = msg->data[9];
    foot_flag = msg->data[10];
    current_time = msg->data[11];
}

void weightCallback(const std_msgs::Float64MultiArray::ConstPtr& msg)
{
    w[0] = msg->data[0];
    w[1] = msg->data[1];
    w[2] = msg->data[2];
    w[3] = msg->data[3];
    w[4] = msg->data[4];
    w[5] = msg->data[5];
    w[6] = msg->data[6];
    w[7] = msg->data[7];
    w[8] = msg->data[8];
    w[9] = msg->data[9];
    w[10] = msg->data[10];
    w[11] = msg->data[11];
}

int main(int argc, char ** argv){
    Vector4d x_opt,y_opt;
    const double LOOP_RATE = 100;
    int iter_main = 0;
    ros::init(argc, argv, "gradient_descent_footPlanner_node");
    ros::NodeHandle n;
    ros::Rate loop_rate(LOOP_RATE);
    ros::Publisher planner_output_pub = n.advertise<std_msgs::Float64MultiArray>("/time_slider_gazebo/footstep_plan", 1);
    ros::Subscriber sub_planner_input = n.subscribe<std_msgs::Float64MultiArray>("/time_slider_gazebo/planner_input", 'None', plannerInputCallback);
    ros::Subscriber sub_RK4_planner_input = n.subscribe<std_msgs::Float64MultiArray>("/time_slider_gazebo/footstep_plan_rk4", 'None', footstepPlanCallbackRK4);
    ros::Subscriber sub_weight        = n.subscribe<std_msgs::Float64MultiArray>("/time_slider_gazebo/cost_weight", 'None', weightCallback);
    support_foot_flag = foot_flag;

    solution.x_k << 0.4, 0.4, 0.4, 0.0, 0.0, -0.15, 0.15;

    while (ros::ok()){

        if(rk4_received){
//            for(int i =0; i<2; i++){
            auto t1 = std::chrono::high_resolution_clock::now();
            // for (int i=0; i <max_iter; i+=2){
            cout << "u ini is \t" << u_s << endl;
            cout << "t ini is \t" << dt_s << endl;
            cout << "current robot COM pos is \t" << x_0 << endl;
            cout << "current robot COM vel is \t" << x_dot_0 << endl;
            ////////////// Gradient Descent ////////////////////////////////
            if (foot_flag == -1){
                u_s(0,0) = left_x;
                u_s(1,0) = left_y;
            }
            else if (foot_flag == 1){
                u_s(0,0) = right_x;
                u_s(1,0) = right_y;
            }
            bfgs prev_solution = solution;
            solution = BFGS(prev_solution);
            prev_solution = solution;
            solution = BFGS(prev_solution);
            prev_solution = solution;
            solution = BFGS(prev_solution);
            prev_solution = solution;
            solution = BFGS(prev_solution);
            prev_solution = solution;
            solution = BFGS(prev_solution);

            dt_s = solution.x_k.segment(0, 3);				// get dts
            u_s.block(0, 1, 1, 2) = solution.x_k.segment(3, 2).transpose();
            u_s.block(1, 1, 1, 2) = solution.x_k.segment(5, 2).transpose();
            // }
            auto t2 = chrono::high_resolution_clock::now();
            auto duration = chrono::duration_cast<chrono::microseconds>( t2 - t1 ).count();
            cout << "Duration: \n" << duration << " microseconds" << endl;
//            }target': -55.82480402640354, 'params': {'w1': 0.1, 'w10': 100.0, 'w11': 100.0, 'w12': 100.0, 'w2': 0.1, 'w3': 0.1, 'w4': 100.0, 'w5': 100.0, 'w6': 100.0, 'w7': 0.1, 'w8': 0.1, 'w9': 100.0}}

            // publish
            if(support_foot_flag == foot_flag){ // && dt_s(0) >= 1.0/LOOP_RATE
                std_msgs::Float64MultiArray footstep_plan;
                footstep_plan.data.push_back(u_s(0,1));
                footstep_plan.data.push_back(u_s(1,1));
                footstep_plan.data.push_back(u_s(0,2));
                footstep_plan.data.push_back(u_s(1,2));
                footstep_plan.data.push_back(dt_s(0));
                footstep_plan.data.push_back(dt_s(1));
                footstep_plan.data.push_back(dt_s(2));
                footstep_plan.data.push_back(support_foot_flag);
                planner_output_pub.publish(footstep_plan);
//                if(flag==0){
////                    dt_s(0) -= 1/LOOP_RATE;
//                }
//                else if (flag==1){flag = 0;}
            } else {
                cout << "Not publishing -------------------" << endl;
            }
            if(support_foot_flag != foot_flag){
//                dt_s(0) = dt_s(1);
//                dt_s(1) = dt_s(2);
//                u_s(0,0) = u_s(0,1);
//                u_s(1,0) = u_s(1,1);
//                u_s(0,1) = u_s(0,2);
//                u_s(1,1) = u_s(1,2);
//                u_s(0, 2) = u_s(0,2) + (u_s(0,2) - solution.x_k(3));
//                u_s(1, 2) = u_s(1,2) + (solution.x_k(5) - u_s(1, 2));
//                current_time = 0.0;
//                support_foot_flag = foot_flag;
                cout << "switching !!!!!!!!!!!!!!!!!!!!!!!!!!1!!!!!!!!!!!!" << endl;
            }
            // assign prev optimal as initial guess of next iteration

            iter_main += 1;
            cout << "iteration in main loop is \t" << iter_main << endl;
            cout << "u opt is \t" << u_s << endl;
            cout << "t opt is \t" << dt_s << endl;

            rk4_received = false;
    }

        ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;}

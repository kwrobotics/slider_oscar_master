#!/usr/bin/env python
"""
foot_step_planner.py
takes reference trajectory and current state, produces footstep positions
and times for next N steps.

Digby Chappell and Ke Wang
April 2020
"""

import time
import rospy
import numpy as np
from numpy import linalg as la
import sys
np.set_printoptions(threshold=sys.maxsize)
from casadi import *
from footstep_planner import SLIDER
import matplotlib.pyplot as plt
from std_msgs.msg import Float64MultiArray, MultiArrayDimension, Float64

g = 9.81    # gravity
#
# const = [m, m_s, L, h]
#
# T_end = 100
# dt = 0.001
# vmax = 3             # m/s
# x_dot_ref = [0.4, 0.0]     # m/s
# weight_x = 1.0
# weight_y = 10.0
# weight_penalty_swing = 2
# weight_penalty_clearance = 2
# # Parameters subscribed from the slider_controller node #######################################
x = 0.0
x_dot = 0.0
x_dotdot = 0.0
y = 0.0
y_dot = 0.0
y_dotdot = 0.0
left_x = 0.00
left_y = 0.21
right_x = 0.0
right_y = -0.21
foot_flag = 1
previous_foot_flag = -1
current_time = 0.01
# foot_radius = 0.20
# foot_clearance_y = 0.25
# ###############################################################################################

# v_ref = np.array([0.2, 0.0])

# def planner_callback(msg):
#     global x, x_dot, x_dotdot, y, y_dot, y_dotdot, left_x, left_y, right_x, right_y, foot_flag, current_time
#     x, x_dot, x_dotdot, y, y_dot, y_dotdot = msg.data[:6]
#     left_x, left_y = msg.data[6:8]
#     right_x, right_y = msg.data[8:10]
#     foot_flag = msg.data[10]
#     current_time = msg.data[11]
    # print('x', x, x_dot, x_dotdot)
    # print('y', y, y_dot, y_dotdot)

def isPD(B):
    """Returns true when input is positive-definite, via Cholesky"""
    try:
        _ = la.cholesky(B)
        return True
    except la.LinAlgError:
        return False

def nearestPD(A):
    """Find the nearest positive-definite matrix to input

    A Python/Numpy port of John D'Errico's `nearestSPD` MATLAB code [1], which
    credits [2].

    [1] https://www.mathworks.com/matlabcentral/fileexchange/42885-nearestspd

    [2] N.J. Higham, "Computing a nearest symmetric positive semidefinite
    matrix" (1988): https://doi.org/10.1016/0024-3795(88)90223-6
    """

    B = (A + A.T) / 2
    _, s, V = la.svd(B)

    H = np.dot(V.T, np.dot(np.diag(s), V))

    A2 = (B + H) / 2

    A3 = (A2 + A2.T) / 2

    if isPD(A3):
        return A3

    spacing = np.spacing(la.norm(A))

    I = np.eye(A.shape[0])
    k = 1
    while not isPD(A3):
        mineig = np.min(np.real(la.eigvals(A3)))
        A3 += I * (-mineig * k**2 + spacing)
        k += 1

    return A3

if __name__=="__main__":
    rospy.init_node('footstep_planner', anonymous=True)
    pub = rospy.Publisher('/time_slider_gazebo/footstep_plan_rk4', Float64MultiArray, queue_size=1)
    pub_state = rospy.Publisher('/time_slider_gazebo/planner_input', Float64MultiArray, queue_size=1)
    # planner_input = rospy.Subscriber('/time_slider_gazebo/planner_input', Float64MultiArray, planner_callback)
    pub_weight = rospy.Publisher('/time_slider_gazebo/cost_weight', Float64MultiArray, queue_size=1)
    pub_Jacobian = rospy.Publisher('/time_slider_gazebo/jacobian', Float64MultiArray, queue_size=1)
    pub_HessianInv = rospy.Publisher('/time_slider_gazebo/hessianInv', Float64MultiArray, queue_size=1)
    v_ref = np.array([0.05, 0.0])
    slider = SLIDER(mass=11.5, height=0.7, leg_length=1.2, foot_radius=0.25)
    slider.initialise(model=slider.full_dynamics,
                      cost=slider.step_velocity_cost_function,
                      footsteps=3, intervals=6)
    Q = np.array([[1, 0],
                  [0, 1]])
    slider.set_weightings(Q)
    w = Float64MultiArray()
    J = Float64MultiArray()
    Input = Float64MultiArray()
    Hessian_msg = Float64MultiArray()
    # J.layout.dim.append(MultiArrayDimension())
    # J.layout.dim.append(MultiArrayDimension())
    # J.layout.dim[0].label = "constraint"
    # J.layout.dim[1].label = "state"
    # J.layout.dim[0].size = 30
    # J.layout.dim[1].size = 16
    # J.layout.dim[0].stride = 30*16
    # J.layout.dim[1].stride = 16
    # J.layout.data_offset = 0
    r = rospy.Rate(25)

    # initial values for optimal footsteps
    dT_opt = np.array([0.4, 0.4, 0.4])
    U_opt = np.array([[left_x, right_x, left_x],
                      [left_y, right_y, left_y]])
    # initial values for solver
    dT_i = dT_opt
    U_i = U_opt
    while not rospy.is_shutdown():
        X0 = np.array([x, y, x_dot, y_dot])
        dX0 = np.array([x_dot, y_dot, x_dotdot, y_dotdot])

        print('-------------------------')
        print('x0', X0)
        print('dx0', dX0)
        print('-------------------------')

        D0 = np.array([X0[0] + dX0[0]/slider.omega,
                       X0[1] + dX0[1]/slider.omega,
                       X0[2] + dX0[2]/slider.omega,
                       X0[3] + dX0[3]/slider.omega])
        dt = current_time
        if foot_flag == -1:
            Xs0 = [right_x, right_y]
            U0 = [left_x, left_y]
        elif foot_flag == 1:
            Xs0 = [left_x, left_y]
            U0 = [right_x, right_y]
        dxN = v_ref
        a_ref = (dxN - X0[2:])/1.2
        dcm_v_ref = v_ref + a_ref/slider.omega

        slider.set_parameters(X0, D0, dt, U0, Xs0, foot_flag, v_ref) # DCM: dcm_v_ref, RK4/FULL: v_ref

        #     x, x_dot, x_dotdot, y, y_dot, y_dotdot = msg.data[:6]
        #     left_x, left_y = msg.data[6:8]
        #     right_x, right_y = msg.data[8:10]
        #     foot_flag = msg.data[10]
        #     current_time = msg.data[11]

        # slider.set_initial_solution(slider.dT_opt, slider.U_opt)
        # slider.solve()

        # update initial values for next solver iteration
        if foot_flag != previous_foot_flag or (dT_opt[1] - dT_opt[0]) < 5e-2:
            dT_i[:-1] = dT_opt[1:]
            dT_i[-1] = dT_opt[-1]

            U_i[:-1] = U_opt[1:]
            U_i[-1] = U_opt[-1] + (U_opt[-2] - U_opt[-1])
            previous_foot_flag = foot_flag
            pass
        else:
            dT_i = dT_opt - slider.solve_time * 0.7
            U_i = U_opt
        try:
            slider.set_initial_solution(dT_i, U_i)
            slider.solve()
            U_opt = slider.U_opt
            dT_opt = slider.dT_opt
            new_step = Float64MultiArray()
            new_step.layout.dim = [MultiArrayDimension('', 8, 1)]
            u_x_1 = U_opt[0, 1]
            u_y_1 = U_opt[1, 1]
            u_x_2 = U_opt[0, 2]
            u_y_2 = U_opt[1, 2]
            print('optimal U', U_opt)
            print('optimal T', dT_opt)
            print('optimal dual weight', slider.W_opt)
            # slider.opti.
            g1 = slider.opti.g[9]
            # dt1 = slider.dT[0]
            Jacobian2 = slider.sol.value(jacobian(slider.opti.g, slider.C))
            Jacobian2 = Jacobian2.todense()
            # J_part2 = Jacobian2[12:,:]
            # J_part2 = Jacobian2
            # J_part2 = J_part2.todense()
            # slider.opti.lam_g
            g_part = slider.opti.g[:30]
            lam_g_part = slider.opti.lam_g[:30]
            x_part = slider.opti.x[:16]
            # Jacobian = slider.sol.value(gradient(gradient(slider.opti.f + dot(lam_g_part, g_part), slider.C)[15], x_part))
            Jacobian = slider.sol.value(gradient(lam_g_part[20], x_part))
            # Jacobian = Jacobian.todense()
            x_val = slider.sol.value(slider.opti.x)
            print(slider.sol.value(slider.opti.lam_g, [slider.X[0,1] == x_val[4] + 0.01]))
            g_val = slider.sol.value(slider.opti.lam_g)

            # J_part = Jacobian[18:, :-9]
            # J_part = J_part.todense()
            # J_part = np.round(J_part, decimals=2)
            # J_part = J_part.flatten()
            w.data = g_val
            # J.data = J_part.tolist()

            Hessian = slider.sol.value(hessian(slider.opti.f + dot(slider.opti.lam_g, slider.opti.g), slider.opti.x)[0])  # Python
            Hessian = Hessian.todense()
            Hessian = Hessian[-7:, -7:]
            Hessian_inv = np.linalg.inv(Hessian)
            # Hessian_inv = nearestPD(Hessian_inv)
            # print('positive definite? ', np.all(np.linalg.eigvals(Hessian_inv) > 0))
            # assert (isPD(B))
            Hessian_inv = Hessian_inv.flatten().astype(np.float).tolist()
            Hessian_msg.data = Hessian_inv[0]

            Input.data = [x, x_dot+0.01, x_dotdot, y, y_dot, y_dotdot, left_x, left_y, right_x, right_y, foot_flag,
                          current_time]

            # xx = SX.sym('xx', 1)
            # y = sinh(xx)
            # JJ = jacobian(y,xx)

            pub_weight.publish(w)
            pub_Jacobian.publish(J)
            pub_state.publish(Input)
            pub_HessianInv.publish(Hessian_msg)
            if dT_opt[0] > 1/250:
                new_step.data = [u_x_1, u_y_1, u_x_2, u_y_2, dT_opt[0], dT_opt[1], dT_opt[2], foot_flag]
                pub.publish(new_step)
            else:
                dT_opt = np.array([0.4, 0.4, 0.4])
                U_opt = np.array([[left_x, right_x, left_x],
                                  [left_y, right_y, left_y]])
                # initial values for solver
                dT_i = dT_opt
                U_i = U_opt
        except Exception as e:
            print('could not solve ')
            print(e)
            # initial values for optimal footsteps
            dT_opt = np.array([0.4, 0.4, 0.4])
            U_opt = np.array([[left_x, right_x, left_x],
                              [left_y, right_y, left_y]])
            # initial values for solver
            dT_i = dT_opt
            U_i = U_opt
        r.sleep()
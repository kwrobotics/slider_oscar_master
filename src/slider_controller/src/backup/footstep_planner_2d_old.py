#!/usr/bin/env python
"""
foot_step_planner.py
takes reference trajectory and current state, produces footstep positions
and times for next N steps.

Digby Chappell and Ke Wang
April 2020
"""

import time
import rospy
import numpy as np
from casadi import *
import matplotlib.pyplot as plt
from std_msgs.msg import Float64MultiArray, MultiArrayDimension, Float64

m = 11.5    # body mass
m_s = 0.3   # swing leg mass
L = 1.2     # leg length
h = 0.59     # body height
g = 9.81    # gravity

const = [m, m_s, L, h]

T_end = 100
dt = 0.001
vmax = 3.0       # m/s
x_dot_ref = [0.1, 0.0]     # m/s
weight_x = 1.0
weight_y = 1.0
weight_penalty_swing = 2
weight_penalty_clearance = 2
# Parameters subscribed from the slider_controller node #######################################
x = 0.0
x_dot = 0.0
y = 0.0
y_dot = 0.0
left_x = 0.00
left_y = 0.21
right_x = 0.0
right_y = -0.21
foot_flag = -1
current_time = 0.0
foot_radius = 0.20
foot_clearance_y = 0.25
###############################################################################################
def planner_callback(msg):
    global x, x_dot, y, y_dot, left_x, left_y, right_x, right_y, foot_flag, current_time
    # print('callback foot flag', foot_flag)
    x, x_dot, y, y_dot = msg.data[:4]
    left_x, left_y = msg.data[4:6]
    right_x, right_y = msg.data[6:8]
    foot_flag = msg.data[8]
    current_time = msg.data[9]

def velocity_callback(msg):
    global x_dot_ref
    x_dot_ref[0] = msg.data[0]
    x_dot_ref[1] = msg.data[1]

def main():
    global x, x_dot, y, y_dot, left_x, left_y, right_x, right_y, foot_flag, current_time, x_dot_ref
    rospy.init_node('footstep_planner', anonymous=True)
    pub = rospy.Publisher('/time_slider_gazebo/footstep_plan_rk4', Float64MultiArray, queue_size=1)
    planner_input = rospy.Subscriber('/time_slider_gazebo/planner_input', Float64MultiArray, planner_callback)
    velocity_input = rospy.Subscriber('/time_slider_gazebo/reference_velocity', Float64MultiArray, velocity_callback)

    i = 0
    N = 3
    rate = rospy.Rate(25)
    t_old = time.time()
    remaining_time = 0.4   # s
    support_foot = -1 # left: -1 right: 1
    u_x = 0
    u_y = 0
    step_time = 0
    toc = 0.0
    pre_U = np.zeros((2,3), dtype=float)
    pre_T = np.linspace(0.0, N*0.4, N + 1)
    # opti, parameters, variables = initialise(x_dot_ref)
    intermediate_steps = 1
    opti, parameters, variables = initialise(intermediate_steps) # x_dot_ref,
    avg_toc = 0
    k = 0
    pre_support_foot = support_foot
    now = rospy.Time.now()
    while not rospy.is_shutdown():
        # t_now = time.time() - t_old
        # t_b = time.time()
        # t_a = time.time()
        current_state = (x, y, x_dot, y_dot, left_x, left_y, right_x, right_y)
        support_foot = foot_flag
        # print('The foot flag is  ', foot_flag)
        if support_foot == 2:
            support_foot = -1
        try:
            # print('current state', current_state)
            # print('current time', current_time)
            tic = time.time()
            # print('current time is ', tic)
            t_opt, u_opt, x_opt, y_opt, x_dot_opt, y_dot_opt, plan_foot = \
                plan(opti, variables, parameters, current_state, remaining_time, toc, support_foot, current_time, pre_support_foot, pre_T, pre_U) # , U_pre, t_pre
            toc = time.time() - tic
            pre_support_foot = support_foot
            pre_U = u_opt
            pre_T = t_opt
            step_time = max(t_opt[1] - t_opt[0]-toc, 0)  # optimal step time
            print('planned foot', plan_foot, 'support foot', support_foot)
            if step_time < 3e-2 or plan_foot != support_foot: # if 25 HZ
                pre_U[:, :-1] = u_opt[:, 1:]
                # pre_U[0, 3] = u_opt[0, 3] + x_dot_ref[0] * step_time
                pre_U[0, 3] = u_opt[0, 3] + u_opt[0, 3] - u_opt[0, 2]
                # pre_U[1, 3] = u_opt[1, 3] + foot_flag * (-1) * x_dot_ref[1] * step_time
                pre_U[1, 3] = u_opt[1, 3] - u_opt[1, 3] + u_opt[1, 2]
                pass
            else:
                i += 1
                avg_toc = (avg_toc * (i - 1) + toc) / i
                # print('optimal step time \t', step_time)
                # print('optimal COM x', x_opt[:])
                # print('optimal COM y', y_opt[:])
                print('optimal x location', u_opt[0])
                print('optimal y location', u_opt[1])
                # print('optimal time', t_opt)
                # print('solve time', toc)
                # print('reference velocity is ', x_dot_ref)
                # print('avg solve time \t', avg_toc)

                new_step = Float64MultiArray()
                new_step.layout.dim = [MultiArrayDimension('', 8, 1)]
                u_x_1 = u_opt[0, 1]
                u_y_1 = u_opt[1, 1]
                u_x_2 = u_opt[0, 2]
                u_y_2 = u_opt[1, 2]
                t_2 = t_opt[2]
                t_3 = t_opt[3]
                new_step.data = [u_x_1, u_y_1, u_x_2, u_y_2, step_time, t_2, t_3, support_foot]
                # print('optimal x location 1 \t', u_x_1)
                # print('optimal y location 1 \t', u_y_1)
                # print('optimal x location 2 \t', u_x_2)
                # print('optimal y location 2 \t', u_y_2)
                k += 1
                # print('The current iteration is ', k)
                pub.publish(new_step)
        except Exception as e:
            print(e)

        rate.sleep()


def plan(opti, variables, parameters, current_state, t_rem, t_sol, support_foot, current_time, pre_support_foot, pre_T, pre_U, N=3): # , U_pre, t_pre
    global  x_dot_ref
    x_0, y_0, x_dot_0, y_dot_0, left_x, left_y, right_x, right_y = current_state
    X_0, Y_0, X_dot_0, Y_dot_0, Ux_0, Uy_0, Xx_s0, Xy_s0, remaining_time, solve_time, s_foot, cur_time, X_dot_ref, Y_dot_ref = parameters
    U, X, t = variables
    if support_foot == -1:
        X_s0 = [right_x, right_y]
        U_0 = [left_x, left_y]
    elif support_foot == 1:
        X_s0 = [left_x, left_y]
        U_0 = [right_x, right_y]

    opti.set_value(X_0, x_0)
    opti.set_value(Y_0, y_0)
    opti.set_value(X_dot_0, x_dot_0)
    opti.set_value(Y_dot_0, y_dot_0)
    opti.set_value(Ux_0, U_0[0])
    opti.set_value(Uy_0, U_0[1])
    opti.set_value(Xx_s0, X_s0[0])
    opti.set_value(Xy_s0, X_s0[1])
    opti.set_value(X_dot_ref, x_dot_ref[0])
    opti.set_value(Y_dot_ref, x_dot_ref[1])
    opti.set_value(remaining_time, t_rem)
    opti.set_value(solve_time, t_sol)
    opti.set_value(s_foot, support_foot)
    opti.set_value(cur_time, current_time)

    # ---- initial values for solver ---
    # opti.set_initial(t, np.linspace(0.0, N * 0.7, N + 1))  # initial guess at one step per second
    # if pre_support_foot == support_foot:
        # opti.set_initial(t[2:], pre_T[2:])                         # initial guess at one step per second
    opti.set_initial(t[0], pre_T[0])                         # initial guess at one step per second
    opti.set_initial(t[1:], pre_T[1:] - 0.04)
    opti.set_initial(U, pre_U)
    # if pre_support_foot != support_foot:
    #     opti.set_initial(t[0], pre_T[0])  # initial guess at one step per second
    #     opti.set_initial(t[1:3], pre_T[2:4] - 0.02)
    #     opti.set_initial(t[3], pre_T[3])
    #     opti.set_initial(U[:-1], pre_U[1:])

    # opti.set_initial(U[0, 0], U_0[0])     U_0   # initial guess at steps directly on reference
    # opti.set_initial(U[1, 0], U_0[1])        # initial guess at steps directly on reference
    # opti.set_initial(U[0, 1:], U_0[0] + x_dot_ref[0] * np.linspace(0.0, N, N - 1))        # initial guess at steps directly on reference
    # opti.set_initial(U[1, 1:], U_0[1] + x_dot_ref[1] * np.linspace(0.0, N, N - 1))        # initial guess at steps directly on reference
    # opti.set_initial(X[0, :], x_0 + x_dot_ref[0] * np.linspace(0.0, N, N + 1))    # initial guess at body directly on reference
    # opti.set_initial(X[1, :], y_0 + x_dot_ref[1] * np.linspace(0.0, N, N + 1))    # initial guess at body directly on reference
    # opti.set_initial(X[2, :], x_dot_ref[0] * np.ones(N + 1))
    # opti.set_initial(X[3, :], x_dot_ref[1] * np.ones(N + 1))

    sol = opti.solve()

    t_opt = sol.value(t)
    u_opt = sol.value(U)
    x_opt, y_opt, x_dot_opt, y_dot_opt = sol.value(X)
    return t_opt, u_opt, x_opt, y_opt, x_dot_opt, y_dot_opt, support_foot

def full_dynamic_model(X_k, U_k, dt, constants=None):
    if constants is None:
        constants = const
    m, m_s, L, h = constants

    x_k = X_k[0]
    y_k = X_k[1]
    x_dot_k = X_k[2]
    y_dot_k = X_k[3]

    u_x_k = U_k[0]
    u_y_k = U_k[1]

    omega = sqrt(g/h)

    x_k_1 = u_x_k + (x_k - u_x_k) * cosh(omega * dt) + (x_dot_k / omega) * sinh(omega * dt)
    y_k_1 = u_y_k + (y_k - u_y_k) * cosh(omega * dt) + (y_dot_k / omega) * sinh(omega * dt)
    x_dot_k_1 = x_dot_k * cosh(omega * dt) + (x_k - u_x_k) * omega * sinh(omega * dt)
    y_dot_k_1 = y_dot_k * cosh(omega * dt) + (y_k - u_y_k) * omega * sinh(omega * dt)
    X_k_1 = vertcat(x_k_1, y_k_1, x_dot_k_1, y_dot_k_1)
    return X_k_1

def runge_kutta_model(X_k, U_k, dt, segments=5, constants=None):
    if constants is None:
        constants = const
    m, m_s, L, h = constants

    f = lambda x, u: vertcat(x[2], x[3], (x[0] - u[0]) * g / h, (x[1] - u[1]) * g / h)  # dx/dt = f(x,u)
    X_k_1 = X_k
    dt_ = dt / segments
    for i in range(segments):
        # Runge-Kutta 4 integration
        k1 = f(X_k, U_k)
        k2 = f(X_k + dt_ / 2 * k1, U_k)
        k3 = f(X_k + dt_ / 2 * k2, U_k)
        k4 = f(X_k + dt_ * k3, U_k)
        X_k_1 = X_k + dt_ / 6 * (k1 + 2 * k2 + 2 * k3 + k4)
        X_k = X_k_1
    return X_k

# def initialise(x_dot_ref, N_steps=3, constants=None):
def initialise(intermediate_steps=5, N_steps=3, constants=None): # x_dot_ref,
    if constants is None:
        constants = const
    N = N_steps
    m, m_s, L, h = constants
    # L_max = np.sqrt(2) * 0.6

    opti = Opti()  # Optimization problem

    # ---- decision variables ---------
    # X = opti.variable(4, N + 1)         # state trajectory
    X = opti.variable(4, N * intermediate_steps + 1)  # state trajectory
    x_pos = X[0, :]                     # x position variable
    y_pos = X[1, :]                     # y position variable
    x_vel = X[2, :]                     # x velocity variable
    y_vel = X[3, :]                     # y velocity variable
    U = opti.variable(2, N)             # control trajectory
    t = opti.variable(1, N + 1)         # step time points

    # ---- parameters -----------------
    X_0 = opti.parameter()
    Y_0 = opti.parameter()
    X_dot_0 = opti.parameter()
    Y_dot_0 = opti.parameter()
    Ux_0 = opti.parameter()
    Uy_0 = opti.parameter()
    Xx_s0 = opti.parameter()
    Xy_s0 = opti.parameter()
    X_dot_ref = opti.parameter()
    Y_dot_ref = opti.parameter()
    remaining_time = opti.parameter()
    solve_time = opti.parameter()
    current_time = opti.parameter()
    s_foot = opti.parameter()

    U_0 = vertcat(Ux_0, Uy_0)
    X_s0 = vertcat(Xx_s0, Xy_s0)
    # ---- dynamic constraints --------
    # dts = t[1:] - t[:-1]
    # segments = 5
    # for k in range(N):  # loop over control intervals
    #     x_current = X[:, k]
    #     u_current = U[:, k]
    #     dt = t[k + 1] - t[k]
    #     # x_next = runge_kutta_model(x_current, u_current, dt, segments) # I don't understand it
    #     x_next = full_dynamic_model(x_current, u_current, dt)
    #     # Constraints
    #     opti.subject_to(X[:, k + 1] == x_next)

    # ---- dynamic equation --------
    i = 0
    for k in range(N):  # loop over control intervals
        u_current = U[:, k]
        for j in range(intermediate_steps):
            x_current = X[:, i]
            dt = (t[k + 1] - t[k]) / intermediate_steps
            # x_next = full_dynamic_model(x_current, u_current, dt)
            x_next = runge_kutta_model(x_current, u_current, dt, 6)
            # Constraints
            opti.subject_to(X[:, i + 1] == x_next)
            # X[:, i + 1] = x_next
            i += 1

    for k in range(1, N):
        opti.subject_to(t[k + 1] - t[k] >= 0.2)
        opti.subject_to(t[k + 1] - t[k] <= 1.0)
        # opti.subject_to(t[k + 1] - t[k] == 0.25)
        # opti.subject_to(U[0, k] - U[0, k-1] >= 0.1)
        # opti.subject_to(s_foot*(U[1, k] - U[1, k-1]) >= 0.25)
        #     opti.subject_to(s_foot * (U[1, k] - U[1, k - 1]) <= -0.25)

        opti.subject_to(s_foot * (U[1, k] - U[1, k - 1]) >= 0.15)
        s_foot = s_foot * (-1)

        # opti.subject_to((U[0,2]-U[0, 0])**2 + (U[1,2]-U[1, 0])**2 >= (L ** 2 - h ** 2)*0.50)
        if k < N:
        #     # opti.subject_to(U[0, k+1] - U[0, k-1] <= np.sqrt(L ** 2 - h ** 2)*0.707) # strict constrain
        #     opti.subject_to(s_foot*(U[1, k+1] - U[1, k-1]) <= np.sqrt(L ** 2 - h ** 2)*0.707)  # strict constrain
        #     opti.subject_to((U[0, k+1] - U[0, k-1])**2 + (U[1, k+1] - U[1, k-1])**2 <= L ** 2 - h ** 2)
            opti.subject_to((U[0, k] - X[0, k]) ** 2 + (U[1, k] - X[1, k]) ** 2 <= (L ** 2 - h ** 2)*0.7)
            opti.subject_to((X[0, k + 1] - U[0, k]) ** 2 + (X[1, k + 1] - U[1, k]) ** 2 <= (L ** 2 - h ** 2)*0.7)
        #     opti.subject_to((U[0, k] - X[0, k])  + (U[1, k] - X[1, k])  <= np.sqrt(L ** 2 - h ** 2) * 0.707*0.8)
        #     opti.subject_to((X[0, k + 1] - U[0, k]) + (X[1, k + 1] - U[1, k]) <= np.sqrt(L ** 2 - h ** 2) * 0.707*0.8)
        # opti.subject_to(t[k + 1] - t[k] >= 0.2)

    # ---- objective          ---------
    J = weight_x*mtimes((X[2, :] - X_dot_ref), (X[2, :] - X_dot_ref).T) + weight_y*mtimes((X[3, :] - Y_dot_ref), (X[3, :] - Y_dot_ref).T)
        # + weight_penalty_clearance*foot_distance_penalty + weight_penalty_swing*swing_leg_distance_penalty
       # + mtimes((t[2:] - t[:-2] - 0.7), (t[2:] - t[:-2] - 0.7).T)# - 0.001 * mtimes(dts, dts.T)

    opti.minimize(J)

    opti.subject_to(X[:, 0] == vertcat(X_0, Y_0, X_dot_0, Y_dot_0))
    opti.subject_to(t[0] == 0.0) # Maybe you can use another way to do it, like deltaT
    opti.subject_to(U[:, 0] == U_0)

    # ---- time constraints -----------
    opti.subject_to(t[1] - t[0] <= remaining_time)
    # opti.subject_to(t[1] - t[0] >= 0.2)# + current_time
    opti.subject_to((U[0, 1] - Xx_s0) ** 2 + (U[1, 1] - Xy_s0) ** 2 <= (vmax * (t[1] - t[0])) ** 2)
    opti.subject_to(t[1] - t[0] + current_time >= 0.25)  # + current_time
    # opti.subject_to((U[0, 1] - Xx_s0) ** 2 + (U[1, 1] - Xy_s0) ** 2 <= (vmax * (t[1] - t[0])) ** 2)
    # opti.subject_to((U[0, 2] - U[0, 0]) ** 2 + (U[1, 2] - U[1, 0]) ** 2 <= (vmax * (t[2]-t[1]))**2)
    # opti.subject_to(t[1] - t[0] + current_time == 0.25)  # + current_time
    # print_options
    # ---- solve              ------
    options = {"ipopt.print_level": 0, "print_out": False, "print_in": False, "print_time": False, "ipopt.hessian_approximation":"limited-memory", "ipopt.warm_start_init_point": "yes", "ipopt.max_iter":1000} # "verbose": True,
    # options = {"ipopt.print_level": 0, "print_out": False, "print_in": False, "print_time": False} # "verbose": True, , "ipopt.hessian_approximation":"limited-memory"
    opti.solver("ipopt", options)
    # opti.solver('sqpmethod', struct('qpsol', 'qrqp'))# , options
    return opti, [X_0, Y_0, X_dot_0, Y_dot_0, Ux_0, Uy_0, Xx_s0, Xy_s0, remaining_time, solve_time, s_foot, current_time, X_dot_ref, Y_dot_ref], [U, X, t]

if __name__=="__main__":
    main()

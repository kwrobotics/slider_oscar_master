import numpy as np
import matplotlib.pyplot as plt

m = 15.0    # body mass
m_s = 0.3   # swing leg mass
L = 1.2     # leg length
h = 0.6     # body height
g = 9.81    # gravity

const = [m, m_s, L, h]


def full_dynamic_model(X_k, U_k, dt, constants=None):
    if constants is None:
        constants = const
    m, m_s, L, h = constants

    x_k = X_k[0]
    y_k = X_k[1]
    x_dot_k = X_k[2]
    y_dot_k = X_k[3]

    u_x_k = U_k[0]
    u_y_k = U_k[1]

    omega = np.sqrt(g/h)

    x_k_1 = u_x_k + (x_k - u_x_k) * np.cosh(omega * dt) + (x_dot_k / omega) * np.sinh(omega * dt)
    y_k_1 = u_y_k + (y_k - u_y_k) * np.cosh(omega * dt) + (y_dot_k / omega) * np.sinh(omega * dt)
    x_dot_k_1 = x_dot_k * np.cosh(omega * dt) + (x_k - u_x_k) * omega * np.sinh(omega * dt)
    y_dot_k_1 = y_dot_k * np.cosh(omega * dt) + (y_k - u_y_k) * omega * np.sinh(omega * dt)
    X_k_1 = np.array([x_k_1, y_k_1, x_dot_k_1, y_dot_k_1])
    return X_k_1


def runge_kutta_model(X_k, U_k, dt, segments=1, constants=None):
    if constants is None:
        constants = const
    m, m_s, L, h = constants

    f = lambda x, u: np.array([x[2], x[3], (x[0] - u[0]) * g / h, (x[1] - u[1]) * g / h])  # dx/dt = f(x,u)
    X_k_1 = X_k
    dt_ = dt / segments
    for i in range(segments):
        # Runge-Kutta 4 integration
        k1 = f(X_k, U_k)
        k2 = f(X_k + dt_ / 2 * k1, U_k)
        k3 = f(X_k + dt_ / 2 * k2, U_k)
        k4 = f(X_k + dt_ * k3, U_k)
        X_k_1 = X_k + dt_ / 6 * (k1 + 2 * k2 + 2 * k3 + k4)
        X_k = X_k_1
    return X_k_1


def heun_model(X_k, U_k, dt, segments=1, constants=None):
    if constants is None:
        constants = const
    m, m_s, L, h = constants

    f = lambda x, u: np.array([x[2], x[3], (x[0] - u[0]) * g / h, (x[1] - u[1]) * g / h])  # dx/dt = f(x,u)
    X_k_1 = X_k
    dt_ = dt / segments
    for i in range(segments):
        # Runge-Kutta 4 integration
        k1 = f(X_k, U_k)
        X_k_ = X_k + dt_ * k1
        k2 = f(X_k_, U_k)
        X_k_1 = X_k + dt_ / 2 * (k1 + k2)
        X_k = X_k_1
    return X_k_1


def euler_model(X_k, U_k, dt, segments=1, constants=None):
    if constants is None:
        constants = const
    m, m_s, L, h = constants

    f = lambda x, u: np.array([x[2], x[3], (x[0] - u[0]) * g / h, (x[1] - u[1]) * g / h])  # dx/dt = f(x,u)
    X_k_1 = X_k
    dt_ = dt / segments
    for i in range(segments):
        # Runge-Kutta 4 integration
        k1 = f(X_k, U_k)
        X_k_1 = X_k + dt_ * k1
        X_k = X_k_1
    return X_k_1


def main():
    X_k = np.array([0, 0, 0.1, 0.1])
    U_k = np.array([0.1, 0.1])
    dts = np.linspace(0, 0.5, 21)
    eulers = np.zeros((21, 9, 4))
    heuns = np.zeros((21, 9, 4))
    rk4s = np.zeros((21, 9, 4))
    err_eulers = np.zeros((21, 9, 4))
    err_heuns = np.zeros((21, 9, 4))
    err_rk4s = np.zeros((21, 9, 4))
    fulls = np.zeros((21, 4))
    d = 0
    for dt in dts:
        X_k_1 = full_dynamic_model(X_k, U_k, dt)
        for s in range(1, 10):
            X_k_euler = euler_model(X_k, U_k, dt, s)
            X_k_heun = heun_model(X_k, U_k, dt, s)
            X_k_rk4 = runge_kutta_model(X_k, U_k, dt, s)
            eulers[d, s-1, :] = X_k_euler
            heuns[d, s-1, :] = X_k_heun
            rk4s[d, s-1, :] = X_k_rk4

            err_eulers[d, s-1, :] = (X_k_1 - X_k_euler)/X_k_1
            err_heuns[d, s-1, :] = (X_k_1 - X_k_heun)/X_k_1
            err_rk4s[d, s-1, :] = (X_k_1 - X_k_rk4)/X_k_1
        fulls[d, :] = X_k_1
        d += 1

    plt.plot(dts, fulls[:, 0], label='full dynamic model')
    for j in range(1, 10):
        plt.plot(dts, heuns[:, j-1, 0], label='Heun model '+str(j)+' segments')
    # plt.plot(dts, heuns[:, 8, 0], label='Heun model')
    # plt.plot(dts, rk4s[:, 8, 0], label='RK4 model')
    plt.legend()
    plt.xlabel('t')
    plt.ylabel('x')
    plt.show()

    from mpl_toolkits.mplot3d import Axes3D
    fig = plt.figure()
    ax = Axes3D(fig)
    DT = np.tile(dts, (9, 1)).T
    SEG = np.tile(np.linspace(1, 9, 9), (21, 1))
    print(DT.shape, SEG.shape, err_eulers[:, :, 0].shape)
    ax.plot_surface(DT, SEG, err_eulers[:, :, 0], label='Euler')
    ax.plot_surface(DT, SEG, err_heuns[:, :, 0], label='Heun')
    ax.plot_surface(DT, SEG, err_rk4s[:, :, 0], label='RK4')
    # ax.legend()
    ax.set_title('Prediction Error')
    ax.set_xlabel('t')
    ax.set_ylabel('# of Segments')
    ax.set_zlabel('Prediction Error')
    plt.show()


if __name__=="__main__":
    main()
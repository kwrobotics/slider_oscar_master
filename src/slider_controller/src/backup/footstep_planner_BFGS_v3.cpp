#include <iostream>
#include <chrono>
#include <cmath>
#include <Eigen/Dense>

#include "ros/ros.h"
#include <std_msgs/Float64.h>
#include <std_msgs/Float64MultiArray.h>

using namespace Eigen;
using namespace std;

const double m = 11.5;    // body mass
const double m_s = 0.3;   // swing leg mass
const double L = 1.2;     // leg length
const double h = 0.7;     // body height
const double g = 9.81;    // gravity
const double omega = sqrt(g / h);  // omega

int flag = 0;
int support_foot_flag = 1;
Vector2d x_dot_ref(0.05, 0.00);     // m/s

// Parameters subscribed from the slider_controller node
Vector2d x_0(0.0, 0.0);
Vector2d x_dot_0(0.0, 0.0);
Vector2d x_dot_dot_0(0.0, 0.0);
Vector3d dt_s(0.2, 0.52170278, 0.43679115);
Matrix<double,2,3> u_s((Matrix<double,2,3>() << 0.0, -1.93340069e-02, 1.22170610e-01, -0.21, 2.41075380e-01,  -3.86024011e-02).finished());
// Eigen::Matrix3d A((Eigen::Matrix3d() << 1, 2, 3, 4, 5, 6, 7, 8, 9).finished());

double left_x = 0.0;
double left_y = 0.21;
double right_x = 0.0;
double right_y = -0.21;
int foot_flag = 1;
// penalty weights
const int n_weights = 12;
VectorXd w;

const int n_dynamic_weights = 12;
VectorXd dyn_w;
VectorXd w_update;
MatrixXd Jacobian;
MatrixXd HessianInv;

const double max_iter = 200;
int n_repeats = 1;
bool rk4_received = false;

// constraints
// line up with Python
const double lmax = 0.5*sqrt(pow(L, 2.0) - pow(h, 2.0));
const double rfoot = 0.25;
const double dt0_min = 0.2;
const double dt_min = 0.4;
const double dt_max = 1.2;

double current_time = 0.0;

double dxdot_dt(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s, int kx, int kt);
double dx_dt(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s, int kx, int kt);
double dxdot_du(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s, int kx, int ku);
double dx_du(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s, int kx, int ku);

struct bfgs {
	VectorXd x_k;		// parameters (dt, u_x, u_y, at step k)
	VectorXd delta_x_k;	// parameter update at step k
	VectorXd y_k;		// change in gradient at step k
	MatrixXd B_k;		// Hessian approximation at step k
	MatrixXd H_k;		// inverse Hessian approximation at step k
};

//VectorXd x_k_init(7);
//x_k_init[0] = 0.4;
////x_k_init  = VectorXd::Zero(7);
//
////x_k_init << 0.4, 0.4, 0.4, 0.0, 0.0, -0.15, 0.15;
////x_k_init(7) << 0.4, 0.4, 0.4, 0.0, 0.0, -0.15, 0.15;
////x_k_init << 0.4, 0.4, 0.4, 0.0, 0.0, -0.15, 0.15;

bfgs solution = {
	VectorXd::Zero(19),
	VectorXd::Zero(19),
	VectorXd::Zero(19),
	Matrix<double, 19, 19>::Identity(),
	Matrix<double, 19, 19>::Identity(),
};

double x(double dt, double u, double x_prev, double x_dot_prev){
	// double x_ = (x_prev - u + x_dot_prev / omega) * exp(omega * dt) + (x_prev - u - x_dot_prev / omega) * exp(-omega * dt) + u;
	double x_ = u + (x_prev - u) * cosh(omega * dt) + (x_dot_prev / omega) * sinh(omega * dt);
    return x_;
}

double xdot(double dt, double u, double x_prev, double x_dot_prev){
	// double xdot_ = omega * (x_prev - u + x_dot_prev / omega) * exp(omega * dt) - omega * (x_prev - u - x_dot_prev / omega) * exp(-omega * dt);
	double xdot_ = omega * (x_prev - u) * sinh(omega * dt) + x_dot_prev * cosh(omega * dt);
    return xdot_;
}

double xddot(double dt, double u, double x_prev, double x_dot_prev){
	// double xddot_ = pow(omega, 2) * (x_prev - u + x_dot_prev / omega) * exp(omega * dt) + pow(omega, 2) * (x_prev - u - x_dot_prev / omega) * exp(-omega * dt);
	double xddot_ = omega * omega * (x_prev - u) * cosh(omega * dt) + omega * x_dot_prev * sinh(omega * dt);
    return xddot_;
}

double dx_dt(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s, int kx=1, int kt=0){
    double dx_dt_ = 0.0;
	if (kx == kt + 1){
		dx_dt_ = xdot_s[kx];
	}
	return dx_dt_;
}

double dxdot_dt(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s, int kx=1, int kt=0){
	double dxdot_dt_ = 0.0;
	if (kx == kt + 1){
		dxdot_dt_ = xddot_s[kx];
	}
	return dxdot_dt_;
}

double dx_du(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s, int kx=1, int ku=1){
    double dx_du_ = 0.0;
	if (kx == ku + 1){
		// dx_du_ = -0.5 * (exp(omega * dt_s[kx-1]) + exp(-omega * dt_s[kx-1])) + 1;
		dx_du_= 1 - cosh(omega * dt_s[kx - 1]);
	}
	return dx_du_;
}

double dxdot_du(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s, int kx=1, int ku=1){
    double dxdot_du_ = 0.0;
	if (kx == ku + 1){
		// dxdot_du_ = -0.5 * omega * (exp(omega * dt_s[kx-1]) - exp(-omega * dt_s[kx-1]));
		dxdot_du_ = - omega * sinh(omega * dt_s[kx - 1]);
	}
	return dxdot_du_;
}

double dx_dx(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s, int kxa=1, int kxb=1){
    double dx_dx = 0.0;
	if (kxa == kxb + 1){
		// dx_du_ = -0.5 * (exp(omega * dt_s[kx-1]) + exp(-omega * dt_s[kx-1])) + 1;
		dx_dx = cosh(omega * dt_s[kxa - 1]);
	} else if (kxa == kxb) {
		dx_dx = 1;
	}
	return dx_dx;
}

double dx_dxdot(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s, int kxa=1, int kxb=1){
    double dx_dx = 0.0;
	if (kxa == kxb + 1){
		// dx_du_ = -0.5 * (exp(omega * dt_s[kx-1]) + exp(-omega * dt_s[kx-1])) + 1;
		dx_dx = (1 / omega) * sinh(omega * dt_s[kxa - 1]);
	}
	return dx_dx;
}

double dxdot_dx(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s, int kxa=1, int kxb=1){
    double dx_dx = 0.0;
	if (kxa == kxb + 1){
		// dx_du_ = -0.5 * (exp(omega * dt_s[kx-1]) + exp(-omega * dt_s[kx-1])) + 1;
		dx_dx = omega * sinh(omega * dt_s[kxa - 1]);
	}
	return dx_dx;
}

double dxdot_dxdot(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s, int kxa=1, int kxb=1){
    double dx_dx = 0.0;
	if (kxa == kxb + 1){
		// dx_du_ = -0.5 * (exp(omega * dt_s[kx-1]) + exp(-omega * dt_s[kx-1])) + 1;
		dx_dx = cosh(omega * dt_s[kxa - 1]);
	} else if (kxa == kxb) {
		dx_dx = 1;
	}
	return dx_dx;
}

Vector2d dJ_dux(Vector3d dt_s, Matrix<double,2, 3> u_s, Matrix<double,2, 4> x_s, Matrix<double,2, 4> xdot_s, Matrix<double, 2, 4> xddot_s){
	Vector3d u_x = u_s.row(0);
	Vector3d u_y = u_s.row(1);
	Vector4d x_x = x_s.row(0);
	Vector4d x_y = x_s.row(1);
	Vector4d xdot_x = xdot_s.row(0);
	Vector4d xdot_y = xdot_s.row(1);
	Vector4d xddot_x = xddot_s.row(0);
	Vector4d xddot_y = xddot_s.row(1);
	
	double px1u1 = w[6] * ((x_s.col(1) - u_s.col(1)).transpose() * (x_s.col(1) - u_s.col(1)) - lmax);
	double px2u2 = w[7] * ((x_s.col(2) - u_s.col(2)).transpose() * (x_s.col(2) - u_s.col(2)) - lmax);
	// 3
	double dpx1u1_dux1 = -2 * (x_s(0, 1) - u_s(0, 1)) * w[6];

	double dpx2u2_dux2 = -2 * (x_s(0, 2) - u_s(0, 2)) * w[7];

	double px2u1 = w[8] * ((x_s.col(2) - u_s.col(1)).transpose() * (x_s.col(2) - u_s.col(1)) - lmax);
	double px3u2 = w[9] * ((x_s.col(3) - u_s.col(2)).transpose() * (x_s.col(3) - u_s.col(2)) - lmax);
	// 3
//	double dpx2u1_dux1 = 2 * (dx_du(dt_s, u_x, x_x, xdot_x, xddot_x, 2, 1) - 1) * (x_s(0, 2) - u_s(0, 1)) * w[8];
    double dpx2u1_dux1 = -2 * (x_s(0, 2) - u_s(0, 1)) * w[8];
//	double dpx3u2_dux2 = 2 * (dx_du(dt_s, u_x, x_x, xdot_x, xddot_x, 3, 2) - 1) * (x_s(0, 3) - u_s(0, 2)) * w[9];
	double dpx3u2_dux2 = -2 * (x_s(0, 3) - u_s(0, 2)) * w[9];

//    cout<< "inEquality constraint in x" << endl;
//    cout << dpx1u1_dux1 << endl;
//    cout << dpx2u2_dux2 << endl;
//    cout << dpx2u1_dux1 << endl;
//    cout << dpx3u2_dux2 << endl;

	double dux1_inequality_penalties = dpx1u1_dux1 + dpx2u1_dux1;
	double dux2_inequality_penalties = dpx2u2_dux2 + dpx3u2_dux2;

    // dynamic constraint penalties
    // 4
    double dpx2x2_dux1 = -dyn_w[4] * dx_du(dt_s, u_x, x_x, xdot_x, xddot_x, 2, 1);
    double dpdx2dx2_dux1 = -dyn_w[6] * dxdot_du(dt_s, u_x, x_x, xdot_x, xddot_x, 2, 1);
    // 2
    double dpx3x3_dux2 = -dyn_w[8] * dx_du(dt_s, u_x, x_x, xdot_x, xddot_x, 3, 2);
    double dpdx3dx3_dux2 = -dyn_w[10] * dxdot_du(dt_s, u_x, x_x, xdot_x, xddot_x, 3, 2);

//    cout<< "dynamics x constraint " << endl;
//    cout << dpx2x2_dux1 << endl;
//    cout << dpdx2dx2_dux1 << endl;
//    cout << dpx3x3_dux2 << endl;
//    cout << dpdx3dx3_dux2 << endl;

    double dux1_dynamic_penalties = dpx2x2_dux1 + dpdx2dx2_dux1;
    double dux2_dynamic_penalties = dpx3x3_dux2 + dpdx3dx3_dux2;

    Vector2d dJ_dux_;
    dJ_dux_ << dux1_inequality_penalties + dux1_dynamic_penalties,
               dux2_inequality_penalties + dux2_dynamic_penalties;
    return dJ_dux_;
}


Vector2d dJ_duy(Vector3d dt_s, Matrix<double,2, 3> u_s, Matrix<double,2, 4> x_s, Matrix<double,2, 4> x_dot_s, Matrix<double,2, 4> xd_dot_s){
	Vector3d u_x = u_s.row(0);
	Vector3d u_y = u_s.row(1);
	Vector4d x_x = x_s.row(0);
	Vector4d x_y = x_s.row(1);
	Vector4d xdot_x = x_dot_s.row(0);
	Vector4d xdot_y = x_dot_s.row(1);
	Vector4d xddot_x = xd_dot_s.row(0);
	Vector4d xddot_y = xd_dot_s.row(1);
	
	double px1u1 = w[6] * ((x_s.col(1) - u_s.col(1)).transpose() * (x_s.col(1) - u_s.col(1)) - lmax);
	double px2u2 = w[7] * ((x_s.col(2) - u_s.col(2)).transpose() * (x_s.col(2) - u_s.col(2)) - lmax);

    double dpx1u1_duy1 = -2 * (x_s(1, 1) - u_s(1, 1)) * w[6];

    double dpx2u2_duy2 = -2 * (x_s(1, 2) - u_s(1, 2)) * w[7];

	double px2u1 = w[8] * ((x_s.col(2) - u_s.col(1)).transpose() * (x_s.col(2) - u_s.col(1)) - lmax);
	double px3u2 = w[9] * ((x_s.col(3) - u_s.col(2)).transpose() * (x_s.col(3) - u_s.col(2)) - lmax);

    double dpx2u1_duy1 = -2 * (x_s(1, 2) - u_s(1, 1)) * w[8];
    double dpx3u2_duy2 = -2 * (x_s(1, 3) - u_s(1, 2)) * w[9];

//    cout<< "inEquality constraint in y" << endl;
//    cout << dpx1u1_duy1 << endl;
//    cout << dpx2u2_duy2 << endl;
//    cout << dpx2u1_duy1 << endl;
//    cout << dpx3u2_duy2 << endl;
	double pu1u0 = w[10] * (foot_flag * (u_s(1, 1) - u_s(1, 0)) - rfoot);
	double pu2u1 = w[11] * (-foot_flag * (u_s(1, 2) - u_s(1, 1)) - rfoot);

	double dpu1u0_duy1 = foot_flag * w[10];

    double dpu2u1_duy1 = foot_flag * w[11];

    double dpu2u1_duy2 = - foot_flag * w[11];

    double duy1_inequality_penalties = dpx1u1_duy1 + dpx2u1_duy1 + dpu1u0_duy1 + dpu2u1_duy1;
    double duy2_inequality_penalties = dpx2u2_duy2 + dpx3u2_duy2 + dpu2u1_duy2;	// 3

    // dynamic constraint penalties
    double dpy2y2_duy1 = -dyn_w[5] * dx_du(dt_s, u_y, x_y, xdot_y, xddot_y, 2, 1);
    double dpdy2dy2_duy1 = -dyn_w[7] * dxdot_du(dt_s, u_y, x_y, xdot_y, xddot_y, 2, 1);

    double dpy3y3_duy2 = -dyn_w[9] * dx_du(dt_s, u_y, x_y, xdot_y, xddot_y, 3, 2);
    double dpdy3dy3_duy2 = -dyn_w[11] * dxdot_du(dt_s, u_y, x_y, xdot_y, xddot_y, 3, 2);

//    cout<< "dynamic constrains for y " << endl;
//    cout << dpy2y2_duy1 << endl;
//    cout << dpdy2dy2_duy1 << endl;
//    cout << dpy3y3_duy2 << endl;
//    cout << dpdy3dy3_duy2 << endl;
//	cout << "The weight is " << w << endl;
//	cout << "The dynamic weight is " << dyn_w << endl;
	double duy1_dynamic_penalties = dpy2y2_duy1 + dpdy2dy2_duy1;	// 4
    double duy2_dynamic_penalties = dpy3y3_duy2 + dpdy3dy3_duy2;	// 2

    Vector2d dJ_duy_;
    dJ_duy_ << duy1_inequality_penalties + duy1_dynamic_penalties,
               duy2_inequality_penalties + duy2_dynamic_penalties;
    return dJ_duy_;
}

Vector3d dJ_dt(Vector3d dt_s, Matrix<double,2, 3> u_s, Matrix<double,2, 4> x_s, Matrix<double,2, 4> xdot_s, Matrix<double, 2, 4> xddot_s){
    double dt0 = dt_s[0];
    double dt0_low = max(dt0_min - current_time, 0.001);
    double dt0_high = dt_max;
    double dt1 = dt_s[1];
    double dt1_low = dt_min;
    double dt1_high = dt_max;
    double dt2 = dt_s[2];
    double dt2_low = dt_min;
    double dt2_high = dt_max;

	Vector3d u_x = u_s.row(0);
	Vector3d u_y = u_s.row(1);
	Vector4d x_x = x_s.row(0);
	Vector4d x_y = x_s.row(1);
	Vector4d xdot_x = xdot_s.row(0);
	Vector4d xdot_y = xdot_s.row(1);
	Vector4d xddot_x = xddot_s.row(0);
	Vector4d xddot_y = xddot_s.row(1);

	// 6
	double dpt_dt0 = w[0] + w[1];
    double dpt_dt1 = w[2] + w[4]; // changed
    double dpt_dt2 = w[3] + w[5];

    double dt0_inequality_penalties = dpt_dt0;
    double dt1_inequality_penalties = dpt_dt1;
    double dt2_inequality_penalties = dpt_dt2;
//    cout<< "dt inequality constraint" << endl;

    // dynamic constraint penalties
    // 12
    double dpx1x1_dt0 = -dyn_w[0] * dx_dt(dt_s, u_x, x_x, xdot_x, xddot_x, 1, 0);
    double dpy1y1_dt0 = -dyn_w[1] * dx_dt(dt_s, u_y, x_y, xdot_y, xddot_y, 1, 0);
    double dpdx1dx1_dt0 = -dyn_w[2] * dxdot_dt(dt_s, u_x, x_x, xdot_x, xddot_x, 1, 0);
    double dpdy1dy1_dt0 = -dyn_w[3] * dxdot_dt(dt_s, u_y, x_y, xdot_y, xddot_y, 1, 0);
    // 8
    double dpx2x2_dt1 = -dyn_w[4] * dx_dt(dt_s, u_x, x_x, xdot_x, xddot_x, 2, 1);
    double dpy2y2_dt1 = -dyn_w[5] * dx_dt(dt_s, u_y, x_y, xdot_y, xddot_y, 2, 1);
    double dpdx2dx2_dt1 = -dyn_w[6] * dxdot_dt(dt_s, u_x, x_x, xdot_x, xddot_x, 2, 1);
    double dpdy2dy2_dt1 = -dyn_w[7] * dxdot_dt(dt_s, u_y, x_y, xdot_y, xddot_y, 2, 1);

    // 4
    double dpx3x3_dt2 = -dyn_w[8] * dx_dt(dt_s, u_x, x_x, xdot_x, xddot_x, 3, 2);
    double dpy3y3_dt2 = -dyn_w[9] * dx_dt(dt_s, u_y, x_y, xdot_y, xddot_y, 3, 2);
    double dpdx3dx3_dt2 = -dyn_w[10] * dxdot_dt(dt_s, u_x, x_x, xdot_x, xddot_x, 3, 2);
    double dpdy3dy3_dt2 = -dyn_w[11] * dxdot_dt(dt_s, u_y, x_y, xdot_y, xddot_y, 3, 2);

//    cout << "Jacobian dt element " << endl;
//    cout << dpx1x1_dt0 << endl;
//    cout << dpy1y1_dt0 << endl;
//    cout << dpdx1dx1_dt0 << endl;
//    cout << dpdy1dy1_dt0 << endl;
//    cout << dpx2x2_dt1 << endl;
//    cout << dpy2y2_dt1 << endl;
//    cout << dpdx2dx2_dt1 << endl;
//    cout << dpdy2dy2_dt1 << endl;
//    cout << dpx3x3_dt2 << endl;
//    cout << dpy3y3_dt2 << endl;
//    cout << dpdx3dx3_dt2 << endl;
//    cout << dpdy3dy3_dt2 << endl;
//    cout << "The dynamic weight is " << dyn_w << endl;


    double dt0_dynamic_penalties = dpx1x1_dt0 + dpy1y1_dt0 + dpdx1dx1_dt0 + dpdy1dy1_dt0;
    double dt1_dynamic_penalties = dpx2x2_dt1 + dpy2y2_dt1 + dpdx2dx2_dt1 + dpdy2dy2_dt1;
    double dt2_dynamic_penalties = dpx3x3_dt2 + dpy3y3_dt2 + dpdx3dx3_dt2 + dpdy3dy3_dt2;

    Vector3d dJ_dt_;

    dJ_dt_ << dt0_inequality_penalties + dt0_dynamic_penalties,
              dt1_inequality_penalties + dt1_dynamic_penalties,
              dt2_inequality_penalties + dt2_dynamic_penalties;

    return dJ_dt_;
}


Vector3d dJ_dx(Vector3d dt_s, Matrix<double,2, 3> u_s, Matrix<double,2, 4> x_s, Matrix<double,2, 4> xdot_s, Matrix<double, 2, 4> xddot_s){
	Vector3d u_x = u_s.row(0);
	Vector3d u_y = u_s.row(1);
	Vector4d x_x = x_s.row(0);
	Vector4d x_y = x_s.row(1);
	Vector4d xdot_x = xdot_s.row(0);
	Vector4d xdot_y = xdot_s.row(1);
	Vector4d xddot_x = xddot_s.row(0);
	Vector4d xddot_y = xddot_s.row(1);

	double dpx1u1_x1 = 2 * (x_s(0, 1) - u_s(0, 1)) * w[6];
	double x1_inequality_penalties = dpx1u1_x1;

	double dpx2u2_x2 = 2 * (x_s(0, 2) - u_s(0, 2)) * w[7];
	double dpx2u1_x2 = 2 * (x_s(0, 2) - u_s(0, 1)) * w[8];
	double x2_inequality_penalties = dpx2u2_x2 + dpx2u1_x2;

	double dpx3u2_x3 = 2 * (x_s(0, 3) - u_s(0, 2)) * w[8];
	double x3_inequality_penalties = dpx3u2_x3;

	double dpx1x1_x1 = dyn_w[0] * dx_dx(dt_s, u_x, x_x, xdot_x, xddot_x, 1, 1);
	double dpx2x2_x1 = -dyn_w[4] * dx_dx(dt_s, u_x, x_x, xdot_x, xddot_x, 2, 1);
	double dpdx2dx2_x1 = -dyn_w[6] * dxdot_dx(dt_s, u_x, x_x, xdot_x, xddot_x, 2, 1);
	double x1_dynamic_penalties = dpx1x1_x1 + dpx2x2_x1 + dpdx2dx2_x1;

	double dpx2x2_x2 = dyn_w[4] * dx_dx(dt_s, u_x, x_x, xdot_x, xddot_x, 2, 2);
	double dpx3x3_x2 = -dyn_w[8] * dx_dx(dt_s, u_x, x_x, xdot_x, xddot_x, 3, 2);
	double dpdx3dx3_x2 = -dyn_w[10] * dxdot_dx(dt_s, u_x, x_x, xdot_x, xddot_x, 3, 2);
	double x2_dynamic_penalties = dpx2x2_x2 + dpx3x3_x2 + dpdx3dx3_x2;

	double dpx3x3_x3 = dyn_w[8] * dx_dx(dt_s, u_x, x_x, xdot_x, xddot_x, 3, 3);
	double x3_dynamic_penalties = dpx3x3_x3;

	Vector3d dJ_dx_;
	dJ_dx_ << x1_inequality_penalties + x1_dynamic_penalties,
              x2_inequality_penalties + x2_dynamic_penalties,
              x3_inequality_penalties + x3_dynamic_penalties;

    return dJ_dx_;
}

Vector3d dJ_dy(Vector3d dt_s, Matrix<double,2, 3> u_s, Matrix<double,2, 4> x_s, Matrix<double,2, 4> xdot_s, Matrix<double, 2, 4> xddot_s){
	Vector3d u_x = u_s.row(0);
	Vector3d u_y = u_s.row(1);
	Vector4d x_x = x_s.row(0);
	Vector4d x_y = x_s.row(1);
	Vector4d xdot_x = xdot_s.row(0);
	Vector4d xdot_y = xdot_s.row(1);
	Vector4d xddot_x = xddot_s.row(0);
	Vector4d xddot_y = xddot_s.row(1);

	double dpx1u1_x1 = 2 * (x_s(1, 1) - u_s(1, 1)) * w[6];
	double x1_inequality_penalties = dpx1u1_x1;

	double dpx2u2_x2 = 2 * (x_s(1, 2) - u_s(1, 2)) * w[7];
	double dpx2u1_x2 = 2 * (x_s(1, 2) - u_s(1, 1)) * w[8];
	double x2_inequality_penalties = dpx2u2_x2 + dpx2u1_x2;

	double dpx3u2_x3 = 2 * (x_s(1, 3) - u_s(1, 2)) * w[8];
	double x3_inequality_penalties = dpx3u2_x3;

	double dpx1x1_x1 = dyn_w[1] * dx_dx(dt_s, u_y, x_y, xdot_y, xddot_y, 1, 1);
	double dpx2x2_x1 = -dyn_w[5] * dx_dx(dt_s, u_y, x_y, xdot_y, xddot_y, 2, 1);
	double dpdx2dx2_x1 = -dyn_w[7] * dxdot_dx(dt_s, u_y, x_y, xdot_y, xddot_y, 2, 1);
	double x1_dynamic_penalties = dpx1x1_x1 + dpx2x2_x1 + dpdx2dx2_x1;

	double dpx2x2_x2 = dyn_w[5] * dx_dx(dt_s, u_y, x_y, xdot_y, xddot_y, 2, 2);
	double dpx3x3_x2 = -dyn_w[9] * dx_dx(dt_s, u_y, x_y, xdot_y, xddot_y, 3, 2);
	double dpdx3dx3_x2 = -dyn_w[11] * dxdot_dx(dt_s, u_y, x_y, xdot_y, xddot_y, 3, 2);
	double x2_dynamic_penalties = dpx2x2_x2 + dpx3x3_x2 + dpdx3dx3_x2;

	double dpx3x3_x3 = dyn_w[9] * dx_dx(dt_s, u_y, x_y, xdot_y, xddot_y, 3, 3);
	double x3_dynamic_penalties = dpx3x3_x3;

	Vector3d dJ_dx_;
	dJ_dx_ << x1_inequality_penalties + x1_dynamic_penalties,
              x2_inequality_penalties + x2_dynamic_penalties,
              x3_inequality_penalties + x3_dynamic_penalties;

    return dJ_dx_;
}

Vector3d dJ_dxdot(Vector3d dt_s, Matrix<double,2, 3> u_s, Matrix<double,2, 4> x_s, Matrix<double,2, 4> xdot_s, Matrix<double, 2, 4> xddot_s){
	Vector3d u_x = u_s.row(0);
	Vector3d u_y = u_s.row(1);
	Vector4d x_x = x_s.row(0);
	Vector4d x_y = x_s.row(1);
	Vector4d xdot_x = xdot_s.row(0);
	Vector4d xdot_y = xdot_s.row(1);
	Vector4d xddot_x = xddot_s.row(0);
	Vector4d xddot_y = xddot_s.row(1);

	double dJ_dxdot1_ = 2 * (xdot_x[1] - x_dot_ref[0]);
	double dJ_dxdot2_ = 2 * (xdot_x[2] - x_dot_ref[0]);
	double dJ_dxdot3_ = 2 * (xdot_x[3] - x_dot_ref[0]);

	double dpdx1dx1_dx1 = dyn_w[2] * dxdot_dxdot(dt_s, u_x, x_x, xdot_x, xddot_x, 1, 1);
	double dpx2x2_dx1 = -dyn_w[4] * dx_dxdot(dt_s, u_x, x_x, xdot_x, xddot_x, 2, 1);
	double dpdx2dx2_dx1 = -dyn_w[6] * dxdot_dxdot(dt_s, u_x, x_x, xdot_x, xddot_x, 2, 1);
	double x1_dynamic_penalties = dpdx1dx1_dx1 + dpx2x2_dx1 + dpdx2dx2_dx1;
	
	double dpdx2dx2_dx2 = dyn_w[6] * dxdot_dxdot(dt_s, u_x, x_x, xdot_x, xddot_x, 2, 2);
	double dpx3x3_dx2 = -dyn_w[8] * dx_dxdot(dt_s, u_x, x_x, xdot_x, xddot_x, 3, 2);
	double dpdx3dx3_dx2 = -dyn_w[10] * dxdot_dxdot(dt_s, u_x, x_x, xdot_x, xddot_x, 3, 2);
	double x2_dynamic_penalties = dpdx2dx2_dx2 + dpx3x3_dx2 + dpdx3dx3_dx2;
	
	double dpdx3dx3_dx3 = dyn_w[10] * dxdot_dxdot(dt_s, u_x, x_x, xdot_x, xddot_x, 3, 3);
	double x3_dynamic_penalties = dpdx3dx3_dx3;

	Vector3d dJ_dxdot_;
	dJ_dxdot_ << dJ_dxdot1_ + x1_dynamic_penalties,
              	 dJ_dxdot2_ + x2_dynamic_penalties,
              	 dJ_dxdot3_ + x3_dynamic_penalties;

    return dJ_dxdot_;
}


Vector3d dJ_dydot(Vector3d dt_s, Matrix<double,2, 3> u_s, Matrix<double,2, 4> x_s, Matrix<double,2, 4> xdot_s, Matrix<double, 2, 4> xddot_s){
	Vector3d u_x = u_s.row(0);
	Vector3d u_y = u_s.row(1);
	Vector4d x_x = x_s.row(0);
	Vector4d x_y = x_s.row(1);
	Vector4d xdot_x = xdot_s.row(0);
	Vector4d xdot_y = xdot_s.row(1);
	Vector4d xddot_x = xddot_s.row(0);
	Vector4d xddot_y = xddot_s.row(1);

	double dJ_dxdot1_ = 2 * (xdot_y[1] - x_dot_ref[1]);
	double dJ_dxdot2_ = 2 * (xdot_y[2] - x_dot_ref[1]);
	double dJ_dxdot3_ = 2 * (xdot_y[3] - x_dot_ref[1]);

	double dpdx1dx1_dx1 = dyn_w[3] * dxdot_dxdot(dt_s, u_y, x_y, xdot_y, xddot_y, 1, 1);
	double dpx2x2_dx1 = -dyn_w[5] * dx_dxdot(dt_s, u_y, x_y, xdot_y, xddot_y, 2, 1);
	double dpdx2dx2_dx1 = -dyn_w[7] * dxdot_dxdot(dt_s, u_y, x_y, xdot_y, xddot_y, 2, 1);
	double x1_dynamic_penalties = dpdx1dx1_dx1 + dpx2x2_dx1 + dpdx2dx2_dx1;

	double dpdx2dx2_dx2 = dyn_w[7] * dxdot_dxdot(dt_s, u_y, x_y, xdot_y, xddot_y, 2, 2);
	double dpx3x3_dx2 = -dyn_w[8] * dx_dxdot(dt_s, u_y, x_y, xdot_y, xddot_y, 3, 2);
	double dpdx3dx3_dx2 = -dyn_w[11] * dxdot_dxdot(dt_s, u_y, x_y, xdot_y, xddot_y, 3, 2);
	double x2_dynamic_penalties = dpdx2dx2_dx2 + dpx3x3_dx2 + dpdx3dx3_dx2;

	double dpdx3dx3_dx3 = dyn_w[11] * dxdot_dxdot(dt_s, u_y, x_y, xdot_y, xddot_y, 3, 3);
	double x3_dynamic_penalties = dpdx3dx3_dx3;

	Vector3d dJ_dxdot_;
	dJ_dxdot_ << dJ_dxdot1_ + x1_dynamic_penalties,
              	 dJ_dxdot2_ + x2_dynamic_penalties,
              	 dJ_dxdot3_ + x3_dynamic_penalties;

    return dJ_dxdot_;
}

double J(Vector3d dt_s, Matrix<double,2, 3> u_s, Matrix<double,2, 4> x_s, Matrix<double,2, 4> xdot_s, Matrix<double, 2, 4> xddot_s){
    double J = 0.0;
    for(int i=1; i<u_s.size()/2+1; i++){
        J += pow(xdot_s(0, i)-x_dot_ref[0],2) + pow(xdot_s(1, i)-x_dot_ref[1],2);
    }
    return J;
 }


bfgs BFGS(bfgs& prev_result, bool verbose = true) {
	// previous solution
	VectorXd x_k = prev_result.x_k;
	VectorXd delta_x_k = prev_result.delta_x_k;
	//	VectorXd y_k = prev_result.y_k;x_k
	MatrixXd B_k = prev_result.B_k;
	MatrixXd H_k = prev_result.H_k;

	MatrixXd I = MatrixXd::Identity(19, 19);
//	MatrixXd I = HessianInv;

	MatrixXd x_s, xdot_s, xddot_s;
	Vector3d dt_old = x_k.segment(0, 3);				// get dts

	Matrix<double, 2, 3>& u_old = u_s;						// get us
	u_old.block(0, 1, 1, 2) = x_k.segment(3, 2).transpose();
	u_old.block(1, 1, 1, 2) = x_k.segment(5, 2).transpose();

	x_s = MatrixXd::Zero(2, 4);
	x_s(0, 0) = x_0(0);
	x_s(1, 0) = x_0(1);
	x_s.block(0, 1, 1, 3) = x_k.segment(7, 3).transpose();
	x_s.block(1, 1, 1, 3) = x_k.segment(10, 3).transpose();

	xdot_s = MatrixXd::Zero(2, 4);
	xdot_s(0, 0) = x_dot_0(0);
	xdot_s(1, 0) = x_dot_0(1);
	xdot_s.block(0, 1, 1, 3) = x_k.segment(13, 3).transpose();
	xdot_s.block(1, 1, 1, 3) = x_k.segment(16, 3).transpose();

	xddot_s = MatrixXd::Zero(2, 4);
	xddot_s(0, 0) = xddot(0.0, u_old(0, 0), x_s(0, 0), xdot_s(0, 0));
	xddot_s(1, 0) = xddot(0.0, u_old(1, 0), x_s(1, 0), xdot_s(1, 0));
	for (int k = 0; k < 3; k++) {
		xddot_s(0, k + 1) = xddot(dt_old[k], u_old(0, k), x_s(0, k), xdot_s(0, k));
		xddot_s(1, k + 1) = xddot(dt_old[k], u_old(1, k), x_s(1, k), xdot_s(1, k));
	}

	cout << "x_old\n" << x_s << endl;
	cout << "xdot_old\n" << xdot_s << endl;
	cout << "xddot_old\n" << xddot_s << endl;

	// calculate gradients at step k
	Vector3d grad_t = dJ_dt(dt_old, u_old, x_s, xdot_s, xddot_s);
	Vector2d grad_ux = dJ_dux(dt_old, u_old, x_s, xdot_s, xddot_s);
	Vector2d grad_uy = dJ_duy(dt_old, u_old, x_s, xdot_s, xddot_s);
	Vector3d grad_x = dJ_dx(dt_old, u_old, x_s, xdot_s, xddot_s);
	Vector3d grad_y = dJ_dy(dt_old, u_old, x_s, xdot_s, xddot_s);
	Vector3d grad_dx = dJ_dxdot(dt_old, u_old, x_s, xdot_s, xddot_s);
	Vector3d grad_dy = dJ_dydot(dt_old, u_old, x_s, xdot_s, xddot_s);
	VectorXd grad_k(19);
	grad_k << grad_t, grad_ux, grad_uy, grad_x, grad_y, grad_dx, grad_dy;
    cout<< "old gradients\n" << grad_k << endl;
    cout << "old solution\n" << x_k << endl;
	// line search to find update size
	double alpha_k = 1e5;
	double best_cost = 1e5;
	//    cout<< "line search " << endl;
	for (int i = 0; i < 1000; i++) {
		double alpha_int =   (1000-i)/10000.0;						// intermediate value of alpha
		// get intermediate dt_int and u_int
		VectorXd delta_x_int = - alpha_int * H_k * grad_k;
		VectorXd x_k_int = x_k + delta_x_int;		// intermediate parameter valu
	//		cout<< "x_k_int is " << x_k_int << endl;
		MatrixXd x_int, xdot_int, xddot_int;
		Vector3d dt_int = x_k_int.segment(0, 3);				// get dts

		Matrix<double, 2, 3>& u_int = u_s;						// get us
		u_int.block(0, 1, 1, 2) = x_k_int.segment(3, 2).transpose();
		u_int.block(1, 1, 1, 2) = x_k_int.segment(5, 2).transpose();

		x_int = MatrixXd::Zero(2, 4);
		x_int(0, 0) = x_0(0);
		x_int(1, 0) = x_0(1);
		x_int.block(0, 1, 1, 3) = x_k_int.segment(7, 3).transpose();
		x_int.block(1, 1, 1, 3) = x_k_int.segment(10, 3).transpose();

		xdot_int = MatrixXd::Zero(2, 4);
		xdot_int(0, 0) = x_dot_0(0);
		xdot_int(1, 0) = x_dot_0(1);
		xdot_int.block(0, 1, 1, 3) = x_k_int.segment(13, 3).transpose();
		xdot_int.block(1, 1, 1, 3) = x_k_int.segment(16, 3).transpose();

		xddot_int = MatrixXd::Zero(2, 4);
		xddot_int(0, 0) = xddot(0.0, u_int(0, 0), x_int(0, 0), xdot_int(0, 0));
		xddot_int(1, 0) = xddot(0.0, u_int(1, 0), x_int(1, 0), xdot_int(1, 0));
		for (int k = 0; k < 3; k++) {
			xddot_int(0, k + 1) = xddot(dt_int[k], u_int(0, k), x_int(0, k), xdot_int(0, k));
			xddot_int(1, k + 1) = xddot(dt_int[k], u_int(1, k), x_int(1, k), xdot_int(1, k));
		}

		// calculate cost with intermediate values
		double cost = J(dt_int, u_int, x_int, xdot_int, xddot_int);		// calculate cost
	//		cout << "alpha" << alpha_int << endl;
	//		cout << "cost" << cost << endl;
		if (cost < best_cost) {											// if cost has improved, keep corresponding best alpha
			alpha_k = alpha_int;
			best_cost = cost;
		}

	}
    cout << "final alpha is " << alpha_k << endl;
    cout << "final cost is " << best_cost << endl;
//	cout << "final value of alpha " << alpha_k << endl;
    delta_x_k = - alpha_k * H_k * grad_k;
	VectorXd x_k_new(19);
	x_k_new << x_k + delta_x_k;
	//	cout << "delta_x_k" << delta_x_k << endl;
	//    cout << "new solution" << x_k_new << endl;
	MatrixXd x_new, xdot_new, xddot_new;
	Vector3d dt_new = x_k_new.segment(0, 3);				// get dts

	Matrix<double, 2, 3>& u_new = u_s;						// get us
	u_new.block(0, 1, 1, 2) = x_k_new.segment(3, 2).transpose();
	u_new.block(1, 1, 1, 2) = x_k_new.segment(5, 2).transpose();

	x_new = MatrixXd::Zero(2, 4);
	x_new(0, 0) = x_0(0);
	x_new(1, 0) = x_0(1);
	x_new.block(0, 1, 1, 3) = x_k_new.segment(7, 3).transpose();
	x_new.block(1, 1, 1, 3) = x_k_new.segment(10, 3).transpose();

	xdot_new = MatrixXd::Zero(2, 4);
	xdot_new(0, 0) = x_dot_0(0);
	xdot_new(1, 0) = x_dot_0(1);
	xdot_new.block(0, 1, 1, 3) = x_k_new.segment(13, 3).transpose();
	xdot_new.block(1, 1, 1, 3) = x_k_new.segment(16, 3).transpose();

	xddot_new = MatrixXd::Zero(2, 4);
	xddot_new(0, 0) = xddot(0.0, u_new(0, 0), x_new(0, 0), xdot_new(0, 0));
	xddot_new(1, 0) = xddot(0.0, u_new(1, 0), x_new(1, 0), xdot_new(1, 0));
	for (int k = 0; k < 3; k++) {
		xddot_new(0, k + 1) = xddot(dt_new[k], u_new(0, k), x_new(0, k), xdot_new(0, k));
		xddot_new(1, k + 1) = xddot(dt_new[k], u_new(1, k), x_new(1, k), xdot_new(1, k));
	}

	cout << "new solution\n" << x_k_new << endl;
	cout << "x_new\n" << x_new << endl;
	cout << "xdot_new\n" << xdot_new << endl;
	cout << "xddot_new\n" << xddot_new << endl;

	// calculate gradients at step k
	Vector3d grad_t_new = dJ_dt(dt_new, u_new, x_new, xdot_new, xddot_new);
	Vector2d grad_ux_new = dJ_dux(dt_new, u_new, x_new, xdot_new, xddot_new);
	Vector2d grad_uy_new = dJ_duy(dt_new, u_new, x_new, xdot_new, xddot_new);
	Vector3d grad_x_new = dJ_dx(dt_new, u_new, x_new, xdot_new, xddot_new);
	Vector3d grad_y_new = dJ_dy(dt_new, u_new, x_new, xdot_new, xddot_new);
	Vector3d grad_dx_new = dJ_dxdot(dt_new, u_new, x_new, xdot_new, xddot_new);
	Vector3d grad_dy_new = dJ_dydot(dt_new, u_new, x_new, xdot_new, xddot_new);
	VectorXd grad_k_new(19);
	grad_k_new << grad_t_new, grad_ux_new, grad_uy_new, grad_x_new, grad_y_new, grad_dx_new, grad_dy_new;
	cout<< "new gradients\n" << grad_k_new << endl;
	VectorXd y_k(19);
	y_k << grad_k_new - grad_k;
	//    cout << "y_k" << y_k << endl;
	MatrixXd B_k_new(19, 19);
	B_k_new<< B_k + y_k * y_k.transpose() / (y_k.transpose() * delta_x_k) - B_k * delta_x_k * (B_k * delta_x_k).transpose() / (delta_x_k.transpose() * B_k * delta_x_k);
	MatrixXd H_k_new(19, 19);
	H_k_new << (I - delta_x_k * y_k.transpose() / (y_k.transpose() * delta_x_k)) * H_k * (I - y_k * delta_x_k.transpose() / (y_k.transpose() * delta_x_k)) + delta_x_k * delta_x_k.transpose() / (y_k.transpose() * delta_x_k);

	// cout << "y_k " << y_k << endl;
	// cout << "delta_x_k " << delta_x_k << endl;
	// cout << "y_k.transpose() * delta_x_k\n" << y_k.transpose() * delta_x_k << endl;
	// cout << "delta_x_k * y_k.transpose()\n" << delta_x_k * y_k.transpose() / (y_k.transpose() * delta_x_k) << endl;
	// cout << "y_k * delta_x_k.transpose()\n" << y_k * delta_x_k.transpose() / (y_k.transpose() * delta_x_k) << endl;
	// cout << "delta_x_k * delta_x_k.transpose()\n" << delta_x_k * delta_x_k.transpose() / (y_k.transpose() * delta_x_k) << endl;
	// cout << "H_k * (I - y_k * delta_x_k.transpose() / (y_k.transpose() * delta_x_k))\n" << H_k * (I - y_k * delta_x_k.transpose() / (y_k.transpose() * delta_x_k)) << endl;
	// cout << "(I - delta_x_k * y_k.transpose() / (y_k.transpose() * delta_x_k)) *H_k * (I - y_k * delta_x_k.transpose() / (y_k.transpose() * delta_x_k))\n" << (I - delta_x_k * y_k.transpose() / (y_k.transpose() * delta_x_k)) *H_k * (I - y_k * delta_x_k.transpose() / (y_k.transpose() * delta_x_k)) << endl;
	// cout << "H_k new\n" << H_k_new << endl;

	bfgs result;
	result.x_k = x_k_new;
	result.delta_x_k = delta_x_k;
	result.y_k = y_k;
	result.B_k = B_k_new;
	result.H_k = H_k_new;
//	cout << "BFGS Values " << endl;
//	cout << "x_k" << result.x_k << endl;
//	cout << "delta_k" << result.delta_x_k << endl;
//	cout << "y_k" << result.y_k << endl;
//	cout << "B_k" << result.B_k << endl;
//	cout << "H_k" << result.H_k << endl;
	return result;
}

// Callback function for receiving the planner outputs
void footstepPlanCallbackRK4(const std_msgs::Float64MultiArray::ConstPtr& msg)
{
    u_s(0,1) = msg->data[0];
    u_s(1,1) = msg->data[1];
    u_s(0,2) = msg->data[2];
    u_s(1,2) = msg->data[3];
    dt_s(0) = msg->data[4];
    dt_s(1) = msg->data[5];
    dt_s(2) = msg->data[6];
    support_foot_flag = msg->data[7];
//    flag = 1;
    cout << "--------------------------------" << endl;
    cout<< "received u \n" << u_s << endl;
    cout<< "received t \n" << dt_s << endl;

    solution.x_k = VectorXd::Zero(19);
	solution.x_k.segment(0, 3) = dt_s.transpose();
	solution.x_k.segment(3, 2) = u_s.block(0, 1, 1, 2).transpose();
	solution.x_k.segment(5, 2) = u_s.block(1, 1, 1, 2).transpose();
	solution.delta_x_k = VectorXd::Zero(19);
	solution.y_k = VectorXd::Zero(19);
	solution.B_k = Matrix<double, 19, 19>::Identity();
	solution.H_k = Matrix<double, 19, 19>::Identity();
	rk4_received = true;
}

void plannerInputCallback(const std_msgs::Float64MultiArray::ConstPtr& msg)
{
//    x, x_dot, y, y_dot = msg.data[:4]
    x_0(0) = msg->data[0];
    x_dot_0(0) = msg->data[1];
    x_dot_dot_0(0) = msg->data[2];
    x_0(1) = msg->data[3];
    x_dot_0(1) = msg->data[4];
    x_dot_dot_0(1) = msg->data[5];
    left_x  = msg->data[6];
    left_y  = msg->data[7];
    right_x = msg->data[8];
    right_y = msg->data[9];
    foot_flag = msg->data[10];
    current_time = msg->data[11];
    cout << "received states " << endl;
    cout << x_0(0) << endl;
    cout << x_dot_0(0) << endl;
    cout << x_dot_dot_0(0) << endl;
    cout << x_0(1) << endl;
    cout << x_dot_0(1) << endl;
    cout << x_dot_dot_0(1) << endl;
    cout << left_x << endl;
    cout << left_y << endl;
    cout << right_x << endl;
    cout << right_y << endl;
    cout << foot_flag << endl;
    cout << current_time << endl;
}

void weightCallback(const std_msgs::Float64MultiArray::ConstPtr& msg)
{
    w[0] = msg->data[18];
    w[1] = msg->data[19];
    w[2] = msg->data[20];
    w[3] = msg->data[21];
    w[4] = msg->data[22];
    w[5] = msg->data[23];
    w[6] = msg->data[24];
    w[7] = msg->data[25];
    w[8] = msg->data[26];
    w[9] = msg->data[27];
    w[10] = msg->data[28];
    w[11] = msg->data[29];
//    w[0] = 1.0;
//    w[1] = 1.0;
//    w[2] = 1.0;
//    w[3] = 1.0;
//    w[4] = 1.0;
//    w[5] = 1.0;
//    w[6] = 1.0;
//    w[7] = 1.0;
//    w[8] = 1.0;
//    w[9] = 1.0;
//    w[10] = 1.0;
//    w[11] = 1.0;
    dyn_w[0] = msg->data[0];
    dyn_w[1] = msg->data[1];
    dyn_w[2] = msg->data[2];
    dyn_w[3] = msg->data[3];
    dyn_w[4] = msg->data[4];
    dyn_w[5] = msg->data[5];
    dyn_w[6] = msg->data[6];
    dyn_w[7] = msg->data[7];
    dyn_w[8] = msg->data[8];
    dyn_w[9] = msg->data[9];
    dyn_w[10] = msg->data[10];
    dyn_w[11] = msg->data[11];
//    dyn_w[0] = 1.0;
//    dyn_w[1] = 1.0;
//    dyn_w[2] = 1.0;
//    dyn_w[3] = 1.0;
//    dyn_w[4] = 1.0;
//    dyn_w[5] = 1.0;
//    dyn_w[6] = 1.0;
//    dyn_w[7] = 1.0;
//    dyn_w[8] = 1.0;
//    dyn_w[9] = 1.0;
//    dyn_w[10] = 1.0;
//    dyn_w[11] = 1.0;

//    cout << "The received weights are " << w << endl;
}

void jacobianCallback(const std_msgs::Float64MultiArray::ConstPtr& msg)
{
	std::vector<double> data = msg->data;
	Eigen::Map<Eigen::MatrixXd> mat(data.data(), 30, 16);
	Jacobian = mat;
//	cout<<"The received jacobian is " << Jacobian << endl;
}

void hessianCallback(const std_msgs::Float64MultiArray::ConstPtr& msg)
{
	std::vector<double> data = msg->data;
	Eigen::Map<Eigen::MatrixXd> mat(data.data(), 19, 19);
//	solution.H_k = mat;
//	HessianInv = mat;
//	cout<<"The received jacobian is " << Jacobian << endl;
}


int main(int argc, char ** argv){
    Vector4d x_opt,y_opt;
    const double LOOP_RATE = 25;
    int iter_main = 0;
    ros::init(argc, argv, "gradient_descent_footPlanner_node");

    ros::NodeHandle n;
    ros::Rate loop_rate(LOOP_RATE);
    ros::Publisher planner_output_pub = n.advertise<std_msgs::Float64MultiArray>("/time_slider_gazebo/footstep_plan", 1);
    ros::Publisher planner_gradient_pub = n.advertise<std_msgs::Float64MultiArray>("/time_slider_gazebo/gd_gradients", 1);
    ros::Subscriber sub_planner_input = n.subscribe<std_msgs::Float64MultiArray>("/time_slider_gazebo/planner_input", 'None', plannerInputCallback);
    ros::Subscriber sub_RK4_planner_input = n.subscribe<std_msgs::Float64MultiArray>("/time_slider_gazebo/footstep_plan_rk4", 'None', footstepPlanCallbackRK4);
    ros::Subscriber sub_weight        = n.subscribe<std_msgs::Float64MultiArray>("/time_slider_gazebo/cost_weight", 'None', weightCallback);
    ros::Subscriber sub_jacobian      = n.subscribe<std_msgs::Float64MultiArray>("/time_slider_gazebo/jacobian", 'None', jacobianCallback);
    ros::Subscriber sub_hessian       = n.subscribe<std_msgs::Float64MultiArray>("/time_slider_gazebo/hessianInv", 'None', hessianCallback);
    support_foot_flag                 = foot_flag;

	Jacobian = MatrixXd::Zero(30, 16);
	HessianInv = MatrixXd::Zero(19, 19);
	solution.x_k = VectorXd::Zero(19);
    w  = VectorXd::Zero(n_weights);
    dyn_w  = VectorXd::Zero(n_weights);
    cout << "started" << endl;
    MatrixXd x_s, xdot_s, xddot_s;
	x_s = MatrixXd::Zero(2, 4);
	xdot_s = MatrixXd::Zero(2, 4);
	xddot_s = MatrixXd::Zero(2, 4);
	x_s(0, 0) = x_0(0);
	x_s(1, 0) = x_0(1);
	xdot_s(0, 0) = x_dot_0(0);
	xdot_s(1, 0) = x_dot_0(1);
	xddot_s(0, 0) = xddot(0.0, u_s(0, 0), x_s(0, 0), xdot_s(0, 0));
	xddot_s(1, 0) = xddot(0.0, u_s(1, 0), x_s(1, 0), xdot_s(1, 0));
	for (int k = 0; k < 3; k++) {
		x_s(0, k + 1) = x(dt_s[k], u_s(0, k), x_s(0, k), xdot_s(0, k));
		x_s(1, k + 1) = x(dt_s[k], u_s(1, k), x_s(1, k), xdot_s(1, k));
		xdot_s(0, k + 1) = xdot(dt_s[k], u_s(0, k), x_s(0, k), xdot_s(0, k));
		xdot_s(1, k + 1) = xdot(dt_s[k], u_s(1, k), x_s(1, k), xdot_s(1, k));
		xddot_s(0, k + 1) = xddot(dt_s[k], u_s(0, k), x_s(0, k), xdot_s(0, k));
		xddot_s(1, k + 1) = xddot(dt_s[k], u_s(1, k), x_s(1, k), xdot_s(1, k));
	}
	cout << "u_s" << u_s << endl;
	cout << "dT_s" << dt_s << endl;
	cout << "x_s" << x_s << endl;
	cout << "xdot_s" << xdot_s << endl;
	cout << "xddot_s" << xddot_s << endl;

	// calculate gradients at step k
	Vector3d grad_t = dJ_dt(dt_s, u_s, x_s, xdot_s, xddot_s);
	cout<< "calculate dJ_dt, first iteration" << endl;
	Vector2d grad_ux = dJ_dux(dt_s, u_s, x_s, xdot_s, xddot_s);
	cout<< "calculate dJ_dux, first iteration" << endl;
	Vector2d grad_uy = dJ_duy(dt_s, u_s, x_s, xdot_s, xddot_s);
	cout<< "calculate dJ_duy, first iteration" << endl;
	VectorXd grad_k(7);
	grad_k << grad_t, grad_ux, grad_uy;
	cout << "The weight is " << w << endl;
	cout << "The dynamic weight is " << dyn_w << endl;
	cout << "The gradients are " << grad_k << endl;
	VectorXd lamda_grad;

    while (ros::ok()){

        if(rk4_received){
//            for(int i =0; i<2; i++){
            auto t1 = std::chrono::high_resolution_clock::now();
            // for (int i=0; i <max_iter; i+=2){
            cout << "u ini is \t" << u_s << endl;
            cout << "t ini is \t" << dt_s << endl;
            cout << "current robot COM pos is \t" << x_0 << endl;
            cout << "current robot COM vel is \t" << x_dot_0 << endl;
            ////////////// Gradient Descent ////////////////////////////////
            if (foot_flag == -1){
                u_s(0,0) = left_x;
                u_s(1,0) = left_y;
            }
            else if (foot_flag == 1){
                u_s(0,0) = right_x;
                u_s(1,0) = right_y;
            }
	     	MatrixXd x_s, xdot_s, xddot_s;
			x_s = MatrixXd::Zero(2, 4);
			xdot_s = MatrixXd::Zero(2, 4);
			xddot_s = MatrixXd::Zero(2, 4);
			x_s(0, 0) = x_0(0);
			x_s(1, 0) = x_0(1);
			xdot_s(0, 0) = x_dot_0(0);
			xdot_s(1, 0) = x_dot_0(1);
			xddot_s(0, 0) = xddot(0.0, u_s(0, 0), x_s(0, 0), xdot_s(0, 0));
			xddot_s(1, 0) = xddot(0.0, u_s(1, 0), x_s(1, 0), xdot_s(1, 0));
			for (int k = 0; k < 3; k++) {
				x_s(0, k + 1) = x(dt_s[k], u_s(0, k), x_s(0, k), xdot_s(0, k));
				x_s(1, k + 1) = x(dt_s[k], u_s(1, k), x_s(1, k), xdot_s(1, k));
				xdot_s(0, k + 1) = xdot(dt_s[k], u_s(0, k), x_s(0, k), xdot_s(0, k));
				xdot_s(1, k + 1) = xdot(dt_s[k], u_s(1, k), x_s(1, k), xdot_s(1, k));
				xddot_s(0, k + 1) = xddot(dt_s[k], u_s(0, k), x_s(0, k), xdot_s(0, k));
				xddot_s(1, k + 1) = xddot(dt_s[k], u_s(1, k), x_s(1, k), xdot_s(1, k));
			}
	//        cout
			cout << "x_s" << x_s << endl;
			cout << "xdot_s" << xdot_s << endl;
			cout << "xddot_s" << xddot_s << endl;
			cout << "x block " << x_s.block(0, 1, 1, 3) << endl;
			solution.x_k.segment(7, 3) = x_s.block(0, 1, 1, 3).transpose();
			solution.x_k.segment(10, 3) = x_s.block(1, 1, 1, 3).transpose();
			solution.x_k.segment(13, 3) = xdot_s.block(0, 1, 1, 3).transpose();
			solution.x_k.segment(16, 3) = xdot_s.block(1, 1, 1, 3).transpose();
			cout << "sol " << solution.x_k << endl;

			lamda_grad = Constraints(dt_s, u_s, x_s, xdot_s, xddot_s);

	        bfgs prev_solution = solution;
	        solution = BFGS(prev_solution);
	        dt_s = solution.x_k.segment(0, 3);
	        u_s.block(0, 1, 1, 2) = solution.x_k.segment(3, 2).transpose();
	        u_s.block(1, 1, 1, 2) = solution.x_k.segment(5, 2).transpose();
	        cout<< "result dt is " << dt_s << endl;
	        cout << "u_s is " << u_s << endl;
	        // cout << "HK is " << solution.H_k << endl;


	        prev_solution = solution;
	        solution = BFGS(prev_solution);

	        dt_s = solution.x_k.segment(0, 3);				// get dts
	        u_s.block(0, 1, 1, 2) = solution.x_k.segment(3, 2).transpose();
	        u_s.block(1, 1, 1, 2) = solution.x_k.segment(5, 2).transpose();
	        cout<< "result dt is " << dt_s << endl;
	        cout << "u_s is " << u_s << endl;
            rk4_received = false;
    	}

        ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;}

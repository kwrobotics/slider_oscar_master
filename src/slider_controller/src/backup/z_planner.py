import numpy as np
from math import sin, cos

class SLIP:
    def __init__(self):

        self.T = 0.7 # step duration
        self.z0 = 0.7 # com height
        self.g = 9.8 # g
        self.m = 15 # mass

        self.k = 70.0  # spring stiffness
        self.omega = np.sqrt(self.k / self.m)

        self.r0 = self.z0
        self.rT = self.z0

    def update_state(self, z0, zd0):
        self.z0 = z0
        self.zd0 = zd0
        self.r0 = z0
        self.d1 = self.z0 - self.r0 + self.g/(self.omega**2)
        self.d2 = self.zd0/self.omega - (self.rT-self.r0)/(self.T*self.omega)

    def r(self,t):
        r_t = t / self.T * (self.rT - self.r0) + self.r0
        return r_t

    def get_com_state(self, t):

        z = self.d1*cos(self.omega*t) + self.d2*sin(self.omega*t) + self.r(t) - self.g/(self.omega**2)
        zd = -self.d1*sin(self.omega*t)*self.omega + self.d2*cos(self.omega*t)*self.omega + 1/self.T*(self.rT-self.r0)
        zdd = -self.d1*cos(self.omega*t)*self.omega**2 - self.d2*sin(self.omega*t)*self.omega**2

        return z, zd, zdd
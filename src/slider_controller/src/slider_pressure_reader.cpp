#include "osc_foot.h"
#include "gen_trajectory.h"
#include "define.h"
#include "LIPM.h"

#include "ros/ros.h"
#include "ros/package.h"
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>

#include <geometry_msgs/Vector3.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Float64MultiArray.h>

#include <gazebo/gazebo.hh>
#include <gazebo/transport/transport.hh>
#include <gazebo/msgs/msgs.hh>
#include <gazebo/gazebo_client.hh>
#include <gazebo/sensors/sensors.hh>

#include <sensor_msgs/JointState.h>
#include <gazebo_msgs/LinkStates.h>
#include <Eigen/Geometry>
#include <eigen_conversions/eigen_msg.h>

#include <iostream>
#include <chrono>
#include <vector>   
#include <cmath>
#include <typeinfo>

Vector3d pad_force(0,0,0);
VectorXd pad_force_R(12);
VectorXd pad_force_L(12);
VectorXd pad_force_tots(24);

void pressure_pad_callback(ConstContactsPtr &_msg ) 
{	const char *sides[2] {"Right_Foot", "Left_Foot"};
	const char *pad[4] {"A", "B", "C", "D"};
	string name ("");

	for ( int i = 0; i < _msg->contact_size(); i++)
	{
		pad_force(0) = _msg->contact(i).wrench().Get(0).body_1_wrench().force().x();
	    pad_force(1) = _msg->contact(i).wrench().Get(0).body_1_wrench().force().y();
	    pad_force(2) = _msg->contact(i).wrench().Get(0).body_1_wrench().force().z();
	    name = _msg->contact(i).collision1();

	    for (int j = 0; j<2; j++){
	    	for (int k = 0; k<4; k++){
	    		if ( (name.find(sides[j])!= string::npos) && (name.find(pad[k]) != string::npos) ){
	    			pad_force_tots(j*12+k*3) = _msg->contact(i).wrench().Get(0).body_1_wrench().force().x();
	    			pad_force_tots(j*12+k*3+1) = _msg->contact(i).wrench().Get(0).body_1_wrench().force().y();
	    			pad_force_tots(j*12+k*3+2) = _msg->contact(i).wrench().Get(0).body_1_wrench().force().z();
	    		}
	    	}
	    }


	}

}

int main(int argc, char ** argv)
{
	ros::init(argc, argv, "ros_SLIDER_contact");
  	ros::NodeHandle n;
	
	gazebo::client::setup(argc,argv);

    // Create Gazebo node and init

    gazebo::transport::NodePtr node(new gazebo::transport::Node());
    node->Init();

 	ros::Rate loop_rate(1000);
 	ros::Publisher pad_forces = n.advertise<std_msgs::Float64MultiArray>("/slider_gazebo/pad_forces", 1);
    
    // Subscribe to gazebo pressure pads
    gazebo::transport::SubscriberPtr pressure_pads = node->Subscribe("/gazebo/default/physics/contacts", pressure_pad_callback);

    while (ros::ok()){
    	std_msgs::Float64MultiArray to_publish; 

	    for (int i = 0; i<24; i++){
	    	to_publish.data.push_back(pad_force_tots(i));
		} 
/*    	cout<<"-------------------------to publish-----------"<< to_publish<<endl;*/

    	pad_forces.publish(to_publish);
   // 	gazebo::common::Time::MSleep(100);
    	loop_rate.sleep();

    }
        // Make sure to shut everything down.
  	gazebo::client::shutdown();

}
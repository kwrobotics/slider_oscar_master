#include <iostream>
#include <chrono>
#include <cmath>
#include <Eigen/Dense>

#include "ros/ros.h"
#include <std_msgs/Float64.h>
#include <std_msgs/Float64MultiArray.h>

using namespace Eigen;
using namespace std;

const double m = 15.0;    // body mass
const double m_s = 0.3;   // swing leg mass
const double L = 1.2;     // leg length
const double h = 0.7;     // body height
const double g = 9.81;    // gravity
const double omega = sqrt(g / h);  // omega

// penalty weight
const double k0 = 1.0;
const double k1 = 1.0;
const double k2 = 0.15;
const double T_end = 100;
const double dt = 0.001;
const double vmax = 5.0;              // m/s
const double step_time = 0.7;
int flag = 0;
int support_foot_flag = -1;
Vector2d x_dot_ref(0.05, 0.00);     // m/s
// Parameters subscribed from the slider_controller node
Vector2d x_0(0.0, 0.0);
Vector2d x_dot_0(0.0, 0.0);
Vector4d t_s(0, 0.2, 0.5, 0.9);
Matrix<double,2,3> u_s((Matrix<double,2,3>() << 0.0, 0.0,  0.02, 0.15, -0.21924587,  0.03075412).finished());
// Eigen::Matrix3d A((Eigen::Matrix3d() << 1, 2, 3, 4, 5, 6, 7, 8, 9).finished());

double left_x = 0.0;
double left_y = 0.15;
double right_x = 0.0;
double right_y = -0.15;
int foot_flag = -1;
// penalty weights
const double kdt0 = 1.0;
const double kdt1 = 0.5;
const double kdt2 = 0.5;

const double k_t_len = 0.8;
const double k_ux_len = 1.0;
const double k_uy_len = 1.0;

const double k_foot = 2.0;

double a0x = 0.5 * (x_0[0] - u_s(0, 0) + x_dot_0[0] / omega);
double b0x = 0.5 * (x_0[0] - u_s(0, 0)- x_dot_0[0] / omega);

double a0y = 0.5 * (x_0[1] - u_s(1, 0) + x_dot_0[1] / omega);
double b0y = 0.5 * (x_0[1] - u_s(1, 0) - x_dot_0[1] / omega);

double swing_x = 0.0;
double swing_z = 0.0;
double swing_y = -0.15;

const double max_iter = 300;
int n_repeats = 1;

// constraints
double lmax = 2*sqrt(pow(L, 2.0) - pow(h, 2.0));
const double rfoot = 0.45;

const double eta = 1.0;
const double eta_t = 5e-10; //0.0000025 * 2;
const double eta_u = 1e-11; //0.0000025 * 2;
//const double eta_t = 0; //0.0000025 * 2;
//const double eta_u = 0; //0.0000025 * 2;

double current_time = 0.0;

struct optimal{
    Vector4d time;
    Matrix<double,2,3> u_s;
};

double x(Vector4d t_s, Matrix<double,2, 3> u_s, int kx=0){
    double x_ = 0.0;
    if (kx == 1) {x_ = a0x * exp(omega * (t_s[1] - t_s[0])) + b0x * exp(-omega * (t_s[1] - t_s[0])) + u_s(0, 0);}
    else if (kx == 2){
        x_ = a0x * exp(omega * (t_s[2] - t_s[0])) + b0x * exp(-omega * (t_s[2] - t_s[0])) + 0.5 * (u_s(0, 0) - u_s(0, 1)) * (exp(omega * (t_s[2] - t_s[1])) + exp(-omega * (t_s[2] - t_s[1]))) + u_s(0, 1);}
    else if(kx == 3){
        x_ = a0x * exp(omega * (t_s(3) - t_s(0))) + b0x * exp(-omega * (t_s(3) - t_s(0))) + 0.5 * (u_s(0, 0) - u_s(0, 1)) * (exp(omega * (t_s(3) - t_s(1))) + exp(-omega * (t_s(3) - t_s(1)))) 
            + 0.5 * (u_s(0, 1) - u_s(0, 2)) * (exp(omega * (t_s(3) - t_s(2))) + exp(-omega * (t_s(3) - t_s(2)))) + u_s(0, 2);}
    // else if(kx == 4){
    //     x_ = a0x * exp(omega * (t_s(4) - t_s(0))) + b0x * exp(-omega * (t_s(4) - t_s(0))) + 0.5 * (u_s(0, 0) - u_s(0, 1)) * (exp(omega * (t_s(4) - t_s(1))) + exp(-omega * (t_s(4) - t_s(1)))) 
    //         + 0.5 * (u_s(0, 1) - u_s(0, 2)) * (exp(omega * (t_s(4) - t_s(2))) + exp(-omega * (t_s(4) - t_s(2)))) + 0.5 * (u_s(0, 2) - u_s(0, 3)) * (exp(omega * (t_s(4) - t_s(3))) + exp(-omega * (t_s(4) - t_s(3))))
    //          + u_s(0, 3);}
    else if (kx == 0){x_ = x_0(0);}
    return x_;
}

double y(Vector4d t_s, Matrix<double,2, 3> u_s, int ky=0){
    double y_ = 0.0;
    if (ky == 1) {y_ = a0y * exp(omega * (t_s(1) - t_s(0))) + b0y * exp(-omega * (t_s(1) - t_s(0))) + u_s(1, 0);}
    else if (ky == 2){
        y_ = a0y * exp(omega * (t_s(2) - t_s(0))) + b0y * exp(-omega * (t_s(2) - t_s(0))) + 0.5 * (u_s(1, 0) - u_s(1, 1)) * (exp(omega * (t_s(2) - t_s(1))) + exp(-omega * (t_s(2) - t_s(1)))) + u_s(1, 1);}
    else if(ky == 3){
        y_ = a0y * exp(omega * (t_s(3) - t_s(0))) + b0y * exp(-omega * (t_s(3) - t_s(0))) + 0.5 * (u_s(1, 0) - u_s(1, 1)) * (exp(omega * (t_s(3) - t_s(1))) + exp(-omega * (t_s(3) - t_s(1)))) 
            + 0.5 * (u_s(1, 1) - u_s(1, 2)) * (exp(omega * (t_s(3) - t_s(2))) + exp(-omega * (t_s(3) - t_s(2)))) + u_s(1, 2);}
    // else if(ky == 4){
    //     y_ = a0y * exp(omega * (t_s(4) - t_s(0))) + b0y * exp(-omega * (t_s(4) - t_s(0))) + 0.5 * (u_s(1, 0) - u_s(1, 1)) * (exp(omega * (t_s(4) - t_s(1))) + exp(-omega * (t_s(4) - t_s(1)))) 
    //         + 0.5 * (u_s(1, 1) - u_s(1, 2)) * (exp(omega * (t_s(4) - t_s(2))) + exp(-omega * (t_s(4) - t_s(2)))) + 0.5 * (u_s(1, 2) - u_s(1, 3)) * (exp(omega * (t_s(4) - t_s(3))) + exp(-omega * (t_s(4) - t_s(3))))
    //          + u_s(1, 3);}
    else if (ky == 0){y_ = x_0(1);}
    return y_;
}

double xdot(Vector4d t_s, Matrix<double,2, 3> u_s, int kx=0){
    double xdot_ = 0.0;
    if(kx == 1){ xdot_ = a0x * omega * exp(omega * (t_s[1] - t_s[0])) - b0x * omega * exp(-omega * (t_s[1] - t_s[0]));}
    else if (kx == 2){
        xdot_ = a0x * omega * exp(omega * (t_s[2] - t_s[0])) - b0x * omega * exp(-omega * (t_s[2] - t_s[0])) 
        + 0.5 * (u_s(0, 0) - u_s(0, 1)) * omega * (exp(omega * (t_s[2] - t_s[1])) - exp(-omega * (t_s[2] - t_s[1])));
         }
    else if (kx == 3){
        xdot_ = a0x * omega * exp(omega * (t_s[3] - t_s[0])) - b0x * omega * exp(-omega * (t_s[3] - t_s[0])) +
                0.5 * (u_s(0, 0) - u_s(0, 1)) * omega * (exp(omega * (t_s[3] - t_s[1])) - exp(-omega * (t_s[3] - t_s[1]))) +
                0.5 * (u_s(0, 1) - u_s(0, 2)) * omega * (exp(omega * (t_s[3] - t_s[2])) - exp(-omega * (t_s[3] - t_s[2])));}
    // else if (kx == 4){
    //     xdot_ = a0x * omega * exp(omega * (t_s[4] - t_s[0])) - b0x * omega * exp(-omega * (t_s[4] - t_s[0])) +
    //             0.5 * (u_s(0, 0) - u_s(0, 1)) * omega * (exp(omega * (t_s[4] - t_s[1])) - exp(-omega * (t_s[4] - t_s[1]))) +
    //             0.5 * (u_s(0, 1) - u_s(0, 2)) * omega * (exp(omega * (t_s[4] - t_s[2])) - exp(-omega * (t_s[4] - t_s[2]))) +
    //             0.5 * (u_s(0, 2) - u_s(0, 3)) * omega * (exp(omega * (t_s[4] - t_s[3])) - exp(-omega * (t_s[4] - t_s[3])));}
    else if (kx == 0){xdot_ = x_dot_0[0];}
    return xdot_;
}

double ydot(Vector4d t_s, Matrix<double,2, 3> u_s, int ky=0){
    double ydot_ = 0.0;
    if(ky == 1){ ydot_ = a0y * omega * exp(omega * (t_s[1] - t_s[0])) - b0y * omega * exp(-omega * (t_s[1] - t_s[0]));}
    else if (ky == 2){
        ydot_ = a0y * omega * exp(omega * (t_s[2] - t_s[0])) - b0y * omega * exp(-omega * (t_s[2] - t_s[0])) 
        + 0.5 * (u_s(1, 0) - u_s(1, 1)) * omega * (exp(omega * (t_s[2] - t_s[1])) - exp(-omega * (t_s[2] - t_s[1])));
         }
    else if (ky == 3){
        ydot_ = a0y * omega * exp(omega * (t_s[3] - t_s[0])) - b0y * omega * exp(-omega * (t_s[3] - t_s[0])) +
                0.5 * (u_s(1, 0) - u_s(1, 1)) * omega * (exp(omega * (t_s[3] - t_s[1])) - exp(-omega * (t_s[3] - t_s[1]))) +
                0.5 * (u_s(1, 1) - u_s(1, 2)) * omega * (exp(omega * (t_s[3] - t_s[2])) - exp(-omega * (t_s[3] - t_s[2])));}
    // else if (ky == 4){
    //     ydot_ = a0y * omega * exp(omega * (t_s[4] - t_s[0])) - b0y * omega * exp(-omega * (t_s[4] - t_s[0])) +
    //             0.5 * (u_s(1, 0) - u_s(1, 1)) * omega * (exp(omega * (t_s[4] - t_s[1])) - exp(-omega * (t_s[4] - t_s[1]))) +
    //             0.5 * (u_s(1, 1) - u_s(1, 2)) * omega * (exp(omega * (t_s[4] - t_s[2])) - exp(-omega * (t_s[4] - t_s[2]))) +
    //             0.5 * (u_s(1, 2) - u_s(1, 3)) * omega * (exp(omega * (t_s[4] - t_s[3])) - exp(-omega * (t_s[4] - t_s[3])));}
    else if (ky == 0){ydot_ = x_dot_0[1];}
    return ydot_;
}

double dxdot_dt(Vector4d t_s, Matrix<double,2, 3> u_s, int kx=1, int kt=0){
    double dxdot_dt_ = 0.0;
    dxdot_dt_ = a0x * pow(omega, 2) * exp(omega * (t_s[kx] - t_s[0])) + b0x * pow(omega, 2) * exp(-omega * (t_s[kx] - t_s[0]));
    if(kt >= 2){ dxdot_dt_ += 0.5 * (u_s(0, 1) - u_s(0, 0)) * pow(omega,2) * (exp(omega * (t_s[kx] - t_s[1])) + exp(-omega * (t_s[kx] - t_s[1])));}
    if (kt >= 3){ dxdot_dt_ += 0.5 * (u_s(0, 2) - u_s(0, 1)) * pow(omega ,2) * (exp(omega * (t_s[kx] - t_s[2])) + exp(-omega * (t_s[kx] - t_s[2])));}
    // if (kt >= 4){ dxdot_dt_ += 0.5 * (u_s(0, 3) - u_s(0, 2)) * pow(omega ,2) * (exp(omega * (t_s[kx] - t_s[3])) + exp(-omega * (t_s[kx] - t_s[3])));}
    return dxdot_dt_;
    }

double dydot_dt(Vector4d t_s, Matrix<double,2, 3> u_s, int ky=1, int kt=0){
    double dydot_dt_ = 0.0;
    dydot_dt_ = a0y * pow(omega, 2) * exp(omega * (t_s[ky] - t_s[0])) + b0y * pow(omega, 2) * exp(-omega * (t_s[ky] - t_s[0]));
    if(kt >= 2){ dydot_dt_ += 0.5 * (u_s(1, 1) - u_s(1, 0)) * pow(omega,2) * (exp(omega * (t_s[ky] - t_s[1])) + exp(-omega * (t_s[ky] - t_s[1])));}
    if (kt >= 3){ dydot_dt_ += 0.5 * (u_s(1, 2) - u_s(1, 1)) * pow(omega ,2) * (exp(omega * (t_s[ky] - t_s[2])) + exp(-omega * (t_s[ky] - t_s[2])));}
    // if (kt >= 4){ dydot_dt_ += 0.5 * (u_s(1, 3) - u_s(1, 2)) * pow(omega ,2) * (exp(omega * (t_s[ky] - t_s[3])) + exp(-omega * (t_s[ky] - t_s[3])));}
    return dydot_dt_;
    }

double dx_dt(Vector4d t_s, Matrix<double,2, 3> u_s, int kx=1, int kt=0){
    double dx_dt_ = a0x * omega * exp(omega * (t_s[kx] - t_s[0])) - b0x * omega * exp(-omega * (t_s[kx] - t_s[0]));
    if (kt >= 2){
        dx_dt_ += 0.5 * (u_s(0, 1) - u_s(0, 0)) * omega * (exp(omega * (t_s[kx] - t_s[1])) + exp(-omega * (t_s[kx] - t_s[1])));}
    if (kt >= 3){
        dx_dt_ += 0.5 * (u_s(0, 2) - u_s(0, 1)) * omega * (exp(omega * (t_s[kx] - t_s[2])) + exp(-omega * (t_s[kx] - t_s[2])));}
    // if (kt >= 4){
    //     dx_dt_ += 0.5 * (u_s(0, 3) - u_s(0, 2)) * omega * (exp(omega * (t_s[kx] - t_s[3])) + exp(-omega * (t_s[kx] - t_s[3])));}
    return dx_dt_;
}

double dy_dt(Vector4d t_s, Matrix<double,2, 3> u_s, int ky=1, int kt=0){
    double dy_dt_ = a0y * omega * exp(omega * (t_s[ky] - t_s[0])) - b0y * omega * exp(-omega * (t_s[ky] - t_s[0]));
    if (kt >= 2){
        dy_dt_ += 0.5 * (u_s(1, 1) - u_s(1, 0)) * omega * (exp(omega * (t_s[ky] - t_s[1])) + exp(-omega * (t_s[ky] - t_s[1])));}
    if (kt >= 3){
        dy_dt_ += 0.5 * (u_s(1, 2) - u_s(1, 1)) * omega * (exp(omega * (t_s[ky] - t_s[2])) + exp(-omega * (t_s[ky] - t_s[2])));}
    // if (kt >= 4){
    //     dy_dt_ += 0.5 * (u_s(1, 3) - u_s(1, 2)) * omega * (exp(omega * (t_s[ky] - t_s[3])) + exp(-omega * (t_s[ky] - t_s[3])));}
    return dy_dt_;
}

double dx_du(Vector4d t_s, Matrix<double,2, 3> u_s, int kx=2, int ku=1){
    double dx_du_ = 0.0;
    double i = 1;
    for(int k=ku; k<kx; k++){
        dx_du_ += pow(-1, (i - 1));
        dx_du_ += pow(-1, i) * 0.5 * (exp(omega * (t_s[kx] - t_s[k])) + exp(-omega * (t_s[kx] - t_s[k])));
        i = 0;   // after fpow(-1, i)irst term, it is positive
    }
    return dx_du_;
}

double dy_du(Vector4d t_s, Matrix<double,2, 3> u_s, int ky=2, int ku=1){
    double dy_du_ = 0;
    int i = 1;
    for(int k=ku; k<ky; k++){
        dy_du_ += pow(-1, (i - 1));
        dy_du_ += pow(-1, i) * 0.5 * (exp(omega * (t_s[ky] - t_s[k])) + exp(-omega * (t_s[ky] - t_s[k])));
        i = 0;   // after first term, it is positive
    }
    return dy_du_;
}

double dxdot_du(Vector4d t_s, Matrix<double,2, 3> u_s, int kx=2, int ku=1){
    double dxdot_du_ = 0;
    int i = 1;
    for(int k=ku; k<kx; k++){
        dxdot_du_ += pow(-1, i) * 0.5 * omega * (exp(omega * (t_s[kx] - t_s[k])) - exp(-omega * (t_s[kx] - t_s[k])));
        i = 0;   // after first term, it is positive
    }
    return dxdot_du_;
}

double dydot_du(Vector4d t_s, Matrix<double,2, 3> u_s, int ky=2, int ku=1){
    double dydot_du_ = 0;
    int i = 1;
    for(int k=ku; k<ky; k++){
        dydot_du_ +=  pow(-1, i) * 0.5 * omega * (exp(omega * (t_s[ky] - t_s[k])) - exp(-omega * (t_s[ky] - t_s[k])));
        i = 0;   // after first term, it is positive
    }
    return dydot_du_;
}

double dJ_dux1(Vector4d t_s, Matrix<double,2, 3> u_s){
    double grad = 0;
    for(int j=2; j<4; j++){
        grad += 2 * dxdot_du(t_s, u_s, j, 1) * (xdot(t_s, u_s, j) - x_dot_ref[0]);}
    return grad;
}


double dJ_duy1(Vector4d t_s, Matrix<double,2, 3> u_s){
    double grad = 0;
    for(int j=2; j<4; j++){
        grad += 2 * dydot_du(t_s, u_s, j, 1) * (ydot(t_s, u_s, j) - x_dot_ref[1]);}
    return grad;
}

double dJ_dux2(Vector4d t_s, Matrix<double,2, 3> u_s){
    double grad = 0;
    for(int j=3; j<4; j++){
        grad += 2 * dxdot_du(t_s, u_s, j, 2) * (xdot(t_s, u_s, j) - x_dot_ref[0]);}
    return grad;
}

double dJ_duy2(Vector4d t_s, Matrix<double,2, 3> u_s){
    double grad = 0;
    for(int j=3; j<4; j++){
        grad += 2 * dydot_du(t_s, u_s, j, 2) * (ydot(t_s, u_s, j) - x_dot_ref[1]);}
    return grad;
}

double dJ_dt0(Vector4d t_s, Matrix<double,2, 3> u_s){
    double grad = 0;
    for(int j=1; j<4; j++){
        grad += 2 * dxdot_dt(t_s, u_s, j, 0) * (xdot(t_s, u_s, j) - x_dot_ref[0]);
        grad += 2 * dydot_dt(t_s, u_s, j, 0) * (ydot(t_s, u_s, j) - x_dot_ref[1]);
    }
    return grad;
}

double dJ_dt1(Vector4d t_s, Matrix<double,2, 3> u_s){
    double grad = 0;
    for(int j=2; j<4; j++){
        grad += 2 * dxdot_dt(t_s, u_s, j, 1) * (xdot(t_s, u_s, j) - x_dot_ref[0]);
        grad += 2 * dydot_dt(t_s, u_s, j, 1) * (ydot(t_s, u_s, j) - x_dot_ref[1]);
    }
    return grad;
}

double dJ_dt2(Vector4d t_s, Matrix<double,2, 3> u_s){
    double grad = 0;
    for(int j=3; j<4; j++){
        grad += 2 * dxdot_dt(t_s, u_s, j, 2) * (xdot(t_s, u_s, j) - x_dot_ref[0]);
        grad += 2 * dydot_dt(t_s, u_s, j, 2) * (ydot(t_s, u_s, j) - x_dot_ref[1]);
    }
    return grad;
}

double gaussian_penalty(Vector2d x, Vector2d mean, double std){
    Vector2d var = x-mean;
    double square_var = var.transpose() * var;
    double penalty = exp(- square_var/ pow(std, 2));
    return penalty;
}

double inverse_gaussian_penalty(Vector2d x, Vector2d mean, double std){
    Vector2d var = x-mean;
    double square_var = var.transpose() * var;
    double penalty = exp(square_var / pow(std, 2));
    return penalty;
}

Vector2d dJ_dux(Vector4d t_s, Matrix<double,2, 3> u_s, Matrix<double,2, 4> x_s, Matrix<double,2, 4> x_dot_s){
    double px1u1 = inverse_gaussian_penalty(x_s.col(1), u_s.col(1), lmax);
    double px2u2 = inverse_gaussian_penalty(x_s.col(2), u_s.col(2), lmax);

    double dpx1u1_dux1 = -(x_s(0, 1) - u_s(0, 1)) * px1u1;

    double dpx2u2_dux1 = (dx_du(t_s, u_s, 2, 1)) * (x_s(0, 2) - u_s(0, 1)) * px2u2;

    double dpx2u2_dux2 = -(x_s(0, 2) - u_s(0, 2)) * px2u2;

    double px2u1 = inverse_gaussian_penalty(x_s.col(2), u_s.col(1), lmax);
    double px3u2 = inverse_gaussian_penalty(x_s.col(3), u_s.col(2), lmax);

    double dpx2u1_dux1 = (dx_du(t_s, u_s, 2, 1) - 1) * (x_s(0, 2) - u_s(0, 1)) * px2u1;

    double dpx3u2_dux2 = (dx_du(t_s, u_s, 3, 2) - 1) * (x_s(0, 3) - u_s(0, 2)) * px3u2;

    double dpx3u2_dux1 = (dx_du(t_s, u_s, 3, 1)) * (x_s(0, 3) - u_s(0, 2)) * px3u2;

    /* double pu1u0 = gaussian_penalty(u_s.col(1), u_s.col(0), rfoot);
    double pu2u1 = gaussian_penalty(u_s.col(2), u_s.col(1), rfoot);

    double dpu1u0_dux1 = -(u_s(0, 1) - u_s(0, 0)) * pu1u0;

    double dpu2u1_dux1 = (u_s(0, 2) - u_s(0, 1)) * pu2u1;

    double dpu2u1_dux2 = -(u_s(0, 2) - u_s(0, 1)) * pu2u1; */

    Vector2d dJ_dux_;
    dJ_dux_ << 2 * (dJ_dux1(t_s, u_s) + k_ux_len * (dpx1u1_dux1 + dpx2u2_dux1 + dpx2u1_dux1 + dpx3u2_dux1)), //+ k_foot * (dpu1u0_dux1 + dpu2u1_dux1)),
               2 * (dJ_dux2(t_s, u_s) + k_ux_len * (dpx2u2_dux2 + dpx3u2_dux2)); //+ k_foot * (dpu2u1_dux2));
    return dJ_dux_;
}


Vector2d dJ_duy(Vector4d t_s, Matrix<double,2, 3> u_s, Matrix<double,2, 4> x_s, Matrix<double,2, 4> x_dot_s){
    double px1u1 = inverse_gaussian_penalty(x_s.col(1), u_s.col(1), lmax);
    double px2u2 = inverse_gaussian_penalty(x_s.col(2), u_s.col(2), lmax);

    double dpx1u1_duy1 = -(x_s(1, 1) - u_s(1, 1)) * px1u1;

    double dpx2u2_duy1 = (dy_du(t_s, u_s, 2, 1)) * (x_s(1, 2) - u_s(1, 1)) * px2u2;

    double dpx2u2_duy2 = -(x_s(1, 2) - u_s(1, 2)) * px2u2;

    double px2u1 = inverse_gaussian_penalty(x_s.col(2), u_s.col(1), lmax);
    double px3u2 = inverse_gaussian_penalty(x_s.col(3), u_s.col(2), lmax);

    double dpx2u1_duy1 = (dy_du(t_s, u_s, 2, 1) - 1) * (x_s(1, 2) - u_s(1, 1)) * px2u1;

    double dpx3u2_duy2 = (dy_du(t_s, u_s, 3, 2) - 1) * (x_s(1, 3) - u_s(1, 2)) * px3u2;

    double dpx3u2_duy1 = (dy_du(t_s, u_s, 3, 1)) * (x_s(1, 3) - u_s(1, 2)) * px3u2;

    /* double pu1u0 = gaussian_penalty(u_s.col(1), u_s.col(0), rfoot);
    double pu2u1 = gaussian_penalty(u_s.col(2), u_s.col(1), rfoot);*/
	
	double pu1u0 = exp(-pow((foot_flag*u_s(1, 0) - u_s(1, 1)), 2)/pow(rfoot, 2));
	double pu2u1 = exp(-pow((foot_flag*u_s(1, 2) - u_s(1, 1)), 2)/pow(rfoot, 2));

    double dpu1u0_duy1 = -(u_s(1, 1) - u_s(1, 0)) * pu1u0;

    double dpu2u1_duy1 = (u_s(1, 2) - u_s(1, 1)) * pu2u1;

    double dpu2u1_duy2 = -(u_s(1, 2) - u_s(1, 1)) * pu2u1;

//    cout << "px1u1" << px1u1 << endl;
//    cout << "px2u2" << px2u2 << endl;
//    cout << "px2u1" << px2u1 << endl;
//    cout << "px3u2" << px3u2 << endl;
//    cout << "pu1u0" << pu1u0 << endl;
//    cout << "pu2u1" << pu2u1 << endl;

    Vector2d dJ_duy_;
    dJ_duy_ << 2 * (dJ_duy1(t_s, u_s) + k_uy_len * (dpx1u1_duy1 + dpx2u2_duy1 + dpx2u1_duy1 + dpx3u2_duy1) + k_foot * (dpu1u0_duy1 + dpu2u1_duy1)),
               2 * (dJ_duy2(t_s, u_s) + k_uy_len * (dpx2u2_duy2 + dpx3u2_duy2) + k_foot * (dpu2u1_duy2));
    return dJ_duy_;
}

Vector3d dJ_dt(Vector4d t_s, Matrix<double,2, 3> u_s, Matrix<double,2, 4> x_s, Matrix<double,2, 4> x_dot_s){
    double dt0 = t_s[1] - t_s[0];
    // dt_0_low = np.sqrt((u_s(0, 1) - swing_x)**2 + (u_s(1, 1) - swing_y)**2) / vmax
    double dt_0_low = max(0.2 - current_time, 0.001);
    double dt_0_high = 2.0;
    double dt1 = t_s[2] - t_s[1];
    // dt_1_low = np.sqrt((u_s(0, 2) - u_s(0, 0))**2 + (u_s(1, 2) - u_s(1, 0))**2) / vmax
    double dt_1_low = 0.4;
    double dt_1_high = 2.0;
    double dt2 = t_s[3] - t_s[2];
    double dt_2_low = 0.4;
    double dt_2_high = 2.0;

    double dpt_dt0 = kdt0 * (exp(dt0 / dt_0_high) - 10 * exp(- (dt0 / dt_0_low)));
    double dpt_dt1 = kdt1 * (exp(dt1 / dt_1_high) - 5 *  exp(- (dt1 / dt_1_low)));
    double dpt_dt2 = kdt2 * (exp(dt2 / dt_2_high) - 25 * exp(- (dt2 / dt_2_low)));

    double px1u1 = inverse_gaussian_penalty(x_s.col(1), u_s.col(1), lmax);
    double px2u2 = inverse_gaussian_penalty(x_s.col(2), u_s.col(2), lmax);

    double dpx1u1_dt0 = (dx_dt(t_s, u_s, 1, 0) * (x_s(0, 1) - u_s(0, 1)) +
                   dy_dt(t_s, u_s, 1, 0) * (x_s(1, 1) - u_s(1, 1))) * px1u1;

    double dpx2u2_dt0 = (dx_dt(t_s, u_s, 2, 0) * (x_s(0, 2) - u_s(0, 2)) +
                   dy_dt(t_s, u_s, 2, 0) * (x_s(1, 2) - u_s(1, 2))) * px2u2;

    double dpx2u2_dt1 = (dx_dt(t_s, u_s, 2, 1) * (x_s(0, 2) - u_s(0, 2)) +
                   dy_dt(t_s, u_s, 2, 1) * (x_s(1, 2) - u_s(1, 2))) * px2u2;

    double px2u1 = inverse_gaussian_penalty(x_s.col(1), u_s.col(1), lmax);
    double px3u2 = inverse_gaussian_penalty(x_s.col(2), u_s.col(2), lmax);

    double dpx2u1_dt0 = (dx_dt(t_s, u_s, 2, 0) * (x_s(0, 2) - u_s(0, 1)) +
                   dy_dt(t_s, u_s, 2, 0) * (x_s(1, 2) - u_s(1, 1))) * px2u1;

    double dpx3u2_dt0 = (dx_dt(t_s, u_s, 3, 0) * (x_s(0, 3) - u_s(0, 2)) +
                   dy_dt(t_s, u_s, 3, 0) * (x_s(1, 3) - u_s(1, 2))) * px3u2;

    double dpx2u1_dt1 = (dx_dt(t_s, u_s, 2, 1) * (x_s(0, 2) - u_s(0, 1)) +
                   dy_dt(t_s, u_s, 2, 1) * (x_s(1, 2) - u_s(1, 1))) * px2u1;

    double dpx3u2_dt1 = (dx_dt(t_s, u_s, 3, 1) * (x_s(0, 3) - u_s(0, 2)) +
                   dy_dt(t_s, u_s, 3, 1) * (x_s(1, 3) - u_s(1, 2))) * px3u2;

    double dpx3u2_dt2 = (dx_dt(t_s, u_s, 3, 2) * (x_s(0, 3) - u_s(0, 2)) +
                   dy_dt(t_s, u_s, 3, 2) * (x_s(1, 3) - u_s(1, 2))) * px3u2;
    Vector3d dJ_dt_;

    dJ_dt_ << 2 * (dJ_dt0(t_s, u_s) + dpt_dt0 + k_t_len * (dpx1u1_dt0 + dpx2u2_dt0 + dpx2u1_dt0 + dpx3u2_dt0)),
              2 * (dJ_dt1(t_s, u_s) + dpt_dt1 + k_t_len * (dpx2u2_dt1 + dpx2u1_dt1 + dpx3u2_dt1)),
              2 * (dJ_dt2(t_s, u_s) + dpt_dt2 + k_t_len * (dpx3u2_dt2));

    return dJ_dt_;
}

double J(Vector4d t_s, Matrix<double,2, 3> u_s){
    double J = 0.0;
    for(int i=1; i<u_s.size()/2+1; i++){
        J += pow(xdot(t_s, u_s, i)-x_dot_ref[0],2) + pow(ydot(t_s, u_s, i)-x_dot_ref[1],2);
    }
    return J;
 }

optimal gradient_descent(Vector4d& t_s, Matrix<double,2, 3>& u_s, int iter=max_iter, bool verbose=true){
    double update = 1.0;
    int i = 0;
    Matrix<double,2, 4> x_s, x_dot_s;
    for(int k=0; k<4; k++){
        x_s(0,k) = x(t_s, u_s, k);
        x_s(1,k) = y(t_s, u_s, k);
        x_dot_s(0,k) = xdot(t_s, u_s, k);
        x_dot_s(1,k) = ydot(t_s, u_s, k);
    }
    Vector3d grad_t = dJ_dt(t_s, u_s, x_s, x_dot_s);
    Vector2d grad_ux = dJ_dux(t_s, u_s, x_s, x_dot_s);
    Vector2d grad_uy = dJ_duy(t_s, u_s, x_s, x_dot_s);
    cout << "t  grad" << grad_t << endl;
    cout << "ux grad" << grad_ux << endl;
    cout << "uy grad" << grad_uy << endl;
    while (update > 0.5 * (eta_u + eta_t)){
    // for iteration in range(iter):
        for(int k=0; k<4; k++){
            x_s(0,k) = x(t_s, u_s, k);
            x_s(1,k) = y(t_s, u_s, k);
            x_dot_s(0,k) = xdot(t_s, u_s, k);
            x_dot_s(1,k) = ydot(t_s, u_s, k);
        }
        grad_t = dJ_dt(t_s, u_s, x_s, x_dot_s);
        grad_ux = dJ_dux(t_s, u_s, x_s, x_dot_s);
        grad_uy = dJ_duy(t_s, u_s, x_s, x_dot_s);
        VectorXd grad(7);
        grad.segment(0, 3) = grad_t * eta_t;
        grad.segment(3,2)  = grad_ux * eta_u;
        grad.segment(5,2) = grad_uy * eta_u;
        grad = grad.cwiseAbs();
        update = grad.sum();
        // grad = np.concatenate((grad_t, grad_ux, grad_uy))
        // update = np.sum(abs(np.concatenate((grad[:3] * eta_t, grad[3:] * eta_u))));
        t_s.segment(1,3) -= eta_t * grad_t;
        u_s.block(0,1,1,2) -= eta_u * grad_ux.transpose();
        u_s.block(1,1,1,2) -= eta_u * grad_uy.transpose();
        i += 1;
        if (i > iter) {break;}
    }
    // print(i)
    cout<<"iteration is :" << i << endl;
    // print('grad_t \t \t \t', grad_t)
    // print('grad_ux \t \t', grad_ux)
    // print('grad_uy \t \t', grad_uy)
    // print('cost', J(t_s, u_s))
    // print('t \t \t', t_s)
    // print('ux \t \t', u_s(0, :])
    // print('uy \t \t', u_s(1, :])
    // print('velx \t \t', [xdot(t_s, u_s, kx=k) for k in range(len(t_s))])
    // print('vely \t \t', [ydot(t_s, u_s, ky=k) for k in range(len(t_s))])
    optimal result;
    result.time = t_s; result.u_s = u_s;
    return result;
}

// Callback function for receiving the planner outputs
void footstepPlanCallbackRK4(const std_msgs::Float64MultiArray::ConstPtr& msg)
{
    u_s(0,1) = msg->data[0];
    u_s(1,1) = msg->data[1];
    u_s(0,2) = msg->data[2];
    u_s(1,2) = msg->data[3];
    t_s(1) = msg->data[4];
    t_s(2) = msg->data[5];
    t_s(3) = msg->data[6];
//    support_foot_flag = msg->data[7];
    flag = 1;
    cout<< "received u \n" << u_s << endl;
    cout<< "received t \n" << t_s << endl;
}

void plannerInputCallback(const std_msgs::Float64MultiArray::ConstPtr& msg)
{
//    x, x_dot, y, y_dot = msg.data[:4]
    x_0(0) = msg->data[0];
    x_0(1) = msg->data[1];
    x_dot_0(0) = msg->data[2];
    x_dot_0(1) = msg->data[3];
    left_x  = msg->data[4];
    left_y  = msg->data[5];
    right_x = msg->data[6];
    right_y = msg->data[7];
    foot_flag = msg->data[8];
    current_time = msg->data[9];
}

int main(int argc, char ** argv){
    // Vector4d t_s(0.0, 0.5, 1.0, 1.5);      // initial guess
    // Vector3d u_s(0.0, -0.01, 0.02/0);   // step locations
//    double x_0 = 0.0;                      // initial COM position
//    double x_dot_0 = 0.0;                  //initial COM velocity
    Vector4d x_opt,y_opt;
    optimal opt;
    const double LOOP_RATE = 100;
    int iter_main = 0;
    ros::init(argc, argv, "gradient_descent_footPlanner_node");
    ros::NodeHandle n;
    ros::Rate loop_rate(LOOP_RATE);
    ros::Publisher planner_output_pub = n.advertise<std_msgs::Float64MultiArray>("/time_slider_gazebo/footstep_plan", 1);
    ros::Subscriber sub_planner_input = n.subscribe<std_msgs::Float64MultiArray>("/time_slider_gazebo/planner_input", 'None', plannerInputCallback);
    ros::Subscriber sub_RK4_planner_input = n.subscribe<std_msgs::Float64MultiArray>("/time_slider_gazebo/footstep_plan_rk4", 'None', footstepPlanCallbackRK4);
    support_foot_flag = foot_flag;

    while (ros::ok()){

        auto t1 = std::chrono::high_resolution_clock::now();
        // for (int i=0; i <max_iter; i+=2){
//        cout << "u ini is \t" << u_s << endl;
//        cout << "t ini is \t" << t_s << endl;
//        cout << "current robot COM pos is \t" << x_0 << endl;
//        cout << "current robot COM vel is \t" << x_dot_0 << endl;
        ////////////// Gradien Descent ////////////////////////////////
        opt = gradient_descent(t_s, u_s);
        // }
        auto t2 = chrono::high_resolution_clock::now();
        auto duration = chrono::duration_cast<chrono::microseconds>( t2 - t1 ).count();
//        cout << "Duration: \n" << duration << " microseconds" << endl;

        iter_main += 1;
//        cout << "iteration in main loop is \t" << iter_main << endl;
//        cout << "u opt is \t" << opt.u_s << endl;
//        cout << "t opt is \t" << opt.time << endl;

        // publish
        if(support_foot_flag == foot_flag && opt.time(1) >= 1/LOOP_RATE){
            t_s = opt.time;
            u_s = opt.u_s;
            std_msgs::Float64MultiArray footstep_plan;
            footstep_plan.data.push_back(u_s(0,1));
            footstep_plan.data.push_back(u_s(1,1));
            footstep_plan.data.push_back(u_s(0,2));
            footstep_plan.data.push_back(u_s(1,2));
            footstep_plan.data.push_back(t_s(1)-t_s(0));
            footstep_plan.data.push_back(support_foot_flag);
            planner_output_pub.publish(footstep_plan);
            if(flag==0){
                t_s(1) -= 1/LOOP_RATE;
                t_s(2) -= 1/LOOP_RATE;
                t_s(3) -= 1/LOOP_RATE;
            }
            else if (flag==1){flag = 0;}
        } else {
            cout << "Not publishing -------------------" << endl;
        }

        if (foot_flag == -1){
            u_s(0,0) = left_x;
            u_s(1,0) = left_y;
        }
        else if (foot_flag == 1){
            u_s(0,0) = right_x;
            u_s(1,0) = right_y;
        }
        if(support_foot_flag != foot_flag){
            t_s(1) = opt.time(2);
            t_s(2) = opt.time(3);
            t_s(3) = opt.time(3) + opt.time(3) - opt.time(2);
            u_s(0,0) = opt.u_s(0,1);
            u_s(1,0) = opt.u_s(1,1);
            u_s(0,1) = opt.u_s(0,2);
            u_s(1,1) = opt.u_s(1,2);
            u_s(0, 2) = opt.u_s(0,2) + (opt.u_s(0,2) - opt.u_s(0, 1));
            u_s(1, 2) = opt.u_s(1,2) + (opt.u_s(1,2) - opt.u_s(1, 1));
            current_time = 0.0;
            support_foot_flag = foot_flag;
            cout << "switching !!!!!!!!!!!!!!!!!!!!!!!!!!1!!!!!!!!!!!!" << endl;
        }
        // assign prev optimal as initial guess of next iteration


        cout << "planned foot \t" << support_foot_flag << endl;
        cout << "real foot \t" << foot_flag << endl;
        cout << "current time is \t"<< current_time << endl;

//        for (int i = 0; i < 4; i++)
//        {
//            x_opt[i] = x(opt.time, opt.u_s, i);
//            y_opt[i] = y(opt.time, opt.u_s, i);
//        }
        // cout << "x_opt is \n" << x_opt << endl;
        // cout << "y_opt is \n" << y_opt << endl;


        ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;}

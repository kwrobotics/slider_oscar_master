#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue May 26 14:27:03 2020
 MPC in z_direction
@author: antraes
"""

import math
import numpy as np
from qpsolvers import solve_qp
import time
from scipy.linalg import block_diag
from casadi import *

class Robot:
    def __init__(self,g,k,m,Ts,Upward_velocity,w):
        self.g = g
        self.k = k
        self.m = m
        self.Ts = Ts
        self.Upward_velocity = Upward_velocity
        self.w = w
    def gen_offline_matrices(self):
        w = self.w
        A = np.array([(0,1),(w*w,0)])
        B = np .array([(0),(-np.square(w))])
        ch = math.cos(w*self.Ts)
        sh = math.sin(w*self.Ts)
        self.A = np.array([[ch,sh/w],[-w*sh,ch]])
        self.B = np.array([[1-ch],[w*sh]])
class Current_state:
    def __init__(self,z_hat,p0_z,remaining_time):
         self.z_hat = z_hat
         self.p0_z = p0_z
         self.remaining_time = remaining_time
class MPCparamater:
    def __init__(self,N_steps,Q,Wz,R,P,Robot,Current_state,step_time):
        self.N_steps = N_steps
        self.Q = np.diag(np.array([0,3]))
        self.Wz = np.array([5])
        self.R = 100
        self.P = Q
        self.Ns = int(step_time/Robot.Ts)
        self.Nr = int(round(Current_state.remaining_time/Robot.Ts))
        self.N = self.Nr+self.N_steps*self.Ns
    def genRefCoMTraj(self,Robot,Current_state):
        z_ref = np.zeros((2*self.N,1))
        Times = Robot.Ts/0.01
        z_vel_desired = np.load('/home/robin-lab/SLIDER_ARTO/src/slider_controller/src/z_desired_vel.npy')
        for i in range(1,self.N+1):
            #z_ref[2*i-2,0] = 0.7
            if i < self.Nr:
                z_ref[2*i-1,0] = z_vel_desired[-int(Times*(self.Nr-i)),0]
            else:
                z_ref[2*i-1,0] = z_vel_desired[int(Times*(i-self.Nr)),0]
        self.z_ref = z_ref    
    def genPredictionMatrices(self,Gamma_z,Phi_z):
##input A,B D.T. state matrices x[k+1] = Ax[k]+Bu[k]
## N = horizon length
#        [n,n0] = np.shape(Robot.B)
#        F = np.kron(np.eye(self.N),np.eye(n))+np.kron(np.diag(np.ones(self.N-1),k=-1),-Robot.A)
#        G = np.kron(np.eye(self.N,M=1),Robot.A)
#        H = np.kron(np.eye(self.N),Robot.B)
#        Phi = np.dot(np.linalg.inv(F),G)
#        Gamma = np.dot(np.linalg.inv(F),H)
        self.Phi = Phi_z
        self.Gamma =Gamma_z
    ## def Constrain:
    
    def genCostMatrices(self,Robot,Current_state,Bt,Cz,d_z):
        R_bar = np.kron(np.eye(self.N),self.R)
        I = np.eye(self.N)
        I[self.N-1][self.N-1] = 0
        O = np.zeros((self.N,self.N))
        O[self.N-1][self.N-1] = 1
        Q_bar = np.kron(I,self.Q)+np.kron(O,self.P)
        L = np.tril(np.ones((self.N,self.N)))
        G = (np.dot(self.Phi.T,np.dot((Q_bar.T+Q_bar),self.Gamma))).T
        Gx = (np.dot((Q_bar.T+Q_bar),self.Gamma)).T
        Gu = (R_bar.T+R_bar).T
        Hu = 2*(R_bar+np.dot(self.Gamma.T,np.dot(Q_bar,self.Gamma)))    
        Hp = 2*(np.dot(Bt.T,np.dot(L.T,np.dot(R_bar,np.dot(L,Bt)))))+2*self.Wz
        Hpu = -np.dot(Bt.T,np.dot(L.T,Gu.T))
        fu = np.dot(G,Current_state.z_hat)-np.dot(Gx,self.z_ref)-np.dot(Gu,Cz)
        a = np.size(self.Wz)    
        if a == 1:
            fp = np.dot(np.dot(Bt.T,L.T),np.dot(Gu,Cz))-2*float(self.Wz)*d_z
        else:
            fp = np.dot(np.dot(Bt.T,L.T),np.dot(Gu,Cz))-np.dot(d_z,self.Wz+self.Wz.T)
        Hz = np.vstack((np.hstack((Hu,Hpu.T)),np.hstack((Hpu,Hp))))
        fz = np.vstack((fu,fp))
        (a,b) = np.shape(fz)
        fz = np.reshape(fz,(a,))
        self.Hz =Hz
        self.fz =fz
    
def mpcPlanner_z(Robot,Current_state,MPCparameter,Gamma_z,Phi_z):
    B0 = np.vstack((np.ones((MPCparameter.Nr,1)),np.zeros((MPCparameter.N_steps*MPCparameter.Ns,1))))
    Bt = np.zeros((MPCparameter.N,MPCparameter.N_steps))
    for i in range(1,MPCparameter.N_steps+1):
        index = 1+MPCparameter.Nr+(i-1)*MPCparameter.Ns
        Bt[index-1:index+MPCparameter.Ns-1,i-1] = np.ones((MPCparameter.Ns,))
    #an constant matrix in U_ref
#    L = np.tril(np.ones((MPCparameter.N,MPCparameter.N)))
    Cz = Current_state.p0_z*np.ones((MPCparameter.N,1))
    #Reference of footstep difference between two consecutive steps
    if Current_state.remaining_time == 0.0:
        Bt = Bt
        d_z = np.zeros((MPCparameter.N_steps,1))
    else:
        Bt = np.hstack((B0,Bt))
        d_z = np.zeros((MPCparameter.N_steps+1,1))
    #d_z = np.zeros((MPCparameter.N_steps,1))
    #Reference CoM Trajctories
    MPCparameter.genRefCoMTraj(Robot,Current_state)
    MPCparameter.genPredictionMatrices(Gamma_z,Phi_z)
    MPCparameter.genCostMatrices(Robot,Current_state,Bt,Cz,d_z)
    ##Direct Calculation
#    Uopt_x = np.linalg.solve(Hx, -fx1) 
#    Uopt_y = np.linalg.solve(Hy, -fy1)
#    MPCparameter.genConstrain(Robot,Current_state)
    ##QP interface
    solver = "quadprog"
    #start = time.time()
    #print(np.shape(MPCparameter.Hz))
    #print(np.shape(MPCparameter.fz))
    case = {'P': MPCparameter.Hz, 'q': MPCparameter.fz}
    Uopt = solve_qp(solver = solver,**case)
    #Uopt = Current_state.p0_z*np.ones((MPCparameter.N,1))+np.dot(L,(1/7)*0.00*math.tan(10.0/180.0*3.14159)/2*np.ones((MPCparameter.N,1)))
    #end = time.time()
    #print('QP TIME'+str(end-start))
    return Uopt

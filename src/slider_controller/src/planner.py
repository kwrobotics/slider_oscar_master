# This is the MPC planner, which can generate ZMP trajectory.
# Note that both the ZMP and foot placements are chosen by the QP
import time
import numpy as np
import math 
from qpsolvers import solve_qp
from scipy.linalg import block_diag
from casadi import *

def genLIPM(g,Zc,Ts): 
    # g = gravitational acceleration 
    # Zc = constant CoM height
    # Ts = Sample time of the discrete-time system
    # output: state space matrices of a discrete-time or continuous-time model

    # C.T. Model
    w = math.sqrt(g/Zc);
    A = np.array([[0,1],[w*w,0]])
    B = np.array([[0],[-np.square(w)]])
    C = np.eye(2)
    D = np.zeros((2,1))

    # D.T Model analytical solution
    if (Ts > 0):
        ch = np.cosh(w*Ts)
        sh = np.sinh(w*Ts)
        A = np.array([[ch,sh/w],[w*sh,ch]])
        B = np.array([[1-ch],[-w*sh]])
    return A,B

def genStep(N_steps,N):
    step = []
    for i in range(1,N_steps+1):
        temp = np.zeros((2,2*N+2*N_steps))
        temp[0,N+i-1]= 1 
        temp[1,2*N+N_steps+i-1] = 1
        step.append(temp)
    return step
def genPredictionMatrices(A,B,N):
    # A,B are the discrete-time state matrices x[k+1] = Ax[k]+Bu[k]
    # N is the horizon length
    [n,n0] = np.shape(B)
    F = np.kron(np.eye(N),np.eye(n))+np.kron(np.diag(np.ones(N-1),k=-1),-A)
    G = np.kron(np.eye(N,M=1),A)
    H = np.kron(np.eye(N),B)
    Phi = np.dot(np.linalg.inv(F),G)
    Gamma = np.dot(np.linalg.inv(F),H)
    return (Gamma,Phi)
def genCostMatrices(Gamma,Phi,Q,R,N,N_steps,Bt,W,C,d,x0,X_ref):
    # Gamma and Phi are the prediction matrices
    # Q is the weight on the tracking of CoM state
    # R is the weight on the ZmP tracking
    # P is the terminal weight on the final state
    # N is the prediction horizon
    # N_steps is the walking steps in the prediction horizon
    # W is the weight on the tracking of foot difference
    # d is the reference of the footstep difference
    # x0 is the estimated CoM state
    # X_ref is the Reference CoM state
    P = Q
    R_bar = np.kron(np.eye(N),R)
    I = np.eye(N)
    I[N-1][N-1] = 0
    O = np.zeros((N,N))
    O[N-1][N-1] = 1
    Q_bar = np.kron(I,Q)+np.kron(O,P)
    L = np.tril(np.ones((N_steps,N_steps)))
    G = (np.dot(Phi.T,np.dot((Q_bar.T+Q_bar),Gamma))).T
    Gx = (np.dot((Q_bar.T+Q_bar),Gamma)).T
    Gu = (R_bar.T+R_bar).T
    Hu = 2*(R_bar+np.dot(Gamma.T,np.dot(Q_bar,Gamma)))
    Hp = 2*(np.dot(L.T,np.dot(Bt.T,np.dot(R_bar,np.dot(Bt,L)))))+2*W
    Hpu = -np.dot(L.T,np.dot(Bt.T,Gu.T))
    fu = np.dot(G,x0)-np.dot(Gx,X_ref)-np.dot(Gu,C)
    a = np.size(W)
    if a == 1:
        fp = np.dot(np.dot(L.T,Bt.T),np.dot(Gu,C))-2*float(W)*d
    else:
        fp = np.dot(np.dot(L.T,Bt.T),np.dot(Gu,C))-np.dot(d,W+W.T)
    H = np.vstack((np.hstack((Hu,Hpu.T)),np.hstack((Hpu,Hp))))
    f = np.vstack((fu,fp))
    return H,f


def genConstarins(N,Ns,N_steps,Nr,p0):
    L = np.tril(np.ones((N_steps,N_steps)))
    L_augmented_with_Ns = np.kron(L,np.ones((Ns,1)))
    zero = np.zeros((Nr,N_steps))
    L_bar = np.vstack((zero,L_augmented_with_Ns))
    G = np.hstack((np.identity(N),-L_bar)) 
    h = p0*np.ones((N,1))
    return G,h


def genRefCoMTraj(forward_velocity,lateral_velocity,N):
    x_ref = np.zeros((2*N,1))
    y_ref = np.zeros((2*N,1))
    for i in range(1,N+1):
        x_ref[2*i-1,0] = forward_velocity
        y_ref[2*i-1,0] = lateral_velocity
    return x_ref,y_ref

def ObstacleMatrices(Obstacle,N_steps,N,p0_x,p0_y):
    A1 = np.hstack((np.zeros((N_steps,N)),np.identity(N_steps)))
    A1 = np.vstack((A1,np.zeros((N_steps,N+N_steps))))
    A2 = np.hstack((np.zeros((N_steps,N)),np.identity(N_steps)))
    A2 = np.vstack((np.zeros((N_steps,N+N_steps)),A2))
    A = np.hstack((A1,A2))
    step = []
    for i in range(N_steps):
        Step = np.zeros((1,N_steps))
        Step[0,0:i+1] = np.ones((1,i+1))
        Step = np.kron(np.identity(2),Step)
        step.append(np.dot(Step,A))
    Obs_distance = np.array([[Obstacle.x0-p0_x],[Obstacle.y0-p0_y]])
    Obs_step = step
    return Obs_distance,Obs_step

def mpcPlanner(x_hat,y_hat,p0_x,p0_y,support_foot,Ns,Nr,N_steps,N,inter_feet_clearance,forward_velocity,lateral_velocity,Qx,Qy,Rx,Ry,Wx,Wy,step_length,Obstacle,Gamma,Phi):
    #start0 = time.time()
    N = Nr+N_steps*Ns
    B0 = np.vstack((np.ones((Nr,1)),np.zeros((N_steps*Ns,1))))
    Bt = np.zeros((N,N_steps))
    for i in range(1,N_steps+1):
        index = 1+Nr+(i-1)*Ns
        Bt[index-1:index+Ns-1,i-1] = np.ones((Ns,))

    # Constant matrix in U_ref
    Cx = p0_x*B0+np.dot(Bt,(p0_x*np.ones((N_steps,1))))
    Cy = p0_y*B0+np.dot(Bt,(p0_y*np.ones((N_steps,1))))

    # Reference of footstep difference between two consecutive steps
    d_x = step_length*np.ones((N_steps,1))
    d_y = support_foot*inter_feet_clearance*np.ones((N_steps,1))
    for i in range(1,N_steps+1):
        d_y[i-1,0] = (-1)**(i+1)*d_y[i-1,0]

    # Reference CoM Trajctories
    x_ref,y_ref = genRefCoMTraj(forward_velocity,lateral_velocity,N)
    #start1 = time.time()
    #print("ini 1 "+str(start1-start0))
    #(Gamma,Phi) = genPredictionMatrices(A,B,N)
    #start3 = time.time()
    #print("Predication matrices time "+str(start3-start1))
    Hx,fx = genCostMatrices(Gamma,Phi,Qx,Rx,N,N_steps,Bt,Wx,Cx,d_x,x_hat,x_ref)
    Hy,fy = genCostMatrices(Gamma,Phi,Qy,Ry,N,N_steps,Bt,Wy,Cy,d_y,y_hat,y_ref)
    #start2 = time.time()
    #print("ini 2 " +str(start2 - start1))
    (a1,b1) = np.shape(fx)
    fx = np.reshape(fx,(a1,1))
    (a2,b2) = np.shape(fy)
    fy = np.reshape(fy,(a2,1))
#    start = time.time()
#    #print("initialize time"+str(start-start0))
#    Uoptx = np.linalg.solve(Hx, -fx)
#    Uopty = np.linalg.solve(Hy, -fy)
    #end1 = time.time()
#    print("np time"+ str(end1-start))
    solver = "quadprog"
    fx1 = np.reshape(fx,(a1,))
    fy1 = np.reshape(fy,(a2,))
    case_x = {'P': Hx, 'q': fx1}
    Uoptx = solve_qp(solver = solver,**case_x)
    case_y = {'P': Hy, 'q': fy1}
    Uopty = solve_qp(solver = solver,**case_y)
    #end = time.time()
    #print("qp solving time" +str(end-end1))
    H = block_diag(Hx,Hy)
    f = np.vstack((fx,fy))
    (a,) = np.shape(Uoptx)
#    Gx,hx = genConstarins(N,Ns,N_steps,Nr,p0_x)
#    Gy,hy = genConstarins(N,Ns,N_steps,Nr,p0_y)
#    G = block_diag(Gx,Gy)
#    h = np.vstack((hx,hy))
#    G_final = np.vstack((G,-G))
#    constant = np.vstack((0.5*0.205*np.ones((N,1)),0.5*0.15*np.ones((N,1))))
#    h_final = np.vstack((constant-h,constant+h))
    #solver = "quadprog"
    #(a3,b3) = np.shape(f)
    #f = np.reshape(f,(a3,))
#    (a2,b2) = np.shape(h_final)
#    h_final = np.reshape(h_final,(a2,))
    #case = {'P': H, 'q': f, 'G':G_final,'h':h_final}
    #case = {'P': H, 'q': f}
    #Uopt = solve_qp(solver = solver,**case)
    #Uoptx = Uopt[0:N+N_steps]
    #Uopty = Uopt[N+N_steps:2*(N+N_steps)]
    feasible = 1
    feasible_obs = 1
    Uopt_x = np.reshape(Uoptx,(a,1))
    Uopt_y = np.reshape(Uopty,(a,1))    
    Uopt = np.vstack((Uopt_x,Uopt_y))
    Obs_distance,Obs_step = ObstacleMatrices(Obstacle,N_steps,N,p0_x,p0_y)
    
#    opti = casadi.Opti()
#    x = opti.variable(2*N+2*N_steps,1)
#    opti.minimize(0.5 *mtimes(x.T,mtimes(H_casadi,x))+mtimes(f_casadi.T,x))
#    soption = {"error_on_fail":False,"expand":True}    
#    option = {"print_level":0}
#    opti.solver('ipopt',soption,option)
#    start = time.time()
#    sol  = opti.solve()
#    Uopt = sol.value(x)
#    end = time.time()
#    print("casadi",end-start)
    for i in range(1,N_steps):
        delta_px = Uoptx[N+i]
        delta_py = Uopty[N+i]
        temp =  np.dot(np.dot(Obs_step[i],Uopt).T,np.dot(Obs_step[i],Uopt))-2*np.dot(Obs_distance.T,np.dot(Obs_step[i],Uopt))+np.dot(Obs_distance.T,Obs_distance)
        if temp < Obstacle.R_final*Obstacle.R_final:
            feasible_obs = 0
        if (abs(delta_px) <= 0.205) and (abs(delta_py) <= 0.15): #footlegnth 0.205 foot width 0.15
            feasible = 0;
    #feasible =0;
    #print("fleasible_flag",feasible)
    #print("feasible_obs_flag",feasible_obs)
    if feasible == 1 and feasible_obs == 1:
#        print("delta_x")
#        print(Uoptx[N:N+N_steps])
#        print("delta_y")
#        print(Uopty[N:N+N_steps])
        #end1 = time.time()
        #print("print process",str(end1-end))
        return Uoptx, Uopty 
    if feasible_obs == 0:

        H_casadi = MX(H)
        f_casadi = MX(f)
        opti = casadi.Opti()
        x = opti.variable(2*N+2*N_steps,1)
        opti.minimize(0.5 *mtimes(x.T,mtimes(H_casadi,x))+mtimes(f_casadi.T,x))
        step = genStep(N_steps,N)
        distance_casadi = MX(Obs_distance)
        step_y_casadi = MX(np.reshape(np.array(([0,1])),(1,2)))
        for i in range(N_steps):
            temp = MX(step[i])
            #opti.subject_to(mtimes(mtimes(temp,x).T,mtimes(temp,x))>=(0.205**2+0.15**2)) #0.205 footlenghth
            num = support_foot*(-1)**i
            opti.subject_to(mtimes(num,(mtimes(step_y_casadi,mtimes(temp,x)))) >= 0.2)
            opti.subject_to(mtimes(num,(mtimes(step_y_casadi,mtimes(temp,x)))) <= 0.32)
            opti.subject_to(mtimes(mtimes(temp,x).T,mtimes(temp,x)) <= 0.8 )##0.8 = L^2-H^2
            temp2 = MX(Obs_step[i])
            opti.subject_to(mtimes(mtimes(temp2,x).T,mtimes(temp2,x))-2*mtimes(mtimes(distance_casadi.T,temp2),x)+mtimes(distance_casadi.T,distance_casadi)>=Obstacle.R_final*Obstacle.R_final)
        options = {"ipopt.print_level": 0, "print_out": False, "print_in": False, "print_time": False, "ipopt.hessian_approximation":"limited-memory", "ipopt.max_iter":500, "ipopt.warm_start_init_point": "yes"} # "verbose": True, ,
        #option = {"print_iteration":False,"print_header":False,"print_status":False,"tol_du":0.1, "max_iter":4}
        opti.solver('ipopt',options)
        
        sol  = opti.solve()
        Uopt = sol.value(x)
        #end4 = time.time()
        #opti.set_initial(x[Nr:N+N_steps],Uopt[Nr:N+N_steps])
        #opti.set_initial(x[Nr+N+N_steps:2*N+2*N_steps],Uopt[Nr+N+N_steps:2*N+2*N_steps])
        Uoptx = Uopt[0:N+N_steps]
        Uopty = Uopt[N+N_steps:2*(N+N_steps)]
        #end3 = time.time()
        #print(end3-start)
        #print(end4-start)
        
        
        
#        print("delta_x")
#        print(Uoptx[N:N+N_steps])
#        print("delta_y")
#        print(Uopty[N:N+N_steps])
        return Uoptx, Uopty    
    if feasible == 0:
        #start = time.time()
        H_casadi = MX(H)
        f_casadi = MX(f)
        opti = casadi.Opti()
        x = opti.variable(2*N+2*N_steps,1)
        opti.minimize(0.5*mtimes(x.T,mtimes(H_casadi,x))+mtimes(f_casadi.T,x))
        step = genStep(N_steps,N)
        step_y_casadi = MX(np.reshape(np.array(([0,1])),(1,2)))
        for i in range(N_steps):
            temp = MX(step[i])
            num = support_foot*(-1)**i
            #print(num)
            ####### zhijieyong x[]**2+x[]**2 < = 0.8
            opti.subject_to(mtimes(mtimes(temp,x).T,mtimes(temp,x)) <= 0.8)
            #opti.subject_to(mtimes(mtimes(temp,x).T,mtimes(temp,x))>=0.205**2+0.15**2) #0.205 footlenghth
            opti.subject_to(mtimes(num,(mtimes(step_y_casadi,mtimes(temp,x)))) >= 0.2)
            opti.subject_to(mtimes(num,(mtimes(step_y_casadi,mtimes(temp,x)))) <= 0.3)            
        options = {"ipopt.print_level": 0, "print_out": False, "print_in": False, "print_time": False, "ipopt.warm_start_init_point": "yes","expand":True}       # "verbose": True, ,
        opti.set_initial(x[0:N],Uoptx[0:N])
        opti.set_initial(x[N+N_steps:2*N+N_steps],Uopty[0:N])
        opti.solver('ipopt',options)
        
        
        #options = {"print_out": False, "print_in": False, "print_time": False,"sqpmethod.qpsol":"qpoases"}
        #soption = {{"print_header": False},{"print_iteration":False,"print_status":False}
        #
#        opti.print_header = False
#        opti.print_iteration = False
#        opti.print_status =False
        #opti.solver('sqpmethod',options)
        
        
        #end4 = time.time()
        #opti.set_initial(x[Nr:N+N_steps],Uopt[Nr:N+N_steps])
        #opti.set_initial(x[Nr+N+N_steps:2*N+2*N_steps],Uopt[Nr+N+N_steps:2*N+2*N_steps])
        #start = time.time()
        sol  = opti.solve()
        Uopt = sol.value(x)
        #end = time.time()
        #print("casadi time " + str(end-start))
        Uoptx = Uopt[0:N+N_steps]
        Uopty = Uopt[N+N_steps:2*(N+N_steps)]
#        print("delta_x")
#        print(Uoptx[N:N+N_steps])
#        print("delta_y")
#        print(Uopty[N:N+N_steps])
        #end3 = time.time()
        #print(end3-start)
        #print(end4-start)
        return Uoptx, Uopty
        
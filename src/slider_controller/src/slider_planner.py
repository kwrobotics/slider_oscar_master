#!/usr/bin/env python
import rospy
from planner import *
from z_planner import SLIP
import time
from obstacle import Obstacle
# from slider_controller.msg import FloatArray
from std_msgs.msg import Float64MultiArray
import math
import numpy as np
from planner_z import *
from planner_z import Current_state
# Parameters setting of LIP model
g = 9.8
Zc = 0.69 # CoM height
Ts = 0.1 # sampling time

# Parameters setting of walking 
forward_velocity = 0.0
lateral_velocity = 0.05
slope_angle = 0.0/180.0*3.14159
# foot_length = 0.205
# foot_width = 0.15
step_time = 0.7
step_length = forward_velocity*step_time
inter_feet_clearance = 0.25
# Parameters of MPC planner
N_steps = 4 # number of walking steps in the prediction horizon
Qx = np.diag(np.array([0,3]))*5 # weighting matrix on the CoM state
Qy = np.diag(np.array([0,3]))*2  #5 # weighting matrix on the CoM state
Wx = np.array([10]) # weighting matrix on tracking the difference of consecutive footsteps
Wy = np.array([20]) # weighting matrix on tracking the difference of consecutive footsteps
Rx = 300 # weighting matix on the ZMP tracking
Ry = 100 # weighting matix on the ZMP tracking

# 0.6 step time: 5,10,50
# 0.8 step time: 3,15,100

########### Parameters subscribed from the slider_controller node #############################
x_hat = np.array([[0.0],[0.0]]) # estimated current CoM position and velocity in the x direction
y_hat = np.array([[0.0],[0.0]])   # estimated current CoM position and velocity in the y direction
z_hat = np.array([[Zc],[0.0]])   # estimated current CoM position and velocity in the z direction

remaining_time = step_time # remaining time for the current step
p0_x = 0.0 # current foot position x direction
p0_y = inter_feet_clearance/2 # current foot position y direction
support_foot = -1 # right 1, left -1
###############################################################################################

def callback(msg):
    global x_hat, y_hat, z_hat, remaining_time, p0_x, p0_y, support_foot
    x_hat = np.array([[msg.data[0]],[msg.data[1]]]) # estimated current CoM position and velocity in the x direction
    x_dotdot = msg.data[2]
    y_hat = np.array([[msg.data[3]],[msg.data[4]]])   # estimated current CoM position and velocity in the y direction
    y_dotdot = msg.data[5]
    z_hat = np.array([[Zc],[0.0]])   
    left_x, left_y = msg.data[6:8]
    right_x, right_y = msg.data[8:10]
    support_foot = msg.data[10]
    current_time = msg.data[11]
    remaining_time = step_time - current_time
    if support_foot == -1:
        p0_x = left_x # current support foot 
        p0_y = left_y
    elif support_foot == 1:
        p0_x = right_x
        p0_y = right_y
    

def sliderPlanner():
    # Initialize this node
    rospy.init_node('ros_SLIDER_planner_node', anonymous=True)

    pub = rospy.Publisher('/time_slider_gazebo/footstep_plan', Float64MultiArray, queue_size=1)
    sub = rospy.Subscriber('/time_slider_gazebo/planner_input', Float64MultiArray, callback)

    rate = rospy.Rate(500)

    opt = Float64MultiArray()
    Z = SLIP(Zc, step_time)
    Z.update_state(z_hat[0], 0.0, z_hat[0]+0.12, z_hat[0]+0.12+step_length*math.tan(slope_angle)/2)
    z_currentStep = z_hat[0]
    z_nextStep = z_hat[0]
    current_steps = 0 # number of steps
    support_foot_pre = support_foot

    while not rospy.is_shutdown():   
        start = time.time()     
        A,B = genLIPM(g,Zc,Ts) 
        Ns = int(step_time/Ts) # number of samples per step
        Nr = int(remaining_time/Ts)# number of remaining samples for the current step
        #print("Nr",Nr)
        N = Nr+N_steps*Ns # horizon length (measured by the number of samples)
        Gamma = Gamma_list[Nr]
        Phi = Phi_list[Nr]
#        Gamma_z = Gamma_z_list[Nr]
#        Phi_z = Phi_z_list[Nr]
        if support_foot_pre != support_foot:
            current_steps += 1
            support_foot_pre = support_foot
            # z0 is current z state
            Z.update_state(z_hat[0], 0.0, z_hat[0]+0.12, z_hat[0]+0.12+step_length*math.tan(slope_angle)/2)
            z_currentStep = z_hat[0]
            z_nextStep = z_hat[0]+step_length*math.tan(slope_angle)
            #print('Current step is ', current_steps)
        #z1,zd1,zdd1 = Z.get_com_state(step_time - remaining_time-1)
        z, zd, zdd = Z.get_com_state(step_time - remaining_time)
        #print("z_hat is ", z_hat)
        
        #print('height command is ', z)
        #print('vel command is ',zd)
        #print('acc command is', zdd )
        #print("r0 = ", Z.r0)
        #print("z_hat[0]+0.12",z_hat[0]+0.12)
        p0_z = z_hat[0]+0.12-9.8/(Robot.w**2)
        #print(Z.r0-9.8/(Robot.w**2))
#        print(z_hat[0])
#        print(1/Robot.w**2)
#        print(zdd)
        #print("z_hat =  ",z_hat)
        #print("p0_z = ", p0_z)
        
        Current_state.z_hat = z_hat
        Current_state.p0_z = p0_z
        Current_state.remaining_time = remaining_time
        MPCparameter = MPCparamater(N_steps=2,Q=np.diag(np.array([0,3])),Wz=np.array([5]),R=100,P=np.diag(np.array([0,3])),Robot=Robot,Current_state=Current_state,step_time = step_time)
        Gamma_z = Gamma_z_list[MPCparameter.Nr]
        Phi_z = Phi_z_list[MPCparameter.Nr]
        # Run MPC planner
        #start = time.time() 
        Uopt_x, Uopt_y = mpcPlanner(x_hat,y_hat,p0_x,p0_y,support_foot,Ns,Nr,N_steps,N,inter_feet_clearance,forward_velocity,lateral_velocity,Qx,Qy,Rx,Ry,Wx,Wy,step_length,Obstacle,Gamma,Phi)
        end1 = time.time()
        print("x,y time " +str(end1-start))
        Uopt_z = mpcPlanner_z(Robot,Current_state,MPCparameter,Gamma_z,Phi_z)
        u_z = Uopt_z[0:MPCparameter.N]
        u_z = np.reshape(u_z,(MPCparameter.N,1))
        z_all = np.dot(MPCparameter.Phi,Current_state.z_hat)+np.dot(MPCparameter.Gamma,u_z)
        #print(Uopt_z)
        z_1 = z_all[0]
        z_1_d = z_all[1]
        z_1_dd = -(Robot.w**2)*z_hat[0]+(Robot.w**2)*u_z[0] 
        #print('height command is MPC ', z_1)
        #print('vel command is MPC', z_1_d)
        #print("acc command is MPC",z_1_dd)
        #### formulate of z
        z = np.array([z_1])
        zd = np.array([z_1_d])
        zdd = np.array([z_1_dd])
        ####
        
        end = time.time()
        print("all_time"+str(end-start))
        
        #
        # print('Uopt is ', Uopt_x, Uopt_y)
        # print('Prediction length is ', N)
        # next_zmp_x, next_zmp_y, next_foothold_x, next_foothold_y
        
        opt.data = [Uopt_x[0], Uopt_y[0], (p0_x+Uopt_x[N]), (p0_y+Uopt_y[N]), remaining_time, step_time, support_foot]
        # # next_zmp_x, next_zmp_y, next_foothold_x, next_foothold_y
        # opt.data = [Uopt_x[Nr], Uopt_y[Nr], (p0_x+Uopt_x[N]), (p0_y+Uopt_y[N]), remaining_time, step_time, support_foot]
        #end = time.time()

        # print "-----------optimal solution-------------"
        # print "support: ", support_foot
        # print "p0_x: ", p0_x
        # print "current foot y: ", p0_y
        # print "next foot x: ", p0_x+Uopt_x[N]
        # print "next foot y: ", p0_y+Uopt_y[N]
        # print "----------------------------------------"
        #print "time: ", end-start

        pub.publish(opt)

        rate.sleep()


if __name__ == '__main__':
    #Generate offline matrices
    global Gamma_list, Phi_list, Gamma_z_list, Phi_z_list
    Robot = Robot(g=9.8,k=1208.52,m=15000.0,Ts=0.05,Upward_velocity=0.0,w = 8.975971429)
    Robot.gen_offline_matrices()
    A,B = genLIPM(g,Zc,Ts) 
    Obstacle = Obstacle(x0=1.0,y0=100.2,R=0.001,safedisatnce=0.25)
    Current_state = Current_state(z_hat=z_hat,p0_z=0,remaining_time=7*0.1)
    
    ##Predction matrices offline formulation
    Gamma_list  = [] 
    Phi_list = []
    Gamma_z_list = []
    Phi_z_list = []
    for i in range (int(step_time/Ts)+1):
        Ns = int(step_time/Ts)
        N = i+N_steps*Ns
        (Gamma,Phi) = genPredictionMatrices(A,B,N)
        Gamma_list.append(Gamma)
        Phi_list.append(Phi)
    for i in range (int(round(step_time/Robot.Ts))+1):    
        Ns_z =  int(step_time/Robot.Ts)
        N_z = i +2*Ns_z
        (Gamma_z,Phi_z) = genPredictionMatrices(Robot.A,Robot.B,N_z)
        Gamma_z_list.append(Gamma_z)
        Phi_z_list.append(Phi_z)
    
        
        
    try:
        sliderPlanner()      
    except rospy.ROSInterruptException:
        pass

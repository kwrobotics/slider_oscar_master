///lmax changed--> this has Qxy and walks forever

#include <iostream>
#include <chrono>
#include <cmath>
#include <Eigen/Dense>

#include <fstream>

#include "ros/ros.h"
#include <std_msgs/Float64.h>
#include <std_msgs/Float64MultiArray.h>

using namespace Eigen;
using namespace std;

const double m = 11.5;    // body mass
const double m_s = 0.3;   // swing leg mass
const double L = 1.2;     // leg length
const double h = 0.7;     // body height
const double g = 9.81;    // gravity
const double omega = sqrt(g / h);  // omega

const double mu_step_size = 0.02;

int flag = 0;
int support_foot_flag = 1;
Vector2d x_dot_ref(0.0, 0.0);     // m/s

// Parameters subscribed from the slider_controller node		//// This is what is fed in at any point. These can be used to test the C++ code without anything else
Vector2d x_0(0.0, 0.0);		//// These 3 are the current x and y position-velocity-acceleration
Vector2d x_dot_0(0.0, 0.0);	//
Vector2d x_dot_dot_0(0.0, 0.0);	//
Vector3d dt_s(0.4, 0.4, 0.4);	//// This is the current foostep timing solution coming from the Python file (which uses the footstep positions u)
Vector3d dt_s_backup(0.4, 0.4, 0.4);

Matrix<double, 2, 3> u_s((Matrix<double, 2, 3>() << 0.0, -1.93340069e-02, 1.22170610e-01, -0.21, 0.21, -0.21).finished());
Matrix<double, 2, 3> u_s_backup((Matrix<double, 2, 3>() << 0.0, -1.93340069e-02, 1.22170610e-01, -0.21, 0.21, -0.21).finished());


Vector3d t_s_rk4;		//time received from rk4 (or casadi)
Matrix<double, 2, 3> u_s_rk4;

double Qx = 1.0;
double Qy = 1.0;

double left_x = 0.0;
double left_y = 0.21;
double right_x = 0.0;
double right_y = -0.21;
int foot_flag = 1;
// penalty weights
int n_weights = 14;
int n_collision = 2;


VectorXd w;			//weight that acts as lambda in the lagrangian equations
VectorXd w_collision;			//weight that acts as lambda in the lagrangian equations

VectorXd w_1;
VectorXd w_2;
VectorXd w_backup3;
VectorXd w_com;

const int n_dynamic_weights = 14;
//VectorXd dyn_w;
VectorXd w_update;
MatrixXd Jacobian;
MatrixXd HessianInv;

const double max_iter = 200;
int n_repeats = 1;
bool rk4_received = false;		//This will tell us when we receive the first solution (u and dt) from rk4
bool w_received = false;

// constraints
// line up with Python
const double lmax = pow(0.6 * sqrt(pow(L, 2.0) - pow(h, 2.0)), 2);  /// lmax^2
const double rfoot = 0.2;
const double dt0_min = 0.2;
const double dt_min = 0.4;
const double dt_max = 0.6;

double current_time = 0.0;

double x1 = 5.0;
double yyy1 = 0.0;
double Rx1 = 0.01;
double Ry1 = 0.01;
double theta1 = 0;
double x2 = 5.0;
double y2 = 0.0;
double Rx2 = 0.01;
double Ry2 = 0.01;
double theta2 = 0;
double x3 = 5.0;
double y3 = 0.0;
double Rx3 = 0.01;
double Ry3 = 0.01;
double theta3 = 0;
double x4 = 5.0;
double y4 = 0;
double Rx4 = 0.01;
double Ry4 = 0.01;
double theta4 = 0;
double x5 = 5.0;
double y5 = 0;
double Rx5 = 0.01;
double Ry5 = 0.01;
double theta5 = 0;

double dxdot_dt(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s, int kx, int kt);
double dx_dt(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s, int kx, int kt);
double dxdot_du(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s, int kx, int ku);
double dx_du(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s, int kx, int ku);

struct bfgs { ///- At the end this was not used. Instead we use gradient descent
	VectorXd x_k;		// parameters (dt, u_x, u_y, at step k)
	VectorXd delta_x_k;	// parameter update at step k
	VectorXd y_k;		// change in gradient at step k
	MatrixXd B_k;		// Hessian approximation at step k
	MatrixXd H_k;		// inverse Hessian approximation at step k
};

bfgs solution = {
	VectorXd::Zero(7),
	VectorXd::Zero(7),
	VectorXd::Zero(7),
	Matrix<double, 7, 7>::Identity(),
	Matrix<double, 7, 7>::Identity(),
};

double x(double dt, double u, double x_prev, double x_dot_prev) {
	// double x_ = (x_prev - u + x_dot_prev / omega) * exp(omega * dt) + (x_prev - u - x_dot_prev / omega) * exp(-omega * dt) + u;
	double x_ = u + (x_prev - u) * cosh(omega * dt) + (x_dot_prev / omega) * sinh(omega * dt);
	return x_;
}

double xdot(double dt, double u, double x_prev, double x_dot_prev) {
	// double xdot_ = omega * (x_prev - u + x_dot_prev / omega) * exp(omega * dt) - omega * (x_prev - u - x_dot_prev / omega) * exp(-omega * dt);
	double xdot_ = omega * (x_prev - u) * sinh(omega * dt) + x_dot_prev * cosh(omega * dt);
	return xdot_;
}

double xddot(double dt, double u, double x_prev, double x_dot_prev) {
	// double xddot_ = pow(omega, 2) * (x_prev - u + x_dot_prev / omega) * exp(omega * dt) + pow(omega, 2) * (x_prev - u - x_dot_prev / omega) * exp(-omega * dt);
	double xddot_ = omega * omega * (x_prev - u) * cosh(omega * dt) + omega * x_dot_prev * sinh(omega * dt);
	return xddot_;
}

double dxdot_dt(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s, int kx = 1, int kt = 0) {
	double dxdot_dt_ = 0.0;
	if (kx == kt + 1) {
		dxdot_dt_ = xddot_s[kx]; // question, will be -1?
	}
	else if (kx > kt + 1) {
		dxdot_dt_ = dx_dt(dt_s, u_s, x_s, xdot_s, xddot_s, kx - 1, kt) * omega * sinh(omega * dt_s[kx - 1]) + dxdot_dt(dt_s, u_s, x_s, xdot_s, xddot_s, kx - 1, kt) * cosh(omega * dt_s[kx - 1]);
	}
	return dxdot_dt_;
}

double dx_dt(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s, int kx = 1, int kt = 0) {
	double dx_dt_ = 0.0;
	if (kx == kt + 1) {
		dx_dt_ = xdot_s[kx];
	}
	else if (kx > kt + 1) {
		dx_dt_ = dx_dt(dt_s, u_s, x_s, xdot_s, xddot_s, kx - 1, kt) * cosh(omega * dt_s[kx - 1]) + dxdot_dt(dt_s, u_s, x_s, xdot_s, xddot_s, kx - 1, kt) * sinh(omega * dt_s[kx - 1]) / omega;
	}
	return dx_dt_;
}
double dx_du(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s, int kx = 1, int ku = 1) {
	double dx_du_ = 0.0;
	if (kx == ku + 1) {
		dx_du_ = 1 - cosh(omega * dt_s[kx - 1]);
	}
	else if (kx > ku + 1) {
		dx_du_ = dx_du(dt_s, u_s, x_s, xdot_s, xddot_s, kx - 1, ku) * cosh(omega * dt_s[kx - 1]) + dxdot_du(dt_s, u_s, x_s, xdot_s, xddot_s, kx - 1, ku) * sinh(omega * dt_s[kx - 1]) / omega;
	}
	return dx_du_;
}

double dxdot_du(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s, int kx = 1, int ku = 1) {
	double dxdot_du_ = 0.0;
	if (kx == ku + 1) {
		dxdot_du_ = -omega * sinh(omega * dt_s[kx - 1]);
	}
	else if (kx > ku + 1) {
		dxdot_du_ = dx_du(dt_s, u_s, x_s, xdot_s, xddot_s, kx - 1, ku) * omega * sinh(omega * dt_s[kx - 1]) + dxdot_du(dt_s, u_s, x_s, xdot_s, xddot_s, kx - 1, ku) * cosh(omega * dt_s[kx - 1]);
	}
	return dxdot_du_;
}

double dJ_dux1(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s) {
	double grad = 0;
	for (int j = 2; j < 4; j++) {
		grad += 2 * dxdot_du(dt_s, u_s, x_s, xdot_s, xddot_s, j, 1) * (xdot_s[j] - x_dot_ref[0]) * Qx;
	}
	return grad;
}

double dJ_dux2(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s) {
	double grad = 0;
	for (int j = 3; j < 4; j++) {
		grad += 2 * dxdot_du(dt_s, u_s, x_s, xdot_s, xddot_s, j, 2) * (xdot_s[j] - x_dot_ref[0]) * Qx;
	}
	return grad;
}

double dJ_duy1(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s) {
	double grad = 0;
	for (int j = 2; j < 4; j++) {
		grad += 2 * dxdot_du(dt_s, u_s, x_s, xdot_s, xddot_s, j, 1) * (xdot_s[j] - x_dot_ref[1]) * Qy;
	}
	return grad;
}

double dJ_duy2(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s) {
	double grad = 0;
	for (int j = 3; j < 4; j++) {
		grad += 2 * dxdot_du(dt_s, u_s, x_s, xdot_s, xddot_s, j, 2) * (xdot_s[j] - x_dot_ref[1]) * Qy;
	}
	return grad;
}

double dJ_dt0(Vector3d dt_s, Matrix<double, 2, 3> u_s, Matrix<double, 2, 4> x_s, Matrix<double, 2, 4> xdot_s, Matrix<double, 2, 4> xddot_s) {
	double grad = 0.0;
	Vector3d u_x = u_s.row(0);
	Vector3d u_y = u_s.row(1);
	Vector4d x_x = x_s.row(0);
	Vector4d x_y = x_s.row(1);
	Vector4d xdot_x = xdot_s.row(0);
	Vector4d xdot_y = xdot_s.row(1);
	Vector4d xddot_x = xddot_s.row(0);
	Vector4d xddot_y = xddot_s.row(1);

	for (int j = 1; j < 4; j++) {
		grad += 2 * dxdot_dt(dt_s, u_x, x_x, xdot_x, xddot_x, j, 0) * (xdot_x[j] - x_dot_ref[0]) * Qx;
		grad += 2 * dxdot_dt(dt_s, u_y, x_y, xdot_y, xddot_y, j, 0) * (xdot_y[j] - x_dot_ref[1]) * Qy;
	}
	return grad;
}

double dJ_dt1(Vector3d dt_s, Matrix<double, 2, 3> u_s, Matrix<double, 2, 4> x_s, Matrix<double, 2, 4> xdot_s, Matrix<double, 2, 4> xddot_s) {
	double grad = 0.0;
	Vector3d u_x = u_s.row(0);
	Vector3d u_y = u_s.row(1);
	Vector4d x_x = x_s.row(0);
	Vector4d x_y = x_s.row(1);
	Vector4d xdot_x = xdot_s.row(0);
	Vector4d xdot_y = xdot_s.row(1);
	Vector4d xddot_x = xddot_s.row(0);
	Vector4d xddot_y = xddot_s.row(1);

	for (int j = 1; j < 4; j++) {
		grad += 2 * dxdot_dt(dt_s, u_x, x_x, xdot_x, xddot_x, j, 1) * (xdot_x[j] - x_dot_ref[0]) * Qx;
		grad += 2 * dxdot_dt(dt_s, u_y, x_y, xdot_y, xddot_y, j, 1) * (xdot_y[j] - x_dot_ref[1]) * Qy;
	}
	return grad;
}

double dJ_dt2(Vector3d dt_s, Matrix<double, 2, 3> u_s, Matrix<double, 2, 4> x_s, Matrix<double, 2, 4> xdot_s, Matrix<double, 2, 4> xddot_s) {
	double grad = 0.0;
	Vector3d u_x = u_s.row(0);
	Vector3d u_y = u_s.row(1);
	Vector4d x_x = x_s.row(0);
	Vector4d x_y = x_s.row(1);
	Vector4d xdot_x = xdot_s.row(0);
	Vector4d xdot_y = xdot_s.row(1);
	Vector4d xddot_x = xddot_s.row(0);
	Vector4d xddot_y = xddot_s.row(1);

	for (int j = 1; j < 4; j++) {
		grad += 2 * dxdot_dt(dt_s, u_x, x_x, xdot_x, xddot_x, j, 2) * (xdot_x[j] - x_dot_ref[0]) * Qx;   ///Derivative of the body velocity in respect to footstep duration, multiplied by the actual error
		grad += 2 * dxdot_dt(dt_s, u_y, x_y, xdot_y, xddot_y, j, 2) * (xdot_y[j] - x_dot_ref[1]) * Qy;   ///and we sum both x and y together because its the sum of the squared error --> ALL this is based on the cost function
	}
	return grad;
}

Vector2d dJ_dux(Vector3d dt_s, Matrix<double, 2, 3> u_s, Matrix<double, 2, 4> x_s, Matrix<double, 2, 4> xdot_s, Matrix<double, 2, 4> xddot_s) {
	Vector3d u_x = u_s.row(0);
	Vector3d u_y = u_s.row(1);
	Vector4d x_x = x_s.row(0);
	Vector4d x_y = x_s.row(1);
	Vector4d xdot_x = xdot_s.row(0);
	Vector4d xdot_y = xdot_s.row(1);
	Vector4d xddot_x = xddot_s.row(0);
	Vector4d xddot_y = xddot_s.row(1);

	double px1u1 = w[6] * ((x_s.col(1) - u_s.col(1)).transpose() * (x_s.col(1) - u_s.col(1)) - lmax);   /// x1u1,x2u2 stop the body from travelling too far away from the foot (based on how long the leg can be)
	double px2u2 = w[7] * ((x_s.col(2) - u_s.col(2)).transpose() * (x_s.col(2) - u_s.col(2)) - lmax);

	/// And here we do the gradients of the 2 things above
	double dpx1u1_dux1 = -2 * (x_s(0, 1) - u_s(0, 1)) * w[6];
	double dpx2u2_dux1 = 2 * (dx_du(dt_s, u_x, x_x, xdot_x, xddot_x, 2, 1)) * (x_s(0, 2) - u_s(0, 2)) * w[7];
	double dpx2u2_dux2 = -2 * (x_s(0, 2) - u_s(0, 2)) * w[7];

	double px2u1 = w[8] * ((x_s.col(2) - u_s.col(1)).transpose() * (x_s.col(2) - u_s.col(1)) - lmax);  /// x2u1, x3u2 stop the next foot to be places too far from the body
	double px3u2 = w[9] * ((x_s.col(3) - u_s.col(2)).transpose() * (x_s.col(3) - u_s.col(2)) - lmax);  /// x3u3: make sure that the CoM at the 3rd footstep is not too far from the 2nd step (1,2,3 are which footstep you are on)

	/// And here we do the gradients of the 2 things above
	double dpx2u1_dux1 = 2 * (dx_du(dt_s, u_x, x_x, xdot_x, xddot_x, 2, 1) - 1) * (x_s(0, 2) - u_s(0, 1)) * w[8];
	double dpx3u2_dux2 = 2 * (dx_du(dt_s, u_x, x_x, xdot_x, xddot_x, 3, 2) - 1) * (x_s(0, 3) - u_s(0, 2)) * w[9];
	double dpx3u2_dux1 = 2 * (dx_du(dt_s, u_x, x_x, xdot_x, xddot_x, 3, 1)) * (x_s(0, 3) - u_s(0, 2)) * w[9];

	double dux1_inequality_penalties = dpx1u1_dux1 + dpx2u2_dux1 + dpx2u1_dux1 + dpx3u2_dux1;
	double dux2_inequality_penalties = dpx2u2_dux2 + dpx3u2_dux2;
	///These penalties are added to the cost function. These are the multipliers penalties. You need to take the gradient of these in respect of each of the optimization variables (from which you get e.g. dpx1u1_dux1)




	////////1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111

	double pu1e1 = (- pow((u_s(0, 1) - x1) * cos(theta1) + (u_s(1, 1) - yyy1) * sin(theta1),2) / pow(Rx1,2) - pow((u_s(0, 1) - x1) * sin(theta1) + (u_s(1, 1) - yyy1) * cos(theta1),2) / pow(Ry1,2) + 1) * w_collision[0];
	// double pu1e2 = (- pow((u_s(0, 1) - x2) * cos(theta2) + (u_s(1, 1) - y2) * sin(theta2),2) / pow(Rx2,2) - pow((u_s(0, 1) - x2) * sin(theta2) + (u_s(1, 1) - y2) * cos(theta2),2) / pow(Ry2,2) + 1) * w[13];
	// double pu1e3 = (- pow((u_s(0, 1) - x3) * cos(theta3) + (u_s(1, 1) - y3) * sin(theta3),2) / pow(Rx3,2) - pow((u_s(0, 1) - x3) * sin(theta3) + (u_s(1, 1) - y3) * cos(theta3),2) / pow(Ry3,2) + 1) * w[14];
	// double pu1e4 = (- pow((u_s(0, 1) - x4) * cos(theta4) + (u_s(1, 1) - y4) * sin(theta4),2) / pow(Rx4,2) - pow((u_s(0, 1) - x4) * sin(theta4) + (u_s(1, 1) - y4) * cos(theta4),2) / pow(Ry4,2) + 1) * w[15];
	// double pu1e5 = (- pow((u_s(0, 1) - x5) * cos(theta5) + (u_s(1, 1) - y5) * sin(theta5),2) / pow(Rx5,2) - pow((u_s(0, 1) - x5) * sin(theta5) + (u_s(1, 1) - y5) * cos(theta5),2) / pow(Ry5,2) + 1) * w[16];
	
	double pu2e1 = (- pow((u_s(0, 2) - x1) * cos(theta1) + (u_s(1, 2) - yyy1) * sin(theta1),2) / pow(Rx1,2) - pow((u_s(0, 2) - x1) * sin(theta1) + (u_s(1, 2) - yyy1) * cos(theta1),2) / pow(Ry1,2) + 1) * w_collision[1];
	// double pu2e2 = (- pow((u_s(0, 2) - x2) * cos(theta2) + (u_s(1, 2) - y2) * sin(theta2),2) / pow(Rx2,2) - pow((u_s(0, 2) - x2) * sin(theta2) + (u_s(1, 2) - y2) * cos(theta2),2) / pow(Ry2,2) + 1) * w[18];
	// double pu2e3 = (- pow((u_s(0, 2) - x3) * cos(theta3) + (u_s(1, 2) - y3) * sin(theta3),2) / pow(Rx3,2) - pow((u_s(0, 2) - x3) * sin(theta3) + (u_s(1, 2) - y3) * cos(theta3),2) / pow(Ry3,2) + 1) * w[19];
	// double pu2e4 = (- pow((u_s(0, 2) - x4) * cos(theta4) + (u_s(1, 2) - y4) * sin(theta4),2) / pow(Rx4,2) - pow((u_s(0, 2) - x4) * sin(theta4) + (u_s(1, 2) - y4) * cos(theta4),2) / pow(Ry4,2) + 1) * w[20];
	// double pu2e5 = (- pow((u_s(0, 2) - x5) * cos(theta5) + (u_s(1, 2) - y5) * sin(theta5),2) / pow(Rx5,2) - pow((u_s(0, 2) - x5) * sin(theta5) + (u_s(1, 2) - y5) * cos(theta5),2) / pow(Ry5,2) + 1) * w[21];



	double dpu1e1_dux1 = (2 * cos(theta1) * (-((u_s(0, 1) - x1) * cos(theta1) + (u_s(1, 1) - yyy1) * sin(theta1)) / pow(Rx1,2)) + 2 * sin(theta1) * (-((u_s(0, 1) - x1) * sin(theta1) - (u_s(1, 1) - yyy1) * cos(theta1)) / pow(Ry1,2))) * w_collision[0];
	// double dpu1e2_dux1 = (2 * cos(theta2) * (-((u_s(0, 1) - x2) * cos(theta2) + (u_s(1, 1) - y2) * sin(theta2)) / pow(Rx2,2)) - 2 * sin(theta2) * (((u_s(0, 1) - x2) * sin(theta2) + (u_s(1, 1) - y2) * cos(theta2)) / pow(Ry2,2))) * w[13];
	// double dpu1e3_dux1 = (2 * cos(theta3) * (-((u_s(0, 1) - x3) * cos(theta3) + (u_s(1, 1) - y3) * sin(theta3)) / pow(Rx3,2)) - 2 * sin(theta3) * (((u_s(0, 1) - x3) * sin(theta3) + (u_s(1, 1) - y3) * cos(theta3)) / pow(Ry3,2))) * w[14];
	// double dpu1e4_dux1 = (2 * cos(theta4) * (-((u_s(0, 1) - x4) * cos(theta4) + (u_s(1, 1) - y4) * sin(theta4)) / pow(Rx4,2)) - 2 * sin(theta4) * (((u_s(0, 1) - x4) * sin(theta4) + (u_s(1, 1) - y4) * cos(theta4)) / pow(Ry4,2))) * w[15];
	// double dpu1e5_dux1 = (2 * cos(theta5) * (-((u_s(0, 1) - x5) * cos(theta5) + (u_s(1, 1) - y5) * sin(theta5)) / pow(Rx5,2)) - 2 * sin(theta5) * (((u_s(0, 1) - x5) * sin(theta5) + (u_s(1, 1) - y5) * cos(theta5)) / pow(Ry5,2))) * w[16];

	double dpu2e1_dux2 = (2 * cos(theta1) * (-((u_s(0, 2) - x1) * cos(theta1) + (u_s(1, 2) - yyy1) * sin(theta1)) / pow(Rx1,2)) + 2 * sin(theta1) * (-((u_s(0, 2) - x1) * sin(theta1) - (u_s(1, 2) - yyy1) * cos(theta1)) / pow(Ry1,2))) * w_collision[1];
	// double dpu2e2_dux2 = (2 * cos(theta2) * (-((u_s(0, 2) - x2) * cos(theta2) + (u_s(1, 2) - y2) * sin(theta2)) / pow(Rx2,2)) - 2 * sin(theta2) * (((u_s(0, 2) - x2) * sin(theta2) + (u_s(1, 2) - y2) * cos(theta2)) / pow(Ry2,2))) * w[18];
	// double dpu2e3_dux2 = (2 * cos(theta3) * (-((u_s(0, 2) - x3) * cos(theta3) + (u_s(1, 2) - y3) * sin(theta3)) / pow(Rx3,2)) - 2 * sin(theta3) * (((u_s(0, 2) - x3) * sin(theta3) + (u_s(1, 2) - y3) * cos(theta3)) / pow(Ry3,2))) * w[19];
	// double dpu2e4_dux2 = (2 * cos(theta4) * (-((u_s(0, 2) - x4) * cos(theta4) + (u_s(1, 2) - y4) * sin(theta4)) / pow(Rx4,2)) - 2 * sin(theta4) * (((u_s(0, 2) - x4) * sin(theta4) + (u_s(1, 2) - y4) * cos(theta4)) / pow(Ry4,2))) * w[20];
	// double dpu2e5_dux2 = (2 * cos(theta5) * (-((u_s(0, 2) - x5) * cos(theta5) + (u_s(1, 2) - y5) * sin(theta5)) / pow(Rx5,2)) - 2 * sin(theta5) * (((u_s(0, 2) - x5) * sin(theta5) + (u_s(1, 2) - y5) * cos(theta5)) / pow(Ry5,2))) * w[21];

	double dux1_vision_penalties = dpu1e1_dux1; // + dpu1e2_dux1 + dpu1e3_dux1 + dpu1e4_dux1 + dpu1e5_dux1;
	double dux2_vision_penalties = dpu2e1_dux2; // + dpu2e2_dux2 + dpu2e3_dux2 + dpu2e4_dux2 + dpu2e5_dux2;

	////////22222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222







	/////////AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA

	double AL_dpx1u1_dux1 = 0.0;	// a
	double AL_dpx2u2_dux1 = 0.0;	// a
	double AL_dpx2u2_dux2 = 0.0;	// b	
	double AL_dpx2u1_dux1 = 0.0;	// a
	double AL_dpx3u2_dux2 = 0.0;	// b
	double AL_dpx3u2_dux1 = 0.0;	// a

	if ((px1u1 / w[6]) <= 0) {
		AL_dpx1u1_dux1 = 0;
	}
	else if ((px1u1 / w[6]) > 0) {
		AL_dpx1u1_dux1 = 2 * mu_step_size * (px1u1 / w[6]) * dpx1u1_dux1 / w[6];	// a
	}

	if ((px2u2 / w[7]) <= 0) {
		AL_dpx2u2_dux1 = 0;
	}
	else if ((px2u2 / w[7]) > 0) {
		AL_dpx2u2_dux1 = 2 * mu_step_size * (px2u2 / w[7]) * dpx2u2_dux1 / w[7];	// a
	}

	if ((px2u2 / w[7]) <= 0) {
		AL_dpx2u2_dux2 = 0;
	}
	else if ((px2u2 / w[7]) > 0) {
		AL_dpx2u2_dux2 = 2 * mu_step_size * (px2u2 / w[7]) * dpx2u2_dux2 / w[7];	// b	
	}

	if ((px2u1 / w[8]) <= 0) {
		AL_dpx2u1_dux1 = 0;
	}
	else if ((px2u1 / w[8]) > 0) {
		AL_dpx2u1_dux1 = 2 * mu_step_size * (px2u1 / w[8]) * dpx2u1_dux1 / w[8];	// a
	}

	if ((px3u2 / w[9]) <= 0) {
		AL_dpx3u2_dux2 = 0;
	}
	else if ((px3u2 / w[9]) > 0) {
		AL_dpx3u2_dux2 = 2 * mu_step_size * (px3u2 / w[9]) * dpx3u2_dux2 / w[9];	// b
	}

	if ((px3u2 / w[9]) <= 0) {
		AL_dpx3u2_dux1 = 0;
	}
	else if ((px3u2 / w[9]) > 0) {
		AL_dpx3u2_dux1 = 2 * mu_step_size * (px3u2 / w[9]) * dpx3u2_dux1 / w[9];	// a
	}




	//	double AL_dpx1u1_dux1 = 2 * mu_step_size * (px1u1/w[6]) * dpx1u1_dux1/w[6];	// a
	//	double AL_dpx2u2_dux1 = 2 * mu_step_size * (px2u2/w[7]) * dpx2u2_dux1/w[7];	// a
	//	double AL_dpx2u2_dux2 = 2 * mu_step_size * (px2u2/w[7]) * dpx2u2_dux2/w[7];	// b	
	//	double AL_dpx2u1_dux1 = 2 * mu_step_size * (px2u1/w[8]) * dpx2u1_dux1/w[8];	// a
	//	double AL_dpx3u2_dux2 = 2 * mu_step_size * (px3u2/w[9]) * dpx3u2_dux2/w[9];	// b
	//	double AL_dpx3u2_dux1 = 2 * mu_step_size * (px3u2/w[9]) * dpx3u2_dux1/w[9];	// a

	double AL_dux1_inequality_penalties = AL_dpx1u1_dux1 + AL_dpx2u2_dux1 + AL_dpx2u1_dux1 + AL_dpx3u2_dux1;
	double AL_dux2_inequality_penalties = AL_dpx2u2_dux2 + AL_dpx3u2_dux2;

	////////BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB











	/////////11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111

	double AL_dpu1e1_dux1 = 0.0;	// a
	double AL_dpu1e2_dux1 = 0.0;	// a
	double AL_dpu1e3_dux1 = 0.0;	// 	
	double AL_dpu1e4_dux1 = 0.0;	// a
	double AL_dpu1e5_dux1 = 0.0;	// 

	double AL_dpu2e1_dux2 = 0.0;	// a
	double AL_dpu2e2_dux2 = 0.0;	// a
	double AL_dpu2e3_dux2 = 0.0;	// 	
	double AL_dpu2e4_dux2 = 0.0;	// a
	double AL_dpu2e5_dux2 = 0.0;	// 

	if ((pu1e1 / w_collision[0]) <= 0) {
		AL_dpu1e1_dux1 = 0;
	}
	else if ((pu1e1 / w_collision[0]) > 0) {
		AL_dpu1e1_dux1 = 2 * mu_step_size * (pu1e1 / w_collision[0]) * dpu1e1_dux1 / w_collision[0];	// a
	}

	// if ((pu1e2 / w[13]) <= 0) {
	// 	AL_dpu1e2_dux1 = 0;
	// }
	// else if ((pu1e2 / w[13]) > 0) {
	// 	AL_dpu1e2_dux1 = 2 * mu_step_size * (pu1e2 / w[13]) * dpu1e2_dux1 / w[13];	// a
	// }

	// if ((pu1e3 / w[14]) <= 0) {
	// 	AL_dpu1e3_dux1 = 0;
	// }
	// else if ((pu1e3 / w[14]) > 0) {
	// 	AL_dpu1e3_dux1 = 2 * mu_step_size * (pu1e3 / w[14]) * dpu1e3_dux1 / w[14];	// b	
	// }

	// if ((pu1e4 / w[15]) <= 0) {
	// 	AL_dpu1e4_dux1 = 0;
	// }
	// else if ((pu1e4 / w[15]) > 0) {
	// 	AL_dpu1e4_dux1 = 2 * mu_step_size * (pu1e4 / w[15]) * dpu1e4_dux1 / w[15];	// a
	// }

	// if ((pu1e5 / w[16]) <= 0) {
	// 	AL_dpu1e5_dux1 = 0;
	// }
	// else if ((pu1e5 / w[16]) > 0) {
	// 	AL_dpu1e5_dux1 = 2 * mu_step_size * (pu1e5 / w[16]) * dpu1e5_dux1 / w[16];	// b
	// }





	if ((pu2e1 / w_collision[1]) <= 0) {
		AL_dpu2e1_dux2 = 0;
	}
	else if ((pu2e1 / w_collision[1]) > 0) {
		AL_dpu2e1_dux2 = 2 * mu_step_size * (pu2e1 / w_collision[1]) * dpu2e1_dux2 / w_collision[1];	// a
	}

	// if ((pu2e2 / w[18]) <= 0) {
	// 	AL_dpu2e2_dux2 = 0;
	// }
	// else if ((pu2e2 / w[18]) > 0) {
	// 	AL_dpu2e2_dux2 = 2 * mu_step_size * (pu2e2 / w[18]) * dpu2e2_dux2 / w[18];	// a
	// }

	// if ((pu2e3 / w[19]) <= 0) {
	// 	AL_dpu2e3_dux2 = 0;
	// }
	// else if ((pu2e3 / w[19]) > 0) {
	// 	AL_dpu2e3_dux2 = 2 * mu_step_size * (pu2e3 / w[19]) * dpu2e3_dux2 / w[19];	// a
	// }

	// if ((pu2e4 / w[20]) <= 0) {
	// 	AL_dpu2e4_dux2 = 0;
	// }
	// else if ((pu2e4 / w[20]) > 0) {
	// 	AL_dpu2e4_dux2 = 2 * mu_step_size * (pu2e4 / w[20]) * dpu2e4_dux2 / w[20];	// a
	// }

	// if ((pu2e5 / w[21]) <= 0) {
	// 	AL_dpu2e5_dux2 = 0;
	// }
	// else if ((pu2e5 / w[21]) > 0) {
	// 	AL_dpu2e5_dux2 = 2 * mu_step_size * (pu2e5 / w[21]) * dpu2e5_dux2 / w[21];	// a
	// }




	double AL_vision_dux1_inequality_penalties = AL_dpu1e1_dux1; // + AL_dpu1e2_dux1 + AL_dpu1e3_dux1 + AL_dpu1e4_dux1 + AL_dpu1e5_dux1;

	double AL_vision_dux2_inequality_penalties = AL_dpu2e1_dux2; // + AL_dpu2e2_dux2 + AL_dpu2e3_dux2 + AL_dpu2e4_dux2 + AL_dpu2e5_dux2;

	////////22222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222






	//SHOW this stuff
//	cout << "dx inequality penalty " << endl;
//	cout << dpx1u1_dux1 <<  endl;
//	cout << dpx2u2_dux1 <<  endl;
//	cout << dpx2u1_dux1 <<  endl;
//	cout << dpx3u2_dux1 <<  endl;
//	cout << dpx2u2_dux2 <<  endl;
//	cout << dpx3u2_dux2 <<  endl;

//    cout << "dJ with dux gradient" << endl;
//    cout << dJ_dux1(dt_s, u_x, x_x, xdot_x, xddot_x) << endl;
//    cout << dJ_dux2(dt_s, u_x, x_x, xdot_x, xddot_x) << endl;

	Vector2d dJ_dux_;
	dJ_dux_ << dJ_dux1(dt_s, u_x, x_x, xdot_x, xddot_x) + dux1_inequality_penalties + AL_dux1_inequality_penalties + dux1_vision_penalties + AL_vision_dux1_inequality_penalties, //dJ_dux1(dt_s, u_x, x_x, xdot_x, xddot_x) -
		dJ_dux2(dt_s, u_x, x_x, xdot_x, xddot_x) + dux2_inequality_penalties + AL_dux2_inequality_penalties + dux2_vision_penalties + AL_vision_dux2_inequality_penalties; // dJ_dux2(dt_s, u_x, x_x, xdot_x, xddot_x) -
	return dJ_dux_;
}

Vector2d dJ_duy(Vector3d dt_s, Matrix<double, 2, 3> u_s, Matrix<double, 2, 4> x_s, Matrix<double, 2, 4> x_dot_s, Matrix<double, 2, 4> xd_dot_s) {
	Vector3d u_x = u_s.row(0);
	Vector3d u_y = u_s.row(1);
	Vector4d x_x = x_s.row(0);
	Vector4d x_y = x_s.row(1);
	Vector4d xdot_x = x_dot_s.row(0);
	Vector4d xdot_y = x_dot_s.row(1);
	Vector4d xddot_x = xd_dot_s.row(0);
	Vector4d xddot_y = xd_dot_s.row(1);

	double px1u1 = w[6] * ((x_s.col(1) - u_s.col(1)).transpose() * (x_s.col(1) - u_s.col(1)) - lmax);
	double px2u2 = w[7] * ((x_s.col(2) - u_s.col(2)).transpose() * (x_s.col(2) - u_s.col(2)) - lmax);

	double dpx1u1_duy1 = -2 * (x_s(1, 1) - u_s(1, 1)) * w[6];
	double dpx2u2_duy1 = 2 * (dx_du(dt_s, u_y, x_y, xdot_y, xddot_y, 2, 1)) * (x_s(1, 2) - u_s(1, 2)) * w[7];
	double dpx2u2_duy2 = -2 * (x_s(1, 2) - u_s(1, 2)) * w[7];

	double px2u1 = w[8] * ((x_s.col(2) - u_s.col(1)).transpose() * (x_s.col(2) - u_s.col(1)) - lmax);
	double px3u2 = w[9] * ((x_s.col(3) - u_s.col(2)).transpose() * (x_s.col(3) - u_s.col(2)) - lmax);

	//    double dpx2u1_duy1 = -2 * (x_s(1, 2) - u_s(1, 1)) * w[8];
	//    double dpx3u2_duy2 = -2 * (x_s(1, 3) - u_s(1, 2)) * w[9];

	double dpx2u1_duy1 = 2 * (dx_du(dt_s, u_y, x_y, xdot_y, xddot_y, 2, 1) - 1) * (x_s(1, 2) - u_s(1, 1)) * w[8];
	double dpx3u2_duy2 = 2 * (dx_du(dt_s, u_y, x_y, xdot_y, xddot_y, 3, 2) - 1) * (x_s(1, 3) - u_s(1, 2)) * w[9];
	double dpx3u2_duy1 = 2 * (dx_du(dt_s, u_y, x_y, xdot_y, xddot_y, 3, 1)) * (x_s(1, 3) - u_s(1, 2)) * w[9];


	//    cout<< "inEquality constraint in y" << endl;
	//    cout << dpx1u1_duy1 << endl;
	//    cout << dpx2u2_duy2 << endl;
	//    cout << dpx2u1_duy1 << endl;
	//    cout << dpx3u2_duy2 << endl;






		///		CHANGE W[1] AND W[11]
		///foot_flag is which leg you are stepping with
		///The foot_flag doesn't matter with x, but it matters with y.
	double pu1u0 = w[10] * (foot_flag * (u_s(1, 1) - u_s(1, 0)) - rfoot);			///This makes sure the the 2 feet don't step on the top of each other
	double pu2u1 = w[11] * (-foot_flag * (u_s(1, 2) - u_s(1, 1)) - rfoot);			///rfoot is the radius of the foot and the flag will tell you how you need to keep the foot (left or right?)

	double dpu1u0_duy1 = -foot_flag * w[10];
	double dpu2u1_duy1 = -foot_flag * w[11];
	double dpu2u1_duy2 = foot_flag * w[11];


	double duy1_inequality_penalties = dpx1u1_duy1 + dpx2u2_duy1 + dpx2u1_duy1 + dpx3u2_duy1 + dpu1u0_duy1 + dpu2u1_duy1;//
	double duy2_inequality_penalties = dpx2u2_duy2 + dpx3u2_duy2 + dpu2u1_duy2;	//



		////////1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111

	double pu1e1 = (-pow((u_s(0, 1) - x1) * cos(theta1) + (u_s(1, 1) - yyy1) * sin(theta1),2) / pow(Rx1,2) - pow((u_s(0, 1) - x1) * sin(theta1) + (u_s(1, 1) - yyy1) * cos(theta1),2) / pow(Ry1,2) + 1) * w_collision[0];
	// double pu1e2 = (-pow((u_s(0, 1) - x2) * cos(theta2) + (u_s(1, 1) - y2) * sin(theta2),2) / pow(Rx2,2) - pow((u_s(0, 1) - x2) * sin(theta2) + (u_s(1, 1) - y2) * cos(theta2),2) / pow(Ry2,2) + 1) * w[13];
	// double pu1e3 = (-pow((u_s(0, 1) - x3) * cos(theta3) + (u_s(1, 1) - y3) * sin(theta3),2) / pow(Rx3,2) - pow((u_s(0, 1) - x3) * sin(theta3) + (u_s(1, 1) - y3) * cos(theta3),2) / pow(Ry3,2) + 1) * w[14];
	// double pu1e4 = (-pow((u_s(0, 1) - x4) * cos(theta4) + (u_s(1, 1) - y4) * sin(theta4),2) / pow(Rx4,2) - pow((u_s(0, 1) - x4) * sin(theta4) + (u_s(1, 1) - y4) * cos(theta4),2) / pow(Ry4,2) + 1) * w[15];
	// double pu1e5 = (-pow((u_s(0, 1) - x5) * cos(theta5) + (u_s(1, 1) - y5) * sin(theta5),2) / pow(Rx5,2) - pow((u_s(0, 1) - x5) * sin(theta5) + (u_s(1, 1) - y5) * cos(theta5),2) / pow(Ry5,2) + 1) * w[16];

	double pu2e1 = (-pow((u_s(0, 2) - x1) * cos(theta1) + (u_s(1, 2) - yyy1) * sin(theta1),2) / pow(Rx1,2) - pow((u_s(0, 2) - x1) * sin(theta1) + (u_s(1, 2) - yyy1) * cos(theta1),2) / pow(Ry1,2) + 1) * w_collision[1];
	// double pu2e2 = (-pow((u_s(0, 2) - x2) * cos(theta2) + (u_s(1, 2) - y2) * sin(theta2),2) / pow(Rx2,2) - pow((u_s(0, 2) - x2) * sin(theta2) + (u_s(1, 2) - y2) * cos(theta2),2) / pow(Ry2,2) + 1) * w[18];
	// double pu2e3 = (-pow((u_s(0, 2) - x3) * cos(theta3) + (u_s(1, 2) - y3) * sin(theta3),2) / pow(Rx3,2) - pow((u_s(0, 2) - x3) * sin(theta3) + (u_s(1, 2) - y3) * cos(theta3),2) / pow(Ry3,2) + 1) * w[19];
	// double pu2e4 = (-pow((u_s(0, 2) - x4) * cos(theta4) + (u_s(1, 2) - y4) * sin(theta4),2) / pow(Rx4,2) - pow((u_s(0, 2) - x4) * sin(theta4) + (u_s(1, 2) - y4) * cos(theta4),2) / pow(Ry4,2) + 1) * w[20];
	// double pu2e5 = (-pow((u_s(0, 2) - x5) * cos(theta5) + (u_s(1, 2) - y5) * sin(theta5),2) / pow(Rx5,2) - pow((u_s(0, 2) - x5) * sin(theta5) + (u_s(1, 2) - y5) * cos(theta5),2) / pow(Ry5,2) + 1) * w[21];



	double dpu1e1_duy1 = (2 * sin(theta1) * (-((u_s(0, 1) - x1) * cos(theta1) + (u_s(1, 1) - yyy1) * sin(theta1)) / pow(Rx1,2)) + 2 * cos(theta1) * (-((u_s(0, 1) - x1) * sin(theta1) - (u_s(1, 1) - yyy1) * cos(theta1)) / pow(Ry1,2))) * w_collision[0];
	// double dpu1e2_duy1 = (2 * sin(theta2) * (-((u_s(0, 1) - x2) * cos(theta2) + (u_s(1, 1) - y2) * sin(theta2)) / pow(Rx2,2)) - 2 * cos(theta2) * (((u_s(0, 1) - x2) * sin(theta2) + (u_s(1, 1) - y2) * cos(theta2)) / pow(Ry2,2))) * w[13];
	// double dpu1e3_duy1 = (2 * sin(theta3) * (-((u_s(0, 1) - x3) * cos(theta3) + (u_s(1, 1) - y3) * sin(theta3)) / pow(Rx3,2)) - 2 * cos(theta3) * (((u_s(0, 1) - x3) * sin(theta3) + (u_s(1, 1) - y3) * cos(theta3)) / pow(Ry3,2))) * w[14];
	// double dpu1e4_duy1 = (2 * sin(theta4) * (-((u_s(0, 1) - x4) * cos(theta4) + (u_s(1, 1) - y4) * sin(theta4)) / pow(Rx4,2)) - 2 * cos(theta4) * (((u_s(0, 1) - x4) * sin(theta4) + (u_s(1, 1) - y4) * cos(theta4)) / pow(Ry4,2))) * w[15];
	// double dpu1e5_duy1 = (2 * sin(theta5) * (-((u_s(0, 1) - x5) * cos(theta5) + (u_s(1, 1) - y5) * sin(theta5)) / pow(Rx5,2)) - 2 * cos(theta5) * (((u_s(0, 1) - x5) * sin(theta5) + (u_s(1, 1) - y5) * cos(theta5)) / pow(Ry5,2))) * w[16];

	double dpu2e1_duy2 = (2 * sin(theta1) * (-((u_s(0, 2) - x1) * cos(theta1) + (u_s(1, 2) - yyy1) * sin(theta1)) / pow(Rx1,2)) + 2 * cos(theta1) * (-((u_s(0, 2) - x1) * sin(theta1) - (u_s(1, 2) - yyy1) * cos(theta1)) / pow(Ry1,2))) * w_collision[1];
	// double dpu2e2_duy2 = (2 * sin(theta2) * (-((u_s(0, 2) - x2) * cos(theta2) + (u_s(1, 2) - y2) * sin(theta2)) / pow(Rx2,2)) - 2 * cos(theta2) * (((u_s(0, 2) - x2) * sin(theta2) + (u_s(1, 2) - y2) * cos(theta2)) / pow(Ry2,2))) * w[18];
	// double dpu2e3_duy2 = (2 * sin(theta3) * (-((u_s(0, 2) - x3) * cos(theta3) + (u_s(1, 2) - y3) * sin(theta3)) / pow(Rx3,2)) - 2 * cos(theta3) * (((u_s(0, 2) - x3) * sin(theta3) + (u_s(1, 2) - y3) * cos(theta3)) / pow(Ry3,2))) * w[19];
	// double dpu2e4_duy2 = (2 * sin(theta4) * (-((u_s(0, 2) - x4) * cos(theta4) + (u_s(1, 2) - y4) * sin(theta4)) / pow(Rx4,2)) - 2 * cos(theta4) * (((u_s(0, 2) - x4) * sin(theta4) + (u_s(1, 2) - y4) * cos(theta4)) / pow(Ry4,2))) * w[20];
	// double dpu2e5_duy2 = (2 * sin(theta5) * (-((u_s(0, 2) - x5) * cos(theta5) + (u_s(1, 2) - y5) * sin(theta5)) / pow(Rx5,2)) - 2 * cos(theta5) * (((u_s(0, 2) - x5) * sin(theta5) + (u_s(1, 2) - y5) * cos(theta5)) / pow(Ry5,2))) * w[21];

	double duy1_vision_penalties = dpu1e1_duy1; // + dpu1e2_duy1 + dpu1e3_duy1 + dpu1e4_duy1 + dpu1e5_duy1;
	double duy2_vision_penalties = dpu2e1_duy2; // + dpu2e2_duy2 + dpu2e3_duy2 + dpu2e4_duy2 + dpu2e5_duy2;

	////////22222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222








	/////////AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA

	double AL_dpx1u1_duy1 = 0.0;	// a
	double AL_dpx2u2_duy1 = 0.0;	// a
	double AL_dpx2u2_duy2 = 0.0;	// b	
	double AL_dpx2u1_duy1 = 0.0;	// a
	double AL_dpx3u2_duy2 = 0.0;	// b
	double AL_dpx3u2_duy1 = 0.0;	// a

	double AL_dpu1u0_duy1 = 0.0;	// a
	double AL_dpu2u1_duy1 = 0.0;	// a
	double AL_dpu2u1_duy2 = 0.0;	// b

	if ((px1u1 / w[6]) <= 0) {
		AL_dpx1u1_duy1 = 0;
	}
	else if ((px1u1 / w[6]) > 0) {
		AL_dpx1u1_duy1 = 2 * mu_step_size * (px1u1 / w[6]) * dpx1u1_duy1 / w[6];	// a
	}

	if ((px2u2 / w[7]) <= 0) {
		AL_dpx2u2_duy1 = 0;
	}
	else if ((px2u2 / w[7]) > 0) {
		AL_dpx2u2_duy1 = 2 * mu_step_size * (px2u2 / w[7]) * dpx2u2_duy1 / w[7];	// a
	}

	if ((px2u2 / w[7]) <= 0) {
		AL_dpx2u2_duy2 = 0;
	}
	else if ((px2u2 / w[7]) > 0) {
		AL_dpx2u2_duy2 = 2 * mu_step_size * (px2u2 / w[7]) * dpx2u2_duy2 / w[7];	// b
	}

	if ((px2u1 / w[8]) <= 0) {
		AL_dpx2u1_duy1 = 0;
	}
	else if ((px2u1 / w[8]) > 0) {
		AL_dpx2u1_duy1 = 2 * mu_step_size * (px2u1 / w[8]) * dpx2u1_duy1 / w[8];	// a
	}

	if ((px3u2 / w[9]) <= 0) {
		AL_dpx3u2_duy2 = 0;
	}
	else if ((px3u2 / w[9]) > 0) {
		AL_dpx3u2_duy2 = 2 * mu_step_size * (px3u2 / w[9]) * dpx3u2_duy2 / w[9];	// b
	}

	if ((px3u2 / w[9]) <= 0) {
		AL_dpx3u2_duy1 = 0;
	}
	else if ((px3u2 / w[9]) > 0) {
		AL_dpx3u2_duy1 = 2 * mu_step_size * (px3u2 / w[9]) * dpx3u2_duy1 / w[9];	// a
	}





	if ((pu1u0 / w[10]) <= 0) {
		AL_dpu1u0_duy1 = 0;
	}
	else if ((pu1u0 / w[10]) > 0) {
		AL_dpu1u0_duy1 = 2 * mu_step_size * (pu1u0 / w[10]) * dpu1u0_duy1 / w[10];	// a
	}

	if ((pu2u1 / w[11]) <= 0) {
		AL_dpu2u1_duy1 = 0;
	}
	else if ((pu2u1 / w[11]) > 0) {
		AL_dpu2u1_duy1 = 2 * mu_step_size * (pu2u1 / w[11]) * dpu2u1_duy1 / w[11];	// a
	}

	if ((pu2u1 / w[11]) <= 0) {
		AL_dpu2u1_duy2 = 0;
	}
	else if ((pu2u1 / w[11]) > 0) {
		AL_dpu2u1_duy2 = 2 * mu_step_size * (pu2u1 / w[11]) * dpu2u1_duy2 / w[11];	// b
	}


	//	double AL_dpx1u1_duy1 = 2 * mu_step_size * (px1u1/w[6]) * dpx1u1_duy1/w[6];	// a
	//	double AL_dpx2u2_duy1 = 2 * mu_step_size * (px2u2/w[7]) * dpx2u2_duy1/w[7];	// a
	//	double AL_dpx2u2_duy2 = 2 * mu_step_size * (px2u2/w[7]) * dpx2u2_duy2/w[7];	// b	
	//	double AL_dpx2u1_duy1 = 2 * mu_step_size * (px2u1/w[8]) * dpx2u1_duy1/w[8];	// a
	//	double AL_dpx3u2_duy2 = 2 * mu_step_size * (px3u2/w[9]) * dpx3u2_duy2/w[9];	// b
	//	double AL_dpx3u2_duy1 = 2 * mu_step_size * (px3u2/w[9]) * dpx3u2_duy1/w[9];	// a
	//
	//		double AL_dpu1u0_duy1 = 2 * mu_step_size * (pu1u0/w[10]) * dpu1u0_duy1/w[10];	// a
	//    	double AL_dpu2u1_duy1 = 2 * mu_step_size * (pu2u1/w[11]) * dpu2u1_duy1/w[11];	// a
	//    	double AL_dpu2u1_duy2 = 2 * mu_step_size * (pu2u1/w[11]) * dpu2u1_duy2/w[11];	// b

	double AL_duy1_inequality_penalties = AL_dpx1u1_duy1 + AL_dpx2u2_duy1 + AL_dpx2u1_duy1 + AL_dpx3u2_duy1 + AL_dpu1u0_duy1 + AL_dpu2u1_duy1;
	double AL_duy2_inequality_penalties = AL_dpx2u2_duy2 + AL_dpx3u2_duy2 + AL_dpu2u1_duy2;

	////////BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB









	/////////11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111

	double AL_dpu1e1_duy1 = 0.0;	// a
	double AL_dpu1e2_duy1 = 0.0;	// a
	double AL_dpu1e3_duy1 = 0.0;	// 	
	double AL_dpu1e4_duy1 = 0.0;	// a
	double AL_dpu1e5_duy1 = 0.0;	// 

	double AL_dpu2e1_duy2 = 0.0;	// a
	double AL_dpu2e2_duy2 = 0.0;	// a
	double AL_dpu2e3_duy2 = 0.0;	// 	
	double AL_dpu2e4_duy2 = 0.0;	// a
	double AL_dpu2e5_duy2 = 0.0;	// 

	if ((pu1e1 / w_collision[0]) <= 0) {
		AL_dpu1e1_duy1 = 0;
	}
	else if ((pu1e1 / w_collision[0]) > 0) {
		AL_dpu1e1_duy1 = 2 * mu_step_size * (pu1e1 / w_collision[0]) * dpu1e1_duy1 / w_collision[0];	// a
	}

	// if ((pu1e2 / w[13]) <= 0) {
	// 	AL_dpu1e2_duy1 = 0;
	// }
	// else if ((pu1e2 / w[13]) > 0) {
	// 	AL_dpu1e2_duy1 = 2 * mu_step_size * (pu1e2 / w[13]) * dpu1e2_duy1 / w[13];	// a
	// }

	// if ((pu1e3 / w[14]) <= 0) {
	// 	AL_dpu1e3_duy1 = 0;
	// }
	// else if ((pu1e3 / w[14]) > 0) {
	// 	AL_dpu1e3_duy1 = 2 * mu_step_size * (pu1e3 / w[14]) * dpu1e3_duy1 / w[14];	// b	
	// }

	// if ((pu1e4 / w[15]) <= 0) {
	// 	AL_dpu1e4_duy1 = 0;
	// }
	// else if ((pu1e4 / w[15]) > 0) {
	// 	AL_dpu1e4_duy1 = 2 * mu_step_size * (pu1e4 / w[15]) * dpu1e4_duy1 / w[15];	// a
	// }

	// if ((pu1e5 / w[16]) <= 0) {
	// 	AL_dpu1e5_duy1 = 0;
	// }
	// else if ((pu1e5 / w[16]) > 0) {
	// 	AL_dpu1e5_duy1 = 2 * mu_step_size * (pu1e5 / w[16]) * dpu1e5_duy1 / w[16];	// b
	// }





	if ((pu2e1 / w_collision[1]) <= 0) {
		AL_dpu2e1_duy2 = 0;
	}
	else if ((pu2e1 / w_collision[1]) > 0) {
		AL_dpu2e1_duy2 = 2 * mu_step_size * (pu2e1 / w_collision[1]) * dpu2e1_duy2 / w_collision[1];	// a
	}

	// if ((pu2e2 / w[18]) <= 0) {
	// 	AL_dpu2e2_duy2 = 0;
	// }
	// else if ((pu2e2 / w[18]) > 0) {
	// 	AL_dpu2e2_duy2 = 2 * mu_step_size * (pu2e2 / w[18]) * dpu2e2_duy2 / w[18];	// a
	// }

	// if ((pu2e3 / w[19]) <= 0) {
	// 	AL_dpu2e3_duy2 = 0;
	// }
	// else if ((pu2e3 / w[19]) > 0) {
	// 	AL_dpu2e3_duy2 = 2 * mu_step_size * (pu2e3 / w[19]) * dpu2e3_duy2 / w[19];	// a
	// }

	// if ((pu2e4 / w[20]) <= 0) {
	// 	AL_dpu2e4_duy2 = 0;
	// }
	// else if ((pu2e4 / w[20]) > 0) {
	// 	AL_dpu2e4_duy2 = 2 * mu_step_size * (pu2e4 / w[20]) * dpu2e4_duy2 / w[20];	// a
	// }

	// if ((pu2e5 / w[21]) <= 0) {
	// 	AL_dpu2e5_duy2 = 0;
	// }
	// else if ((pu2e5 / w[21]) > 0) {
	// 	AL_dpu2e5_duy2 = 2 * mu_step_size * (pu2e5 / w[21]) * dpu2e5_duy2 / w[21];	// a
	// }




	double AL_vision_duy1_inequality_penalties = AL_dpu1e1_duy1; // + AL_dpu1e2_duy1 + AL_dpu1e3_duy1 + AL_dpu1e4_duy1 + AL_dpu1e5_duy1;

	double AL_vision_duy2_inequality_penalties = AL_dpu2e1_duy2; // + AL_dpu2e2_duy2 + AL_dpu2e3_duy2 + AL_dpu2e4_duy2 + AL_dpu2e5_duy2;

	////////22222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222222


//    cout << "dy inequality penalty " << endl;
//    cout << dpx1u1_duy1 << endl;
//    cout << dpx2u2_duy1 << endl;
//    cout << dpx2u1_duy1 << endl;
//    cout << dpx3u2_duy1 << endl;
//    cout << dpu1u0_duy1 << endl;
//    cout << dpu2u1_duy1 << endl;
//    cout << dpx2u2_duy2 << endl;
//    cout << dpx3u2_duy2 << endl;
//    cout << dpu2u1_duy2 << endl;
//    cout << "dJ with duy gradient" << endl;
//    cout << dJ_duy1(dt_s, u_y, x_y, xdot_y, xddot_y) << endl;
//    cout << dJ_duy2(dt_s, u_y, x_y, xdot_y, xddot_y) << endl;

	Vector2d dJ_duy_;
	dJ_duy_ << dJ_duy1(dt_s, u_y, x_y, xdot_y, xddot_y) + duy1_inequality_penalties + AL_duy1_inequality_penalties + duy1_vision_penalties + AL_vision_duy1_inequality_penalties,
		dJ_duy2(dt_s, u_y, x_y, xdot_y, xddot_y) + duy2_inequality_penalties + AL_duy2_inequality_penalties + duy2_vision_penalties + AL_vision_duy2_inequality_penalties;
	return dJ_duy_;
}

Vector3d dJ_dt(Vector3d dt_s, Matrix<double, 2, 3> u_s, Matrix<double, 2, 4> x_s, Matrix<double, 2, 4> xdot_s, Matrix<double, 2, 4> xddot_s) {
	double dt0 = dt_s[0];
	double dt0_low = max(dt0_min - current_time, 0.001);
	double dt0_high = dt_max;
	double dt1 = dt_s[1];
	double dt1_low = dt_min;
	double dt1_high = dt_max;
	double dt2 = dt_s[2];
	double dt2_low = dt_min;
	double dt2_high = dt_max;

	Vector3d u_x = u_s.row(0);
	Vector3d u_y = u_s.row(1);
	Vector4d x_x = x_s.row(0);
	Vector4d x_y = x_s.row(1);
	Vector4d xdot_x = xdot_s.row(0);
	Vector4d xdot_y = xdot_s.row(1);
	Vector4d xddot_x = xddot_s.row(0);
	Vector4d xddot_y = xddot_s.row(1);

	// first 6 are making sure that minimum and maximum time are within constraints
	double dpt_dt0 = -w[0] + w[1];
	double dpt_dt1 = -w[2] + w[3]; // changed
	double dpt_dt2 = -w[4] + w[5];

	// make sure the next step isn't too far away
	double px1u1 = w[6] * ((x_s.col(1) - u_s.col(1)).transpose() * (x_s.col(1) - u_s.col(1)) - lmax);
	double px2u2 = w[7] * ((x_s.col(2) - u_s.col(2)).transpose() * (x_s.col(2) - u_s.col(2)) - lmax);


	VectorXd v1(2); v1 << dx_dt(dt_s, u_x, x_x, xdot_x, xddot_x, 1, 0),
		dx_dt(dt_s, u_y, x_y, xdot_y, xddot_y, 1, 0);
	double dpx1u1_dt0 = 2 * w[6] * v1.transpose() * (x_s.col(1) - u_s.col(1));



	VectorXd v2(2); v2 << dx_dt(dt_s, u_x, x_x, xdot_x, xddot_x, 2, 0),
		dx_dt(dt_s, u_y, x_y, xdot_y, xddot_y, 2, 0);
	double dpx2u2_dt0 = 2 * w[7] * v2.transpose() * (x_s.col(2) - u_s.col(2));



	VectorXd v3(2); v3 << dx_dt(dt_s, u_x, x_x, xdot_x, xddot_x, 2, 1),
		dx_dt(dt_s, u_y, x_y, xdot_y, xddot_y, 2, 1);
	double dpx2u2_dt1 = 2 * w[7] * v3.transpose() * (x_s.col(2) - u_s.col(2));



	// make sure the current step isn't too far
	double px2u1 = w[8] * ((x_s.col(2) - u_s.col(1)).transpose() * (x_s.col(2) - u_s.col(1)) - lmax);
	double px3u2 = w[9] * ((x_s.col(3) - u_s.col(2)).transpose() * (x_s.col(3) - u_s.col(2)) - lmax);


	VectorXd v4(2); v4 << dx_dt(dt_s, u_x, x_x, xdot_x, xddot_x, 2, 0),
		dx_dt(dt_s, u_y, x_y, xdot_y, xddot_y, 2, 0);
	double dpx2u1_dt0 = 2 * w[8] * v4.transpose() * (x_s.col(2) - u_s.col(1));



	VectorXd v5(2); v5 << dx_dt(dt_s, u_x, x_x, xdot_x, xddot_x, 3, 0),
		dx_dt(dt_s, u_y, x_y, xdot_y, xddot_y, 3, 0);
	double dpx3u2_dt0 = 2 * w[9] * v5.transpose() * (x_s.col(3) - u_s.col(2));



	VectorXd v6(2); v6 << dx_dt(dt_s, u_x, x_x, xdot_x, xddot_x, 2, 1),
		dx_dt(dt_s, u_y, x_y, xdot_y, xddot_y, 2, 1);
	double dpx2u1_dt1 = 2 * w[8] * v6.transpose() * (x_s.col(2) - u_s.col(1));



	VectorXd v7(2); v7 << dx_dt(dt_s, u_x, x_x, xdot_x, xddot_x, 3, 1),
		dx_dt(dt_s, u_y, x_y, xdot_y, xddot_y, 3, 1);
	double dpx3u2_dt1 = 2 * w[9] * v7.transpose() * (x_s.col(3) - u_s.col(2));


	VectorXd v8(2); v8 << dx_dt(dt_s, u_x, x_x, xdot_x, xddot_x, 3, 2),
		dx_dt(dt_s, u_y, x_y, xdot_y, xddot_y, 3, 2);
	double dpx3u2_dt2 = 2 * w[9] * v8.transpose() * (x_s.col(3) - u_s.col(2));

	double dt0_inequality_penalties = dpt_dt0 + dpx1u1_dt0 + dpx2u2_dt0 + dpx2u1_dt0 + dpx3u2_dt0;
	double dt1_inequality_penalties = dpt_dt1 + dpx2u2_dt1 + dpx2u1_dt1 + dpx3u2_dt1;
	double dt2_inequality_penalties = dpt_dt2 + dpx3u2_dt2;




	/////////AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA

	double AL_dpt_dt0_low = 0.0;
	double AL_dpt_dt1_low = 0.0;
	double AL_dpt_dt2_low = 0.0;

	double AL_dpt_dt0_high = 0.0;
	double AL_dpt_dt1_high = 0.0;
	double AL_dpt_dt2_high = 0.0;

	double AL_dpx1u1_dt0 = 0.0;
	double AL_dpx2u2_dt0 = 0.0;
	double AL_dpx2u2_dt1 = 0.0;

	double AL_dpx2u1_dt0 = 0.0;
	double AL_dpx3u2_dt0 = 0.0;
	double AL_dpx2u1_dt1 = 0.0;
	double AL_dpx3u2_dt1 = 0.0;
	double AL_dpx3u2_dt2 = 0.0;


	if ((-dt0 + dt0_low) <= 0) {
		AL_dpt_dt0_low = 0;
	}
	else if ((-dt0 + dt0_low) > 0) {
		AL_dpt_dt0_low = 2 * mu_step_size * (-1 * (-dt0 + dt0_low));
	}
	if ((dt0 - dt0_high) <= 0) {
		AL_dpt_dt0_high = 0;
	}
	else if ((dt0 - dt0_high) > 0) {
		AL_dpt_dt0_high = 2 * mu_step_size * (1 * (dt0 - dt0_high));
	}


	if ((-dt1 + dt1_low) <= 0) {
		AL_dpt_dt1_low = 0;
	}
	else if ((-dt1 + dt1_low) > 0) {
		AL_dpt_dt1_low = 2 * mu_step_size * (-1 * (-dt1 + dt1_low));
	}
	if ((dt1 - dt1_high) <= 0) {
		AL_dpt_dt1_high = 0;
	}
	else if ((dt1 - dt1_high) > 0) {
		AL_dpt_dt1_high = 2 * mu_step_size * (1 * (dt1 - dt1_high));
	}


	if ((-dt2 + dt2_low) <= 0) {
		AL_dpt_dt2_low = 0;
	}
	else if ((-dt2 + dt2_low) > 0) {
		AL_dpt_dt2_low = 2 * mu_step_size * (-1 * (-dt2 + dt2_low));
	}
	if ((dt2 - dt2_high) <= 0) {
		AL_dpt_dt2_high = 0;
	}
	else if ((dt2 - dt2_high) > 0) {
		AL_dpt_dt2_high = 2 * mu_step_size * (1 * (dt2 - dt2_high));
	}





	if ((px1u1 / w[6]) <= 0) {
		AL_dpx1u1_dt0 = 0;
	}
	else if ((px1u1 / w[6]) > 0) {
		AL_dpx1u1_dt0 = 2 * mu_step_size * (px1u1 / w[6]) * dpx1u1_dt0 / w[6];
	}

	if ((px2u2 / w[7]) <= 0) {
		AL_dpx2u2_dt0 = 0;
	}
	else if ((px2u2 / w[7]) > 0) {
		AL_dpx2u2_dt0 = 2 * mu_step_size * (px2u2 / w[7]) * dpx2u2_dt0 / w[7];
	}

	if ((px2u2 / w[7]) <= 0) {
		AL_dpx2u2_dt1 = 0;
	}
	else if ((px2u2 / w[7]) > 0) {
		AL_dpx2u2_dt1 = 2 * mu_step_size * (px2u2 / w[7]) * dpx2u2_dt1 / w[7];
	}

	if ((px2u1 / w[8]) <= 0) {
		AL_dpx2u1_dt0 = 0;
	}
	else if ((px2u1 / w[8]) > 0) {
		AL_dpx2u1_dt0 = 2 * mu_step_size * (px2u1 / w[8]) * dpx2u1_dt0 / w[8];
	}

	if ((px3u2 / w[9]) <= 0) {
		AL_dpx3u2_dt0 = 0;
	}
	else if ((px3u2 / w[9]) > 0) {
		AL_dpx3u2_dt0 = 2 * mu_step_size * (px3u2 / w[9]) * dpx3u2_dt0 / w[9];
	}

	if ((px2u1 / w[8]) <= 0) {
		AL_dpx2u1_dt1 = 0;
	}
	else if ((px2u1 / w[8]) > 0) {
		AL_dpx2u1_dt1 = 2 * mu_step_size * (px2u1 / w[8]) * dpx2u1_dt1 / w[8];
	}

	if ((px3u2 / w[9]) <= 0) {
		AL_dpx3u2_dt1 = 0;
	}
	else if ((px3u2 / w[9]) > 0) {
		AL_dpx3u2_dt1 = 2 * mu_step_size * (px3u2 / w[9]) * dpx3u2_dt1 / w[9];
	}

	if ((px3u2 / w[9]) <= 0) {
		AL_dpx3u2_dt2 = 0;
	}
	else if ((px3u2 / w[9]) > 0) {
		AL_dpx3u2_dt2 = 2 * mu_step_size * (px3u2 / w[9]) * dpx3u2_dt2 / w[9];
	}


	double AL_dt0_inequality_penalties = AL_dpt_dt0_low + AL_dpt_dt0_high + AL_dpx1u1_dt0 + AL_dpx2u2_dt0 + AL_dpx2u1_dt0 + AL_dpx3u2_dt0;
	double AL_dt1_inequality_penalties = AL_dpt_dt1_low + AL_dpt_dt1_high + AL_dpx2u2_dt1 + AL_dpx2u1_dt1 + AL_dpx3u2_dt1;
	double AL_dt2_inequality_penalties = AL_dpt_dt2_low + AL_dpt_dt2_high + AL_dpx3u2_dt2;


	/////////BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB








//    cout<< "dt inequality constraint" << endl;

//    cout << "Jacobian dt element " << endl;
//    cout << dpt_dt0 << endl;
//    cout << dpt_dt1 << endl;
//    cout << dpt_dt2 << endl;
//    cout << dpx1u1_dt0 << endl;
//    cout << dpx2u1_dt0 << endl;
//    cout << dpx2u2_dt0 << endl;
//    cout << dpx3u2_dt0 << endl;
//    cout << dpx2u1_dt1 << endl;
//    cout << dpx2u2_dt1 << endl;
//    cout << dpx3u2_dt1 << endl;
//    cout << dpx3u2_dt2 << endl;
//    cout << dpdy3dy3_dt2 << endl;

	Vector3d dJ_dt_;

	//    cout << "dJ with dt gradient" << endl;
	//    cout << dJ_dt0(dt_s, u_s, x_s, xdot_s, xddot_s) << endl;
	//    cout << dJ_dt1(dt_s, u_s, x_s, xdot_s, xddot_s) << endl;
	//    cout << dJ_dt2(dt_s, u_s, x_s, xdot_s, xddot_s) << endl;

	dJ_dt_ << dJ_dt0(dt_s, u_s, x_s, xdot_s, xddot_s) + dt0_inequality_penalties + AL_dt0_inequality_penalties, //
		dJ_dt1(dt_s, u_s, x_s, xdot_s, xddot_s) + dt1_inequality_penalties + AL_dt1_inequality_penalties, //
		dJ_dt2(dt_s, u_s, x_s, xdot_s, xddot_s) + dt2_inequality_penalties + AL_dt2_inequality_penalties; //

	return dJ_dt_;
}


double J(Vector3d dt_s, Matrix<double, 2, 3> u_s, Matrix<double, 2, 4> x_s, Matrix<double, 2, 4> xdot_s, Matrix<double, 2, 4> xddot_s) {
	double J = 0.0;
	for (int i = 1; i < u_s.size() / 2 + 1; i++) {   ////Here we are summing the Js over the time horizon you are optimizing for
		J += pow(xdot_s(0, i) - x_dot_ref[0], 2) * Qx + pow(xdot_s(1, i) - x_dot_ref[1], 2) * Qy;    /////     J = J + (xdot_s-x_dot_ref)^2 + (xdot_s-x_dot_ref)^2
	}
	return J;
}








// get lambda gradients		////This is the constrained part. JACOPO will need to deal with the one that has the w
VectorXd Constraints(Vector3d dt_s, Matrix<double, 2, 3> u_s, Matrix<double, 2, 4> x_s, Matrix<double, 2, 4> xdot_s, Matrix<double, 2, 4> xddot_s)
{	//// These are the gradient in respect of each of the constraints
	double dt0_low = 0.2 - (current_time + dt_s(0)); //dt_min
	double dt0_high = (current_time + dt_s(0)) - dt_max;
	double dt1_low = dt_min - dt_s(1);
	double dt1_high = dt_s(1) - dt_max;
	double dt2_low = dt_min - dt_s(2);
	double dt2_high = dt_s(2) - dt_max;

	////Gradients in respect to the lagrange multipliers  ///// Essentially the change in w:   dL/dw
	double l_grad1 = pow(u_s(0, 1) - x_s(0, 1), 2) + pow(u_s(1, 1) - x_s(1, 1), 2) - pow(0.5 * sqrt(pow(L, 2) - pow(h, 2)), 2);
	double l_grad2 = pow(u_s(0, 2) - x_s(0, 2), 2) + pow(u_s(1, 2) - x_s(1, 2), 2) - pow(0.5 * sqrt(pow(L, 2) - pow(h, 2)), 2);
	double l_grad3 = pow(u_s(0, 1) - x_s(0, 2), 2) + pow(u_s(1, 1) - x_s(1, 2), 2) - pow(0.5 * sqrt(pow(L, 2) - pow(h, 2)), 2);
	double l_grad4 = pow(u_s(0, 2) - x_s(0, 3), 2) + pow(u_s(1, 2) - x_s(1, 3), 2) - pow(0.5 * sqrt(pow(L, 2) - pow(h, 2)), 2);


	double foot_grad1 = rfoot - foot_flag * (u_s(1, 1) - u_s(1, 0));
	double foot_grad2 = rfoot + foot_flag * (u_s(1, 2) - u_s(1, 1));

	double collision_grad1 = (-pow((u_s(0, 1) - x1) * cos(theta1) + (u_s(1, 1) - yyy1) * sin(theta1),2) / pow(Rx1,2) - pow((u_s(0, 1) - x1) * sin(theta1) + (u_s(1, 1) - yyy1) * cos(theta1),2) / pow(Ry1,2) + 1);
	double collision_grad2 = (-pow((u_s(0, 2) - x1) * cos(theta1) + (u_s(1, 2) - yyy1) * sin(theta1),2) / pow(Rx1,2) - pow((u_s(0, 2) - x1) * sin(theta1) + (u_s(1, 2) - yyy1) * cos(theta1),2) / pow(Ry1,2) + 1);
	




	VectorXd inEquality_grad(14);
	inEquality_grad << dt0_low,	// 1
		dt0_high,	// 2
		dt1_low, 	// 3
		dt1_high, 	// 4
		dt2_low, 	// 5
		dt2_high, 	// 6
		l_grad1, 	// 7
		l_grad2, 	// 8
		l_grad3, 	// 9
		l_grad4, 	// 10
		foot_grad1, 	// 11
		foot_grad2,	// 12
		-collision_grad1,
		-collision_grad2;


//    cout << "inEquality_grad is " << inEquality_grad << endl;

//    inEquality_grad = inEquality_grad/inEquality_grad.norm();
	return inEquality_grad;
}









// bfgs BFGS(bfgs& prev_result, bool verbose = true) {     ////////TO BE DELETED---------------------------------------------------
// 	// previous solution
// 	VectorXd x_k = prev_result.x_k;
// 	VectorXd delta_x_k = prev_result.delta_x_k;
// 	//	VectorXd y_k = prev_result.y_k;x_k
// //	MatrixXd B_k = prev_result.B_k;
// 	MatrixXd H_k = prev_result.H_k;
// 	MatrixXd I = MatrixXd::Identity(7, 7);
// 	//	MatrixXd I = HessianInv;

// 	MatrixXd x_s, xdot_s, xddot_s;
// 	Vector3d dt_old = x_k.segment(0, 3);				// get dts

// 	Matrix<double, 2, 3>& u_old = u_s;						// get us
// 	u_old.block(0, 1, 1, 2) = x_k.segment(3, 2).transpose();
// 	u_old.block(1, 1, 1, 2) = x_k.segment(5, 2).transpose();

// 	x_s = MatrixXd::Zero(2, 4);
// 	x_s(0, 0) = x_0(0);
// 	x_s(1, 0) = x_0(1);

// 	xdot_s = MatrixXd::Zero(2, 4);
// 	xdot_s(0, 0) = x_dot_0(0);
// 	xdot_s(1, 0) = x_dot_0(1);

// 	xddot_s = MatrixXd::Zero(2, 4);
// 	xddot_s(0, 0) = xddot(0.0, u_old(0, 0), x_s(0, 0), xdot_s(0, 0));
// 	xddot_s(1, 0) = xddot(0.0, u_old(1, 0), x_s(1, 0), xdot_s(1, 0));
// 	for (int k = 0; k < 3; k++) {
// 		x_s(0, k + 1) = x(dt_old[k], u_old(0, k), x_s(0, k), xdot_s(0, k));
// 		x_s(1, k + 1) = x(dt_old[k], u_old(1, k), x_s(1, k), xdot_s(1, k));
// 		xdot_s(0, k + 1) = xdot(dt_old[k], u_old(0, k), x_s(0, k), xdot_s(0, k));
// 		xdot_s(1, k + 1) = xdot(dt_old[k], u_old(1, k), x_s(1, k), xdot_s(1, k));
// 		xddot_s(0, k + 1) = xddot(dt_old[k], u_old(0, k), x_s(0, k), xdot_s(0, k));
// 		xddot_s(1, k + 1) = xddot(dt_old[k], u_old(1, k), x_s(1, k), xdot_s(1, k));
// 	}

// 	// calculate gradients at step k
// 	Vector3d grad_t = dJ_dt(dt_old, u_old, x_s, xdot_s, xddot_s); // 3
// 	Vector2d grad_ux = dJ_dux(dt_old, u_old, x_s, xdot_s, xddot_s); // 2
// 	Vector2d grad_uy = dJ_duy(dt_old, u_old, x_s, xdot_s, xddot_s); // 2
// 	VectorXd grad_k(7);
// 	grad_k << grad_t, grad_ux, grad_uy;
// 	//    cout<< "old gradients\n" << grad_k.transpose() << endl;
// 	//    cout << "old solution\n" << x_k.transpose() << endl;
// 	//    cout << "old COM positions\n" << x_s << endl;
// 	//    cout << "old COM velocities\n" << xdot_s << endl;
// 		// line search to find update size
// 	double alpha_k = 1e5;
// 	double best_cost = 1e5;
// 	double iteration_num = 100;
// 	//    cout<< "line searcwith h " << endl;
// 	for (int i = 0; i < iteration_num; i++) {
// 		double alpha_int = (iteration_num - i) / (iteration_num * 100);						// intermediate value of alpha
// 		// get intermediate dt_int and u_int
// 		VectorXd delta_x_int = -alpha_int * H_k * grad_k;
// 		VectorXd x_k_int = x_k + delta_x_int;		// intermediate parameter valu
// 	//		cout<< "x_k_int is " << x_k_int << endl;
// 		MatrixXd x_int, xdot_int, xddot_int;
// 		Vector3d dt_int = x_k_int.segment(0, 3);				// get dts

// 		Matrix<double, 2, 3>& u_int = u_s;						// get us
// 		u_int.block(0, 1, 1, 2) = x_k_int.segment(3, 2).transpose();
// 		u_int.block(1, 1, 1, 2) = x_k_int.segment(5, 2).transpose();

// 		x_int = MatrixXd::Zero(2, 4);
// 		x_int(0, 0) = x_0(0);
// 		x_int(1, 0) = x_0(1);
// 		xdot_int = MatrixXd::Zero(2, 4);
// 		xdot_int(0, 0) = x_dot_0(0);
// 		xdot_int(1, 0) = x_dot_0(1);
// 		xddot_int = MatrixXd::Zero(2, 4);
// 		xddot_int(0, 0) = xddot(0.0, u_int(0, 0), x_int(0, 0), xdot_int(0, 0));
// 		xddot_int(1, 0) = xddot(0.0, u_int(1, 0), x_int(1, 0), xdot_int(1, 0));

// 		for (int k = 0; k < 3; k++) {
// 			x_int(0, k + 1) = x(dt_int[k], u_int(0, k), x_int(0, k), xdot_int(0, k));
// 			x_int(1, k + 1) = x(dt_int[k], u_int(1, k), x_int(1, k), xdot_int(1, k));
// 			xdot_int(0, k + 1) = xdot(dt_int[k], u_int(0, k), x_int(0, k), xdot_int(0, k));
// 			xdot_int(1, k + 1) = xdot(dt_int[k], u_int(1, k), x_int(1, k), xdot_int(1, k));
// 			xddot_int(0, k + 1) = xddot(dt_int[k], u_int(0, k), x_int(0, k), xdot_int(0, k));
// 			xddot_int(1, k + 1) = xddot(dt_int[k], u_int(1, k), x_int(1, k), xdot_int(1, k));
// 		}

// 		// calculate cost with intermediate values
// 		double cost = J(dt_int, u_int, x_int, xdot_int, xddot_int);		// calculate cost
// 	//		cout << "alpha" << alpha_int << endl;
// 	//		cout << "cost" << cost << endl;
// 		if (cost < best_cost) {											// if cost has improved, keep corresponding best alpha
// 			alpha_k = alpha_int;
// 			best_cost = cost;
// 		}
// 	}

// 	cout << "final alpha is " << alpha_k << endl;
// 	cout << "final cost is " << best_cost << endl;

// 	delta_x_k = -alpha_k * H_k * grad_k;
// 	VectorXd x_k_new(7);
// 	x_k_new << x_k + delta_x_k;
// 	//	cout << "delta_x_k" << delta_x_k << endl;
// 	//    cout << "new solution" << x_k_new << endl;

// 	MatrixXd x_new, xdot_new, xddot_new;
// 	Vector3d dt_new = x_k_new.segment(0, 3);				// get dts
// 	Matrix<double, 2, 3>& u_new = u_s;						// get us
// 	u_new.block(0, 1, 1, 2) = x_k_new.segment(3, 2).transpose();
// 	u_new.block(1, 1, 1, 2) = x_k_new.segment(5, 2).transpose();

// 	x_new = MatrixXd::Zero(2, 4);
// 	x_new(0, 0) = x_0(0);
// 	x_new(1, 0) = x_0(1);
// 	xdot_new = MatrixXd::Zero(2, 4);
// 	xdot_new(0, 0) = x_dot_0(0);
// 	xdot_new(1, 0) = x_dot_0(1);
// 	xddot_new = MatrixXd::Zero(2, 4);
// 	xddot_new(0, 0) = xddot(0.0, u_new(0, 0), x_new(0, 0), xdot_new(0, 0));
// 	xddot_new(1, 0) = xddot(0.0, u_new(1, 0), x_new(1, 0), xdot_new(1, 0));

// 	for (int k = 0; k < 3; k++) {
// 		x_new(0, k + 1) = x(dt_new[k], u_new(0, k), x_new(0, k), xdot_new(0, k));
// 		x_new(1, k + 1) = x(dt_new[k], u_new(1, k), x_new(1, k), xdot_new(1, k));
// 		xdot_new(0, k + 1) = xdot(dt_new[k], u_new(0, k), x_new(0, k), xdot_new(0, k));
// 		xdot_new(1, k + 1) = xdot(dt_new[k], u_new(1, k), x_new(1, k), xdot_new(1, k));
// 		xddot_new(0, k + 1) = xddot(dt_new[k], u_new(0, k), x_new(0, k), xdot_new(0, k));
// 		xddot_new(1, k + 1) = xddot(dt_new[k], u_new(1, k), x_new(1, k), xdot_new(1, k));
// 	}

// 	// cout << "new solution\n" << x_k_new << endl;
// 	// cout << "x_new\n" << x_new << endl;
// 	// cout << "xdot_new\n" << xdot_new << endl;
// 	// cout << "xddot_new\n" << xddot_new << endl;

// 	// calculate gradients at step k
// 	Vector3d grad_t_new = dJ_dt(dt_new, u_new, x_new, xdot_new, xddot_new);
// 	Vector2d grad_ux_new = dJ_dux(dt_new, u_new, x_new, xdot_new, xddot_new);
// 	Vector2d grad_uy_new = dJ_duy(dt_new, u_new, x_new, xdot_new, xddot_new);
// 	VectorXd grad_k_new(7);
// 	grad_k_new << grad_t_new, grad_ux_new, grad_uy_new;
// 	//	cout<< "new gradients\n" << grad_k_new.transpose() << endl;	// should get smaller (hopefully)
// 	//    cout << "new solution\n" << x_k_new.transpose() << endl;
// 	//    cout << "new COM positions\n" << x_new << endl;
// 	//    cout << "new COM velocities\n" << xdot_new << endl;
// 	//    cout << "---------------------------------------------------------------------" << endl;
// 	VectorXd y_k(7);
// 	y_k << grad_k_new - grad_k;
// 	//    cout << "y_k" << y_k << endl;
// 	MatrixXd B_k_new(7, 7);
// 	//	B_k_new<< B_k + y_k * y_k.transpose() / (y_k.transpose() * delta_x_k) - B_k * delta_x_k * (B_k * delta_x_k).transpose() / (delta_x_k.transpose() * B_k * delta_x_k);
// 	MatrixXd H_k_new(7, 7);
// 	H_k_new << (I - delta_x_k * y_k.transpose() / (y_k.transpose() * delta_x_k)) * H_k * (I - y_k * delta_x_k.transpose() / (y_k.transpose() * delta_x_k)) + delta_x_k * delta_x_k.transpose() / (y_k.transpose() * delta_x_k);

// 	// cout << "y_k " << y_k << endl;
// 	// cout << "delta_x_k " << delta_x_k << endl;
// 	// cout << "y_k.transpose() * delta_x_k\n" << y_k.transpose() * delta_x_k << endl;
// 	// cout << "delta_x_k * y_k.transpose()\n" << delta_x_k * y_k.transpose() / (y_k.transpose() * delta_x_k) << endl;
// 	// cout << "y_k * delta_x_k.transpose()\n" << y_k * delta_x_k.transpose() / (y_k.transpose() * delta_x_k) << endl;
// 	// cout << "delta_x_k * delta_x_k.transpose()\n" << delta_x_k * delta_x_k.transpose() / (y_k.transpose() * delta_x_k) << endl;
// 	// cout << "H_k * (I - y_k * delta_x_k.transpose() / (y_k.transpose() * delta_x_k))\n" << H_k * (I - y_k * delta_x_k.transpose() / (y_k.transpose() * delta_x_k)) << endl;
// 	// cout << "(I - delta_x_k * y_k.transpose() / (y_k.transpose() * delta_x_k)) *H_k * (I - y_k * delta_x_k.transpose() / (y_k.transpose() * delta_x_k))\n" << (I - delta_x_k * y_k.transpose() / (y_k.transpose() * delta_x_k)) *H_k * (I - y_k * delta_x_k.transpose() / (y_k.transpose() * delta_x_k)) << endl;
// 	// cout << "H_k new\n" << H_k_new << endl;

// 	bfgs result;
// 	result.x_k = x_k_new;
// 	result.delta_x_k = delta_x_k;
// 	result.y_k = y_k;
// 	//	result.B_k = B_k_new;
// 	result.H_k = H_k_new;
// 	//	cout << "BFGS Values " << endl;
// 	//	cout << "x_k" << result.x_k << endl;
// 	//	cout << "delta_k" << result.delta_x_k << endl;
// 	//	cout << "y_k" << result.y_k << endl;
// 	//	cout << "B_k" << result.B_k << endl;
// 	//	cout << "H_k" << result.H_k << endl;
// 	return result;
// }





////These are the callbacks. Note that we don't use RK4 anymore
// Callback function for receiving the planner outputs
void footstepPlanCallbackRK4(const std_msgs::Float64MultiArray::ConstPtr& msg)
{
	//    cout<< "received rk4 data " << endl;
	u_s_rk4(0, 1) = msg->data[0];
	u_s_rk4(1, 1) = msg->data[1];
	u_s_rk4(0, 2) = msg->data[2];
	u_s_rk4(1, 2) = msg->data[3];
	t_s_rk4(0) = msg->data[4];
	t_s_rk4(1) = msg->data[5];
	t_s_rk4(2) = msg->data[6];
	u_s = u_s_rk4;
	dt_s = t_s_rk4;
	support_foot_flag = msg->data[7];
	// flag = 1;
//    cout << "--------------------------------" << endl;
//    cout<< "received u \n" << u_s << endl;
//    cout<< "received t \n" << dt_s << endl;

	solution.x_k = VectorXd::Zero(7);
	solution.x_k.segment(0, 3) = dt_s.transpose();
	solution.x_k.segment(3, 2) = u_s.block(0, 1, 1, 2).transpose();
	solution.x_k.segment(5, 2) = u_s.block(1, 1, 1, 2).transpose();
	solution.delta_x_k = VectorXd::Zero(7);
	solution.y_k = VectorXd::Zero(7);
	solution.B_k = Matrix<double, 7, 7>::Identity();
	solution.H_k = Matrix<double, 7, 7>::Identity();
	rk4_received = true;
}



/////This receives a message from whole body controller. Then sets the values to the data
void plannerInputCallback(const std_msgs::Float64MultiArray::ConstPtr& msg)
{
	//    x, x_dot, y, y_dot = msg.data[:4]
	x_0(0) = msg->data[0];
	x_dot_0(0) = msg->data[1];
	x_dot_dot_0(0) = msg->data[2];
	x_0(1) = msg->data[3];
	x_dot_0(1) = msg->data[4];
	x_dot_dot_0(1) = msg->data[5];
	left_x = msg->data[6];
	left_y = msg->data[7];
	right_x = msg->data[8];
	right_y = msg->data[9];
	foot_flag = msg->data[10];
	current_time = msg->data[11];
	//    cout << "received states " << endl;
	//    cout << "x " << x_0(0) << endl;
	//    cout << "x dot " << x_dot_0(0) << endl;
	//    cout << x_dot_dot_0(0) << endl;
	//    cout << "y " << x_0(1) << endl;
	//    cout << "y dot " << x_dot_0(1) << endl;
	//    cout << x_dot_dot_0(1) << endl;
	//    cout << left_x << endl;
	//    cout << left_y << endl;
	//    cout << right_x << endl;
	//    cout << right_y << endl;
	//    cout << foot_flag << endl;
	//    cout << current_time << endl;
}



////Setting the weights (i.e. Lagrange multipliers) from the most recent data
void weightCallback(const std_msgs::Float64MultiArray::ConstPtr& msg)
{
	w[0] = msg->data[0];	// lambda    dt0
	w[1] = msg->data[1];	// lambda    dt0
	w[2] = msg->data[2];	// lambda    dt1
	w[3] = msg->data[3];	// lambda    dt1
	w[4] = msg->data[4];	// lambda    dt2
	w[5] = msg->data[5];	// lambda    dt2
	w[6] = msg->data[6];	// lambda1   lmax
	w[7] = msg->data[7];	// lambda2   lmax
	w[8] = msg->data[8];	// lambda3   lmax
	w[9] = msg->data[9];	// lambda4   lmax
	w[10] = msg->data[10];	// lambda1   rfoot
	w[11] = msg->data[11];	// lambda2   rfoot
	// w_collision[0] = msg->data[12];
	// w_collision[1] = msg->data[13];
	w_collision[0] = 0.0;
	w_collision[1] = 0.0;
	// w[14] = msg->data[14];
	// w[15] = msg->data[15];
	// w[16] = msg->data[16];
	// w[17] = msg->data[17];
	// w[18] = msg->data[18];
	// w[19] = msg->data[19];
	// w[20] = msg->data[20];
	// w[21] = msg->data[21];
//    w[0] = 1.0;
//    w[1] = 1.0;
//    w[2] = 1.0;
//    w[3] = 1.0;
//    w[4] = 1.0;
//    w[5] = 1.0;
//    w[6] = 1.0;
//    w[7] = 1.0;
//    w[8] = 1.0;
//    w[9] = 1.0;
//    w[10] = 1.0;
//    w[11] = 1.0;
	cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << w << endl;
	cout << "The received weights are " << w << endl;
	cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << w << endl;
	w_received = true;
}



///// Setting Jacobian in respect to weight. In case you are using second order approximation (which we dont)
void jacobianCallback(const std_msgs::Float64MultiArray::ConstPtr& msg)
{
	std::vector<double> data = msg->data;
	Eigen::Map<Eigen::MatrixXd> mat(data.data(), 30, 16);
	Jacobian = mat;
	//	cout<<"The received jacobian is " << Jacobian << endl;
}



/////Function that receives the Jacobian and converts to Hessian (NOTE: but no worries, we don't need that)
void hessianCallback(const std_msgs::Float64MultiArray::ConstPtr& msg)
{
	std::vector<double> data = msg->data;
	Eigen::Map<Eigen::MatrixXd> mat(data.data(), 19, 19);
	//	solution.H_k = mat;
	//	HessianInv = mat;
	//	cout<<"The received jacobian is " << Jacobian << endl;
}

void ellipseCallback(const std_msgs::Float64MultiArray::ConstPtr& msg)
{
	x1 = msg->data[0];
	yyy1 = msg->data[1];
	Rx1 = msg->data[2];
	Ry1 = msg->data[3];
	theta1 = msg->data[4];
	// x2 = msg->data[5];
	// y2 = msg->data[6];
	// Rx2 = msg->data[7];
	// Ry2 = msg->data[8];
	// theta2 = msg->data[9];
	// x3 = msg->data[10];
	// y3 = msg->data[11];
	// Rx3 = msg->data[12];
	// Ry3 = msg->data[13];
	// theta3 = msg->data[14];
	// x4 = msg->data[15];
	// y4 = msg->data[16];
	// Rx4 = msg->data[17];
	// Ry4 = msg->data[18];
	// theta4 = msg->data[19];
	// x5 = msg->data[20];
	// y5 = msg->data[21];
	// Rx5 = msg->data[22];
	// Ry5 = msg->data[23];
	// theta5 = msg->data[24];

}

ofstream myFile;

int main(int argc, char** argv) {
	Vector4d x_opt, y_opt;
	const double LOOP_RATE = 200;
	int iter_main = 0;
	ros::init(argc, argv, "gradient_descent_footPlanner_node");
	myFile.open("COM_AL.csv");



	//////Suscribers that need to be used (i.e. Planner input) for the test environment
	ros::NodeHandle n;
	ros::Rate loop_rate(LOOP_RATE);
	ros::Publisher planner_output_pub = n.advertise<std_msgs::Float64MultiArray>("/time_slider_gazebo/footstep_plan", 1);
	ros::Publisher planner_gradient_pub = n.advertise<std_msgs::Float64MultiArray>("/time_slider_gazebo/gd_gradients", 1);
	ros::Subscriber sub_planner_input = n.subscribe<std_msgs::Float64MultiArray>("/time_slider_gazebo/planner_input", 'None', plannerInputCallback);
	ros::Subscriber sub_RK4_planner_input = n.subscribe<std_msgs::Float64MultiArray>("/time_slider_gazebo/footstep_plan_rk4", 'None', footstepPlanCallbackRK4);
	ros::Subscriber sub_weight = n.subscribe<std_msgs::Float64MultiArray>("/time_slider_gazebo/cost_weight", 'None', weightCallback);
	ros::Subscriber sub_ellipse = n.subscribe<std_msgs::Float64MultiArray>("/time_slider_gazebo/footstep_plan/ellipses", 'None', ellipseCallback);
	//    ros::Subscriber sub_jacobian      = n.subscribe<std_msgs::Float64MultiArray>("/time_slider_gazebo/jacobian", 'None', jacobianCallback);
	//    ros::Subscriber sub_hessian       = n.subscribe<std_msgs::Float64MultiArray>("/time_slider_gazebo/hessianInv", 'None', hessianCallback);
	support_foot_flag = foot_flag;




	///// Getting the existing solutions 
	Jacobian = MatrixXd::Zero(30, 16);
	HessianInv = MatrixXd::Zero(19, 19);
	solution.x_k = VectorXd::Zero(7);
	w = VectorXd::Zero(12);
	w_collision = VectorXd::Zero(2);
	//    dyn_w  = VectorXd::Zero(n_weights);
		//cout << "started" << endl;


	MatrixXd x_s, xdot_s, xddot_s;			/////We are defining x_s for x and y (?)
	x_s = MatrixXd::Zero(2, 4);
	xdot_s = MatrixXd::Zero(2, 4);
	xddot_s = MatrixXd::Zero(2, 4);

	x_s(0, 0) = x_0(0);
	x_s(1, 0) = x_0(1);
	xdot_s(0, 0) = x_dot_0(0);
	xdot_s(1, 0) = x_dot_0(1);
	xddot_s(0, 0) = xddot(0.0, u_s(0, 0), x_s(0, 0), xdot_s(0, 0));
	xddot_s(1, 0) = xddot(0.0, u_s(1, 0), x_s(1, 0), xdot_s(1, 0));

	for (int k = 0; k < 3; k++) {
		x_s(0, k + 1) = x(dt_s[k], u_s(0, k), x_s(0, k), xdot_s(0, k));
		x_s(1, k + 1) = x(dt_s[k], u_s(1, k), x_s(1, k), xdot_s(1, k));
		xdot_s(0, k + 1) = xdot(dt_s[k], u_s(0, k), x_s(0, k), xdot_s(0, k));
		xdot_s(1, k + 1) = xdot(dt_s[k], u_s(1, k), x_s(1, k), xdot_s(1, k));
		xddot_s(0, k + 1) = xddot(dt_s[k], u_s(0, k), x_s(0, k), xdot_s(0, k));
		xddot_s(1, k + 1) = xddot(dt_s[k], u_s(1, k), x_s(1, k), xdot_s(1, k));
	}
	//cout << "u_s" << u_s << endl;
	//cout << "dT_s" << dt_s << endl;
	//cout << "x_s" << x_s << endl;
	//cout << "xdot_s" << xdot_s << endl;
	//cout << "xddot_s" << xddot_s << endl;

	// calculate gradients at step k
	Vector3d grad_t = dJ_dt(dt_s, u_s, x_s, xdot_s, xddot_s);
	Vector2d grad_ux = dJ_dux(dt_s, u_s, x_s, xdot_s, xddot_s);
	Vector2d grad_uy = dJ_duy(dt_s, u_s, x_s, xdot_s, xddot_s);
	//	cout<< "calculate dJ_dt, first iteration" << endl;
	//	cout<< "calculate dJ_dux, first iteration" << endl;
	//	cout<< "calculate dJ_duy, first iteration" << endl;

	VectorXd grad_k(7);
	grad_k << grad_t, grad_ux, grad_uy;

	VectorXd lamda_grad;
	double lamda_step_size = 0.005;
	double step_size = 1e-4;
	// double step_size = 0.0001;


	/////////AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA

	//double mu_step_size = 0.005;
	//double phi_scaling  = 1.0001;


	/////////BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB





	//cout << "The weight is " << w << endl;
	//cout << "The gradients are " << grad_k << endl;
	MatrixXd J_dxdt(4, 3);
	MatrixXd J_dydt(4, 3);
	MatrixXd w_buffer(3, 12);
	MatrixXd w_buffer_collision(3, 2);
	w_buffer << 0.0, 0.0, 0.0,
		0.0, 0.0, 0.0,
		0.0, 0.0, 0.0,
		0.0, 0.0, 0.0,
		0.0, 0.0, 0.0,
		0.0, 0.0, 0.0,
		0.0, 0.0, 0.0,
		0.0, 0.0, 0.0,
		0.0, 0.0, 0.0,
		0.0, 0.0, 0.0,
		0.0, 0.0, 0.0,
		0.0, 0.0, 0.0;

	w_buffer_collision << 0.0, 0.0, 0.0,
		0.0, 0.0, 0.0;

	//	J_dxdt = MatrixXd::Zero(4,3);

	while (ros::ok()) {
		//        if(rk4_received){
		auto t1 = std::chrono::high_resolution_clock::now();
		//            cout << "u ini is \t" << u_s << endl;
		//            cout << "t ini is \t" << dt_s << endl;
		//            cout << "current robot COM pos is \t" << x_0 << endl;
		//            cout << "current robot COM vel is \t" << x_dot_0 << endl;

				////////////// Gradient Descent ////////////////////////////////
		if (foot_flag == -1) {
			u_s(0, 0) = left_x;
			u_s(1, 0) = left_y;
		}
		else if (foot_flag == 1) {
			u_s(0, 0) = right_x;
			u_s(1, 0) = right_y;
		}

		MatrixXd x_s, xdot_s, xddot_s;
		x_s = MatrixXd::Zero(2, 4);
		xdot_s = MatrixXd::Zero(2, 4);
		xddot_s = MatrixXd::Zero(2, 4);
		x_s(0, 0) = x_0(0);
		x_s(1, 0) = x_0(1);
		xdot_s(0, 0) = x_dot_0(0);
		xdot_s(1, 0) = x_dot_0(1);
		xddot_s(0, 0) = xddot(0.0, u_s(0, 0), x_s(0, 0), xdot_s(0, 0));
		xddot_s(1, 0) = xddot(0.0, u_s(1, 0), x_s(1, 0), xdot_s(1, 0));
		for (int k = 0; k < 3; k++) {
			x_s(0, k + 1) = x(dt_s[k], u_s(0, k), x_s(0, k), xdot_s(0, k));
			x_s(1, k + 1) = x(dt_s[k], u_s(1, k), x_s(1, k), xdot_s(1, k));
			xdot_s(0, k + 1) = xdot(dt_s[k], u_s(0, k), x_s(0, k), xdot_s(0, k));
			xdot_s(1, k + 1) = xdot(dt_s[k], u_s(1, k), x_s(1, k), xdot_s(1, k));
			xddot_s(0, k + 1) = xddot(dt_s[k], u_s(0, k), x_s(0, k), xdot_s(0, k));
			xddot_s(1, k + 1) = xddot(dt_s[k], u_s(1, k), x_s(1, k), xdot_s(1, k));
		}
		//cout << "x_s" << x_s << endl;
		//cout << "xdot_s" << xdot_s << endl;
//		cout << "u_s " << u_s << endl;


			// calculate gradients at step k
		Vector3d grad_t = dJ_dt(dt_s, u_s, x_s, xdot_s, xddot_s);
		Vector2d grad_ux = dJ_dux(dt_s, u_s, x_s, xdot_s, xddot_s);
		Vector2d grad_uy = dJ_duy(dt_s, u_s, x_s, xdot_s, xddot_s);

		VectorXd grad_k(7);  ////?????????????????????????????????????????????????????????????????????????????
		grad_k << grad_t, grad_ux, grad_uy;
		//            cout << "The gradients are " << grad_k << endl;

		//			cout << "sol " << solution.x_k << endl;

		//            for(int i=0; i<10; i++){
		//                x_s = MatrixXd::Zero(2, 4);
		//                x_s(0, 0) = x_0(0);
		//                x_s(1, 0) = x_0(1);
		//                xdot_s = MatrixXd::Zero(2, 4);
		//                xdot_s(0, 0) = x_dot_0(0);
		//                xdot_s(1, 0) = x_dot_0(1);
		//                xddot_s = MatrixXd::Zero(2, 4);
		//	            xddot_s(0, 0) = xddot(0.0, u_s(0, 0), x_s(0, 0), xdot_s(0, 0));
		//	            xddot_s(1, 0) = xddot(0.0, u_s(1, 0), x_s(1, 0), xdot_s(1, 0));
		//
		//                for (int k = 0; k < 3; k++) {
		//                    x_s(0, k + 1) = x(dt_s[k], u_s(0, k), x_s(0, k), xdot_s(0, k));
		//                    x_s(1, k + 1) = x(dt_s[k], u_s(1, k), x_s(1, k), xdot_s(1, k));
		//                    xdot_s(0, k + 1) = xdot(dt_s[k], u_s(0, k), x_s(0, k), xdot_s(0, k));
		//                    xdot_s(1, k + 1) = xdot(dt_s[k], u_s(1, k), x_s(1, k), xdot_s(1, k));
		//                    xddot_s(0, k + 1) = xddot(dt_s[k], u_s(0, k), x_s(0, k), xdot_s(0, k));
		//                    xddot_s(1, k + 1) = xddot(dt_s[k], u_s(1, k), x_s(1, k), xdot_s(1, k));
		//                }
		////                lamda_grad = Constraints(dt_s, u_s, x_s, xdot_s, xddot_s);
		////                cout << "state is " << endl;
		////                cout << "x is " << x_s << endl;
		////                cout << "xdot is " << xdot_s << endl;
		////                w     += lamda_step_size * lamda_grad.segment(0, 12);
		//
		//                bfgs prev_solution = solution;
		//                solution = BFGS(prev_solution);
		//                dt_s = solution.x_k.segment(0, 3);
		//                u_s.block(0, 1, 1, 2) = solution.x_k.segment(3, 2).transpose();
		//                u_s.block(1, 1, 1, 2) = solution.x_k.segment(5, 2).transpose();
		////                cout<< "result dt is " << dt_s << endl;
		////                cout << "u_s is " << u_s << endl;
		//
		//                while(solution.delta_x_k.norm() > 1e-4){   ////(NOTICE how this part is very similar to the gradient descent below. Digby and Ke were trying to do the same thing but with bfgs)
		//                    prev_solution = solution;
		//                    solution = BFGS(prev_solution);      
		//                    dt_s = solution.x_k.segment(0, 3);				// get dts
		//                    u_s.block(0, 1, 1, 2) = solution.x_k.segment(3, 2).transpose();
		//                    u_s.block(1, 1, 1, 2) = solution.x_k.segment(5, 2).transpose();
		//                    cout<< "result dt is " << dt_s << endl;
		//                    cout << "u_s is " << u_s << endl;
		//                }
		//                cout << "--------------- Final Solution ---------------------" << endl;
		//                cout<< "result dt is " << dt_s << endl;
		//                cout << "result u_s is " << u_s << endl;
		//                cout << "----------------------------------------------------" << endl;
		//            }
		//

		//////////////////ABOVE here is bfgs which needs to be deleted---------------------------------------------------------------------




				//// GRADIENT descent of footstep position u and lagrange multiplier lambda, from here down----   The stuff above is bfgs, which didnt work------------ 
				//// Stuff down is commenetd out just because they were doing some tests. Currently, ART in Gazebo uses only RK4 (IPOP) --------------------------------------------------------------------------------------------------
		VectorXd grad_k_new = grad_k;
		MatrixXd x_new, xdot_new, xddot_new;
		x_new = x_s;
		xdot_new = xdot_s;
		xddot_new = xddot_s;


		//for(int j=0; j<3; j++){
		double mu_step_size = 0.2;////=0.02
		double phi_scaling = 1.0001;
		double countz = 0;
		double alphaz = 0.006;


		myFile << "\n";


		while (grad_k_new.norm() > 0.05 && countz < 35000) {   		/// WHILE the solution is not satisfying (i.e. larger than an arbitrary 0.5) the tollerace, keep iterating
//                    cout << "grad norm is " << grad_k_new.norm() << endl;


			double cost1 = J(dt_s, u_s, x_new, xdot_new, xddot_new);
			//          UPDATE stuff for u and dt:
			dt_s -= step_size * grad_k_new.segment(0, 3) * 1.0;				///dt is Length of the footstep. Here we are doing a simple gradient descent
			u_s.block(0, 1, 1, 2) -= step_size * grad_k_new.segment(3, 2).transpose();// /2;	///u is Position of footstep x
			u_s.block(1, 1, 1, 2) -= step_size * grad_k_new.segment(5, 2).transpose();	///u is Position of footstep y        ---> both dt and u above are updated
//                    cout<< "result dt is " << dt_s << endl;									      ---> NOTE: No 'z'
//                    cout << "u_s is " << u_s << endl;
			lamda_grad = Constraints(dt_s, u_s, x_new, xdot_new, xddot_new);  	//// Here we get the gradient of the cost function in respect of the Lagrange multipliers (i.e. dJ/dLamda)


			w.segment(0, 1) += mu_step_size * lamda_grad.segment(0, 1) * 6;
			w.segment(1, 1) += mu_step_size * lamda_grad.segment(1, 1) * 6;
			w.segment(2, 8) += mu_step_size * lamda_grad.segment(2, 8) * 6;  //// This is UPDATING the lagrangian multipliers USING MU----------------
			w.segment(10, 2) += mu_step_size * lamda_grad.segment(10, 2) * 6;  //// This is UPDATING the lagrangian multipliers USING MU----------------
			// w_collision += mu_step_size * lamda_grad.segment(12, 2) * 1;

//////////////////////////////////////For slopes
// 		while(grad_k_new.norm()> 0.5 && countz < 35000){   		/// WHILE the solution is not satisfying (i.e. larger than an arbitrary 0.5) the tollerace, keep iterating
// //                    cout << "grad norm is " << grad_k_new.norm() << endl;


// 		    double cost1 = J(dt_s, u_s, x_new, xdot_new, xddot_new);
// //          UPDATE stuff for u and dt:
//             dt_s -= step_size*grad_k_new.segment(0,3)*1;				///dt is Length of the footstep. Here we are doing a simple gradient descent
//             u_s.block(0, 1, 1, 2) -= step_size*grad_k_new.segment(3,2).transpose();// /2;	///u is Position of footstep x
//             u_s.block(1, 1, 1, 2) -= step_size*grad_k_new.segment(5,2).transpose();	///u is Position of footstep y        ---> both dt and u above are updated
// //                    cout<< "result dt is " << dt_s << endl;									      ---> NOTE: No 'z'
// //                    cout << "u_s is " << u_s << endl;
// 		    lamda_grad = Constraints(dt_s, u_s, x_new, xdot_new, xddot_new);  	//// Here we get the gradient of the cost function in respect of the Lagrange multipliers (i.e. dJ/dLamda)


// 		    w+= mu_step_size * lamda_grad.segment(0, 12);  //
//////////////////////////////////////



			// w_1 = w + alphaz * lamda_grad.segment(0, 12)*1;  //// This is UPDATING the lagrangian multipliers USING MU----------------
			// w_2 = (1-alphaz/mu_step_size)*w;
			// if (countz == 0 && w_received == false){
			// 	//copy = MatrixXd::Zero(n_weights, 3);
			// 	w_buffer << 0.0, 0.0, 0.0,
			// 				0.0, 0.0, 0.0,
			// 				0.0, 0.0, 0.0,
			// 				0.0, 0.0, 0.0,
			// 				0.0, 0.0, 0.0,
			// 				0.0, 0.0, 0.0,
			// 				0.0, 0.0, 0.0,
			// 				0.0, 0.0, 0.0,
			// 				0.0, 0.0, 0.0,
			// 				0.0, 0.0, 0.0,
			// 				0.0, 0.0, 0.0,
			// 				0.0, 0.0, 0.0;
			// }
			w_received = false;
			// cout<< 'buffer is:::::::::: ' << w_buffer << endl;


			for (int i = 0; i < n_weights; i++) {  		      //// Also Lagrange Multipliers need to be >=0, otherwise they are put =0
				
				if (i < 12){
					if (w[i] < 0) w[i] = 0;

					//    if(w_1[i]>=w_2[i]){
					   // 		w[i] = w_1[i];
					   // }
					   // else if(w_1[i]<w_2[i]){
					   // 		w[i] = w_2[i];
					   // }


					if (w[i] > 10000000) {
						w[i] = (w_buffer(0, i) + w_buffer(1, i) + w_buffer(2, i)) / 3;
					}

					w_buffer(0, i) = w_buffer(1, i);
					w_buffer(1, i) = w_buffer(2, i);
					w_buffer(2, i) = w[i];
				}

				if (i>=12 && i < 14){
					if (w_collision[i-12] < 0) w[i-12] = 0;

					// if (w_collision[i-12] > 10000000) {
					// 	w_collision[i-12] = (w_buffer_collision(0, i-12) + w_buffer_collision(1, i-12) + w_buffer_collision(2, i-12)) / 3;
					// }
					w_collision[i-12] = 0;

					w_buffer_collision(0, i-12) = w_buffer_collision(1, i-12);
					w_buffer_collision(1, i-12) = w_buffer_collision(2, i-12);
					w_buffer_collision(2, i-12) = w[i-12];


				}


				// cout<< 'buffer 2222 is:::::::::: ' << w_buffer << endl;
			}


			//		    w_com = VectorXd::Zero(n_weights);
			//		    w_com = mu_step_size * lamda_grad.segment(0, 12)*1;
			//                    for(int i=0; i<n_weights; i++){  		      //// Also Lagrange Multipliers need to be >=0, otherwise they are put =0
			//                        if(w_com[i]<0) w[i] = w_com[i];
			//                    }	    



			mu_step_size = phi_scaling * mu_step_size;
			//cout << "the MUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUUU is: " << mu_step_size;


					// project to positive side
			  //// From here we are setting up the dynamic model that the robot will do after it is optimized fom the above steps
			x_new = MatrixXd::Zero(2, 4);
			x_new(0, 0) = x_0(0);
			x_new(1, 0) = x_0(1);
			xdot_new = MatrixXd::Zero(2, 4);
			xdot_new(0, 0) = x_dot_0(0);
			xdot_new(1, 0) = x_dot_0(1);
			xddot_new = MatrixXd::Zero(2, 4);
			xddot_new(0, 0) = xddot(0.0, u_s(0, 0), x_new(0, 0), xdot_new(0, 0));
			xddot_new(1, 0) = xddot(0.0, u_s(1, 0), x_new(1, 0), xdot_new(1, 0));

			countz += 1;
			double cost2 = J(dt_s, u_s, x_new, xdot_new, xddot_new);		// calculate cost
			//cout << "COOOOOOOOOOOOST is: " << (cost1-cost2);
			//cout << " in number of ite:  " << countz << endl;
			myFile << countz << ",";

			for (int k = 0; k < 3; k++) {  ////3 FOOTSTEPS in the future ----- ARTO does the optimization over the next 3 footsteps------------------------------------------------------------------------------------------------------------
				x_new(0, k + 1) = x(dt_s[k], u_s(0, k), x_new(0, k), xdot_new(0, k));
				x_new(1, k + 1) = x(dt_s[k], u_s(1, k), x_new(1, k), xdot_new(1, k));
				xdot_new(0, k + 1) = xdot(dt_s[k], u_s(0, k), x_new(0, k), xdot_new(0, k));
				xdot_new(1, k + 1) = xdot(dt_s[k], u_s(1, k), x_new(1, k), xdot_new(1, k));
				xddot_new(0, k + 1) = xddot(dt_s[k], u_s(0, k), x_new(0, k), xdot_new(0, k));
				xddot_new(1, k + 1) = xddot(dt_s[k], u_s(1, k), x_new(1, k), xdot_new(1, k));
			}
			Vector3d grad_t_new = dJ_dt(dt_s, u_s, x_new, xdot_new, xddot_new);
			Vector2d grad_ux_new = dJ_dux(dt_s, u_s, x_new, xdot_new, xddot_new);
			Vector2d grad_uy_new = dJ_duy(dt_s, u_s, x_new, xdot_new, xddot_new);
			grad_k_new << grad_t_new, grad_ux_new, grad_uy_new;
			//                    cout << "grad is " << grad_k_new << endl;
		}//while
//              cout << "lambda grad is " << lamda_grad << endl;
		myFile << "\n";


		/////////AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
		if (rk4_received) {

			if (dt_s(0) >= 0) {
				dt_s_backup(0) = dt_s(0);
			}
			else if (dt_s_backup(0) - 1 / LOOP_RATE < 0) {
				dt_s(0) = 0.00001;
			}
			else {
				dt_s(0) = dt_s_backup(0) - 1 / LOOP_RATE;
			}

			for (int j = 0; j < 3; j++) {
				if (j > 0) {
					if (dt_s(j) < 0.4) {
						dt_s(j) = 0.4;
						cout << "PERFORMED: Projection on 0.4 sideeeeeeeeeeeeeeeeeeee" << endl;
					}
				}

				if (j == 0 && dt_s(j) + current_time > 0.6) { dt_s(j) = max(0.4 - current_time, 0.05); }

				if (j > 0 && dt_s(j) > 0.6) {
					if (j == 0) { dt_s(j) = 0.6 - current_time; }
					dt_s(j) = 0.6;
					cout << "PERFORMED: Projection on 0.6 sideeeeeeeeeeeeeeeeeeee" << endl;
				}
			}




			if (abs(u_s(0, 0) - u_s(0, 1)) > sqrt(lmax)) {         //lmax				x 0-1
				u_s(0, 1) = u_s(0, 0) + sqrt(lmax) * 0.8;
			}
			if (abs(u_s(0, 1) - u_s(0, 2)) > sqrt(lmax)) {         //lmax				x 1-2
				u_s(0, 2) = u_s(0, 1) + sqrt(lmax) * 0.8;
			}


			if (abs(u_s(1, 0) - u_s(1, 1)) > sqrt(lmax)) {         //lmax				y 0-1
				u_s(1, 1) = u_s(1, 0) + foot_flag * sqrt(lmax) * 0.3;
			}
			if (abs(u_s(1, 1) - u_s(1, 2)) > sqrt(lmax)) {         //lmax				y 1-2
				u_s(1, 2) = u_s(1, 1) + foot_flag * sqrt(lmax) * 0.3;
			}



			// if (abs(u_s(0,0)-u_s(0,1))<sqrt(lmax) && abs(u_s(0,0)-u_s(0,1))>rfoot){
			// 	u_s_backup(0,0) = u_s(0,0);
			// }
			// else{
			// 	u_s(0,1) = u_s_backup(0,0) + sqrt(lmax);
			// }

			// if (abs(u_s(0,1)-u_s(0,2))<sqrt(lmax) && abs(u_s(0,1)-u_s(0,2))>rfoot){
			// 	u_s_backup(0,1) = u_s(0,1);
			// }
			// else{ 
			// 	u_s(0,2) = u_s_backup(0,1) + sqrt(lmax);
			// }





			// if (abs(u_s(1,0)-u_s(1,1))<sqrt(lmax) && abs(u_s(1,0)-u_s(1,1))>rfoot){
			// 	u_s_backup(1,0) = u_s(1,0);
			// }
			// else{
			// 	u_s(1,1) = u_s_backup(1,0) + sqrt(lmax);
			// }

			// if (abs(u_s(1,1)-u_s(1,2))<sqrt(lmax) && abs(u_s(1,1)-u_s(1,2))>rfoot){
			// 	u_s_backup(1,1) = u_s(1,1);
			// }
			// else{ 
			// 	u_s(1,2) = u_s_backup(1,1); + sqrt(lmax);
			// }










			//  Constraints for the crossing in y
			if (abs(u_s(1, 0) - u_s(1, 1)) < rfoot && abs(u_s(0, 0) - u_s(0, 1)) < rfoot * 1) {
				u_s(1, 1) = u_s(1, 0) + foot_flag * rfoot;         									//rfoot			y 0-1
			}
			if (abs(u_s(1, 1) - u_s(1, 2)) < rfoot && abs(u_s(0, 1) - u_s(0, 2)) < rfoot * 1) {              //rfoot				y 1-2
				u_s(1, 2) = u_s(1, 1) + foot_flag * rfoot;
			}

		}






		cout << "The final weights is " << w << endl;

		/////////BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB

					//}//for						/////////NOTE: you eed to inser the solutions above in a for/while loop to converge them. Currently they are done just once.

		cout << "--------------- Final Solution ---------------------" << endl;
		cout << "result dt is " << dt_s << endl;
		cout << "result u_s is " << u_s << endl;
		cout << "----------------------------------------------------" << endl;
		x_s = MatrixXd::Zero(2, 4);
		x_s(0, 0) = x_0(0);
		x_s(1, 0) = x_0(1);
		xdot_s = MatrixXd::Zero(2, 4);
		xdot_s(0, 0) = x_dot_0(0);
		xdot_s(1, 0) = x_dot_0(1);
		xddot_s = MatrixXd::Zero(2, 4);
		xddot_s(0, 0) = xddot(0.0, u_s(0, 0), x_s(0, 0), xdot_s(0, 0));
		xddot_s(1, 0) = xddot(0.0, u_s(1, 0), x_s(1, 0), xdot_s(1, 0));

		for (int k = 0; k < 3; k++) {
			x_s(0, k + 1) = x(dt_s[k], u_s(0, k), x_s(0, k), xdot_s(0, k));
			x_s(1, k + 1) = x(dt_s[k], u_s(1, k), x_s(1, k), xdot_s(1, k));
			xdot_s(0, k + 1) = xdot(dt_s[k], u_s(0, k), x_s(0, k), xdot_s(0, k));
			xdot_s(1, k + 1) = xdot(dt_s[k], u_s(1, k), x_s(1, k), xdot_s(1, k));
			xddot_s(0, k + 1) = xddot(dt_s[k], u_s(0, k), x_s(0, k), xdot_s(0, k));
			xddot_s(1, k + 1) = xddot(dt_s[k], u_s(1, k), x_s(1, k), xdot_s(1, k));
		}






		//                if(dt_s(0)>2.5){
		//                    dt_s = t_s_rk4;
		//                    u_s = u_s_rk4;
		//                }
		//                cout << "Final State is " << endl;
		//                cout << "X is \n" << x_s << endl;
		//                cout << "X dot is \n" << xdot_s << endl;
		auto t2 = std::chrono::high_resolution_clock::now();
		auto duration = chrono::duration_cast<chrono::microseconds>(t2 - t1).count();
		//cout << "Duration: \n" << duration << " microseconds" << endl;
//            }
//            rk4_received = false;

		if (support_foot_flag == foot_flag && dt_s(0) > 10 / LOOP_RATE && !dt_s.hasNaN() && !u_s.hasNaN() && dt_s(0) < 0.6) {
			//            cout << "Publish Plan !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << endl;
			std_msgs::Float64MultiArray footstep_plan;
			footstep_plan.data.push_back(u_s(0, 1));
			footstep_plan.data.push_back(u_s(1, 1));
			footstep_plan.data.push_back(u_s(0, 2));
			footstep_plan.data.push_back(u_s(1, 2));
			footstep_plan.data.push_back(dt_s(0));
			footstep_plan.data.push_back(dt_s(1));
			footstep_plan.data.push_back(support_foot_flag);
			planner_output_pub.publish(footstep_plan);
			if (flag == 0) {
				dt_s(0) -= 1 / LOOP_RATE;
				dt_s(1) -= 1 / LOOP_RATE;
				dt_s(2) -= 1 / LOOP_RATE;
			}
			else if (flag == 1) { flag = 0; }
		}
		else if (dt_s.hasNaN() || u_s.hasNaN()) {
			cout << "Publishing RK4 -------------------" << endl;
			dt_s = t_s_rk4;
			u_s = u_s_rk4;
			std_msgs::Float64MultiArray footstep_plan;
			footstep_plan.data.push_back(u_s(0, 1));
			footstep_plan.data.push_back(u_s(1, 1));
			footstep_plan.data.push_back(u_s(0, 2));
			footstep_plan.data.push_back(u_s(1, 2));
			footstep_plan.data.push_back(dt_s(0));
			footstep_plan.data.push_back(dt_s(1));
			footstep_plan.data.push_back(support_foot_flag);
			planner_output_pub.publish(footstep_plan);
		}
		else {
			cout << "Not publishing -------------------" << endl;
			if (flag == 0) {
				dt_s(0) -= 1 / LOOP_RATE;
				dt_s(1) -= 1 / LOOP_RATE;
				dt_s(2) -= 1 / LOOP_RATE;
			}
		}
		// to be modified
		if (support_foot_flag != foot_flag) {
			// dt_s(0) = dt_s(1);
			// dt_s(1) = dt_s(2);
			// dt_s(2) = dt_s(2) + 0.4;//opt.time(3) - opt.time(2);
			// u_s(0,0) = u_s(0,1);
			// u_s(1,0) = u_s(1,1);
			// u_s(0,1) = u_s(0,2);
			// u_s(1,1) = u_s(1,2);
			// u_s(0,2) = u_s(0,2) + (u_s(0,2) - u_s(0,1));
			// u_s(1,2) = u_s(1,2) + (u_s(1,1) - u_s(1,2));
			// current_time = 0.0;
			// support_foot_flag = foot_flag;



			current_time = 0.0;
			support_foot_flag = foot_flag;
			dt_s(0) = 0.4;
			dt_s(1) = 0.4;
			dt_s(2) = 0.4;//opt.time(3) - opt.time(2);
			u_s(0, 0) = u_s(0, 0) + x_dot_ref(0) * 0.4;
			u_s(1, 0) = u_s(1, 0) - foot_flag * rfoot;
			u_s(0, 1) = u_s(0, 0) + x_dot_ref(0) * 0.4;
			u_s(1, 1) = u_s(1, 0) - foot_flag * rfoot * (-1);
			u_s(0, 2) = u_s(0, 1) + x_dot_ref(0) * 0.4;
			u_s(1, 2) = u_s(1, 1) - foot_flag * rfoot;

			std_msgs::Float64MultiArray footstep_plan;
			footstep_plan.data.push_back(u_s(0, 1));
			footstep_plan.data.push_back(u_s(1, 1));
			footstep_plan.data.push_back(u_s(0, 2));
			footstep_plan.data.push_back(u_s(1, 2));
			footstep_plan.data.push_back(dt_s(0));
			footstep_plan.data.push_back(dt_s(1));
			footstep_plan.data.push_back(support_foot_flag);
			planner_output_pub.publish(footstep_plan);

			cout << "switching !!!!!!!!!!!!!!!!!!!!!!!!!!1!!!!!!!!!!!!" << endl;
		}

		ros::spinOnce();
		loop_rate.sleep();
	}//while ok
	return 0;
}
#!/usr/bin/env python
"""
footstep_planner_2d_v2.py
Script depends on footstep_planner.py (SLIDER class)
Takes reference trajectory and current state, produces footstep positions and times for next N steps  wrt constraints.

Digby Chappell, Ke Wang, Peter Tisnikar
October 2021
"""

import time
import rospy
import numpy as np
from numpy import linalg as la
import sys
np.set_printoptions(threshold=sys.maxsize)
from casadi import *
from footstep_planner_v2 import SLIDER
import matplotlib.pyplot as plt
from std_msgs.msg import Float64MultiArray, MultiArrayDimension, Float64

g = 9.81    # gravity

# # Parameters subscribed from the slider_controller node #######################################
x = 0.0
x_dot = 0.0
x_dotdot = 0.0
y = 0.0
y_dot = 0.0
y_dotdot = 0.0
left_x = 0.00
left_y = 0.21
right_x = 0.0
right_y = -0.21
foot_flag = 1
previous_foot_flag = -1
current_time = 0.001
##################################################################################################

# # Parameters subscribed from the traversability listener node ##################################
x1 = 50
y1 = 0 
rx1 = 0.1 
ry1 = 0.1 
a1 = 0 
x2 = 50
y2 = 0 
rx2 = 0.1 
ry2 = 0.1 
a2 = 0 
x3 = 50 
y3 = 0 
rx3 = 0.1 
ry3 = 0.1 
a3 = 0 
x4 = 50 
y4 = 0 
rx4 = 0.1 
ry4 = 0.1 
a4 = 0 
x5 = 50 
y5 = 0 
rx5 = 0.1 
ry5 = 0.1 
a5 = 0

def planner_callback(msg):
    global x, x_dot, x_dotdot, y, y_dot, y_dotdot, left_x, left_y, right_x, right_y, foot_flag, current_time
    x, x_dot, x_dotdot, y, y_dot, y_dotdot = msg.data[:6]
    left_x, left_y = msg.data[6:8]
    right_x, right_y = msg.data[8:10]
    foot_flag = msg.data[10]
    current_time = msg.data[11]
    # print('x', x, x_dot, x_dotdot)
    # print('y', y, y_dot, y_dotdot)

def ellipse_callback(msg):
    global x1, y1, rx1, ry1, a1 # , x2, y2, rx2, ry2, a2, x3, y3, rx3, ry3, a3, x4, y4, rx4, ry4, a4, x5, y5, rx5, ry5, a5
    
    x1, y1, rx1, ry1, a1 = msg.data[:5]
    # x2, y2, rx2, ry2, a2 = msg.data[5:10]
    # x3, y3, rx3, ry3, a3 = msg.data[10:15]
    # x4, y4, rx4, ry4, a4 = msg.data[15:20]
    # x5, y5, rx5, ry5, a5 = msg.data[20:]

if __name__=="__main__":
    rospy.init_node('ros_SLIDER_planner_node', anonymous=True)
    pub = rospy.Publisher('/time_slider_gazebo/footstep_plan_rk4', Float64MultiArray, queue_size=1)
    pub_state = rospy.Publisher('/time_slider_gazebo/planner_input', Float64MultiArray, queue_size=1)
    planner_input = rospy.Subscriber('/time_slider_gazebo/planner_input', Float64MultiArray, planner_callback)
    pub_weight = rospy.Publisher('/time_slider_gazebo/cost_weight', Float64MultiArray, queue_size=1)
    ellipses = rospy.Subscriber('/time_slider_gazebo/footstep_plan/ellipses',Float64MultiArray,ellipse_callback)

    ###############################################################################################
    v_ref = np.array([0.0, 0.0])
    ###############################################################################################
    
    # for ROTO Gait, mass = 11.5, for slider with FOOT, mass = 15
    slider = SLIDER(mass=15, height=0.7, leg_length=1.2, foot_radius=0.2)
    slider.initialise(model=slider.full_dynamics,
                      cost=slider.step_velocity_cost_function,
                      footsteps=3, intervals=6)
    Q = np.array([[1, 0],
                  [0, 1]])
    slider.set_weightings(Q)
    w = Float64MultiArray()
    rate=20
    r = rospy.Rate(rate)
    delta_t = 1/rate
    # initial values for optimal footsteps
    dT_opt = np.array([0.4, 0.4, 0.4])
    U_opt = np.array([[left_x, right_x, left_x],            # U_opt = [CURRENT FOOT LOCATION, NEXT LOC, NEXT NEXT LOC]
                      [left_y, right_y, left_y]])
    # initial values for solver
    dT_i = dT_opt
    U_i = U_opt
    count = 0
    total_time = 0.0
    while not rospy.is_shutdown():
        start = time.time()
        if foot_flag == -1:                                # FOOT FLAG -1 MEANS LEFT IS SUPPORT
            Xs0 = [right_x, right_y]
            U0 = [left_x, left_y]
        elif foot_flag == 1:                               # FOOT FLAG 1 MEANS RIGHT IS SUPPORT
            Xs0 = [left_x, left_y]
            U0 = [right_x, right_y]

        X0 = np.array([x, y, x_dot, y_dot])
        dX0 = np.array([x_dot, y_dot, x_dotdot, y_dotdot])

        print('------------------------------------------------')
        print("COM position is ", X0[0], X0[1])
        print("Current foot position is ", U0[0], U0[1])
        print("Foot flag is ", foot_flag)
        print('------------------------------------------------')

        D0 = np.array([X0[0] + dX0[0]/slider.omega,
                       X0[1] + dX0[1]/slider.omega,
                       X0[2] + dX0[2]/slider.omega,
                       X0[3] + dX0[3]/slider.omega])

        dt = current_time

        dxN = v_ref
        a_ref = (dxN - X0[2:])/1.2
        dcm_v_ref = v_ref + a_ref/slider.omega
        slider.set_parameters(X0, D0, dt, U0, Xs0, foot_flag, v_ref) # DCM: dcm_v_ref, RK4/FULL: v_ref
        slider.set_ellipses(x1, y1, rx1, ry1, a1, x2, y2, rx2, ry2, a2, x3, y3, rx3, ry3, a3, x4, y4, rx4, ry4, a4, x5, y5, rx5, ry5, a5)

        # update initial values for next solver iteration
        if foot_flag != previous_foot_flag or (dT_opt[0]) < 5e-2:
            dT_i[:-1] = dT_opt[1:]
            dT_i[-1] = dT_opt[-1]

            U_i[:-1] = U_opt[1:]
            U_i[-1] = U_opt[-1] + (U_opt[-2] - U_opt[-1])
            previous_foot_flag = foot_flag
            pass
        else:
            dT_i = dT_opt - delta_t
            U_i = U_opt
        try:
            slider.set_initial_solution(dT_i, U_i)
            slider.solve()
            U_opt = slider.U_opt
            dT_opt = slider.dT_opt
            new_step = Float64MultiArray()
            new_step.layout.dim = [MultiArrayDimension('', 8, 1)]
            u_x_1 = U_opt[0, 1]
            u_y_1 = U_opt[1, 1]
            u_x_2 = U_opt[0, 2]
            u_y_2 = U_opt[1, 2]
            print('optimal U', U_opt)
            print('optimal T', dT_opt)
            
            g_val = slider.sol.value(slider.opti.lam_g)
            w.data = g_val

            if dT_opt[0] > 5/250:
                new_step.data = [u_x_1, u_y_1, u_x_2, u_y_2, dT_opt[0], dT_opt[1], dT_opt[2], foot_flag]
                pub.publish(new_step)
                pub_weight.publish(w)
                print("publish THE W!!!")
                print(w)
            else:
                dT_opt = np.array([dT_opt[1], dT_opt[2], dT_opt[2]+0.4])
                U_opt = np.array([[u_x_1, u_x_2, u_x_2 + (u_x_2 - u_x_1)],
                                  [u_y_1, u_y_2, u_y_1]])
                # initial values for solver
                dT_i = dT_opt
                U_i = U_opt
        except Exception as e:
            print('could not solve ')
            print(e)
            print("failed at state", x, x_dot, x_dotdot, y, y_dot, y_dotdot, left_x, left_y, right_x, right_y, foot_flag, current_time)
            # initial values for optimal footsteps
            dT_opt = np.array([dT_opt[1], dT_opt[2], dT_opt[2]+0.4])
            U_opt = np.array([[u_x_1, u_x_2, u_x_2+(u_x_2-u_x_1)],
                              [u_y_1, u_y_2, u_y_1]])
            # initial values for solver
            dT_i = dT_opt
            U_i = U_opt

        end = time.time()
        print('time spent ', end-start)
        count += 1
        total_time += end-start
        print('average time is ', total_time/count)
        r.sleep()

#include <iostream>
#include <chrono>
#include <cmath>
#include <Eigen/Dense>

#include "ros/ros.h"
#include <std_msgs/Float64.h>
#include <std_msgs/Float64MultiArray.h>

using namespace Eigen;
using namespace std;

const double m = 11.5;    // body mass
const double m_s = 0.3;   // swing leg mass
const double L = 1.2;     // leg length
const double h = 0.7;     // body height
const double g = 9.81;    // gravity
const double omega = sqrt(g / h);  // omega

int flag = 0;
int support_foot_flag = 1;
Vector2d x_dot_ref(0.05, 0.00);     // m/s

// Parameters subscribed from the slider_controller node
Vector2d x_0(0.0, 0.0);
Vector2d x_dot_0(0.0, 0.0);
Vector2d x_dot_dot_0(0.0, 0.0);
Vector3d dt_s(0.2, 0.52170278, 0.43679115);
Matrix<double,2,3> u_s((Matrix<double,2,3>() << 0.0, -1.93340069e-02, 1.22170610e-01, -0.21, 2.41075380e-01,  -3.86024011e-02).finished());

double left_x = 0.0;
double left_y = 0.21;
double right_x = 0.0;
double right_y = -0.21;
int foot_flag = 1;
// penalty weights
const int n_weights = 12;
VectorXd w;

const int n_dynamic_weights = 12;
//VectorXd dyn_w;
VectorXd w_update;
MatrixXd Jacobian;
MatrixXd HessianInv;

const double max_iter = 200;
int n_repeats = 1;
bool rk4_received = false;

// constraints
// line up with Python
const double lmax = 0.5*sqrt(pow(L, 2.0) - pow(h, 2.0));
const double rfoot = 0.25;
const double dt0_min = 0.2;
const double dt_min = 0.4;
const double dt_max = 1.2;

double current_time = 0.0;

double dxdot_dt(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s, int kx, int kt);
double dx_dt(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s, int kx, int kt);
double dxdot_du(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s, int kx, int ku);
double dx_du(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s, int kx, int ku);

struct bfgs {
	VectorXd x_k;		// parameters (dt, u_x, u_y, at step k)
	VectorXd delta_x_k;	// parameter update at step k
	VectorXd y_k;		// change in gradient at step k
	MatrixXd B_k;		// Hessian approximation at step k
	MatrixXd H_k;		// inverse Hessian approximation at step k
	bool converged;
};

bfgs solution = {
	VectorXd::Zero(7),
	VectorXd::Zero(7),
	VectorXd::Zero(7),
	Matrix<double, 7, 7>::Identity(),
	Matrix<double, 7, 7>::Identity(),
	false,
};

double x(double dt, double u, double x_prev, double x_dot_prev){
	// double x_ = (x_prev - u + x_dot_prev / omega) * exp(omega * dt) + (x_prev - u - x_dot_prev / omega) * exp(-omega * dt) + u;
	double x_ = u + (x_prev - u) * cosh(omega * dt) + (x_dot_prev / omega) * sinh(omega * dt);
    return x_;
}

double xdot(double dt, double u, double x_prev, double x_dot_prev){
	// double xdot_ = omega * (x_prev - u + x_dot_prev / omega) * exp(omega * dt) - omega * (x_prev - u - x_dot_prev / omega) * exp(-omega * dt);
	double xdot_ = omega * (x_prev - u) * sinh(omega * dt) + x_dot_prev * cosh(omega * dt);
    return xdot_;
}

double xddot(double dt, double u, double x_prev, double x_dot_prev){
	// double xddot_ = pow(omega, 2) * (x_prev - u + x_dot_prev / omega) * exp(omega * dt) + pow(omega, 2) * (x_prev - u - x_dot_prev / omega) * exp(-omega * dt);
	double xddot_ = omega * omega * (x_prev - u) * cosh(omega * dt) + omega * x_dot_prev * sinh(omega * dt);
    return xddot_;
}

double dxdot_dt(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s, int kx=1, int kt=0){
	double dxdot_dt_ = 0.0;
	if (kx == kt+1){
		dxdot_dt_ = xddot_s[kx]; // question, will be -1?
	}else if (kx > kt+1){
		dxdot_dt_ = dx_dt(dt_s, u_s, x_s, xdot_s, xddot_s, kx-1, kt) * omega * sinh(omega * dt_s[kx-1]) + dxdot_dt(dt_s, u_s, x_s, xdot_s, xddot_s, kx-1, kt) * cosh(omega * dt_s[kx-1]);
	}
	return dxdot_dt_;
}

double dx_dt(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s, int kx=1, int kt=0){
    double dx_dt_ = 0.0;
	if (kx == kt+1){
		dx_dt_ = xdot_s[kx];
	}else if (kx > kt+1){
		dx_dt_ = dx_dt(dt_s, u_s, x_s, xdot_s, xddot_s, kx-1, kt) * cosh(omega * dt_s[kx-1]) + dxdot_dt(dt_s, u_s, x_s, xdot_s, xddot_s, kx-1, kt) * sinh(omega *dt_s[kx-1]) / omega;
	}
	return dx_dt_;
}

double dx_du(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s, int kx=1, int ku=1){
    double dx_du_ = 0.0;
	if (kx == ku + 1){
		dx_du_ =1 - cosh(omega * dt_s[kx - 1]);
	}else if (kx > ku + 1){
		dx_du_ = dx_du(dt_s, u_s, x_s, xdot_s, xddot_s, kx-1, ku) * cosh(omega * dt_s[kx-1]) + dxdot_du(dt_s, u_s, x_s, xdot_s, xddot_s, kx-1, ku) * sinh(omega * dt_s[kx-1]) / omega;
	}
	return dx_du_;
}

double dxdot_du(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s, int kx=1, int ku=1){
    double dxdot_du_ = 0.0;
	if (kx == ku + 1){
		dxdot_du_ = -omega * sinh(omega * dt_s[kx - 1]);
	}else if (kx > ku + 1){
		dxdot_du_ = dx_du(dt_s, u_s, x_s, xdot_s, xddot_s, kx-1, ku) * omega * sinh(omega * dt_s[kx-1]) + dxdot_du(dt_s, u_s, x_s, xdot_s, xddot_s, kx-1, ku) * cosh(omega * dt_s[kx-1]);
	}
	return dxdot_du_;
}

double dJ_dux1(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s){
    double grad = 0;
    for(int j=2; j<4; j++){
        grad += 2 * dxdot_du(dt_s, u_s, x_s, xdot_s, xddot_s, j, 1) * (xdot_s[j] - x_dot_ref[0]);}
    return grad;
}

double dJ_dux2(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s){
    double grad = 0;
    for(int j=3; j<4; j++){
        grad += 2 * dxdot_du(dt_s, u_s, x_s, xdot_s, xddot_s, j, 2) * (xdot_s[j] - x_dot_ref[0]);}
    return grad;
}

double dJ_duy1(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s){
    double grad = 0;
    for(int j=2; j<4; j++){
        grad += 2 * dxdot_du(dt_s, u_s, x_s, xdot_s, xddot_s, j, 1) * (xdot_s[j] - x_dot_ref[1]);}
    return grad;
}

double dJ_duy2(Vector3d dt_s, Vector3d u_s, Vector4d x_s, Vector4d xdot_s, Vector4d xddot_s){
    double grad = 0;
    for(int j=3; j<4; j++){
        grad += 2 * dxdot_du(dt_s, u_s, x_s, xdot_s, xddot_s, j, 2) * (xdot_s[j] - x_dot_ref[1]);}
    return grad;
}

double dJ_dt0(Vector3d dt_s, Matrix<double,2, 3> u_s, Matrix<double,2, 4> x_s, Matrix<double,2, 4> xdot_s, Matrix<double, 2, 4> xddot_s){
    double grad = 0.0;
	Vector3d u_x = u_s.row(0);
	Vector3d u_y = u_s.row(1);
	Vector4d x_x = x_s.row(0);
	Vector4d x_y = x_s.row(1);
	Vector4d xdot_x = xdot_s.row(0);
	Vector4d xdot_y = xdot_s.row(1);
	Vector4d xddot_x = xddot_s.row(0);
	Vector4d xddot_y = xddot_s.row(1);

    for(int j=1; j<4; j++){
        grad += 2 * dxdot_dt(dt_s, u_x, x_x, xdot_x, xddot_x, j, 0) * (xdot_x[j] - x_dot_ref[0]);
        grad += 2 * dxdot_dt(dt_s, u_y, x_y, xdot_y, xddot_y, j, 0) * (xdot_y[j] - x_dot_ref[1]);
    }
    return grad;
}

double dJ_dt1(Vector3d dt_s, Matrix<double,2, 3> u_s, Matrix<double,2, 4> x_s, Matrix<double,2, 4> xdot_s, Matrix<double, 2, 4> xddot_s){
    double grad = 0.0;
	Vector3d u_x = u_s.row(0);
	Vector3d u_y = u_s.row(1);
	Vector4d x_x = x_s.row(0);
	Vector4d x_y = x_s.row(1);
	Vector4d xdot_x = xdot_s.row(0);
	Vector4d xdot_y = xdot_s.row(1);
	Vector4d xddot_x = xddot_s.row(0);
	Vector4d xddot_y = xddot_s.row(1);

    for(int j=1; j<4; j++){
        grad += 2 * dxdot_dt(dt_s, u_x, x_x, xdot_x, xddot_x, j, 1) * (xdot_x[j] - x_dot_ref[0]);
        grad += 2 * dxdot_dt(dt_s, u_y, x_y, xdot_y, xddot_y, j, 1) * (xdot_y[j] - x_dot_ref[1]);
    }
    return grad;
}

double dJ_dt2(Vector3d dt_s, Matrix<double,2, 3> u_s, Matrix<double,2, 4> x_s, Matrix<double,2, 4> xdot_s, Matrix<double, 2, 4> xddot_s){
    double grad = 0.0;
	Vector3d u_x = u_s.row(0);
	Vector3d u_y = u_s.row(1);
	Vector4d x_x = x_s.row(0);
	Vector4d x_y = x_s.row(1);
	Vector4d xdot_x = xdot_s.row(0);
	Vector4d xdot_y = xdot_s.row(1);
	Vector4d xddot_x = xddot_s.row(0);
	Vector4d xddot_y = xddot_s.row(1);

    for(int j=1; j<4; j++){
        grad += 2 * dxdot_dt(dt_s, u_x, x_x, xdot_x, xddot_x, j, 2) * (xdot_x[j] - x_dot_ref[0]);
        grad += 2 * dxdot_dt(dt_s, u_y, x_y, xdot_y, xddot_y, j, 2) * (xdot_y[j] - x_dot_ref[1]);
    }
    return grad;
}

Vector2d dJ_dux(Vector3d dt_s, Matrix<double,2, 3> u_s, Matrix<double,2, 4> x_s, Matrix<double,2, 4> xdot_s, Matrix<double, 2, 4> xddot_s){
	Vector3d u_x = u_s.row(0);
	Vector3d u_y = u_s.row(1);
	Vector4d x_x = x_s.row(0);
	Vector4d x_y = x_s.row(1);
	Vector4d xdot_x = xdot_s.row(0);
	Vector4d xdot_y = xdot_s.row(1);
	Vector4d xddot_x = xddot_s.row(0);
	Vector4d xddot_y = xddot_s.row(1);
	
	double px1u1 = w[6] * ((x_s.col(1) - u_s.col(1)).transpose() * (x_s.col(1) - u_s.col(1)) - lmax);
	double px2u2 = w[7] * ((x_s.col(2) - u_s.col(2)).transpose() * (x_s.col(2) - u_s.col(2)) - lmax);

	double dpx1u1_dux1 = -2 * (x_s(0, 1) - u_s(0, 1)) * w[6];

	double dpx2u2_dux1 = 2 * (dx_du(dt_s, u_x, x_x, xdot_x, xddot_x, 2, 1)) * (x_s(0, 2) - u_s(0, 2)) * w[7];

	double dpx2u2_dux2 = -2 * (x_s(0, 2) - u_s(0, 2)) * w[7];

	double px2u1 = w[8] * ((x_s.col(2) - u_s.col(1)).transpose() * (x_s.col(2) - u_s.col(1)) - lmax);
	double px3u2 = w[9] * ((x_s.col(3) - u_s.col(2)).transpose() * (x_s.col(3) - u_s.col(2)) - lmax);

	double dpx2u1_dux1 = 2 * (dx_du(dt_s, u_x, x_x, xdot_x, xddot_x, 2, 1) - 1) * (x_s(0, 2) - u_s(0, 1)) * w[8];

	double dpx3u2_dux2 = 2 * (dx_du(dt_s, u_x, x_x, xdot_x, xddot_x, 3, 2) - 1) * (x_s(0, 3) - u_s(0, 2)) * w[9];

	double dpx3u2_dux1 = 2 * (dx_du(dt_s, u_x, x_x, xdot_x, xddot_x, 3, 1)) * (x_s(0, 3) - u_s(0, 2)) * w[9];

	double dux1_inequality_penalties = dpx1u1_dux1 + dpx2u2_dux1 + dpx2u1_dux1 + dpx3u2_dux1;
	double dux2_inequality_penalties = dpx2u2_dux2 + dpx3u2_dux2;

//	cout << "dx inequality penalty " << endl;
//	cout << dpx1u1_dux1 <<  endl;
//	cout << dpx2u2_dux1 <<  endl;
//	cout << dpx2u1_dux1 <<  endl;
//	cout << dpx3u2_dux1 <<  endl;
//	cout << dpx2u2_dux2 <<  endl;
//	cout << dpx3u2_dux2 <<  endl;

//    cout << "dJ with dux gradient" << endl;
//    cout << dJ_dux1(dt_s, u_x, x_x, xdot_x, xddot_x) << endl;
//    cout << dJ_dux2(dt_s, u_x, x_x, xdot_x, xddot_x) << endl;

    Vector2d dJ_dux_;
    dJ_dux_ << dJ_dux1(dt_s, u_x, x_x, xdot_x, xddot_x) + dux1_inequality_penalties, //dJ_dux1(dt_s, u_x, x_x, xdot_x, xddot_x) -
               dJ_dux2(dt_s, u_x, x_x, xdot_x, xddot_x) + dux2_inequality_penalties; // dJ_dux2(dt_s, u_x, x_x, xdot_x, xddot_x) -
    return dJ_dux_;
}

Vector2d dJ_duy(Vector3d dt_s, Matrix<double,2, 3> u_s, Matrix<double,2, 4> x_s, Matrix<double,2, 4> x_dot_s, Matrix<double,2, 4> xd_dot_s){
	Vector3d u_x = u_s.row(0);
	Vector3d u_y = u_s.row(1);
	Vector4d x_x = x_s.row(0);
	Vector4d x_y = x_s.row(1);
	Vector4d xdot_x = x_dot_s.row(0);
	Vector4d xdot_y = x_dot_s.row(1);
	Vector4d xddot_x = xd_dot_s.row(0);
	Vector4d xddot_y = xd_dot_s.row(1);
	
	double px1u1 = w[6] * ((x_s.col(1) - u_s.col(1)).transpose() * (x_s.col(1) - u_s.col(1)) - lmax);
	double px2u2 = w[7] * ((x_s.col(2) - u_s.col(2)).transpose() * (x_s.col(2) - u_s.col(2)) - lmax);

    double dpx1u1_duy1 = -2 * (x_s(1, 1) - u_s(1, 1)) * w[6];
    double dpx2u2_duy1 = 2 * (dx_du(dt_s, u_y, x_y, xdot_y, xddot_y, 2, 1)) * (x_s(1, 2) - u_s(1, 2)) * w[7];
    double dpx2u2_duy2 = -2 * (x_s(1, 2) - u_s(1, 2)) * w[7];

	double px2u1 = w[8] * ((x_s.col(2) - u_s.col(1)).transpose() * (x_s.col(2) - u_s.col(1)) - lmax);
	double px3u2 = w[9] * ((x_s.col(3) - u_s.col(2)).transpose() * (x_s.col(3) - u_s.col(2)) - lmax);

//    double dpx2u1_duy1 = -2 * (x_s(1, 2) - u_s(1, 1)) * w[8];
//    double dpx3u2_duy2 = -2 * (x_s(1, 3) - u_s(1, 2)) * w[9];
    double dpx2u1_duy1 = 2 * (dx_du(dt_s, u_y, x_y, xdot_y, xddot_y, 2, 1) - 1) * (x_s(1, 2) - u_s(1, 1)) * w[8];

    double dpx3u2_duy2 = 2 * (dx_du(dt_s, u_y, x_y, xdot_y, xddot_y, 3, 2) - 1) * (x_s(1, 3) - u_s(1, 2)) * w[9];

    double dpx3u2_duy1 = 2 * (dx_du(dt_s, u_y, x_y, xdot_y, xddot_y, 3, 1)) * (x_s(1, 3) - u_s(1, 2)) * w[9];
//    cout<< "inEquality constraint in y" << endl;
//    cout << dpx1u1_duy1 << endl;
//    cout << dpx2u2_duy2 << endl;
//    cout << dpx2u1_duy1 << endl;
//    cout << dpx3u2_duy2 << endl;
	double pu1u0 = w[10] * (foot_flag * (u_s(1, 1) - u_s(1, 0)) - rfoot);
	double pu2u1 = w[11] * (-foot_flag * (u_s(1, 2) - u_s(1, 1)) - rfoot);

	double dpu1u0_duy1 = -foot_flag * w[10];

    double dpu2u1_duy1 = -foot_flag * w[11];

    double dpu2u1_duy2 = foot_flag * w[11];

    double duy1_inequality_penalties = dpx1u1_duy1 + dpx2u2_duy1 + dpx2u1_duy1 + dpx3u2_duy1 + dpu1u0_duy1 + dpu2u1_duy1;//
    double duy2_inequality_penalties = dpx2u2_duy2 + dpx3u2_duy2 + dpu2u1_duy2;	//

//    cout << "dy inequality penalty " << endl;
//    cout << dpx1u1_duy1 << endl;
//    cout << dpx2u2_duy1 << endl;
//    cout << dpx2u1_duy1 << endl;
//    cout << dpx3u2_duy1 << endl;
//    cout << dpu1u0_duy1 << endl;
//    cout << dpu2u1_duy1 << endl;
//    cout << dpx2u2_duy2 << endl;
//    cout << dpx3u2_duy2 << endl;
//    cout << dpu2u1_duy2 << endl;
//    cout << "dJ with duy gradient" << endl;
//    cout << dJ_duy1(dt_s, u_y, x_y, xdot_y, xddot_y) << endl;
//    cout << dJ_duy2(dt_s, u_y, x_y, xdot_y, xddot_y) << endl;

    Vector2d dJ_duy_;
    dJ_duy_ << dJ_duy1(dt_s, u_y, x_y, xdot_y, xddot_y) + duy1_inequality_penalties,
               dJ_duy2(dt_s, u_y, x_y, xdot_y, xddot_y) + duy2_inequality_penalties;
    return dJ_duy_;
}

Vector3d dJ_dt(Vector3d dt_s, Matrix<double,2, 3> u_s, Matrix<double,2, 4> x_s, Matrix<double,2, 4> xdot_s, Matrix<double, 2, 4> xddot_s){
    double dt0 = dt_s[0];
    double dt0_low = max(dt0_min - current_time, 0.001);
    double dt0_high = dt_max;
    double dt1 = dt_s[1];
    double dt1_low = dt_min;
    double dt1_high = dt_max;
    double dt2 = dt_s[2];
    double dt2_low = dt_min;
    double dt2_high = dt_max;

	Vector3d u_x = u_s.row(0);
	Vector3d u_y = u_s.row(1);
	Vector4d x_x = x_s.row(0);
	Vector4d x_y = x_s.row(1);
	Vector4d xdot_x = xdot_s.row(0);
	Vector4d xdot_y = xdot_s.row(1);
	Vector4d xddot_x = xddot_s.row(0);
	Vector4d xddot_y = xddot_s.row(1);

	// 6
	double dpt_dt0 = -w[0] + w[1];
    double dpt_dt1 = -w[2] + w[3]; // changed
    double dpt_dt2 = -w[4] + w[5];

	// make sure the next step isn't too far away
	double px1u1 = w[6] * ((x_s.col(1) - u_s.col(1)).transpose() * (x_s.col(1) - u_s.col(1)) - lmax);
	double px2u2 = w[7] * ((x_s.col(2) - u_s.col(2)).transpose() * (x_s.col(2) - u_s.col(2)) - lmax);

	VectorXd v1(2); v1 <<	dx_dt(dt_s, u_x, x_x, xdot_x, xddot_x, 1, 0) ,
							dx_dt(dt_s, u_y, x_y, xdot_y, xddot_y, 1, 0) ;
    double dpx1u1_dt0 = 2 * w[6] * v1.transpose() * (x_s.col(1) - u_s.col(1));

	VectorXd v2(2); v2 <<	dx_dt(dt_s, u_x, x_x, xdot_x, xddot_x, 2, 0),
							dx_dt(dt_s, u_y, x_y, xdot_y, xddot_y, 2, 0);
    double dpx2u2_dt0 = 2 * w[7] * v2.transpose() * (x_s.col(2) - u_s.col(2));

	VectorXd v3(2); v3 <<	dx_dt(dt_s, u_x, x_x, xdot_x, xddot_x, 2, 1),
							dx_dt(dt_s, u_y, x_y, xdot_y, xddot_y, 2, 1);
    double dpx2u2_dt1 = 2 * w[7] * v3.transpose() * (x_s.col(2) - u_s.col(2));

	// make sure the current step isn't too far
	double px2u1 = w[8] * ((x_s.col(2) - u_s.col(1)).transpose() * (x_s.col(2) - u_s.col(1)) - lmax);
	double px3u2 = w[9] * ((x_s.col(3) - u_s.col(2)).transpose() * (x_s.col(3) - u_s.col(2)) - lmax);

	VectorXd v4(2); v4 <<	dx_dt(dt_s, u_x, x_x, xdot_x, xddot_x, 2, 0),
							dx_dt(dt_s, u_y, x_y, xdot_y, xddot_y, 2, 0);
    double dpx2u1_dt0 = 2 * w[8] * v4.transpose() * (x_s.col(2) - u_s.col(1));

	VectorXd v5(2); v5 <<	dx_dt(dt_s, u_x, x_x, xdot_x, xddot_x, 3, 0),
							dx_dt(dt_s, u_y, x_y, xdot_y, xddot_y, 3, 0);
    double dpx3u2_dt0 = 2 * w[9] * v5.transpose() * (x_s.col(3) - u_s.col(2));

	VectorXd v6(2); v6 <<	dx_dt(dt_s, u_x, x_x, xdot_x, xddot_x, 2, 1),
							dx_dt(dt_s, u_y, x_y, xdot_y, xddot_y, 2, 1);
    double dpx2u1_dt1 = 2 * w[8] * v6.transpose() * (x_s.col(2) - u_s.col(1));

	VectorXd v7(2); v7 <<	dx_dt(dt_s, u_x, x_x, xdot_x, xddot_x, 3, 1),
							dx_dt(dt_s, u_y, x_y, xdot_y, xddot_y, 3, 1);
    double dpx3u2_dt1 = 2 * w[9] * v7.transpose() * (x_s.col(3) - u_s.col(2));

	VectorXd v8(2); v8 << dx_dt(dt_s, u_x, x_x, xdot_x, xddot_x, 3, 2), dx_dt(dt_s, u_y, x_y, xdot_y, xddot_y, 3, 2);
    double dpx3u2_dt2 = 2 * w[9] * v8.transpose() * (x_s.col(3) - u_s.col(2));

    double dt0_inequality_penalties = dpt_dt0 + dpx1u1_dt0 + dpx2u2_dt0 + dpx2u1_dt0 + dpx3u2_dt0;
    double dt1_inequality_penalties = dpt_dt1 + dpx2u2_dt1 + dpx2u1_dt1 + dpx3u2_dt1;
    double dt2_inequality_penalties = dpt_dt2 + dpx3u2_dt2;
//    cout<< "dt inequality constraint" << endl;

//    cout << "Jacobian dt element " << endl;
//    cout << dpt_dt0 << endl;
//    cout << dpt_dt1 << endl;
//    cout << dpt_dt2 << endl;
//    cout << dpx1u1_dt0 << endl;
//    cout << dpx2u1_dt0 << endl;
//    cout << dpx2u2_dt0 << endl;
//    cout << dpx3u2_dt0 << endl;
//    cout << dpx2u1_dt1 << endl;
//    cout << dpx2u2_dt1 << endl;
//    cout << dpx3u2_dt1 << endl;
//    cout << dpx3u2_dt2 << endl;
//    cout << dpdy3dy3_dt2 << endl;

    Vector3d dJ_dt_;

//    cout << "dJ with dt gradient" << endl;
//    cout << dJ_dt0(dt_s, u_s, x_s, xdot_s, xddot_s) << endl;
//    cout << dJ_dt1(dt_s, u_s, x_s, xdot_s, xddot_s) << endl;
//    cout << dJ_dt2(dt_s, u_s, x_s, xdot_s, xddot_s) << endl;

    dJ_dt_ << dJ_dt0(dt_s, u_s, x_s, xdot_s, xddot_s) + dt0_inequality_penalties, //
              dJ_dt1(dt_s, u_s, x_s, xdot_s, xddot_s) + dt1_inequality_penalties, //
              dJ_dt2(dt_s, u_s, x_s, xdot_s, xddot_s) + dt2_inequality_penalties; //

    return dJ_dt_;
}


double J(Vector3d dt_s, Matrix<double,2, 3> u_s, Matrix<double,2, 4> x_s, Matrix<double,2, 4> xdot_s, Matrix<double, 2, 4> xddot_s){
    double J = 0.0;
    for(int i=1; i<u_s.size()/2+1; i++){
        J += pow(xdot_s(0, i)-x_dot_ref[0],2) + pow(xdot_s(1, i)-x_dot_ref[1],2);
    }
    return J;
 }

// get lambda gradients
VectorXd Constraints(Vector3d dt_s, Matrix<double,2, 3> u_s, Matrix<double,2, 4> x_s, Matrix<double,2, 4> xdot_s, Matrix<double, 2, 4> xddot_s)
{
    double dt0_low  =  0.2 - (current_time + dt_s(0)); //dt_min
    double dt0_high =  (current_time + dt_s(0)) - dt_max;
    double dt1_low  =  dt_min - dt_s(1);
    double dt1_high =  dt_s(1) - dt_max;
    double dt2_low  =  dt_min - dt_s(2);
    double dt2_high =  dt_s(2) - dt_max;
    double l_grad1 = pow(u_s(0, 1) - x_s(0, 1), 2) + pow(u_s(1, 1) - x_s(1, 1), 2) - 0.5*(pow(L,2) - pow(h,2));
    double l_grad2 = pow(u_s(0, 2) - x_s(0, 2), 2) + pow(u_s(1, 2) - x_s(1, 2), 2) - 0.5*(pow(L,2) - pow(h,2));
    double l_grad3 = pow(u_s(0, 1) - x_s(0, 2), 2) + pow(u_s(1, 1) - x_s(1, 2), 2) - 0.5*(pow(L,2) - pow(h,2));
    double l_grad4 = pow(u_s(0, 2) - x_s(0, 3), 2) + pow(u_s(1, 2) - x_s(1, 3), 2) - 0.5*(pow(L,2) - pow(h,2));
    double foot_grad1 = rfoot - foot_flag * (u_s(1, 1) - u_s(1, 0));
    double foot_grad2 = rfoot + foot_flag * (u_s(1, 2) - u_s(1, 1));
    VectorXd inEquality_grad(12);
    inEquality_grad << dt0_low, dt0_high, dt1_low, dt1_high, dt2_low, dt2_high, l_grad1, l_grad2, l_grad3, l_grad4, foot_grad1, foot_grad2;
    cout << "inEquality_grad is " << inEquality_grad << endl;

    inEquality_grad = inEquality_grad/inEquality_grad.norm();

    return inEquality_grad;
}

bfgs BFGS(bfgs& prev_result, bool verbose = true) {
	// previous solution
	VectorXd x_k = prev_result.x_k;
	VectorXd delta_x_k = prev_result.delta_x_k;
	//	VectorXd y_k = prev_result.y_k;x_k
	MatrixXd B_k = prev_result.B_k;
	MatrixXd H_k = prev_result.H_k;
	MatrixXd I = MatrixXd::Identity(7, 7);
//	MatrixXd I = HessianInv;

	MatrixXd x_s, xdot_s, xddot_s;
	Vector3d dt_old = x_k.segment(0, 3);				// get dts

	Matrix<double, 2, 3>& u_old = u_s;						// get us
	u_old.block(0, 1, 1, 2) = x_k.segment(3, 2).transpose();
	u_old.block(1, 1, 1, 2) = x_k.segment(5, 2).transpose();

	x_s = MatrixXd::Zero(2, 4);
	x_s(0, 0) = x_0(0);
	x_s(1, 0) = x_0(1);

	xdot_s = MatrixXd::Zero(2, 4);
	xdot_s(0, 0) = x_dot_0(0);
	xdot_s(1, 0) = x_dot_0(1);

	xddot_s = MatrixXd::Zero(2, 4);
	xddot_s(0, 0) = xddot(0.0, u_old(0, 0), x_s(0, 0), xdot_s(0, 0));
	xddot_s(1, 0) = xddot(0.0, u_old(1, 0), x_s(1, 0), xdot_s(1, 0));
	for (int k = 0; k < 3; k++) {
		x_s(0, k + 1) = x(dt_old[k], u_old(0, k), x_s(0, k), xdot_s(0, k));
		x_s(1, k + 1) = x(dt_old[k], u_old(1, k), x_s(1, k), xdot_s(1, k));
		xdot_s(0, k + 1) = xdot(dt_old[k], u_old(0, k), x_s(0, k), xdot_s(0, k));
		xdot_s(1, k + 1) = xdot(dt_old[k], u_old(1, k), x_s(1, k), xdot_s(1, k));
		xddot_s(0, k + 1) = xddot(dt_old[k], u_old(0, k), x_s(0, k), xdot_s(0, k));
		xddot_s(1, k + 1) = xddot(dt_old[k], u_old(1, k), x_s(1, k), xdot_s(1, k));
	}

	// calculate gradients at step k
	Vector3d grad_t = dJ_dt(dt_old, u_old, x_s, xdot_s, xddot_s); // 3
	Vector2d grad_ux = dJ_dux(dt_old, u_old, x_s, xdot_s, xddot_s); // 2
	Vector2d grad_uy = dJ_duy(dt_old, u_old, x_s, xdot_s, xddot_s); // 2
	VectorXd grad_k(7);
	grad_k << grad_t, grad_ux, grad_uy;
//    cout<< "old gradients\n" << grad_k.transpose() << endl;
//    cout << "old solution\n" << x_k.transpose() << endl;
//    cout << "old COM positions\n" << x_s << endl;
//    cout << "old COM velocities\n" << xdot_s << endl;
	// line search to find update size
	double alpha_k = 1e5;
	double best_cost = 1e5;
	double iteration_num = 100;
	//    cout<< "line searcwith h " << endl;
	for (int i = 0; i < iteration_num; i++) {
		double alpha_int =   (iteration_num-i)/(iteration_num*100);						// intermediate value of alpha
		// get intermediate dt_int and u_int
		VectorXd delta_x_int = - alpha_int * H_k * grad_k;
		VectorXd x_k_int = x_k + delta_x_int;		// intermediate parameter valu
	//		cout<< "x_k_int is " << x_k_int << endl;
		MatrixXd x_int, xdot_int, xddot_int;
		Vector3d dt_int = x_k_int.segment(0, 3);				// get dts

		Matrix<double, 2, 3>& u_int = u_s;						// get us
		u_int.block(0, 1, 1, 2) = x_k_int.segment(3, 2).transpose();
		u_int.block(1, 1, 1, 2) = x_k_int.segment(5, 2).transpose();

		x_int = MatrixXd::Zero(2, 4);
		x_int(0, 0) = x_0(0);
		x_int(1, 0) = x_0(1);
		xdot_int = MatrixXd::Zero(2, 4);
		xdot_int(0, 0) = x_dot_0(0);
		xdot_int(1, 0) = x_dot_0(1);
		xddot_int = MatrixXd::Zero(2, 4);
		xddot_int(0, 0) = xddot(0.0, u_int(0, 0), x_int(0, 0), xdot_int(0, 0));
		xddot_int(1, 0) = xddot(0.0, u_int(1, 0), x_int(1, 0), xdot_int(1, 0));

		for (int k = 0; k < 3; k++) {
			x_int(0, k + 1) = x(dt_int[k], u_int(0, k), x_int(0, k), xdot_int(0, k));
			x_int(1, k + 1) = x(dt_int[k], u_int(1, k), x_int(1, k), xdot_int(1, k));
			xdot_int(0, k + 1) = xdot(dt_int[k], u_int(0, k), x_int(0, k), xdot_int(0, k));
			xdot_int(1, k + 1) = xdot(dt_int[k], u_int(1, k), x_int(1, k), xdot_int(1, k));
			xddot_int(0, k + 1) = xddot(dt_int[k], u_int(0, k), x_int(0, k), xdot_int(0, k));
			xddot_int(1, k + 1) = xddot(dt_int[k], u_int(1, k), x_int(1, k), xdot_int(1, k));
		}

		// calculate cost with intermediate values
		double cost = J(dt_int, u_int, x_int, xdot_int, xddot_int);		// calculate cost
	//		cout << "alpha" << alpha_int << endl;
	//		cout << "cost" << cost << endl;
		if (cost < best_cost) {											// if cost has improved, keep corresponding best alpha
			alpha_k = alpha_int;
			best_cost = cost;
		}
	}
    cout << "final alpha is " << alpha_k << endl;
    cout << "final cost is " << best_cost << endl;

    delta_x_k = - alpha_k * H_k * grad_k;
	VectorXd x_k_new(7);
	x_k_new << x_k + delta_x_k;
	//	cout << "delta_x_k" << delta_x_k << endl;
	//    cout << "new solution" << x_k_new << endl;

	// calculate gradients at step k
	bfgs result;
	result.x_k = x_k_new;
	result.delta_x_k = delta_x_k;
	result.B_k = B_k;

	if (delta_x_k.norm() > 1e-4) {
		MatrixXd x_new, xdot_new, xddot_new;
	Vector3d dt_new = x_k_new.segment(0, 3);				// get dts
	Matrix<double, 2, 3>& u_new = u_s;						// get us
	u_new.block(0, 1, 1, 2) = x_k_new.segment(3, 2).transpose();
	u_new.block(1, 1, 1, 2) = x_k_new.segment(5, 2).transpose();

	x_new = MatrixXd::Zero(2, 4);
	x_new(0, 0) = x_0(0);
	x_new(1, 0) = x_0(1);
	xdot_new = MatrixXd::Zero(2, 4);
	xdot_new(0, 0) = x_dot_0(0);
	xdot_new(1, 0) = x_dot_0(1);
	xddot_new = MatrixXd::Zero(2, 4);
	xddot_new(0, 0) = xddot(0.0, u_new(0, 0), x_new(0, 0), xdot_new(0, 0));
	xddot_new(1, 0) = xddot(0.0, u_new(1, 0), x_new(1, 0), xdot_new(1, 0));

	for (int k = 0; k < 3; k++) {
		x_new(0, k + 1) = x(dt_new[k], u_new(0, k), x_new(0, k), xdot_new(0, k));
		x_new(1, k + 1) = x(dt_new[k], u_new(1, k), x_new(1, k), xdot_new(1, k));
		xdot_new(0, k + 1) = xdot(dt_new[k], u_new(0, k), x_new(0, k), xdot_new(0, k));
		xdot_new(1, k + 1) = xdot(dt_new[k], u_new(1, k), x_new(1, k), xdot_new(1, k));
		xddot_new(0, k + 1) = xddot(dt_new[k], u_new(0, k), x_new(0, k), xdot_new(0, k));
		xddot_new(1, k + 1) = xddot(dt_new[k], u_new(1, k), x_new(1, k), xdot_new(1, k));
	}
		Vector3d grad_t_new = dJ_dt(dt_new, u_new, x_new, xdot_new, xddot_new);
		Vector2d grad_ux_new = dJ_dux(dt_new, u_new, x_new, xdot_new, xddot_new);
		Vector2d grad_uy_new = dJ_duy(dt_new, u_new, x_new, xdot_new, xddot_new);
		VectorXd grad_k_new(7);
		grad_k_new << grad_t_new, grad_ux_new, grad_uy_new;
		//	cout<< "new gradients\n" << grad_k_new.transpose() << endl;	// should get smaller (hopefully)
		//    cout << "new solution\n" << x_k_new.transpose() << endl;
		//    cout << "new COM positions\n" << x_new << endl;
		//    cout << "new COM velocities\n" << xdot_new << endl;
		cout << "---------------------------------------------------------------------" << endl;
		VectorXd y_k(7);
		y_k << grad_k_new - grad_k;
		//    cout << "y_k" << y_k << endl;
		//MatrixXd B_k_new(7, 7);
		//B_k_new<< B_k + y_k * y_k.transpose() / (y_k.transpose() * delta_x_k) - B_k * delta_x_k * (B_k * delta_x_k).transpose() / (delta_x_k.transpose() * B_k * delta_x_k);
		MatrixXd H_k_new(7, 7);
		H_k_new << (I - delta_x_k * y_k.transpose() / (y_k.transpose() * delta_x_k)) * H_k * (I - y_k * delta_x_k.transpose() / (y_k.transpose() * delta_x_k)) + delta_x_k * delta_x_k.transpose() / (y_k.transpose() * delta_x_k);
		result.y_k = y_k;
		result.H_k = H_k_new;
		result.converged = false;
	}
	else {
		result.y_k = prev_result.y_k;

		result.H_k = H_k;
		result.converged = true;
	}
	// cout << "y_k " << y_k << endl;
	// cout << "delta_x_k " << delta_x_k << endl;
	// cout << "y_k.transpose() * delta_x_k\n" << y_k.transpose() * delta_x_k << endl;
	// cout << "delta_x_k * y_k.transpose()\n" << delta_x_k * y_k.transpose() / (y_k.transpose() * delta_x_k) << endl;
	// cout << "y_k * delta_x_k.transpose()\n" << y_k * delta_x_k.transpose() / (y_k.transpose() * delta_x_k) << endl;
	// cout << "delta_x_k * delta_x_k.transpose()\n" << delta_x_k * delta_x_k.transpose() / (y_k.transpose() * delta_x_k) << endl;
	// cout << "H_k * (I - y_k * delta_x_k.transpose() / (y_k.transpose() * delta_x_k))\n" << H_k * (I - y_k * delta_x_k.transpose() / (y_k.transpose() * delta_x_k)) << endl;
	// cout << "(I - delta_x_k * y_k.transpose() / (y_k.transpose() * delta_x_k)) *H_k * (I - y_k * delta_x_k.transpose() / (y_k.transpose() * delta_x_k))\n" << (I - delta_x_k * y_k.transpose() / (y_k.transpose() * delta_x_k)) *H_k * (I - y_k * delta_x_k.transpose() / (y_k.transpose() * delta_x_k)) << endl;
	// cout << "H_k new\n" << H_k_new << endl;

	

	//	cout << "BFGS Values " << endl;
	//	cout << "x_k" << result.x_k << endl;
	//	cout << "delta_k" << result.delta_x_k << endl;
	//	cout << "y_k" << result.y_k << endl;
	//	cout << "B_k" << result.B_k << endl;
	//	cout << "H_k" << result.H_k << endl;
	return result;
}

// Callback function for receiving the planner outputs
void footstepPlanCallbackRK4(const std_msgs::Float64MultiArray::ConstPtr& msg)
{
    cout<< "received rk4 data " << endl;
    u_s(0,1) = msg->data[0];
    u_s(1,1) = msg->data[1];
    u_s(0,2) = msg->data[2];
    u_s(1,2) = msg->data[3];
    dt_s(0) = msg->data[4];
    dt_s(1) = msg->data[5];
    dt_s(2) = msg->data[6];
    support_foot_flag = msg->data[7];
//    flag = 1;
    cout << "--------------------------------" << endl;
    cout<< "received u \n" << u_s << endl;
    cout<< "received t \n" << dt_s << endl;

    solution.x_k = VectorXd::Zero(7);
	solution.x_k.segment(0, 3) = dt_s.transpose();
	solution.x_k.segment(3, 2) = u_s.block(0, 1, 1, 2).transpose();
	solution.x_k.segment(5, 2) = u_s.block(1, 1, 1, 2).transpose();
	solution.delta_x_k = VectorXd::Zero(7);
	solution.y_k = VectorXd::Zero(7);
	solution.B_k = Matrix<double, 7, 7>::Identity();
	solution.H_k = Matrix<double, 7, 7>::Identity();
	rk4_received = true;
}

void plannerInputCallback(const std_msgs::Float64MultiArray::ConstPtr& msg)
{
//    x, x_dot, y, y_dot = msg.data[:4]
    x_0(0) = msg->data[0];
    x_dot_0(0) = msg->data[1];
    x_dot_dot_0(0) = msg->data[2];
    x_0(1) = msg->data[3];
    x_dot_0(1) = msg->data[4];
    x_dot_dot_0(1) = msg->data[5];
    left_x  = msg->data[6];
    left_y  = msg->data[7];
    right_x = msg->data[8];
    right_y = msg->data[9];
    foot_flag = msg->data[10];
    current_time = msg->data[11];
    cout << "received states " << endl;
    cout << x_0(0) << endl;
    cout << x_dot_0(0) << endl;
    cout << x_dot_dot_0(0) << endl;
    cout << x_0(1) << endl;
    cout << x_dot_0(1) << endl;
    cout << x_dot_dot_0(1) << endl;
    cout << left_x << endl;
    cout << left_y << endl;
    cout << right_x << endl;
    cout << right_y << endl;
    cout << foot_flag << endl;
    cout << current_time << endl;
}

void weightCallback(const std_msgs::Float64MultiArray::ConstPtr& msg)
{
    w[0] = msg->data[0];
    w[1] = msg->data[1];
    w[2] = msg->data[2];
    w[3] = msg->data[3];
    w[4] = msg->data[4];
    w[5] = msg->data[5];
    w[6] = msg->data[6];
    w[7] = msg->data[7];
    w[8] = msg->data[8];
    w[9] = msg->data[9];
    w[10] = msg->data[10];
    w[11] = msg->data[11];
//    w[0] = 1.0;
//    w[1] = 1.0;
//    w[2] = 1.0;
//    w[3] = 1.0;
//    w[4] = 1.0;
//    w[5] = 1.0;
//    w[6] = 1.0;
//    w[7] = 1.0;
//    w[8] = 1.0;
//    w[9] = 1.0;
//    w[10] = 1.0;
//    w[11] = 1.0;
//    cout << "The received weights are " << w << endl;
}

void jacobianCallback(const std_msgs::Float64MultiArray::ConstPtr& msg)
{
	std::vector<double> data = msg->data;
	Eigen::Map<Eigen::MatrixXd> mat(data.data(), 30, 16);
	Jacobian = mat;
//	cout<<"The received jacobian is " << Jacobian << endl;
}

void hessianCallback(const std_msgs::Float64MultiArray::ConstPtr& msg)
{
	std::vector<double> data = msg->data;
	Eigen::Map<Eigen::MatrixXd> mat(data.data(), 19, 19);
//	solution.H_k = mat;
//	HessianInv = mat;
//	cout<<"The received jacobian is " << Jacobian << endl;
}


int main(int argc, char ** argv){
    Vector4d x_opt,y_opt;
    const double LOOP_RATE = 25;
    int iter_main = 0;
    ros::init(argc, argv, "gradient_descent_footPlanner_node");

    ros::NodeHandle n;
    ros::Rate loop_rate(LOOP_RATE);
    ros::Publisher planner_output_pub = n.advertise<std_msgs::Float64MultiArray>("/time_slider_gazebo/footstep_plan", 1);
    ros::Publisher planner_gradient_pub = n.advertise<std_msgs::Float64MultiArray>("/time_slider_gazebo/gd_gradients", 1);
    ros::Subscriber sub_planner_input = n.subscribe<std_msgs::Float64MultiArray>("/time_slider_gazebo/planner_input", 'None', plannerInputCallback);
    ros::Subscriber sub_RK4_planner_input = n.subscribe<std_msgs::Float64MultiArray>("/time_slider_gazebo/footstep_plan_rk4", 'None', footstepPlanCallbackRK4);
    ros::Subscriber sub_weight        = n.subscribe<std_msgs::Float64MultiArray>("/time_slider_gazebo/cost_weight", 'None', weightCallback);
    ros::Subscriber sub_jacobian      = n.subscribe<std_msgs::Float64MultiArray>("/time_slider_gazebo/jacobian", 'None', jacobianCallback);
    ros::Subscriber sub_hessian       = n.subscribe<std_msgs::Float64MultiArray>("/time_slider_gazebo/hessianInv", 'None', hessianCallback);
    support_foot_flag                 = foot_flag;

	Jacobian = MatrixXd::Zero(30, 16);
	HessianInv = MatrixXd::Zero(19, 19);
	solution.x_k = VectorXd::Zero(7);
    w  = VectorXd::Zero(n_weights);
//    dyn_w  = VectorXd::Zero(n_weights);
    cout << "started" << endl;
    MatrixXd x_s, xdot_s, xddot_s;
	x_s = MatrixXd::Zero(2, 4);
	xdot_s = MatrixXd::Zero(2, 4);
	xddot_s = MatrixXd::Zero(2, 4);
	x_s(0, 0) = x_0(0);
	x_s(1, 0) = x_0(1);
	xdot_s(0, 0) = x_dot_0(0);
	xdot_s(1, 0) = x_dot_0(1);
	xddot_s(0, 0) = xddot(0.0, u_s(0, 0), x_s(0, 0), xdot_s(0, 0));
	xddot_s(1, 0) = xddot(0.0, u_s(1, 0), x_s(1, 0), xdot_s(1, 0));
	for (int k = 0; k < 3; k++) {
		x_s(0, k + 1) = x(dt_s[k], u_s(0, k), x_s(0, k), xdot_s(0, k));
		x_s(1, k + 1) = x(dt_s[k], u_s(1, k), x_s(1, k), xdot_s(1, k));
		xdot_s(0, k + 1) = xdot(dt_s[k], u_s(0, k), x_s(0, k), xdot_s(0, k));
		xdot_s(1, k + 1) = xdot(dt_s[k], u_s(1, k), x_s(1, k), xdot_s(1, k));
		xddot_s(0, k + 1) = xddot(dt_s[k], u_s(0, k), x_s(0, k), xdot_s(0, k));
		xddot_s(1, k + 1) = xddot(dt_s[k], u_s(1, k), x_s(1, k), xdot_s(1, k));
	}
	cout << "u_s" << u_s << endl;
	cout << "dT_s" << dt_s << endl;
	cout << "x_s" << x_s << endl;
	cout << "xdot_s" << xdot_s << endl;
	cout << "xddot_s" << xddot_s << endl;

	// calculate gradients at step k
	Vector3d grad_t = dJ_dt(dt_s, u_s, x_s, xdot_s, xddot_s);
	cout<< "calculate dJ_dt, first iteration" << endl;
	Vector2d grad_ux = dJ_dux(dt_s, u_s, x_s, xdot_s, xddot_s);
	cout<< "calculate dJ_dux, first iteration" << endl;
	Vector2d grad_uy = dJ_duy(dt_s, u_s, x_s, xdot_s, xddot_s);
	cout<< "calculate dJ_duy, first iteration" << endl;
	VectorXd grad_k(7);
	grad_k << grad_t, grad_ux, grad_uy;
	VectorXd lamda_grad;
	double lamda_step_size = 0.01;
	cout << "The weight is " << w << endl;
	cout << "The gradients are " << grad_k << endl;
	MatrixXd J_dxdt(4,3);
	MatrixXd J_dydt(4,3);
//	J_dxdt = MatrixXd::Zero(4,3);

    while (ros::ok()){

        if(rk4_received){
//            for(int i =0; i<2; i++){
            auto t1 = std::chrono::high_resolution_clock::now();
            // for (int i=0; i <max_iter; i+=2){
            cout << "u ini is \t" << u_s << endl;
            cout << "t ini is \t" << dt_s << endl;
            cout << "current robot COM pos is \t" << x_0 << endl;
            cout << "current robot COM vel is \t" << x_dot_0 << endl;

            ////////////// Gradient Descent ////////////////////////////////
            if (foot_flag == -1){
                u_s(0,0) = left_x;
                u_s(1,0) = left_y;
            }
            else if (foot_flag == 1){
                u_s(0,0) = right_x;
                u_s(1,0) = right_y;
            }
	     	MatrixXd x_s, xdot_s, xddot_s;
			x_s = MatrixXd::Zero(2, 4);
			xdot_s = MatrixXd::Zero(2, 4);
			xddot_s = MatrixXd::Zero(2, 4);
			x_s(0, 0) = x_0(0);
			x_s(1, 0) = x_0(1);
			xdot_s(0, 0) = x_dot_0(0);
			xdot_s(1, 0) = x_dot_0(1);
			xddot_s(0, 0) = xddot(0.0, u_s(0, 0), x_s(0, 0), xdot_s(0, 0));
			xddot_s(1, 0) = xddot(0.0, u_s(1, 0), x_s(1, 0), xdot_s(1, 0));
			for (int k = 0; k < 3; k++) {
				x_s(0, k + 1) = x(dt_s[k], u_s(0, k), x_s(0, k), xdot_s(0, k));
				x_s(1, k + 1) = x(dt_s[k], u_s(1, k), x_s(1, k), xdot_s(1, k));
				xdot_s(0, k + 1) = xdot(dt_s[k], u_s(0, k), x_s(0, k), xdot_s(0, k));
				xdot_s(1, k + 1) = xdot(dt_s[k], u_s(1, k), x_s(1, k), xdot_s(1, k));
				xddot_s(0, k + 1) = xddot(dt_s[k], u_s(0, k), x_s(0, k), xdot_s(0, k));
				xddot_s(1, k + 1) = xddot(dt_s[k], u_s(1, k), x_s(1, k), xdot_s(1, k));
			}
			cout << "x_s" << x_s << endl;
			cout << "xdot_s" << xdot_s << endl;
			cout << "xddot_s" << xddot_s << endl;
			cout << "u_s " << u_s << endl;

            // calculate gradients at step k
            Vector3d grad_t = dJ_dt(dt_s, u_s, x_s, xdot_s, xddot_s);
            Vector2d grad_ux = dJ_dux(dt_s, u_s, x_s, xdot_s, xddot_s);
            Vector2d grad_uy = dJ_duy(dt_s, u_s, x_s, xdot_s, xddot_s);
            VectorXd grad_k(7);
            grad_k << grad_t, grad_ux, grad_uy;
//            cout << "The gradients are " << grad_k << endl;

//			cout << "sol " << solution.x_k << endl;

            for(int i=0; i<1; i++){
                bfgs prev_solution = solution;
                solution = BFGS(prev_solution);
                dt_s = solution.x_k.segment(0, 3);
                u_s.block(0, 1, 1, 2) = solution.x_k.segment(3, 2).transpose();
                u_s.block(1, 1, 1, 2) = solution.x_k.segment(5, 2).transpose();
                cout<< "result dt is " << dt_s << endl;
                cout << "u_s is " << u_s << endl;

                while(solution.converged == false){
                    prev_solution = solution;
                    solution = BFGS(prev_solution);
                    dt_s = solution.x_k.segment(0, 3);				// get dts
                    u_s.block(0, 1, 1, 2) = solution.x_k.segment(3, 2).transpose();
                    u_s.block(1, 1, 1, 2) = solution.x_k.segment(5, 2).transpose();
                    cout<< "result dt is " << dt_s << endl;
                    cout << "u_s is " << u_s << endl;
                }
				solution.converged = false;
                cout << "--------------- Final Solution ---------------------" << endl;
                cout<< "result dt is " << dt_s << endl;
                cout << "result u_s is " << u_s << endl;
                cout << "----------------------------------------------------" << endl;
                x_s = MatrixXd::Zero(2, 4);
                x_s(0, 0) = x_0(0);
                x_s(1, 0) = x_0(1);
                xdot_s = MatrixXd::Zero(2, 4);
                xdot_s(0, 0) = x_dot_0(0);
                xdot_s(1, 0) = x_dot_0(1);
                xddot_s = MatrixXd::Zero(2, 4);
	            xddot_s(0, 0) = xddot(0.0, u_s(0, 0), x_s(0, 0), xdot_s(0, 0));
	            xddot_s(1, 0) = xddot(0.0, u_s(1, 0), x_s(1, 0), xdot_s(1, 0));

                for (int k = 0; k < 3; k++) {
                    x_s(0, k + 1) = x(dt_s[k], u_s(0, k), x_s(0, k), xdot_s(0, k));
                    x_s(1, k + 1) = x(dt_s[k], u_s(1, k), x_s(1, k), xdot_s(1, k));
                    xdot_s(0, k + 1) = xdot(dt_s[k], u_s(0, k), x_s(0, k), xdot_s(0, k));
                    xdot_s(1, k + 1) = xdot(dt_s[k], u_s(1, k), x_s(1, k), xdot_s(1, k));
                    xddot_s(0, k + 1) = xddot(dt_s[k], u_s(0, k), x_s(0, k), xdot_s(0, k));
                    xddot_s(1, k + 1) = xddot(dt_s[k], u_s(1, k), x_s(1, k), xdot_s(1, k));
                }
                lamda_grad = Constraints(dt_s, u_s, x_s, xdot_s, xddot_s);
                w     += lamda_step_size/(i+1) * lamda_grad.segment(0, 12);
                cout << "result states is " << endl;
                cout << "x_s " << x_s << endl;
                cout << "xdot_s" << xdot_s << endl;
//                cout <<
            }

//            double step_size = 0.0005;
//            for(int i=0; i<10; i++){
////                dt_s = solution.x_k.segment(0, 3);
////                u_s.block(0, 1, 1, 2) = solution.x_k.segment(3, 2).transpose();
////                u_s.block(1, 1, 1, 2) = solution.x_k.segment(5, 2).transpose();
//                cout<< "result dt is " << dt_s << endl;
//                cout << "u_s is " << u_s << endl;
//
//                VectorXd delta_grad = grad_k;
//                VectorXd grad_prev = grad_k;
//
//                while(delta_grad.norm()> 1e-4){
//                    MatrixXd x_new, xdot_new, xddot_new;
//                    x_new = MatrixXd::Zero(2, 4);
//                    x_new(0, 0) = x_0(0);
//                    x_new(1, 0) = x_0(1);
//                    xdot_new = MatrixXd::Zero(2, 4);
//                    xdot_new(0, 0) = x_dot_0(0);
//                    xdot_new(1, 0) = x_dot_0(1);
//                    xddot_new = MatrixXd::Zero(2, 4);
//                    xddot_new(0, 0) = xddot(0.0, u_s(0, 0), x_new(0, 0), xdot_new(0, 0));
//                    xddot_new(1, 0) = xddot(0.0, u_s(1, 0), x_new(1, 0), xdot_new(1, 0));
//
//                    for (int k = 0; k < 3; k++) {
//                        x_new(0, k + 1) = x(dt_s[k], u_s(0, k), x_new(0, k), xdot_new(0, k));
//                        x_new(1, k + 1) = x(dt_s[k], u_s(1, k), x_new(1, k), xdot_new(1, k));
//                        xdot_new(0, k + 1) = xdot(dt_s[k], u_s(0, k), x_new(0, k), xdot_new(0, k));
//                        xdot_new(1, k + 1) = xdot(dt_s[k], u_s(1, k), x_new(1, k), xdot_new(1, k));
//                        xddot_new(0, k + 1) = xddot(dt_s[k], u_s(0, k), x_new(0, k), xdot_new(0, k));
//                        xddot_new(1, k + 1) = xddot(dt_s[k], u_s(1, k), x_new(1, k), xdot_new(1, k));
//                    }
//                    Vector3d grad_t_new = dJ_dt(dt_s, u_s, x_new, xdot_new, xddot_new);
//                    Vector2d grad_ux_new = dJ_dux(dt_s, u_s, x_new, xdot_new, xddot_new);
//                    Vector2d grad_uy_new = dJ_duy(dt_s, u_s, x_new, xdot_new, xddot_new);
//                    VectorXd grad_k_new(7);
//                    grad_k_new << grad_t_new, grad_ux_new, grad_uy_new;
//                    delta_grad = grad_k_new - grad_prev;
//                    grad_prev = grad_k_new;
//
//                    dt_s -= step_size*grad_k_new.segment(0,3);
//                    u_s.block(0, 1, 1, 2) -= step_size*grad_k_new.segment(3,2).transpose();
//                    u_s.block(1, 1, 1, 2) -= step_size*grad_k_new.segment(5,2).transpose();
//
//                    cout<< "result dt is " << dt_s << endl;
//                    cout << "u_s is " << u_s << endl;
//                    cout << "grad delta norm is " << delta_grad.norm() << endl;
//
//
//                }
//                cout << "--------------- Final Solution ---------------------" << endl;
//                cout<< "result dt is " << dt_s << endl;
//                cout << "result u_s is " << u_s << endl;
//                cout << "----------------------------------------------------" << endl;
//                x_s = MatrixXd::Zero(2, 4);
//                x_s(0, 0) = x_0(0);
//                x_s(1, 0) = x_0(1);
//                xdot_s = MatrixXd::Zero(2, 4);
//                xdot_s(0, 0) = x_dot_0(0);
//                xdot_s(1, 0) = x_dot_0(1);
//                xddot_s = MatrixXd::Zero(2, 4);
//	            xddot_s(0, 0) = xddot(0.0, u_s(0, 0), x_s(0, 0), xdot_s(0, 0));
//	            xddot_s(1, 0) = xddot(0.0, u_s(1, 0), x_s(1, 0), xdot_s(1, 0));
//
//                for (int k = 0; k < 3; k++) {
//                    x_s(0, k + 1) = x(dt_s[k], u_s(0, k), x_s(0, k), xdot_s(0, k));
//                    x_s(1, k + 1) = x(dt_s[k], u_s(1, k), x_s(1, k), xdot_s(1, k));
//                    xdot_s(0, k + 1) = xdot(dt_s[k], u_s(0, k), x_s(0, k), xdot_s(0, k));
//                    xdot_s(1, k + 1) = xdot(dt_s[k], u_s(1, k), x_s(1, k), xdot_s(1, k));
//                    xddot_s(0, k + 1) = xddot(dt_s[k], u_s(0, k), x_s(0, k), xdot_s(0, k));
//                    xddot_s(1, k + 1) = xddot(dt_s[k], u_s(1, k), x_s(1, k), xdot_s(1, k));
//                }
//                lamda_grad = Constraints(dt_s, u_s, x_s, xdot_s, xddot_s);
//                w+= 1.0/(i+10) * lamda_grad.segment(0, 12);
//            }
            rk4_received = false;
            auto t2 = std::chrono::high_resolution_clock::now();
            auto duration = chrono::duration_cast<chrono::microseconds>( t2 - t1 ).count();
            cout << "Duration: \n" << duration << " microseconds" << endl;
    	}

        ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;}

#!/usr/bin/env python
"""
foot_step_planner.py
takes reference trajectory and current state, produces footstep positions
and times for next N steps.

Digby Chappell and Ke Wang
April 2020
"""

import time
import rospy
import numpy as np
from std_msgs.msg import Float64MultiArray, MultiArrayDimension, Float64


m = 15.0    # body mass
m_s = 0.3   # swing leg mass
L = 1.2     # leg length
h = 0.7     # body height
g = 9.81    # gravity
const = [m, m_s, L, h]
start = time.time()

########### Parameters subscribed from the slider_controller node #############################
u_1 = 0.0
t_1 = 4.0
support_foot = 0.0
###############################################################################################


def main():
    rospy.init_node('state_estimation', anonymous=True)
    footstep_plan_sub = rospy.Subscriber('/slider_gazebo/footstep_plan', Float64MultiArray, footstep_plan_callback)
    planner_pub = rospy.Publisher('/slider_gazebo/planner_input', Float64MultiArray, queue_size=1)

    rate = rospy.Rate(100)
    t_old = time.time()

    T_end = 100
    dt = 0.001
    window = 2

    # message set up
    planner_msg = Float64MultiArray()
    planner_msg.layout.dim = [MultiArrayDimension('', 9, 1)]

    # logging
    x_log = []
    x_support_log = []
    x_swing_log = []
    t_s = []
    x_opt_support_log = []
    t_opt_support_log = []

    # initial state
    x = 0.0
    x_dot = 0.0
    y = 0
    y_dot = 0
    left_x = 0.0
    left_y = 0.0
    right_x = 0.0
    right_y = 0.0
    foot_flag = 0

    flag = True

    now = time.time() - start
    tic = time.time()

    while now < T_end and not rospy.is_shutdown():

        now = time.time() - start
        t_step = t_1 + now
        delta_t = time.time() - tic
        if support_foot == 0:       # left foot support
            x, x_dot = get_state_update(x, x_dot, left_x, delta_t)    # get body state update
            right_x = get_swing_foot_update(right_x, u_1, t_1, delta_t)               # get swing foot update
            print('current foot \t \t', 'left')
        elif support_foot == 1:     # right foot support
            x, x_dot = get_state_update(x, x_dot, right_x, delta_t)    # get body state update
            left_x = get_swing_foot_update(left_x, u_1, t_1, delta_t)               # get swing foot update
            print('current foot \t \t', 'right')



        print('##################################################################')
        print('current state \t \t', (x, x_dot, left_x, right_x))
        print('current time \t \t', now)
        print('next step time \t \t', t_step)
        print('next step location \t', u_1)
        # publish state
        planner_msg.data = [x, x_dot, y, y_dot, left_x, left_y, right_x, right_y, foot_flag]
        planner_pub.publish(planner_msg)
        tic = time.time()
        rate.sleep()


def footstep_plan_callback(msg):
    global u_1, t_1, support_foot
    u_1 = msg.data[0]
    t_1 = msg.data[1]
    support_foot = msg.data[2]


def footstep_completed_callback(msg):
    global completion_signal
    completion_signal = msg.data


def get_state_update(x_0, x_dot_0, u_0, delta_t):
    # ---- dynamic constraints --------
    f = lambda x, u: np.array([x[1], x[0] * g / h - u * g / h])  # dx/dt = f(x,u)
    x_current = [x_0, x_dot_0]
    # Runge-Kutta 4 integration
    k1 = f(x_current, u_0)
    k2 = f(x_current + delta_t / 2 * k1, u_0)
    k3 = f(x_current + delta_t / 2 * k2, u_0)
    k4 = f(x_current + delta_t * k3, u_0)
    x_next = x_current + delta_t / 6 * (k1 + 2 * k2 + 2 * k3 + k4)
    x_current = x_next
    return x_current[0], x_current[1]


def get_swing_foot_update(xs_0, u_1, t_1, delta_t):
    # linear foot trajectory
    xs_t = xs_0 + delta_t * (u_1 - xs_0) / t_1
    return xs_t


if __name__=="__main__":
	main()
0
#!/usr/bin/env python
"""
foot_step_planner.py
takes reference trajectory and current state, produces footstep positions
and times for next N steps.

Digby Chappell and Ke Wang
April 2020
"""

import time
import rospy
import numpy as np
from casadi import *
import matplotlib.pyplot as plt
from std_msgs.msg import Float64MultiArray, MultiArrayDimension, Float64


m = 15.0    # body mass
m_s = 0.3   # swing leg mass
L = 1.2     # leg length
h = 0.6     # body height
g = 9.81    # gravity

const = [m, m_s, L, h]

T_end = 100
dt = 0.001
vmax = 10.0              # m/s
x_dot_ref = [0.5, 0.02]     # m/s


# Parameters subscribed from the slider_controller node #######################################
x = 0
x_dot = 0
y = 0
y_dot = 0
left_x = 0
left_y = 0
right_x = 0
right_y = 0
foot_flag = 0
###############################################################################################


def main():
    rospy.init_node('footstep_planner', anonymous=True)
    pub = rospy.Publisher('/slider_gazebo/footstep_plan', Float64MultiArray, queue_size=1)
    planner_input = rospy.Subscriber('/slider_gazebo/planner_input', Float64MultiArray, planner_callback)

    rate = rospy.Rate(1000)
    t_old = time.time()
    remaining_time = 4.0    # s
    support_foot = 0
    u_x = 0
    u_y = 0
    step_time = 0
    toc = 0

    t_a = time.time()

    opti, parameters, variables = initialise(x_dot_ref)
    avg_toc = 0
    i = 0
    while not rospy.is_shutdown():
        t_now = time.time() - t_old
        t_b = time.time()
        # remaining_time += - t_b + t_a
        t_a = time.time()
        current_state = (x, y, x_dot, y_dot, left_x, left_y, right_x, right_y)


        print('##################################################################')
        print('current time \t \t', t_now)
        print('current state \t \t', current_state)
        print('possible remaining time ', remaining_time)

        # try:
        tic = time.time()
        t_opt, u_opt, x_opt, y_opt, x_dot_opt, y_dot_opt = plan(opti, variables, parameters, current_state, remaining_time, toc, support_foot, x_dot_ref)
        toc = time.time() - tic
        step_time = t_opt[1] - toc    # optimal step time
        # ax1 = plt.subplot(2, 1, 1)
        # ax1.scatter(t_opt[:-1], u_opt[0, :])
        # ax1.scatter(t_opt, x_opt)
        # ax2 = plt.subplot(2, 1, 2)
        # ax2.scatter(t_opt[:-1], u_opt[1, :])
        # ax2.scatter(t_opt, y_opt)
        # # plt.legend()

        # plt.show()

        # plt.scatter(u_opt[0, :], u_opt[1, :])
        # plt.scatter(x_opt, y_opt)
        # plt.show()

        i += 1
        avg_toc = (avg_toc * (i - 1) + toc) / i


        print('optimal step time \t', step_time)
        print('optimal x location \t', u_opt[0])
        print('optimal y location \t', u_opt[1])
        print('solve time \t \t', toc)
        print('avg solve time \t', avg_toc)

        new_step = Float64MultiArray()
        new_step.layout.dim = [MultiArrayDimension('', 4, 1)]
        u_x = u_opt[1, 0]
        u_y = u_opt[1, 1]
        new_step.data = [u_x, u_y, step_time, support_foot]
        pub.publish(new_step)
    # except Exception as e:
    #   print(e)
        # pass

        # if step has completed, reset the remaining time
        # if abs(current_state[3] - u_1) < 1e-2 and step_time < 5e-2:
        #     remaining_time = 4.0
        #     support_foot = (support_foot + 1) % 2
        #     print('STEP!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        # elif remaining_time < 5e-2:
        #     remaining_time = 4.0


def planner_callback(msg):
    global x, x_dot, y, y_dot, left_x, left_y, right_x, right_y, foot_flag
    x, x_dot, y, y_dot = msg.data[:4]
    left_x, left_y = msg.data[4:6]
    right_x, right_y = msg.data[6:8]
    foot_flag = msg.data[8]


def plan(opti, variables, parameters, current_state, t_rem, t_sol, support_foot, x_dot_ref, N=3):
    x_0, x_dot_0, y_0, y_dot_0, left_x, left_y, right_x, right_y = current_state
    X_0, Y_0, X_dot_0, Y_dot_0, Ux_0, Uy_0, Xx_s0, Xy_s0, remaining_time, solve_time = parameters
    U, X, t = variables
    if support_foot == 0:
        X_s0 = [right_x, right_y]
        U_0 = [left_x, left_y]
    elif support_foot == 1:
        X_s0 = [left_x, left_y]
        U_0 = [right_x, right_y]

    opti.set_value(X_0, x_0)
    opti.set_value(Y_0, y_0)
    opti.set_value(X_dot_0, x_dot_0)
    opti.set_value(Y_dot_0, y_dot_0)
    opti.set_value(Ux_0, U_0[0])
    opti.set_value(Uy_0, U_0[1])
    opti.set_value(Xx_s0, X_s0[0])
    opti.set_value(Xy_s0, X_s0[1])
    opti.set_value(remaining_time, t_rem)
    opti.set_value(solve_time, t_sol)

    # # ---- initial values for solver ---
    # opti.set_initial(t, np.linspace(0.0, N, N + 1))                         # initial guess at one step per second
    # opti.set_initial(U[0, 0], U_0[0])        # initial guess at steps directly on reference
    # opti.set_initial(U[1, 0], U_0[1])        # initial guess at steps directly on reference
    # opti.set_initial(U[0, 1:], U_0[0] + x_dot_ref[0] * np.linspace(0.0, N, N - 1))        # initial guess at steps directly on reference
    # opti.set_initial(U[1, 1:], U_0[1] + x_dot_ref[1] * np.linspace(0.0, N, N - 1))        # initial guess at steps directly on reference
    # opti.set_initial(X[0, :], x_0 + x_dot_ref[0] * np.linspace(0.0, N, N + 1))    # initial guess at body directly on reference
    # opti.set_initial(X[1, :], y_0 + x_dot_ref[1] * np.linspace(0.0, N, N + 1))    # initial guess at body directly on reference
    # opti.set_initial(X[2, :], x_dot_ref[0] * np.ones(N + 1))
    # opti.set_initial(X[3, :], x_dot_ref[1] * np.ones(N + 1))

    sol = opti.solve()

    t_opt = sol.value(t)
    u_opt = sol.value(U)
    x_opt, y_opt, x_dot_opt, y_dot_opt = sol.value(X)
    return t_opt, u_opt, x_opt, y_opt, x_dot_opt, y_dot_opt


def full_dynamic_model(X_k, U_k, dt, constants=None):
    if constants is None:
        constants = const
    m, m_s, L, h = constants

    x_k = X_k[0]
    y_k = X_k[1]
    x_dot_k = X_k[2]
    y_dot_k = X_k[3]

    u_x_k = U_k[0]
    u_y_k = U_k[1]

    omega = sqrt(g/h)

    x_k_1 = u_x_k + (x_k - u_x_k) * cosh(omega * dt) + (x_dot_k / omega) * sinh(omega * dt)
    y_k_1 = u_y_k + (y_k - u_y_k) * cosh(omega * dt) + (y_dot_k / omega) * sinh(omega * dt)
    x_dot_k_1 = x_dot_k * cosh(omega * dt) + (x_k - u_x_k) * omega * sinh(omega * dt)
    y_dot_k_1 = y_dot_k * cosh(omega * dt) + (y_k - u_y_k) * omega * sinh(omega * dt)
    X_k_1 = vertcat(x_k_1, y_k_1, x_dot_k_1, y_dot_k_1)
    return X_k_1


def runge_kutta_model(X_k, U_k, dt, segments=1, constants=None):
    if constants is None:
        constants = const
    m, m_s, L, h = constants

    f = lambda x, u: vertcat(x[2], x[3], (x[0] - u[0]) * g / h, (x[1] - u[1]) * g / h)  # dx/dt = f(x,u)
    X_k_1 = X_k
    dt_ = dt / segments
    for i in range(segments):
        # Runge-Kutta 4 integration
        k1 = f(X_k, U_k)
        k2 = f(X_k + dt_ / 2 * k1, U_k)
        k3 = f(X_k + dt_ / 2 * k2, U_k)
        k4 = f(X_k + dt_ * k3, U_k)
        X_k_1 = X_k + dt_ / 6 * (k1 + 2 * k2 + 2 * k3 + k4)
        X_k = X_k_1
    return X_k


def heun_model(X_k, U_k, dt, segments=1, constants=None):
    if constants is None:
        constants = const
    m, m_s, L, h = constants

    f = lambda x, u: vertcat(x[2], x[3], (x[0] - u[0]) * g / h, (x[1] - u[1]) * g / h)  # dx/dt = f(x,u)
    X_k_1 = X_k
    dt_ = dt / segments
    for i in range(segments):
        # Runge-Kutta 4 integration
        k1 = f(X_k, U_k)
        X_k_ = X_k + dt_ * k1
        k2 = f(X_k_, U_k)
        X_k_1 = X_k + dt_ / 2 * (k1 + k2)
        X_k = X_k_1
    return X_k


def euler_model(X_k, U_k, dt, segments=1, constants=None):
    if constants is None:
        constants = const
    m, m_s, L, h = constants

    f = lambda x, u: vertcat(x[2], x[3], (x[0] - u[0]) * g / h, (x[1] - u[1]) * g / h)  # dx/dt = f(x,u)
    X_k_1 = X_k
    dt_ = dt / segments
    for i in range(segments):
        # Runge-Kutta 4 integration
        k1 = f(X_k, U_k)
        X_k_1 = X_k + dt_ * k1
        X_k = X_k_1
    return X_k


def initialise(x_dot_ref, N_steps=2, constants=None):
    if constants is None:
        constants = const
    N = N_steps
    m, m_s, L, h = constants
    L_max = np.sqrt(2) * 0.6


    opti = Opti()  # Optimization problem

    # ---- decision variables ---------
    X = opti.variable(4, N + 1)         # state trajectory
    x_pos = X[0, :]                     # x position variable
    y_pos = X[1, :]                     # y position variable
    x_vel = X[2, :]                     # x velocity variable
    y_vel = X[3, :]                     # y velocity variable
    U = opti.variable(2, N)             # control trajectory
    t = opti.variable(1, N + 1)         # step time points

    # ---- parameters -----------------
    X_0 = opti.parameter()
    Y_0 = opti.parameter()
    X_dot_0 = opti.parameter()
    Y_dot_0 = opti.parameter()
    Ux_0 = opti.parameter()
    Uy_0 = opti.parameter()
    Xx_s0 = opti.parameter()
    Xy_s0 = opti.parameter()
    remaining_time = opti.parameter()
    solve_time = opti.parameter()

    U_0 = vertcat(Ux_0, Uy_0)
    X_s0 = vertcat(Xx_s0, Xy_s0)

    # ---- dynamic constraints --------
    dts = t[1:] - t[:-1]
    segments = 10
    for k in range(N):  # loop over control intervals
        x_current = X[:, k]
        u_current = U[:, k]
        dt = t[k + 1] - t[k]
        x_next = runge_kutta_model(x_current, u_current, dt, segments)
        # Constraints
        opti.subject_to(X[:, k + 1] == x_next)

    for k in range(1, N):
        # opti.subject_to(t[k + 1] >= t[k])
        opti.subject_to(t[k + 1] - t[k] >= 0.5)
        opti.subject_to(t[k + 1] - t[k] <= 2.0)

    # ---- objective          ---------
    J = mtimes((X[2, :] - x_dot_ref[0]), (X[2, :] - x_dot_ref[0]).T) + mtimes((X[3, :] - x_dot_ref[1]), (X[3, :] - x_dot_ref[1]).T)# - 0.001 * mtimes(dts, dts.T)

    opti.minimize(J)

    opti.subject_to(X[:, 0] == vertcat(X_0, Y_0, X_dot_0, Y_dot_0))
    opti.subject_to(t[0] == 0)
    opti.subject_to(U[:, 0] == U_0)

    # ---- distance calculations
    support_leg_dist = lambda X_pos, U_pos: sqrt(mtimes((U_pos - X_pos).T, (U_pos - X_pos)))
    swing_leg_dist = lambda X_pos_0, Swing_pos_0, X_pos_1, U_pos_1: sqrt(mtimes(((U_pos_1 - X_pos_1) - (Swing_pos_0 - X_pos_0)).T, ((U_pos_1 - X_pos_1) - (Swing_pos_0 - X_pos_0))))

    # ---- path constraints -----------
    # don't step too fast for swing leg
    tlim = lambda X_pos_0, Swing_pos_0, X_pos_1, U_pos_1: solve_time + swing_leg_dist(X_pos_0, Swing_pos_0, X_pos_1, U_pos_1)/vmax
    # don't travel too far on current leg
    # opti.subject_to(support_leg_dist(X[:, 1:], U[:, :]) <= np.sqrt(L_max ** 2 - h ** 2))
    # don't place next leg too far away
    # opti.subject_to(support_leg_dist(X[:, :-1], U[:, :]) <= np.sqrt(L_max ** 2 - h ** 2))

    # ---- time constraints -----------
    # opti.subject_to(t[1] >= tlim(X[:2, 0], X[:2, 1], X_s0, U[1]))
    opti.subject_to(t[1] - t[0] <= remaining_time)

    # ---- solve              ------
    options = {"ipopt.print_level": 0, "print_out": False, "print_in": False, "print_time": False}
    opti.solver("ipopt", options)
    return opti, [X_0, Y_0, X_dot_0, Y_dot_0, Ux_0, Uy_0, Xx_s0, Xy_s0, remaining_time, solve_time], [U, X, t]


if __name__=="__main__":
    main()

#!/usr/bin/env python
"""
swing_foot.py
generates optimal swingfoot trajectory

Digby Chappell and Ke Wang
April 2020
"""

import csv
import time
import numpy as np
from casadi import *
from datetime import datetime
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
plt.rcParams.update({'font.size': 16})

m = 15.0    # body mass
m_s = 0.3   # swing leg mass
L = 1.2     # leg length
h = 0.7     # body height
g = 9.81    # gravity
const = [m, m_s, L, h]


def x(t_, v_0, h, g, theta_start):
    """
    :param t_: time points during the step
    :param v_0: the initial velocity of the body
    :param h: the body height
    :param g: gravity (9.81m/s^2)
    :param theta_start: the initial angle of the support leg
    :return: the horizontal position of the body through the step
    """
    sinh_part = v_0 * np.sqrt(h / g) * np.sinh(np.sqrt(g / h) * t_)
    cosh_part = h * np.tan(theta_start) * np.cosh(np.sqrt(g / h) * t_)
    return sinh_part + cosh_part


def x_dot(t_, v0, h_, g_, theta_start_):
    """
    :param t_: time points during the step
    :param v0: the initial velocity of the body
    :param h_: the body height
    :param g_: gravity (9.81m/s^2)
    :param theta_start_: initial angle of the support leg
    :return: the velocity of the body through the step
    """
    cosh_part = v0 * np.cosh(np.sqrt(g_ / h_) * t_)
    sinh_part = np.sqrt(g_ * h_) * np.tan(theta_start_) * np.sinh(np.sqrt(g_ / h_) * t_)
    return cosh_part + sinh_part


def x_dot_dot(t_, v0, h_, g_, theta_start_):
    """
    :param t_: time points during the step
    :param v0: the initial velocity of the body
    :param h_: the body height
    :param g_: gravity (9.81m/s^2)
    :param theta_start_: initial angle of the support leg
    :return: the acceleration of the body through the step
    """
    sinh_part = v0 * np.sqrt(g_ / h_) * np.sinh(np.sqrt(g_ / h_) * t_)
    cosh_part = g_ * h_ * np.tan(theta_start_) * np.cosh(np.sqrt(g_ / h_) * t_)
    return sinh_part + cosh_part


def theta(x_, h):
    """
    :param x_: the horizontal position of the body through the step
    :param h: the body height
    :return: theta_ - the angle of the support leg through the step
    """
    theta_ = np.arctan2(x_, h)
    return theta_


def theta_dot(x_, x_dot_, h):
    """
    :param x_: the horizontal position of the body through the step
    :param x_dot_: the velocity of the body through the step
    :param h: the body height
    :return: theta_dot_ - the angular velocity of the support leg through the step
    """
    theta_dot_ = x_dot_ * h / (x_ ** 2 + h ** 2)
    return theta_dot_


def l(x_, theta_, h):
    """
    :param x_: the horizontal position of the body through the step
    :param theta_: the angle of the support leg through the step
    :param h: the body height
    :return: l_ - the length of the support leg through the step
    """
    if np.any(x_ == 0) or np.any(theta_ == 0):
        l_ = h / np.cos(theta_)
    else:
        l_ = x_ / np.sin(theta_)
    return l_


def generate_trajectory(initial, final, constants=None):
    if constants is None:
        constants = const

    t0, l_0, th_0, l_dot_0, th_dot_0 = initial
    tT, l_T, th_T, l_dot_T, th_dot_T = final
    m, m_s, L, h = constants

    print('body height \t \t \t', h)
    print('joint space initial \t', initial)
    print('joint space final \t \t', final)

    x_0 = (l_0 - L/2) * np.sin(th_0)
    z_0 = -(l_0 - L/2) * np.cos(th_0)
    x_dot_0 = l_dot_0 * np.sin(th_0) + th_dot_0 * (l_0 - L / 2) * np.cos(th_0)
    z_dot_0 = -l_dot_0 * np.cos(th_0) - th_dot_0 * (l_0 - L / 2) * np.sin(th_0)

    foot_1_z = h + z_0 - (L/2)*z_0/np.sqrt(x_0**2 + z_0**2)
    foot_2_z = h + z_0 + (L/2)*z_0/np.sqrt(x_0**2 + z_0**2)
    foot_x = L * np.sin(th_0)
    foot_z = L * np.cos(th_0)
    print(foot_1_z, foot_2_z, (foot_x, foot_z))

    l_com = sqrt(x_0 ** 2 + z_0 ** 2)
    print(l_com)

    x_T = (l_T - L/2) * np.sin(th_T)
    z_T = -(l_T - L/2) * np.cos(th_T)
    x_dot_T = l_dot_T * np.sin(th_T) + th_dot_T * (l_T - L / 2) * np.cos(th_T)
    z_dot_T = -l_dot_T * np.cos(th_T) - th_dot_T * (l_T - L / 2) * np.sin(th_T)

    foot_1_z = h + z_T - (L/2)*z_T/np.sqrt(x_T**2 + z_T**2)
    foot_2_z = h + z_T + (L/2)*z_T/np.sqrt(x_T**2 + z_T**2)
    foot_x = L * np.sin(th_T)
    foot_z = L * np.cos(th_T)
    print(foot_1_z, foot_2_z, (foot_x, foot_z))

    l_com = sqrt(x_0 ** 2 + z_0 ** 2)
    print(l_com)

    cartesian_initials = [t0, x_0, z_0 + 1e-12, x_dot_0, z_dot_0]
    cartesian_finals = [tT, x_T, z_T + 1e-12, x_dot_T, z_dot_T]
    results = optimal_trajectory(cartesian_initials, cartesian_finals, constants)
    x_opt, z_opt, x_dot_opt, z_dot_opt, x_dot_dot_opt, z_dot_dot_opt, t = results
    return x_opt, z_opt, x_dot_opt, z_dot_opt, x_dot_dot_opt, z_dot_dot_opt, t


def optimal_trajectory(initial, final, constants=None):
    if constants is None:
        constants = const

    t_0, x_0, z_0, x_dot_0, z_dot_0 = initial
    t_T, x_T, z_T, x_dot_T, z_dot_T = final
    m, m_s, L, h = constants

    u_0 = [0, 0]
    u_T = [0, 0]

    dt = 0.01

    N = int((t_T - t_0) / dt)

    t = np.linspace(t_0, t_T, N+1)

    opti = Opti()  # Optimization problem

    # ---- decision variables ---------
    X = opti.variable(4, N + 1)         # state trajectory
    U = opti.variable(2, N)             # control trajectory

    l_com = sqrt(X[0, :]**2 + X[1, :]**2)
    foot_1_z = h + X[1, :] + X[1, :]/sqrt(X[0, :]**2 + X[1, :]**2)*L/2
    foot_2_z = h + X[1, :] - X[1, :]/sqrt(X[0, :]**2 + X[1, :]**2)*L/2

    # ---- dynamic constraints --------
    x_dot_dot = lambda x, u: u[0] / m_s
    z_dot_dot = lambda x, u: u[1] / m_s - g
    f = lambda x, u: vertcat(x[2], x[3], x_dot_dot(x, u), z_dot_dot(x, u))

    for k in range(N):  # loop over control intervals
        x_current = X[:, k]
        # Runge-Kutta 4 integration
        k1 = f(x_current, U[:, k])
        k2 = f(x_current + dt / 2 * k1, U[:, k])
        k3 = f(x_current + dt / 2 * k2, U[:, k])
        k4 = f(x_current + dt * k3, U[:, k])
        x_next = x_current + dt / 6 * (k1 + 2 * k2 + 2 * k3 + k4)
        # Constraints
        opti.subject_to(X[:, k + 1] == x_next)

    # ---- path constraints -----------
    opti.subject_to(opti.bounded(0, l_com, L/2))
    # opti.subject_to(opti.bounded(-2*np.pi, theta, 2*np.pi))
    opti.subject_to(foot_1_z >= 0)  # foot above floor
    opti.subject_to(foot_2_z >= 0)  # foot above floor

    # ---- boundary conditions --------
    opti.subject_to(X[:, 0] == initial[1:])
    opti.subject_to(X[:, -1] == final[1:])
    # opti.subject_to(X[1, -1] == 0.0)
    # opti.subject_to(U[:, 0] == u_0)
    # opti.subject_to(U[:, -1] == u_T)

    # ---- objective          ---------
    J = mtimes(U[0, :], U[0, :].T) + mtimes(U[1, :], U[1, :].T)

    opti.minimize(J)

    # ---- initial values for solver ---
    opti.set_initial(X[0, :], np.linspace(initial[1], final[1], N + 1))
    opti.set_initial(X[1, :], np.linspace(initial[2], final[2], N + 1))

    # ---- solve              ------
    options = {"print_out": False, "print_in": False, "print_time": False}
    opti.solver("ipopt", options)
    # option = {"print_iteration": False, "print_header": False, "print_status": False, "tol_du": 0.1, "max_iter": 4}
    # opti.solver('sqpmethod', option)

    print('initial \t \t', initial)
    print('final \t \t \t', final)
    print('N steps', N)

    start = time.time()
    sol = opti.solve()

    end = time.time() - start
    print(end)

    x = X[0, :]
    z = X[1, :]
    x_dot = X[2, :]
    z_dot = X[3, :]
    x_dot_dot = x_dot_dot(X, U)
    z_dot_dot = z_dot_dot(X, U)

    x_opt = sol.value(x)
    z_opt = sol.value(z)
    x_dot_opt = sol.value(x_dot)
    z_dot_opt = sol.value(z_dot)
    x_dot_dot_opt = sol.value(x_dot_dot)
    z_dot_dot_opt = sol.value(z_dot_dot)
    # u_opt = sol.value(U)
    return x_opt, z_opt, x_dot_opt, z_dot_opt, x_dot_dot_opt, z_dot_dot_opt, t


if __name__=="__main__":
    n_tries = 11 * 11 * 5 * 5 * 5 * 5
    j = 0
    print(n_tries)
    v0s = np.linspace(0, 10, 11)
    t1s = np.linspace(0.1, 2.1, 11)
    hs = np.linspace(0.2, 1.0, 5)
    for v0 in v0s:
        for h in hs:
            thmin = -np.arccos(h / L) + 0.1
            thmax = np.arccos(h / L) - 0.1
            th0s = np.linspace(-thmax, thmax, 5)
            thb0s = np.linspace(-thmax, thmax, 5)
            thb1s = np.linspace(-thmax, thmax, 5)
            for th0 in th0s:
                for t1 in t1s:
                    ts = np.linspace(0, t1, 100)
                    xmin = - np.sqrt(L**2 - h**2)
                    xmax = np.sqrt(L**2 - h**2)
                    xs = x(ts, v0, h, g, th0)
                    if np.any(xs > xmax) or np.any(xs < xmin):
                        pass
                    else:
                        th1 = theta(xs[-1], h)
                        v1 = x_dot(t1, v0, h, g, th0)
                        for thb0 in thb0s:
                            for thb1 in thb1s:
                                j += 1
                                t0 = 0
                                l_0 = h / np.cos(thb0)
                                l_1 = h / np.cos(thb1)
                                th_0 = thb0
                                th_1 = thb1
                                l_dot_0 = v0 * np.sin(th_0)
                                l_dot_1 = v1 * np.sin(th_1)
                                th_dot_0 = v0 * h / (xs[0] ** 2 + h ** 2)
                                th_dot_1 = v1 * h / (xs[-1] ** 2 + h ** 2)

                                roto_initials = [t0, l_0, th_0, l_dot_0, th_dot_0]
                                roto_finals = [t1, L - l_1, np.pi + th_1, - l_dot_1, th_dot_1]

                                trad_initials = [t0, l_0, th_0, l_dot_0, th_dot_0]
                                trad_finals = [t1, l_1, th_1, l_dot_1, th_dot_1]
                                constants = [m, m_s, L, h]
                                try:
                                    print('trajectory', j, 'at \t', datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
                                    x_opt, z_opt, x_dot_opt, z_dot_opt, x_dot_dot_opt, z_dot_dot_opt, t = generate_trajectory(roto_initials, roto_finals, constants)
                                    roto_row = [roto_initials, roto_finals, x_opt, z_opt, x_dot_opt, z_dot_opt, x_dot_dot_opt, z_dot_dot_opt]
                                    with open(r'roto_trajectories.csv', 'a') as f1:
                                        writer1 = csv.writer(f1)
                                        writer1.writerow(roto_row)
                                except Exception as e:
                                    pass
                                try:
                                    x_opt, z_opt, x_dot_opt, z_dot_opt, x_dot_dot_opt, z_dot_dot_opt, t = generate_trajectory(trad_initials, trad_finals, constants)
                                    trad_row = [trad_initials, trad_finals, x_opt, z_opt, x_dot_opt, z_dot_opt, x_dot_dot_opt, z_dot_dot_opt]
                                    with open(r'trad_trajectories.csv', 'a') as f2:
                                        writer2 = csv.writer(f2)
                                        writer2.writerow(trad_row)
                                except Exception as e:
                                    pass

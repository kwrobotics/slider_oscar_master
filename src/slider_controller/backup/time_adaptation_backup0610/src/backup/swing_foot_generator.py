#!/usr/bin/env python
"""
swing_foot_generator.py
Generates optimal swingfoot trajectories across the entire joint space. Writes all trajectories
to a .csv file dataset.

Digby Chappell and Ke Wang
April 2020
"""

import csv
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime
from trajectory_optimisation.ICLOCS.SLIDER.swingfoot_caller import matlab_swingfoot, add_iclocs
plt.rcParams.update({'font.size': 16})


g = 9.81

m = 15.0    # body mass
m_s = 0.3   # swing leg mass
L = 1.2     # leg length
g = 9.81    # gravity


def x(t_, v_0, h, g, theta_start):
    """
    :param t_: time points during the step
    :param v_0: the initial velocity of the body
    :param h: the body height
    :param g: gravity (9.81m/s^2)
    :param theta_start: the initial angle of the support leg
    :return: the horizontal position of the body through the step
    """
    sinh_part = v_0 * np.sqrt(h / g) * np.sinh(np.sqrt(g / h) * t_)
    cosh_part = h * np.tan(theta_start) * np.cosh(np.sqrt(g / h) * t_)
    return sinh_part + cosh_part


def x_dot(t_, v0, h_, g_, theta_start_):
    """
    :param t_: time points during the step
    :param v0: the initial velocity of the body
    :param h_: the body height
    :param g_: gravity (9.81m/s^2)
    :param theta_start_: initial angle of the support leg
    :return: the velocity of the body through the step
    """
    cosh_part = v0 * np.cosh(np.sqrt(g_ / h_) * t_)
    sinh_part = np.sqrt(g_ * h_) * np.tan(theta_start_) * np.sinh(np.sqrt(g_ / h_) * t_)
    return cosh_part + sinh_part


def x_dot_dot(t_, v0, h_, g_, theta_start_):
    """
    :param t_: time points during the step
    :param v0: the initial velocity of the body
    :param h_: the body height
    :param g_: gravity (9.81m/s^2)
    :param theta_start_: initial angle of the support leg
    :return: the acceleration of the body through the step
    """
    sinh_part = v0 * np.sqrt(g_ / h_) * np.sinh(np.sqrt(g_ / h_) * t_)
    cosh_part = g_ * h_ * np.tan(theta_start_) * np.cosh(np.sqrt(g_ / h_) * t_)
    return sinh_part + cosh_part


def theta(x_, h):
    """
    :param x_: the horizontal position of the body through the step
    :param h: the body height
    :return: theta_ - the angle of the support leg through the step
    """
    theta_ = np.arctan2(x_, h)
    return theta_


def theta_dot(x_, x_dot_, h):
    """
    :param x_: the horizontal position of the body through the step
    :param x_dot_: the velocity of the body through the step
    :param h: the body height
    :return: theta_dot_ - the angular velocity of the support leg through the step
    """
    theta_dot_ = x_dot_ * h / (x_ ** 2 + h ** 2)
    return theta_dot_


def l(x_, theta_, h):
    """
    :param x_: the horizontal position of the body through the step
    :param theta_: the angle of the support leg through the step
    :param h: the body height
    :return: l_ - the length of the support leg through the step
    """
    if np.any(x_ == 0) or np.any(theta_ == 0):
        l_ = h / np.cos(theta_)
    else:
        l_ = x_ / np.sin(theta_)
    return l_


def step_trajectory(constants, initial, final):
    """
    :param constants: body mass, leg mass, leg length, body height, time step
    :param initial: initial velocity, initial support leg angle, initial swing leg angle
    :param final: final velocity, final support leg angle, final swing leg angle
    :param step: integer from 0 to 3, specifies which step the robot is on (note for traditional
                step 0 is the same as step 2)
    :return: ts - array of time points,
             trajectories - left l roto, left l trad, left th roto, left th trad,
                            right l roto, right l trad, right th roto, right th trad
             costs - roto cost, trad cost
    """
    add_iclocs()
    # constants
    m, m_s, L, h = constants
    t0, l_0, th_0, l_dot_0, th_dot_0 = initial
    t1, l_1, th_1, l_dot_1, th_dot_1 = final

    nts = 10

    terminal_timestep = nts

    bounds = [0, L, -2*np.pi, 2*np.pi]

    parameters = [m_s, L, h]

    roto_initials = [l_0,
                     th_0,
                     l_dot_0,
                     th_dot_0]
    roto_finals = [L - l_0,
                   np.pi + th_1,
                   - l_dot_1,
                   th_dot_1]

    trad_initials = [l_0,
                     th_0,
                     l_dot_0,
                     th_dot_0]
    trad_finals = [l_0,
                   th_1,
                   l_dot_1,
                   th_dot_1]
    print('-------------------------------------------------------------------------------------------------------------')
    print('step duration \t\t', t1)
    print('roto initial \t \t', roto_initials)
    print('roto final \t \t \t', roto_finals)

    print('trad initial \t \t', trad_initials)
    print('trad final \t \t \t', trad_finals)

    roto_params = [terminal_timestep,
                   ts[-1],
                   roto_initials,
                   roto_finals,
                   bounds,
                   parameters]
    trad_params = [terminal_timestep,
                   ts[-1],
                   trad_initials,
                   trad_finals,
                   bounds,
                   parameters]

    roto_cost, roto_l_traj, roto_theta_traj = matlab_swingfoot(roto_params)
    print(roto_cost)
    trad_cost, trad_l_traj, trad_theta_traj = matlab_swingfoot(trad_params)
    print(trad_cost)

    costs = [roto_cost, trad_cost]

    left_l_roto = roto_l_traj
    left_l_trad = trad_l_traj
    left_theta_roto = roto_theta_traj
    left_theta_trad = trad_theta_traj
    trad_trajectories = [left_l_trad, left_theta_trad]
    roto_trajectories = [left_l_roto, left_theta_roto]

    return trad_trajectories, roto_trajectories, costs


if __name__ == "__main__":
    n_tries = 11 * 11 * 5 * 5 * 5 * 5
    j = 0
    print(n_tries)
    v0s = np.linspace(0, 10, 11)
    t1s = np.linspace(0.1, 2.1, 11)
    hs = np.linspace(0.2, 1.0, 5)
    for v0 in v0s:
        for h in hs:
            thmin = -np.arccos(h / L) + 0.1
            thmax = np.arccos(h / L) - 0.1
            th0s = np.linspace(-thmax, thmax, 5)
            thb0s = np.linspace(-thmax, thmax, 5)
            thb1s = np.linspace(-thmax, thmax, 5)
            for th0 in th0s:
                for t1 in t1s:
                    ts = np.linspace(0, t1, 100)
                    xmin = - np.sqrt(L**2 - h**2)
                    xmax = np.sqrt(L**2 - h**2)
                    xs = x(ts, v0, h, g, th0)
                    if np.any(xs > xmax) or np.any(xs < xmin):
                        pass
                    else:
                        th1 = theta(xs[-1], h)
                        v1 = x_dot(t1, v0, h, g, th0)
                        for thb0 in thb0s:
                            for thb1 in thb1s:
                                j += 1
                                t0 = 0
                                l_0 = h / np.cos(thb0)
                                l_1 = h / np.cos(thb1)
                                th_0 = thb0
                                th_1 = thb1
                                l_dot_0 = v0 * np.sin(th_0)
                                l_dot_1 = v1 * np.sin(th_1)
                                th_dot_0 = v0 * h / (xs[0] ** 2 + h ** 2)
                                th_dot_1 = v1 * h / (xs[-1] ** 2 + h ** 2)

                                initial = [t0, l_0, th_0, l_dot_0, th_dot_0]
                                final = [t1, l_1, th_1, l_dot_1, th_dot_1]
                                constants = [m, m_s, L, h]
                                print('trajectory', j, 'at \t', datetime.now().strftime("%d/%m/%Y %H:%M:%S"))
                                trad, roto, costs = step_trajectory(constants, initial, final)
                                trad_row = [initial, final, trad]
                                roto_row = [initial, final, roto]
                                with open(r'trad_trajectories.csv', 'a') as f:
                                    writer = csv.writer(f)
                                    writer.writerow(trad_row)
                                with open(r'roto_trajectories.csv', 'a') as f:
                                    writer = csv.writer(f)
                                    writer.writerow(roto_row)


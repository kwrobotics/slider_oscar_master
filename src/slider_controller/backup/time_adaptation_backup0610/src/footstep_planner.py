#!/usr/bin/env python
"""
foot_step_planner.py
takes reference trajectory and current state, produces footstep positions
and times for next N steps.

Digby Chappell and Ke Wang
April 2020
"""

import time
import rospy
import numpy as np
from casadi import *
import matplotlib.pyplot as plt
from std_msgs.msg import Float64MultiArray, MultiArrayDimension, Float64


m = 15.0    # body mass
m_s = 0.3   # swing leg mass
L = 1.2     # leg length
h = 0.7     # body height
g = 9.81    # gravity

const = [m, m_s, L, h]

T_end = 100
dt = 0.001
vmax = 10.0     		 # m/s
x_dot_ref = 0.1     # m/s


# Parameters subscribed from the slider_controller node #######################################
x = 0
x_dot = 0
y = 0
y_dot = 0
left_x = 0
left_y = 0
right_x = 0
right_y = 0
foot_flag = 0
###############################################################################################

def main():
    rospy.init_node('footstep_planner', anonymous=True)
    pub = rospy.Publisher('/slider_gazebo/footstep_plan', Float64MultiArray, queue_size=1)
    planner_input = rospy.Subscriber('/slider_gazebo/planner_input', Float64MultiArray, planner_callback)

    rate = rospy.Rate(1000)
    t_old = time.time()
    remaining_time = 4.0   	# s
    support_foot = 0
    u_1 = 0
    step_time = 0
    toc = 0

    t_a = time.time()
    while not rospy.is_shutdown():
        t_now = time.time() - t_old
        t_b = time.time()
        remaining_time += - t_b + t_a
        t_a = time.time()
        if support_foot == 0:							# left foot support
        	current_state = (x, x_dot, left_x, right_x)
        elif support_foot == 1:							# right foot support
        	current_state = (x, x_dot, right_x, left_x)

        print('##################################################################')
        print('current time \t \t', t_now)
        print('current state \t \t', current_state)
        print('possible remaining time ', remaining_time)

        try:
	        tic = time.time()
	        t_opt, u_opt, x_opt, x_dot_opt = plan(current_state, x_dot_ref, remaining_time, vmax, toc)
	        toc = time.time() - tic
	        step_time = t_opt[1] - toc    # optimal step time

	        print('optimal step time \t', step_time)
	        print('optimal step location \t', u_opt[1])
	        print('solve time \t \t', toc)

	        new_step = Float64MultiArray()
	        new_step.layout.dim = [MultiArrayDimension('', 3, 1)]
	        u_1 = u_opt[1]
	        new_step.data = [u_1, step_time, support_foot]
        	pub.publish(new_step)
        except Exception as e:
        	print(e)
        	pass

        # if step has completed, reset the remaining time
        if abs(current_state[3] - u_1) < 1e-2 and step_time < 5e-2:
            remaining_time = 4.0
            support_foot = (support_foot + 1) % 2
            print('STEP!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        elif remaining_time < 5e-2:
            remaining_time = 4.0


def planner_callback(msg):
    global x, x_dot, y, y_dot, left_x, left_y, right_x, right_y, foot_flag
    x, x_dot, y, y_dot = msg.data[:4]
    left_x, left_y = msg.data[4:6]
    right_x, right_y = msg.data[6:8]
    foot_flag = msg.data[8]


def plan(current_state, x_dot_ref, remaining_time, vmax, solve_time, N_steps=2, constants=None):
    if constants is None:
        constants = const
    X_0, X_dot_0, U_0, X_s0 = current_state
    N = N_steps
    m, m_s, L, h = constants
    L_max = np.sqrt(2) * 0.6

    opti = Opti()  # Optimization problem

    # ---- decision variables ---------
    X = opti.variable(2, N + 1)         # state trajectory
    pos = X[0, :]                       # position variable
    vel = X[1, :]                       # velocity variable
    U = opti.variable(1, N)             # control trajectory
    t = opti.variable(1, N + 1)         # step time points

    # ---- dynamic constraints --------
    f = lambda x, u: vertcat(x[1], x[0] * g / h - u * g / h)  # dx/dt = f(x,u)

    dts = t[1:] - t[:-1]
    segments = 3
    for k in range(N):  # loop over control intervals
        x_current = X[:, k]
        dt = (t[k + 1] - t[k]) / segments
        x_next = x_current
        for i in range(segments):
            # Runge-Kutta 4 integration
            k1 = f(x_current, U[:, k])
            k2 = f(x_current + dt / 2 * k1, U[:, k])
            k3 = f(x_current + dt / 2 * k2, U[:, k])
            k4 = f(x_current + dt * k3, U[:, k])
            x_next = x_current + dt / 6 * (k1 + 2 * k2 + 2 * k3 + k4)
            x_current = x_next
        # Constraints
        opti.subject_to(X[:, k + 1] == x_next)

    # ---- path constraints -----------
    ulim = lambda pos: pos + np.sqrt(L_max ** 2 - h ** 2)
    llim = lambda pos: pos - np.sqrt(L_max ** 2 - h ** 2)
    utlim = lambda x0, x1, xs0, u1: solve_time + ((u1 - x1) - (xs0 - x0))/vmax
    ltlim = lambda x0, x1, xs0, u1: solve_time + ((xs0 - x0) - (u1 - x1))/vmax

    # don't travel too far on current leg
    opti.subject_to(U <= ulim(pos[:-1]))  # maximum leg length
    opti.subject_to(U >= llim(pos[:-1]))  # maximum leg length
    # don't place next leg too far away
    opti.subject_to(U <= ulim(pos[1:]))  # maximum leg length
    opti.subject_to(U >= llim(pos[1:]))  # maximum leg length

    # ---- time constraints -----------
    opti.subject_to(t[1] >= utlim(pos[0], pos[1], X_s0, U[1]))
    opti.subject_to(t[1] >= ltlim(pos[0], pos[1], X_s0, U[1]))
    opti.subject_to(t[1] - t[0] <= remaining_time)
    for k in range(1, N):
        # opti.subject_to(t[k + 1] >= t[k])
        opti.subject_to(t[k + 1] - t[k] >= 0.5)
        opti.subject_to(t[k + 1] - t[k] <= 2.0)

    # ---- boundary conditions --------
    opti.subject_to(pos[0] == X_0)
    opti.subject_to(vel[0] == X_dot_0)
    opti.subject_to(t[0] == 0)
    opti.subject_to(U[0] == U_0)

    # ---- objective          ---------
    J = mtimes((vel - x_dot_ref), (vel - x_dot_ref).T) #- 0.001 * mtimes(dts, dts.T)

    opti.minimize(J)

    # ---- initial values for solver ---
    opti.set_initial(t, np.linspace(0.0, N_steps, N + 1))                 # initial guess at one step per second
    opti.set_initial(U, x_dot_ref * np.linspace(0.0, N_steps, N))         # initial guess at steps directly on reference
    opti.set_initial(pos, x_dot_ref * np.linspace(0.0, N_steps,  + 1))    # initial guess at body directly on reference

    # ---- solve              ------
    options = {"ipopt.print_level": 0, "print_out": False, "print_in": False, "print_time": False}
    opti.solver("ipopt", options)

    sol = opti.solve()

    t_opt = sol.value(t)
    u_opt = sol.value(U)
    x_opt = sol.value(pos)
    x_dot_opt = sol.value(vel)
    return t_opt, u_opt, x_opt, x_dot_opt


if __name__=="__main__":
    main()

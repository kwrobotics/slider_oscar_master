#include "osc.h"
#include "gen_trajectory.h"
#include "define.h"
#include "LIPM.h"

#include "ros/ros.h"
#include <message_filters/synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>

#include <geometry_msgs/Vector3.h>
#include <std_msgs/Float64.h>
#include <std_msgs/Float64MultiArray.h>

#include <gazebo/gazebo.hh>
#include <gazebo/transport/transport.hh>
#include <gazebo/msgs/msgs.hh>
#include <gazebo/gazebo_client.hh>

#include <sensor_msgs/JointState.h>
#include <gazebo_msgs/LinkStates.h>
#include <Eigen/Geometry>
#include <eigen_conversions/eigen_msg.h>

#include <iostream>
#include <chrono>
#include <cmath>

using namespace gazebo;

// The joint configuration (angle, velocity) should be a column Eigen vector, 
// this should be obtained from sensor data (e.g. Gazebo joint data).
VectorXd q(17);
VectorXd v(16);
VectorXd q_base(7);
VectorXd q_joint(10);
VectorXd v_base(6);
VectorXd v_joint(10);
Vector3d left_foot_pos, right_foot_pos, base_euler_angle;
const double alpha = 10.0*3.14159/180.0;
const double x_offset = 0.0625;//
// Step time
double step_time = 0.7;
int support_foot_flag;
Support whichLeg;
// Remaining and current time for the current step
double remaining_time = step_time;
double current_time = 0;
int support_foot_prev;
bool start = true;
// Result obtained from the MPC planner
VectorXd zmp_foothold(4);
// contact force
Vector4d LcontactForce;
Vector4d RcontactForce;

std_msgs::Float64 getROSmsg(double torque)
{
  std_msgs::Float64 msg;

  msg.data = torque;
  return msg;
}

struct EulerAngles
{
    double roll, pitch, yaw;
};

Vector3d ToEulerAngles(Quaternionf q)
{
    EulerAngles angles;
    Vector3d euler_angles;

    // roll (x-axis rotation)
    double sinr_cosp = +2.0 * (q.w() * q.x() + q.y() * q.z());
    double cosr_cosp = +1.0 - 2.0 * (q.x() * q.x() + q.y() * q.y());
    angles.roll = atan(sinr_cosp/cosr_cosp);

    // pitch (y-axis rotation)
    double sinp = +2.0 * (q.w() * q.y() - q.z() * q.x());
    if (fabs(sinp) >= 1)
        angles.pitch = copysign(PI/2, sinp); // use 90 degrees if out of range
    else
        angles.pitch = asin(sinp);

    // yaw (z-axis rotation)
    double siny_cosp = +2.0 * (q.w() * q.z() + q.x() * q.y());
    double cosy_cosp = +1.0 - 2.0 * (q.y() * q.y() + q.z() * q.z());  
    angles.yaw = atan(siny_cosp/cosy_cosp);
    euler_angles << angles.roll, angles.pitch, angles.yaw;

    return euler_angles;
}

// Joint_state callback function
// file: sensor_msgs/JointState.msg
void jointStateCallback(const sensor_msgs::JointState::ConstPtr& msg)
{
    q_joint << msg->position[3], msg->position[2], msg->position[4], msg->position[0], msg->position[1],
               msg->position[8], msg->position[7], msg->position[9], msg->position[5], msg->position[6];
    v_joint << msg->velocity[3], msg->velocity[2], msg->velocity[4], msg->velocity[0], msg->velocity[1],
               msg->velocity[8], msg->velocity[7], msg->velocity[9], msg->velocity[5], msg->velocity[6];
    // cout << "q_joint: \n" << q_joint << endl;
    // cout << "v_joint: \n" << v_joint << endl;
}

// Link_state callback function
// file: gazebo_msgs/Linkstate.msg
void linkStateCallback(const gazebo_msgs::LinkStates::ConstPtr& msg)
{
    q_base << msg->pose[1].position.x, msg->pose[1].position.y, (msg->pose[1].position.z - pelvis_height),
              msg->pose[1].orientation.x, msg->pose[1].orientation.y,  msg->pose[1].orientation.z, msg->pose[1].orientation.w;
    v_base << msg->twist[1].linear.x, msg->twist[1].linear.y, msg->twist[1].linear.z,
              msg->twist[1].angular.x, msg->twist[1].angular.y, msg->twist[1].angular.z;
    left_foot_pos << msg->pose[6].position.x, msg->pose[6].position.y, (msg->pose[6].position.z - 0.04);
    right_foot_pos << msg->pose[11].position.x, msg->pose[11].position.y, (msg->pose[11].position.z - 0.04);

    // cout << "q_base: \n" << q_base << endl;
    // cout << "v_base: \n" << v_base << endl;
}

// Callback function for receiving the planner outputs
void footstepPlanCallback(const std_msgs::Float64MultiArray::ConstPtr& msg)
{
    zmp_foothold(0) = msg->data[0];
    zmp_foothold(1) = msg->data[1];
    zmp_foothold(2) = msg->data[2];
    zmp_foothold(3) = msg->data[3];
    remaining_time = msg->data[4];
    support_foot_flag = msg->data[5];
    step_time = current_time + remaining_time;
//    else {step_time = 0.25; }
}

// Contact force callback function
void forceCbL1(ConstContactsPtr &_msg){
    if ( _msg->contact_size()!= 0){
        LcontactForce(0) = _msg->contact(0).wrench().Get(0).body_1_wrench().force().z();
//         cout << "The force 1 in Z direction is: " << LcontactForce(0) << endl;
      }
    else  {LcontactForce(0) = 0;}
}
void forceCbL2(ConstContactsPtr &_msg){
    if ( _msg->contact_size()!= 0){
        LcontactForce(1) = _msg->contact(0).wrench().Get(0).body_1_wrench().force().z();
//         cout << "The force 2 in Z direction is: " << LcontactForce(1) << endl;
    }
    else  {LcontactForce(1) = 0;}
}
void forceCbL3(ConstContactsPtr &_msg){
    if ( _msg->contact_size()!= 0){
        LcontactForce(2) = _msg->contact(0).wrench().Get(0).body_1_wrench().force().z();
//         cout << "The force 3 in Z direction is: " << LcontactForce(2) << endl;
    }
    else  {LcontactForce(2) = 0;}
}
void forceCbL4(ConstContactsPtr &_msg){
    if ( _msg->contact_size()!= 0){
        LcontactForce(3) = _msg->contact(0).wrench().Get(0).body_1_wrench().force().z();
//         cout << "The force 4 in Z direction is: " << LcontactForce(3) << endl;
    }
    else  {LcontactForce(3) = 0;}
}

void forceCbR1(ConstContactsPtr &_msg){
    if ( _msg->contact_size()!= 0){
        RcontactForce(0) = _msg->contact(0).wrench().Get(0).body_1_wrench().force().z();
//         cout << "The force 5 in Z direction is: " << RcontactForce(0) << endl;
    }
    else  {RcontactForce(0) = 0;}
}
void forceCbR2(ConstContactsPtr &_msg){
    if ( _msg->contact_size()!= 0){
        RcontactForce(1) = _msg->contact(0).wrench().Get(0).body_1_wrench().force().z();
//         cout << "The force 6 in Z direction is: " << RcontactForce(1) << endl;
    }
    else  {RcontactForce(1) = 0;}
}
void forceCbR3(ConstContactsPtr &_msg){
    if ( _msg->contact_size()!= 0){
        RcontactForce(2) = _msg->contact(0).wrench().Get(0).body_1_wrench().force().z();
//         cout << "The force 7 in Z direction is: " << RcontactForce(2) << endl;
    }
    else  {RcontactForce(2) = 0;}
}
void forceCbR4(ConstContactsPtr &_msg){
    if ( _msg->contact_size()!= 0){
        RcontactForce(3) = _msg->contact(0).wrench().Get(0).body_1_wrench().force().z();
//         cout << "The force 8 in Z direction is: " << RcontactForce(3) << endl;
    }
    else  {RcontactForce(3) = 0;}
}

int main(int argc, char ** argv)
{

    //********************************************************************************************
    //****************************ROS node initialization*****************************************
    //********************************************************************************************
    ros::init(argc, argv, "ros_SLIDER_OSC_node");
    ros::NodeHandle n;
    // Load gazebo
    // client::setup(argc, argv);
    ros::Rate loop_rate(LOOP_RATE);

    // Load gazebo
    client::setup(argc, argv);
      // Create our node for communication
    transport::NodePtr node(new gazebo::transport::Node());
    node->Init();
    // Publisher for ros torque control
    ros::Publisher left_hip_pitch_pub   = n.advertise<std_msgs::Float64>("/time_slider_gazebo/left_hip_pitch_torque_controller/command", 1);
    ros::Publisher left_hip_roll_pub    = n.advertise<std_msgs::Float64>("/time_slider_gazebo/left_hip_roll_torque_controller/command", 1);
    ros::Publisher left_hip_slide_pub   = n.advertise<std_msgs::Float64>("/time_slider_gazebo/left_hip_slide_torque_controller/command", 1);
    ros::Publisher left_ankle_roll_pub  = n.advertise<std_msgs::Float64>("/time_slider_gazebo/left_ankle_roll_torque_controller/command", 1);
    ros::Publisher left_ankle_pitch_pub = n.advertise<std_msgs::Float64>("/time_slider_gazebo/left_ankle_pitch_torque_controller/command", 1);

    ros::Publisher right_hip_pitch_pub   = n.advertise<std_msgs::Float64>("/time_slider_gazebo/right_hip_pitch_torque_controller/command", 1);
    ros::Publisher right_hip_roll_pub    = n.advertise<std_msgs::Float64>("/time_slider_gazebo/right_hip_roll_torque_controller/command", 1);
    ros::Publisher right_hip_slide_pub   = n.advertise<std_msgs::Float64>("/time_slider_gazebo/right_hip_slide_torque_controller/command", 1);
    ros::Publisher right_ankle_roll_pub  = n.advertise<std_msgs::Float64>("/time_slider_gazebo/right_ankle_roll_torque_controller/command", 1);
    ros::Publisher right_ankle_pitch_pub = n.advertise<std_msgs::Float64>("/time_slider_gazebo/right_ankle_pitch_torque_controller/command", 1);
 
    // Publish the data used for MPC
    ros::Publisher planner_input_pub = n.advertise<std_msgs::Float64MultiArray>("/time_slider_gazebo/planner_input", 'None');

    // Subscribe the Gazebo joint_states topic
    ros::Subscriber sub_joint = n.subscribe<sensor_msgs::JointState>("/time_slider_gazebo/joint_states", 1, jointStateCallback);
    ros::Subscriber sub_link = n.subscribe<gazebo_msgs::LinkStates>("/gazebo/link_states", 1, linkStateCallback);

    // Subscribe the data from the MPC planner
    ros::Subscriber sub_planner_output = n.subscribe<std_msgs::Float64MultiArray>("/time_slider_gazebo/footstep_plan", 1, footstepPlanCallback);

      // Listen to Gazebo contact sensor topic
    gazebo::transport::SubscriberPtr sub1 = node->Subscribe("~/time_slider_gazebo/left_ankle_roll_link/left_pad1_contact", forceCbL1, 1);
    gazebo::transport::SubscriberPtr sub2 = node->Subscribe("~/time_slider_gazebo/left_ankle_roll_link/left_pad2_contact", forceCbL2, 1);
    gazebo::transport::SubscriberPtr sub3 = node->Subscribe("~/time_slider_gazebo/left_ankle_roll_link/left_pad3_contact", forceCbL3, 1);
    gazebo::transport::SubscriberPtr sub4 = node->Subscribe("~/time_slider_gazebo/left_ankle_roll_link/left_pad4_contact", forceCbL4, 1);

    gazebo::transport::SubscriberPtr sub5 = node->Subscribe("~/time_slider_gazebo/right_ankle_roll_link/right_pad1_contact", forceCbR1, 1);
    gazebo::transport::SubscriberPtr sub6 = node->Subscribe("~/time_slider_gazebo/right_ankle_roll_link/right_pad2_contact", forceCbR2, 1);
    gazebo::transport::SubscriberPtr sub7 = node->Subscribe("~/time_slider_gazebo/right_ankle_roll_link/right_pad3_contact", forceCbR3, 1);
    gazebo::transport::SubscriberPtr sub8 = node->Subscribe("~/time_slider_gazebo/right_ankle_roll_link/right_pad4_contact", forceCbR4, 1);
    //********************************************************************************************
    //****************************Prepare for the planner I/O*************************************
    //********************************************************************************************
    // Linver Inverted Pendulum model
    Matrix2d A_loop_rate;
    Vector2d B_loop_rate;
    genLIPM(A_loop_rate, B_loop_rate, (1.0/LOOP_RATE));

    // Current support foot
    whichLeg = Support::L;

    // Desired first stage trajectories
    Vector3d ddx_com, dx_com, x_com, ddx_left, dx_left, x_left, ddx_right, dx_right, x_right;

    // Swing foot start position
    double right_foot_start_x = 0;
    double right_foot_start_y = 0;
    double left_foot_start_x = 0;
    double left_foot_start_y = 0;
    double right_foot_start_xVel = 0;
    double right_foot_start_yVel = 0;
    double right_foot_start_zVel = 0;
    double left_foot_start_xVel = 0;
    double left_foot_start_yVel = 0;
    double left_foot_start_zVel = 0;

    // Estimated CoM state in the x and y direction
    Vector2d x_hat, y_hat; // , z_hat

    // Next CoM state to be tracked by the OSC controller
    Vector2d x_next_state, y_next_state;

    // Next foot trajectory (pos, vel, acc) in the x, y and z direction
    Vector3d x_next_foot, y_next_foot, z_next_foot; //
    Vector3d y_next_com;

    // Initial coronal sway (take a right step first)
    double y_start = -step_width/2; // origin relative to the left foot
    double ydt_start = calc_Vel0_given_TPos0Vel1(k_LIPM, step_time/2, y_start, 0); // Initial velocity necessary to travel to apex in Tss/2
    double y_apex = calc_Pos1_given_TPos0Vel0(k_LIPM, step_time/2, y_start, ydt_start); // relative to the left foot
    y_apex = -step_width/2 - y_apex; // relative to the origin
    // cout << "ydt_start: " << ydt_start << endl;
    // cout << "y_apex: " << y_apex << endl;

    //********************************************************************************************
    //*********************************OSC initialization*****************************************
    //********************************************************************************************
    int counter = 0;  

    // Build kinematics and dynamics model from urdf
    string filePath = "/home/robin/Documents/SLIDER_timeAdaptation/src/slider_controller/data/";
    std::string filename_urdf = filePath + "slider2dof_new.urdf";
    pin::Model model;
    pin::urdf::buildModel(filename_urdf,model);
    pin::Data data(model);

    // Initialize OSC controller
    OSC OSC_controller;

    // Commanded acceleration given to OSC controller
    Vector3d ddx_com_cmd, ddx_left_cmd, ddx_right_cmd, ddx_pelvis_orientation;

    // Actual end-effector position, velocity and acceleration
    Vector3d left_pos, left_vel, left_acc, right_pos, right_vel, right_acc;

    // PD parameters, negative feedback
    double Kp_com = -70; //70
    double Kd_com = -5; //5
    double Kp_orientation = -60.0;
    double Kd_orientation = -6.0;
    double Kp_left = -70; 
    double Kd_left = -5;
    double Kp_right = -70; 
    double Kd_right = -5;

    // Parameters for regulating the base orientation 
    Quaternionf base_orientation;
    double kp_base_pitch = 50; //60
    double kd_base_pitch = 3; //5
    double left_hip_pitch_feedback = 0;
    double right_hip_pitch_feedback = 0;
//    bool isFirstStep = true;
    // QP parameters
    VectorXd w(12);

    double k = 0.75/1.414; // The ground friction coefficient is 0.75
    VectorXd torque_limit(10); // The torque limit on each motor
    torque_limit << 700, // Left hip roll
                    500, // Left hip pitch
                    600, // Left hip slide
                    50, // Left ankle pitch
                    50, // Left ankle roll
                    700, // Right hip roll
                    500, // Right hip pitch
                    600, // Right hip slide
                    50, // Right ankle pitch
                    50; // Right ankle roll


//    ddx_pelvis_orientation << 0.0, 0.0, 0.0;

    // Optimal torque 
    VectorXd traj_stage2;
    support_foot_prev = Support::L;
    // Give the initial start at first
    if (whichLeg == Support::L){ right_foot_start_x = 0.0; right_foot_start_y = -0.15; }
    if (whichLeg == Support::R){ left_foot_start_x = 0.0; left_foot_start_y = 0.15; }

    x_right << 0.0, -0.15, 0.04;
    x_left << 0.0, 0.15, 0.04;
    //***************************************************************************************
    //****************************Online Calculations****************************************
    //***************************************************************************************

    while (ros::ok())
    {   
        // cout << "\n \n \n \n \n******************New iteration " << counter << "******************" << endl;

        //***************************************************************************************
        //******************************State Estimation*****************************************
        //***************************************************************************************
        // get base orientation represented in Euler angles
        base_orientation.x() = q_base[3];
        base_orientation.y() = q_base[4];
        base_orientation.z() = q_base[5];
        base_orientation.w() = q_base[6];
        base_euler_angle = ToEulerAngles(base_orientation);
//        cout<<"THe pelvis orientation is " << base_euler_angle << endl;

        // Update joint position and velocity
//        if(whichLeg != Support::S){
//            if(support_foot_flag == -1) whichLeg = Support::L;
//            if(support_foot_flag == 1) whichLeg = Support::R;
//            if(support_foot_prev != support_foot_flag){
//                counter = 0;
//                if(support_foot_flag == Support::R){ left_foot_start_x = left_pos(0); left_foot_start_y = left_pos(1);}
//                if(support_foot_flag == Support::L){ right_foot_start_x = right_pos(0); right_foot_start_y = right_pos(1);}
//            }
//        }
        if((start == true) && (counter <= update_delay)) //whichLeg == Support::S
        {
            q << 0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0;
            v << 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0;
            base_euler_angle << 0.0, 0.0, 0.0;
            v_base << 0.0, 0.0, 0.0, 0.0, 0.0, 0.0;
            zmp_foothold << 0.0, -0.15, 0.0, 0.15; // in case left foot support
        }
        else
        {
            q << q_base, q_joint;
            v << v_base, v_joint;
        }

//        cout << "support leg is "<< whichLeg << endl;
//

        // Update date based on the current configuration of the robot
        OSC_controller.updateData(model,data,q,v,whichLeg,counter);

//         cout << "------------Left foot position------------" << endl;
        left_pos = data.oMf[model.getFrameId("left_ankle_roll_link")].translation();
//         cout << left_pos << endl;
        // cout << "------------Left foot velocity------------" << endl;
        left_vel = pin::getFrameVelocity(model,data,model.getFrameId("left_ankle_roll_link")).linear();
        // cout << left_vel << endl;
        // cout << "------------Left foot acceleration------------" << endl;
        left_acc = pin::getFrameAcceleration(model,data,model.getFrameId("left_ankle_roll_link")).linear();
        // cout << left_acc << endl;

//         cout << "------------Right foot position------------" << endl;
        right_pos = data.oMf[model.getFrameId("right_ankle_roll_link")].translation();
//         cout << right_pos << endl;
        // cout << "------------Right foot velocity------------" << endl;
        right_vel = pin::getFrameVelocity(model,data,model.getFrameId("right_ankle_roll_link")).linear();
        // cout << right_vel << endl;
        // cout << "------------Right foot acceleration------------" << endl;
        right_acc = pin::getFrameAcceleration(model,data,model.getFrameId("right_ankle_roll_link")).linear();
        // cout << right_acc << endl;

        // cout << "------------COM position------------" << endl;
        // cout << data.com[0] << endl;
        // cout << "------------COM velocity------------" << endl;
        // cout << data.vcom[0] << endl;

        x_hat << data.com[0][0], data.vcom[0][0];
        y_hat << data.com[0][1], data.vcom[0][1];
//        z_hat << data.com[0][2], data.vcom[0][2];

        // Publish data for the MPC planner, it will recreate every iteration
        std_msgs::Float64MultiArray planner_input;
        
        planner_input.data.push_back(x_hat(0));
        planner_input.data.push_back(x_hat(1));
        planner_input.data.push_back(y_hat(0));
        planner_input.data.push_back(y_hat(1));

        cout << "Current time" << current_time << endl;
        cout << "Step time" << step_time << endl;
        cout << "Step location" << zmp_foothold(0) << '\t' << zmp_foothold(1) << endl;
//        cout<<"The sum of Left Contact force is " << LcontactForce.block(0, 0, 4, 1).sum() << endl;
        cout<<"The vector of Left Contact force is " << LcontactForce << endl;
//        cout<<"The sum of Right Contact force is " << RcontactForce.block(0, 0, 4, 1).sum() << endl;
        cout<<"The vector of Right Contact force is " << RcontactForce << endl;
        cout<< "The current REAL support foot is " << whichLeg << endl;
        cout << "The command foot flag is "<< support_foot_flag << endl;
        // cout << "--------New iteration (" << whichLeg << ")----------" << endl;

        //***************************************************************************************
        //*****************************Run State Machine*****************************************
        //***************************************************************************************

        // Run state machine and the corresponding planner
        switch(whichLeg) 
        {
            case Support::S : // start motion (assuming take a right step first)

                current_time = double(counter)/LOOP_RATE;
//                cout << "current time is " << current_time << endl;

                // desired left foot trajectory
                ddx_left = Vector3d::Zero();
                dx_left = left_vel;
                x_left = left_pos;

                // desired right foot trajectory
                ddx_right = Vector3d::Zero();
                dx_right = right_vel;
                x_right = right_pos;

                if (current_time <= initial_sway_time) // origin to apex
                {
                    y_next_com = fifthOrderPolynomialInterpolation(initial_sway_time, current_time, 0, 0, 0, y_apex, 0, 0);
                    ddx_com << 0, y_next_com(2), 0;
                    dx_com << 0, y_next_com(1), 0;
                    x_com << 0, y_next_com(0), com_height;
                    planner_input.data.push_back(left_pos(0)); // current step location in the x direction
                    planner_input.data.push_back(left_pos(1)); // current step location in the y direction
                    planner_input.data.push_back(right_pos(0)); // current step location in the x direction
                    planner_input.data.push_back(right_pos(1)); // current step location in the y direction
                    planner_input.data.push_back(whichLeg); // support foot
                    planner_input.data.push_back(current_time); // support foot
                    planner_input_pub.publish(planner_input);

                    // cout << "--origin to apex--" << "y pos: " << y_hat(0) << "y vel: " << y_hat(1) << endl;
                }
                else // apex back to origin
                {
                    ddx_com << 0, (GRAVITY/com_height)*(y_hat(0)-right_pos(1)), 0;
                    y_next_state = A_loop_rate*y_hat + B_loop_rate*right_pos(1);
                    dx_com << 0, y_next_state(1), 0;
                    x_com << 0, y_next_state(0), com_height;

                    if (y_hat(0)>0.001)
                    {
                        whichLeg = Support::L;
                        counter = 0;
                        remaining_time = step_time;
                        current_time = 0.0;
                        planner_input.data.push_back(left_pos(0)); // current step location in the x direction
                        planner_input.data.push_back(left_pos(1)); // current step location in the y direction
                        planner_input.data.push_back(right_pos(0)); // current step location in the x direction
                        planner_input.data.push_back(right_pos(1)); // current step location in the y direction
                        planner_input.data.push_back(whichLeg); // support foot
                        planner_input.data.push_back(current_time); // support foot
                        planner_input_pub.publish(planner_input);
                        right_foot_start_x = right_pos(0);
                        right_foot_start_y = right_pos(1);
                        cout << "Start Motion Finished!!! Switch to left support 1111111111!!!!!!!!!!!!!!!!!!" << endl;
                    }
                    // cout << "--apex to origin--" << "y pos: " << y_hat(0) << "y vel: " << y_hat(1) << endl;
                }

                // OSC weighting parameters
                w << 1, 1, 1, // Weights on com acceleration
                     1, 1, 1, // Weights on left foot acceleration
                     1, 1, 1, // Weights on right foot acceleration
                     1, 1, 0; // Weights on pelvis orientation acceleration

                Kp_com = -90; 
                Kd_com = -9;
                Kp_orientation = -60.0;
                Kd_orientation = -6.0;
                Kp_left = -20;
                Kd_left = -2;
                Kp_right = -20;
                Kd_right = -2;
                // cout << "double support" << endl;
                // cout << "com y: " << y_hat(0) << endl;
                // cout << "vcom y: " << y_hat(1) << endl;

                break;

            case Support::L :
                current_time = double(counter)/LOOP_RATE;
                // desired left foot trajectory
                ddx_left = Vector3d::Zero();
                dx_left = left_vel;
                x_left = left_pos;

                // desired right foot trajectory
//                x_next_foot = fifthOrderPolynomialInterpolation(step_time, min(current_time,step_time), right_foot_start_x, right_foot_start_xVel, 0, zmp_foothold(0), 0, 0); //x
//                y_next_foot = fifthOrderPolynomialInterpolation(step_time, min(current_time, step_time), right_foot_start_y, right_foot_start_yVel, 0, zmp_foothold(1), 0, 0); //y
//                x_next_foot = thirdOrderPolynomialInterpolation(step_time, min(current_time,step_time), right_foot_start_x, 0, 0, zmp_foothold(0), 0, 0); //x
//                y_next_foot = thirdOrderPolynomialInterpolation(step_time, min(current_time, step_time), right_foot_start_y, 0, 0, zmp_foothold(1), 0, 0); //y
//
//                if (current_time <= (step_time/2)) // ground to peak
//                    z_next_foot = thirdOrderPolynomialInterpolation(step_time/2, min(step_time/2, current_time), foot_height, 0, 0, step_height, 0, 0); //z
//                else // peak to ground
//                    z_next_foot = thirdOrderPolynomialInterpolation(step_time/2, min((current_time - step_time/2), step_time/2), step_height, 0, 0, foot_height, 0, 0); //


//                x_next_foot = fifthOrderPolynomialInterpolation(max(1.0/LOOP_RATE, step_time - current_time), 1.0/LOOP_RATE, x_right(0), dx_right(0), ddx_right(0), zmp_foothold(0), 0, 0); //x
//                y_next_foot = fifthOrderPolynomialInterpolation(max(1.0/LOOP_RATE, step_time - current_time), 1.0/LOOP_RATE, x_right(1), dx_right(1), ddx_right(1), zmp_foothold(1), 0, 0); //y
                x_next_foot = thirdOrderPolynomialInterpolation(step_time, min(current_time, step_time), left_foot_start_x, 0, 0, zmp_foothold(0), 0, 0); //x
                y_next_foot = thirdOrderPolynomialInterpolation(step_time, min(current_time, step_time), left_foot_start_y, 0, 0, zmp_foothold(1), 0, 0); //y
//                cout << "step time is \n" << step_time << endl;
//                cout << "y next foot polynomial \n" << y_next_foot << endl;
                if (current_time <= (step_time/2)) // ground to peakx_right
//                    z_next_foot = fifthOrderPolynomialInterpolation(max(step_time/2-current_time, 1.0/LOOP_RATE), 1.0/LOOP_RATE, x_right(2), dx_right(2), ddx_right(2), step_height, 0, 0); //x
                    z_next_foot = thirdOrderPolynomialInterpolation(step_time/2, min(step_time/2, current_time), foot_height, 0, 0, step_height, 0, 0); //z
                else // peak to ground
//                    z_next_foot = fifthOrderPolynomialInterpolation(max(step_time-current_time, 1.0/LOOP_RATE), 1.0/LOOP_RATE, x_right(2), dx_right(2), ddx_right(2), foot_height, -0.05, 0); //x
                    z_next_foot = thirdOrderPolynomialInterpolation(step_time/2, min((current_time - step_time/2), step_time/2), step_height, 0, 0, foot_height, 0, 0);//
                ddx_right << x_next_foot(2), y_next_foot(2), z_next_foot(2);
                dx_right << x_next_foot(1), y_next_foot(1), z_next_foot(1);
                x_right << x_next_foot(0), y_next_foot(0), z_next_foot(0);

                // desired CoM trajectory
                ddx_com << (GRAVITY/com_height)*(x_hat(0)-left_foot_pos(0)), (GRAVITY/com_height)*(y_hat(0)-left_foot_pos(1)), 0.0;//
                x_next_state = A_loop_rate*x_hat + B_loop_rate*left_foot_pos(0);
                y_next_state = A_loop_rate*y_hat + B_loop_rate*left_foot_pos(1);
//                cout << "CoM next state is, x: " << x_next_state(0) << " y: " << y_next_state(0)<< endl;
                dx_com << x_next_state(1), y_next_state(1), 0.0;
                x_com << x_next_state(0), y_next_state(0), com_height;


//                remaining_time = step_time - current_time;
//                cout << "current_time: " << current_time << endl;
//                cout << "remaining_time: " << remaining_time << endl;
//                cout << "step_time: " << step_time << endl;
                planner_input.data.push_back(left_pos(0)); // current step location in the x direction
                planner_input.data.push_back(left_pos(1)); // current step location in the y direction
                planner_input.data.push_back(right_pos(0)); // current step location in the x direction
                planner_input.data.push_back(right_pos(1)); // current step location in the y direction
                planner_input.data.push_back(whichLeg); // support foot
                planner_input.data.push_back(current_time);
                planner_input.data.push_back(left_pos(2));
                planner_input.data.push_back(right_pos(2));
                planner_input.data.push_back(z_next_foot(0));
                planner_input.data.push_back(left_pos(0));
                planner_input.data.push_back(right_pos(0));
                planner_input.data.push_back(x_next_foot(0));
                planner_input.data.push_back(left_pos(1));
                planner_input.data.push_back(right_pos(1));
                planner_input.data.push_back(y_next_foot(0));
                planner_input.data.push_back(x_com(0));
                planner_input.data.push_back(x_com(1));
                planner_input.data.push_back(base_euler_angle(0));
                planner_input.data.push_back(base_euler_angle(1));
                planner_input.data.push_back(base_euler_angle(2));
                planner_input.data.push_back(LcontactForce.block(0, 0, 4, 1).sum()/10000.0);
                planner_input.data.push_back(RcontactForce.block(0, 0, 4, 1).sum()/10000.0);
                planner_input_pub.publish(planner_input);

                // OSC weighting parameters
                w << 1, 1, 1, // Weights on com acceleration
                     1, 1, 1, // Weights on left foot acceleration
                     1, 1, 1, // Weights on right foot acceleration
                     1, 1, 0.0; // Weights on pelvis orientation acceleration
//                Kp_com = -50.0;
//                Kd_com = -3.0;
//                Kp_orientation = -30.0;
//                Kd_orientation = -3.0;
//                Kp_right = -300.0;
//                Kd_right = -30.0;
//                Kp_left = -0.0;
//                Kd_left = -0.0;
                Kp_com = -90;
                Kd_com = -7;
                Kp_orientation = -20;
                Kd_orientation = -2;
                Kp_right = -400.0;
                Kd_right = -40.0;
                Kp_left = -30;
                Kd_left = -3;

                if (current_time >= step_time - 1.0/LOOP_RATE )// %% RcontactForce.block(0, 0, 4, 1).sum()>=1.0 && ) &&whichLeg == support_foot_flag
                {
                    whichLeg = Support::R;
                    if(start == true) start = false;

                    counter = 0;
                    current_time = double(counter)/LOOP_RATE;
                    step_time = 0.5;
//                    remaining_time = step_time - double(counter)/LOOP_RATE;
                    planner_input.data.push_back(left_pos(0)); // current step location in the x direction
                    planner_input.data.push_back(left_pos(1)); // current step location in the y direction
                    planner_input.data.push_back(right_pos(0)); // current step location in the x direction
                    planner_input.data.push_back(right_pos(1)); // current step location in the y direction
                    planner_input.data.push_back(whichLeg); // support foot
                    planner_input.data.push_back(current_time);
                    planner_input.data.push_back(left_pos(2));
                    planner_input.data.push_back(right_pos(2));
                    planner_input.data.push_back(z_next_foot(0));
                    planner_input.data.push_back(left_pos(0));
                    planner_input.data.push_back(right_pos(0));
                    planner_input.data.push_back(x_next_foot(0));
                    planner_input.data.push_back(left_pos(1));
                    planner_input.data.push_back(right_pos(1));
                    planner_input.data.push_back(y_next_foot(0));
                    planner_input.data.push_back(x_com(0));
                    planner_input.data.push_back(x_com(1));
                    planner_input.data.push_back(base_euler_angle(0));
                    planner_input.data.push_back(base_euler_angle(1));
                    planner_input.data.push_back(base_euler_angle(2));
                    planner_input.data.push_back(LcontactForce.block(0, 0, 4, 1).sum()/10000.0);
                    planner_input.data.push_back(RcontactForce.block(0, 0, 4, 1).sum()/10000.0);
                    planner_input_pub.publish(planner_input);
                    left_foot_start_x = left_pos(0);
                    left_foot_start_y = left_pos(1);
                    left_foot_start_xVel = left_vel(0);
                    left_foot_start_yVel = left_vel(1);
                    left_foot_start_zVel = left_vel(2);
                    // desired left foot trajectory
                    ddx_left = Vector3d::Zero();
                    dx_left = left_vel;
                    dx_left(2) += 0.1;
                    x_left = left_pos;
                    // in case the footstep planner hasn't updated, use future step for next step
                    zmp_foothold(0) = zmp_foothold(2);
                    zmp_foothold(1) = zmp_foothold(3);
                    cout << "reach here, swith to Right foot !!!!!!!!!!!!!!!!!!!!!" << endl;
//                    if (isFirstStep) {isFirstStep = false;}
                }
                 cout << "Remaining time" << step_time - current_time << endl;
                 cout << "Desire right foot z" << " pos: " << x_right(2) << " vel: " << dx_right(2) << endl;
//                 cout << "Actual right foot z" << " pos: " << right_pos(2) << " vel: " << right_vel(2) << endl;
//                 cout << "Desire left foot z" << " pos: " << x_left(2) << " vel: " << dx_left(2) << endl;
//                 cout << "Actual left foot z" << " pos: " << left_pos(2) << " vel: " << left_vel(2) << endl;
                // cout << "Desire right foot y" << " pos: " << x_right(1) << " vel: " << dx_right(1) << endl;
                // cout << "Actual right foot y" << " pos: " << right_pos(1) << " vel: " << right_vel(1) << endl;
//                 cout << "Planner right foot x" << " pos: " << zmp_foothold(0) << endl;
//                 cout << "Planner right foot y" << " pos: " << zmp_foothold(1) << endl;
//
//                 cout << "d right foot x: " << x_next_foot(0) << endl;
//                 cout << "a right foot x: " << right_pos(0) << endl;
//
//                 cout << "d right foot y: " << y_next_foot(0) << endl;
//                 cout << "a right foot y: " << right_pos(1) << endl;

                // cout << "d com x: " << x_next_state(0) << endl;
                // cout << "a com x: " << x_hat(0) << endl;

//                 cout << "desired com z: " << x_com(2) << endl;
                // cout << "desired vcom y: " << dx_com(1) << endl;
                // cout << "desired acom y: " << ddx_com(1) << endl;
//                 cout << "actual com z: " << z_hat(0) << endl;
                // cout << "actual vcom y: " << y_hat(1) << endl;
                // cout << "zmp y: " << zmp_foothold(1) << endl;

                // cout << "x_left: " << x_left << endl;
                // cout << "dx_left: " << dx_left << endl;
                // cout << "ddx_left: " << ddx_left << endl;

                break;

            case Support::R :
                current_time = double(counter)/LOOP_RATE;
                ddx_right = Vector3d::Zero();
                dx_right = right_vel;
                x_right = right_pos;
//                cout << "current_time: " << current_time << endl;
//                cout << "remaining_time: " << remaining_time << endl;
//                cout << "step_time: " << step_time << endl;
//                remaining_time = step_time - current_time;
                // cout << "remaining_time: " << remaining_time << endl;

                // desired left foot trajectory
//                x_next_foot = fifthOrderPolynomialInterpolation(step_time, min(current_time, step_time), left_foot_start_x, 0, 0, zmp_foothold(0), 0, 0); //x
//                y_next_foot = fifthOrderPolynomialInterpolation(step_time, min(current_time, step_time), left_foot_start_y, 0, 0, zmp_foothold(1), 0, 0); //y
//                x_next_foot = fifthOrderPolynomialInterpolation(max(1.0/LOOP_RATE, step_time - current_time), 1.0/LOOP_RATE, x_left(0), dx_left(0), ddx_left(0), zmp_foothold(0), 0, 0); //x
//                y_next_foot = fifthOrderPolynomialInterpolation(max(1.0/LOOP_RATE, step_time - current_time), 1.0/LOOP_RATE, x_left(1), dx_left(1), ddx_left(1), zmp_foothold(1), 0, 0); //y
                x_next_foot = thirdOrderPolynomialInterpolation(step_time, min(current_time, step_time), left_foot_start_x, 0, 0, zmp_foothold(0), 0, 0); //x
                y_next_foot = thirdOrderPolynomialInterpolation(step_time, min(current_time, step_time), left_foot_start_y, 0, 0, zmp_foothold(1), 0, 0); //y
//                cout << "step time is \n" << step_time << endl;
//                cout << "y next foot polynomial \n" << y_next_foot << endl;
                if (current_time <= (step_time/2)) // ground to peak
//                    z_next_foot = fifthOrderPolynomialInterpolation(max(1.0/LOOP_RATE, step_time/2-current_time), 1.0/LOOP_RATE, x_left(2), dx_left(2), ddx_left(2), step_height, 0, 0); //x
                    z_next_foot = thirdOrderPolynomialInterpolation(step_time/2, min(step_time/2, current_time), foot_height, 0, 0, step_height, 0, 0); //z
                else // peak to ground
//                    z_next_foot = fifthOrderPolynomialInterpolation(max(step_time-current_time, 1.0/LOOP_RATE), 1.0/LOOP_RATE, x_left(2), dx_left(2), ddx_left(2), foot_height, -0.05, 0); //x
                    z_next_foot = thirdOrderPolynomialInterpolation(step_time/2, min((current_time - step_time/2), step_time/2), step_height, 0, 0, foot_height, 0, 0);//
//                cout << "current pos x" << x_left(2) << endl;
//                cout << "next pos x" << z_next_foot(0) << endl;
//                cout << "current vel x" << dx_left(2) << endl;
//                cout << "next vel x" << z_next_foot(1) << endl;

                ddx_left << x_next_foot(2), y_next_foot(2), z_next_foot(2);
                dx_left << x_next_foot(1), y_next_foot(1), z_next_foot(1);
                x_left << x_next_foot(0), y_next_foot(0), z_next_foot(0);

                // desired CoM trajectory
                ddx_com << (GRAVITY/com_height)*(x_hat(0)-right_foot_pos(0)), (GRAVITY/com_height)*(y_hat(0)-right_foot_pos(1)), 0.0;
                x_next_state = A_loop_rate*x_hat + B_loop_rate*right_foot_pos(0);
                y_next_state = A_loop_rate*y_hat + B_loop_rate*right_foot_pos(1);
//                cout << "CoM next state is, x: " << x_next_state(0) << " y: " << y_next_state(0)<< endl;
                dx_com << x_next_state(1), y_next_state(1), 0.0;
                x_com << x_next_state(0), y_next_state(0), com_height;

                planner_input.data.push_back(left_pos(0)); // current step location in the x direction
                planner_input.data.push_back(left_pos(1)); // current step location in the y direction
                planner_input.data.push_back(right_pos(0)); // current step location in the x direction
                planner_input.data.push_back(right_pos(1)); // current step location in the y direction
                planner_input.data.push_back(whichLeg); // support foot
                planner_input.data.push_back(current_time);
                planner_input.data.push_back(left_pos(2));
                planner_input.data.push_back(right_pos(2));
                planner_input.data.push_back(z_next_foot(0));
                planner_input.data.push_back(left_pos(0));
                planner_input.data.push_back(right_pos(0));
                planner_input.data.push_back(x_next_foot(0));
                planner_input.data.push_back(left_pos(1));
                planner_input.data.push_back(right_pos(1));
                planner_input.data.push_back(y_next_foot(0));
                planner_input.data.push_back(x_com(0));
                planner_input.data.push_back(x_com(1));
                planner_input.data.push_back(base_euler_angle(0));
                planner_input.data.push_back(base_euler_angle(1));
                planner_input.data.push_back(base_euler_angle(2));
                planner_input.data.push_back(LcontactForce.block(0, 0, 4, 1).sum()/10000.0);
                planner_input.data.push_back(RcontactForce.block(0, 0, 4, 1).sum()/10000.0);
                planner_input_pub.publish(planner_input);

                // OSC weighting parameters
                w << 1, 1, 1, // Weights on com acceleration
                     1, 1, 1, // Weights on left foot acceleration
                     1, 1, 1, // Weights on right foot acceleration
                     1, 1, 0.0; // Weights on pelvis orientation acceleration
//                Kp_com = -50.0;
//                Kd_com = -3.0;
//                Kp_orientation = -30.0;
//                Kd_orientation = -3.0;
//                Kp_left = -300.0;
//                Kd_left = -30.0;
//                Kp_right = -0.0;
//                Kd_right = -0.0;
                Kp_com = -90;
                Kd_com = -7;
                Kp_orientation = -20;
                Kd_orientation = -2;
                Kp_right = -400.0;
                Kd_right = -40.0;
                Kp_left = -30;
                Kd_left = -3;

                if (current_time >= step_time- 1.0/LOOP_RATE ) //&& LcontactForce.block(0, 0, 4, 1).sum()>=1.0 && whichLeg == support_foot_flag
                {
                    whichLeg = Support::L;
                    counter = 0;
                    current_time = double(counter)/LOOP_RATE;
                    step_time = 0.5;
//                    remaining_time = step_time;
                    planner_input.data.push_back(left_pos(0)); // current step location in the x direction
                    planner_input.data.push_back(left_pos(1)); // current step location in the y direction
                    planner_input.data.push_back(right_pos(0)); // current step location in the x direction
                    planner_input.data.push_back(right_pos(1)); // current step location in the y direction
                    planner_input.data.push_back(whichLeg); // support foot
                    planner_input.data.push_back(current_time);
                    planner_input.data.push_back(left_pos(2));
                    planner_input.data.push_back(right_pos(2));
                    planner_input.data.push_back(z_next_foot(0));
                    planner_input.data.push_back(left_pos(0));
                    planner_input.data.push_back(right_pos(0));
                    planner_input.data.push_back(x_next_foot(0));
                    planner_input.data.push_back(left_pos(1));
                    planner_input.data.push_back(right_pos(1));
                    planner_input.data.push_back(y_next_foot(0));
                    planner_input.data.push_back(x_com(0));
                    planner_input.data.push_back(x_com(1));
                    planner_input.data.push_back(base_euler_angle(0));
                    planner_input.data.push_back(base_euler_angle(1));
                    planner_input.data.push_back(base_euler_angle(2));
                    planner_input.data.push_back(LcontactForce.block(0, 0, 4, 1).sum()/10000.0);
                    planner_input.data.push_back(RcontactForce.block(0, 0, 4, 1).sum()/10000.0);
                    planner_input_pub.publish(planner_input);
                    right_foot_start_x = right_pos(0);
                    right_foot_start_y = right_pos(1);
                    right_foot_start_xVel = right_vel(0);
                    right_foot_start_yVel = right_vel(1);
                    right_foot_start_zVel = right_vel(2);
                    // desired right foot trajectory
                    ddx_right = Vector3d::Zero();
                    dx_right = right_vel;
                    dx_right(2) += 0.1;
                    x_right = right_pos;
                    // in case the footstep planner hasn't updated, use future step for next step
                    zmp_foothold(0) = zmp_foothold(2);
                    zmp_foothold(1) = zmp_foothold(3);
                    cout << "reach here, swith to left foot !!!!!!!!!!!!!!!!!!!!!" << endl;
                }
                 cout << "Remaining time" << step_time - current_time << endl;
                 cout << "Desire left foot z" << " pos: " << x_left(2) << " vel: " << dx_left(2) << endl;
//                 cout << "Actual left foot z" << " pos: " << left_pos(2) << " vel: " << left_vel(2) << endl;
//                 cout << "Desire right foot z" << " pos: " << x_right(2) << " vel: " << dx_right(2) << endl;
//                 cout << "Actual right foot z" << " pos: " << right_pos(2) << " vel: " << right_vel(2) << endl;
//                 cout << "desired com z: " << x_com(2) << endl;
//                 cout << "actual com z: " << z_hat(0) << endl;
                // cout << "Desire left foot y" << " pos: " << x_left(1) << " vel: " << dx_left(1) << endl;
                // cout << "Actual left foot y" << " pos: " << left_pos(1) << " vel: " << left_vel(1) << endl;
                // cout << "Planne left foot y" << " pos: " << zmp_foothold(3) << endl;
//                 cout << "Planner left foot x" << " pos: " << zmp_foothold(0) << endl;
//                 cout << "Planner left foot y" << " pos: " << zmp_foothold(1) << endl;
//                 cout << "Desire left foot z" << " pos: " << x_left(2) << " vel: " << dx_left(2) << endl;
//                 cout << "Actual left foot z" << " pos: " << left_pos(2) << " vel: " << left_vel(2) << endl;
//                 cout << "d left foot x: " << x_next_foot(0) << endl;
//                 cout << "a left foot x: " << left_pos(0) << endl;
//
//                 cout << "d left foot y: " << y_next_foot(0) << endl;
//                 cout << "a left foot y: " << left_pos(1) << endl;
                break;

            case Support::D : // double support (not implemented yet)            
                
                break;
        }

        //***************************************************************************************
        //*****************************Run OSC controller****************************************
        //***************************************************************************************

        // PD and feedforward controller
        ddx_com_cmd = Kp_com*(data.com[0] - x_com) + Kd_com*(data.vcom[0] - dx_com) + ddx_com;
        ddx_pelvis_orientation = Kp_orientation*(base_euler_angle) + Kd_orientation*(v_base.segment(3,3));
        ddx_left_cmd = Kp_left*(left_pos - x_left) + Kd_left*(left_vel - dx_left) + ddx_left;
        ddx_right_cmd = Kp_right*(right_pos - x_right) + Kd_right*(right_vel - dx_right) + ddx_right;

//         cout << "******************************" << endl;
//         cout << "COM position difference: \n" << data.com[0] - x_com << endl;
//         cout << data.com[0] << endl;
//         cout << x_com << endl;
//         cout << "******************************" << endl;
//         cout << "COM velocity difference: \n" << data.vcom[0] - dx_com << endl;
//         cout<< data.vcom[0] << endl;
//         cout << dx_com << endl;
//         cout << "******************************" << endl;
//         cout << "Left foot position difference: \n" << left_pos - x_left << endl;
//          cout << left_pos << endl;
//         cout << x_left << endl;
//         cout << "******************************" << endl;
//         cout << "Left foot velocity difference: \n" << left_vel - dx_left << endl;
//          cout<< left_vel << endl;
//         cout << dx_left << endl;
//         cout << "******************************" << endl;
//         cout << "Right foot position difference: \n" << right_pos - x_right << endl;
//          cout<< right_pos << endl;
//         cout << x_right << endl;
//         cout << "******************************" << endl;
//         cout << "Right foot velocity difference: \n" << right_vel - dx_right << endl;
//          cout<< right_vel << endl;
//         cout << dx_right << endl;
//         cout << "******************************" << endl;
//

        // Solve for the optimal torque
        traj_stage2 = OSC_controller.solveQP(ddx_com_cmd, ddx_left_cmd, ddx_right_cmd, ddx_pelvis_orientation, w, k, torque_limit, counter);

        // Code for measuring time
        // auto t1 = std::chrono::high_resolution_clock::now();
        // auto t2 = std::chrono::high_resolution_clock::now();
        // auto duration = std::chrono::duration_cast<std::chrono::microseconds>( t2 - t1 ).count();
        // if (duration > 1000)
        //     cout << "Duration: \n" << duration << " microseconds" << endl;


        left_hip_roll_pub.publish(getROSmsg(traj_stage2[0]));
        left_hip_pitch_pub.publish(getROSmsg(traj_stage2[1])); // + left_hip_pitch_feedback
        left_hip_slide_pub.publish(getROSmsg(traj_stage2[2]));
        left_ankle_pitch_pub.publish(getROSmsg(traj_stage2[3]));
        left_ankle_roll_pub.publish(getROSmsg(traj_stage2[4]));
        
        right_hip_roll_pub.publish(getROSmsg(traj_stage2[5]));
        right_hip_pitch_pub.publish(getROSmsg(traj_stage2[6])); //  + right_hip_pitch_feedback
        right_hip_slide_pub.publish(getROSmsg(traj_stage2[7]));
        right_ankle_pitch_pub.publish(getROSmsg(traj_stage2[8]));
        right_ankle_roll_pub.publish(getROSmsg(traj_stage2[9]));

        counter++;
//        cout<<"counter is " << counter << endl;
        support_foot_prev = support_foot_flag;

        ros::spinOnce();

        loop_rate.sleep();
    }

	return 0;
}
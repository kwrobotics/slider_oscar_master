#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 11 19:34:19 2021

@author: robin
"""
import rospy
import tf
import numpy as np

from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point, Pose, Quaternion, Twist, Vector3

if __name__ == '__main__':
    
    
    rospy.init_node('fake_odom_publisher', anonymous = True)

#    odom_publisher=rospy.Publisher("odom", Odometry, queue_size = 10)
    rate=100
    r = rospy.Rate(rate)

    x = 0
    time = 0
    br = tf.TransformBroadcaster()

    while not rospy.is_shutdown():

       # x = np.sin(time)
#        Odometry_to_publish = Odometry()
#        Odometry_to_publish.pose.pose.position.x = 0
#        Odometry_to_publish.pose.pose.position.y = 0
#        Odometry_to_publish.pose.pose.position.z = 0.7
#        
#        
#        Odometry_to_publish.pose.pose.orientation.x = 0
#        Odometry_to_publish.pose.pose.orientation.y = 0
#        Odometry_to_publish.pose.pose.orientation.z = 0
#        Odometry_to_publish.pose.pose.orientation.w = 1
#        
#        Odometry_to_publish.twist.twist = Twist(Vector3(0, 0, 0), Vector3(0, 0, 0))
#        
#        odom_publisher.publish(Odometry_to_publish)
        
        br.sendTransform((0, 0, 0),
                         tf.transformations.quaternion_from_euler(0, 0.523,0 ),
                        rospy.Time.now(),
                        'camera_link',
                        "world")
        
   
        print(rospy.get_rostime())
       # r.sleep()
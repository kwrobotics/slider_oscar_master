#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 10 16:49:15 2021

@author: robin
"""

import rospy
import tf 

from nav_msgs.msg import Odometry


def callback(data):

    position = data.pose.pose.position
    orientation = data.pose.pose.orientation

    tf_publisher = tf.TransformBroadcaster()
    tf_publisher.sendTransform((position.x, position.y, position.z),
                     (orientation.x, orientation.y, orientation.z, orientation.w),
                     rospy.Time.now(),
                     "world",
                     "odom")
    
    

def odom_listener():
    rospy.init_node('odom_listener', anonymous=True)

    rospy.Subscriber("odom", Odometry, callback)

    rospy.spin()



if __name__ == '__main__':
    odom_listener()

"""
CyberGlove.py
Python module to encapsulate the CyberGlove into a class

Digby Chappell
October 2019
"""

import numpy as np
import tensorflow as tf
import cv2


class CyberGlove:
    """ CyberGlove class"""

    def __init__(self, comport, serial_no):
        """ Connect to the CyberGlove over serial, given comport and serial_no"""


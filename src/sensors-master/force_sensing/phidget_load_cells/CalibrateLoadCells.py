"""
CalibrateLoadCells.py
To write calibration data to a csv file for future use.

Digby Chappell
October 2019
"""

<<<<<<< HEAD
from force_sensing.phidget_load_cells.LoadCells import LoadCell
=======
from LoadCells import LoadCell
>>>>>>> 8c3e79415cebc2f57cbe51bb1801a47d575bc76e
import pandas as pd
import numpy as np


if __name__ == "__main__":
    cells = []
    for i in range(4):
        # 472711 or 494011
        cells.append(LoadCell(494011, i))
    serial_number = cells[0].serial_number
    [cell.start() for cell in cells]

    cal1 = np.zeros((4))     # calibration point 1
    cal2 = np.zeros((4))     # calibration point 2

    cal_weight_1 = float(input("Please enter first calibration weight (N): \n"))
    for i in range(4):
        input("Press enter when first calibration weight is on load cell %s\n" % i)
        cal1[i] = cells[i].output
    print(cal1)

    cal_weight_2 = float(input("Please enter second calibration weight (N): \n"))
    for i in range(4):
        input("Press enter when second calibration weight is on load cell %s\n" % i)
        cal2[i] = cells[i].output
    print(cal2)

    print("Thank you, the calculated calibration values are: \n")
    scale_vals = (cal_weight_2 - cal_weight_1)/(cal2 - cal1)
    offset_vals = cal_weight_1 - cal1*scale_vals

    print("Scale factors: ", scale_vals)
    print("Offsets: ", offset_vals)

    df = pd.DataFrame({'serial_no': [serial_number],
                       '0_scale_factor': [scale_vals[0]],
                       '0_offset': [offset_vals[0]],
                       '1_scale_factor': [scale_vals[1]],
                       '1_offset': [offset_vals[1]],
                       '2_scale_factor': [scale_vals[2]],
                       '2_offset': [offset_vals[2]],
                       '3_scale_factor': [scale_vals[3]],
                       '3_offset': [offset_vals[3]]})
    print(df)
    # df.to_csv('calibrated_cells.csv', mode='a', index=False, header=False)

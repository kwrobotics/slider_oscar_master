"""
LoadCells.py
Class to contain and handle load cells read with a PhidgetBridge 4-Input

Digby Chappell
October 2019
"""

from Phidget22.Devices.VoltageRatioInput import *
import pandas as pd


class LoadCell:
    """ Main load cell class, requires serial number - found on phidget control panel,
	and channel - int between 0 and 3 """
    def __init__(self, serial_number, channel):
        """ Sets device channel, etc """
        self.serial_number = serial_number
        self.channel = channel

        self.output = 0
        self.calibrated = False
        self.scale_factor = 1.0
        self.offset = 0

        self.voltage_ratio_input = VoltageRatioInput()
        self.voltage_ratio_input.setDeviceSerialNumber(serial_number)
        self.voltage_ratio_input.setChannel(channel)

        self.get_calibration_data()
        self.voltage_ratio_input.setOnVoltageRatioChangeHandler(self.on_change)

    def start(self):
        """ Opens and attaches device, sets data interval to 8ms (shortest)
         sets change trigger to 0.0 (constant data streaming)"""
        self.voltage_ratio_input.openWaitForAttachment(5000)
        self.voltage_ratio_input.setDataInterval(8)
        self.voltage_ratio_input.setVoltageRatioChangeTrigger(0.0)

    def get_calibration_data(self):
        """ loads calibration data from list of calibrated load cells """
        calibration_data = pd.read_csv('calibrated_cells.csv',
                                       index_col=0)
        scale_factor_string = str(self.channel) + "_scale_factor"
        offset_string = str(self.channel) + "_offset"
        if self.serial_number in calibration_data.index:
            self.calibrated = True
            self.scale_factor = calibration_data[scale_factor_string].loc[self.serial_number]
            self.offset = calibration_data[offset_string].loc[self.serial_number]

    def on_change(self, _, voltage_ratio):
        """ if device is calibrated, return output in N, else return raw data """
        if not self.calibrated:
            self.output = voltage_ratio
        else:
            self.output = voltage_ratio*self.scale_factor + self.offset

    def stop(self):
        """ close connection and stop """
        self.voltage_ratio_input.close()


if __name__ == "__main__":
    lcells = []
    rcells = []
    for i in range(4):
        lcells.append(LoadCell(472711, i))
        rcells.append(LoadCell(494011, i))
    [cell.start() for cell in lcells]
    [cell.start() for cell in rcells]

    running = True
    while running:
        print("Left foot", [cell.output for cell in lcells])
        print("Right foot", [cell.output for cell in rcells])

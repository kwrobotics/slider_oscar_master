"""
LoadCellPublisher.py
Publishes calibrated load cell forces on individual ROS topics

October 2019
Digby Chappell
"""

from force_sensing.phidget_load_cells.LoadCells import LoadCell
import rospy
from std_msgs.msg import Float64


def talker():
    # example shown here is used to publish contact forces for each of 
    # the SLIDER feet

    # connect to each set of 4 load cells
    lcells = []
    rcells = []
    for i in range(4):
        lcells.append(LoadCell(472711, i))
        rcells.append(LoadCell(494011, i))
    [cell.start() for cell in lcells]
    [cell.start() for cell in rcells]
    
    # initialise each individual publisher
    lpubs = []
    rpubs = []
    for i in range(4):    # Left foot forces
        lpubs.append(rospy.Publisher('force CbL' + str(i), Float64, queue_size=10))
    for i in range(4):    # Right foot forces
        rpubs.append(rospy.Publisher('force CbR' + str(i), Float64, queue_size=10))

    rospy.init_node('talker', anonymous=True)
    rate = rospy.Rate(125)    # 125hz (8ms)

    while not rospy.is_shutdown():
	# publish left foot contact forces
	for cell, pub in zip(lcells, lpubs):
	    contact_force = cell.output
	    pub.publish(contact_force)
	    rospy.loginfo(contact_force)

	# publish right foot contact forces
    for cell, pub in zip(rcells, rpubs):
	    contact_force = cell.output
	    pub.publish(contact_force)
	    rospy.loginfo(contact_force)
    rate.sleep()


if __name__ == '__main__':
    try:
	    talker()
    except rospy.ROSInterruptException:
	    pass

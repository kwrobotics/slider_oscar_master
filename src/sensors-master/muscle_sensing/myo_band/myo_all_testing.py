# The MIT License (MIT)
#
# Copyright (c) 2017 Niklas Rosenstein
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.
"""
This example displays the orientation, pose and RSSI as well as EMG data
if it is enabled and whether the device is locked or unlocked in the
terminal.
Enable EMG streaming with double tap and disable it with finger spread.
"""

from __future__ import print_function
from myo.utils import TimeInterval
from pyquaternion import Quaternion
import numpy as np
import matplotlib.pyplot as plt
import myo
import sys


class Listener(myo.DeviceListener):

    def __init__(self):
        # self.interval = TimeInterval(None, 0.05)
        self.orientation = None
        self.acceleration = None
        self.gyroscope = None
        self.emg = None
        self.rssi = None
        self.n = 512
        self.orientation_traces = [np.zeros(self.n) for _ in range(4)]
        self.acceleration_traces = [np.zeros(self.n) for _ in range(3)]
        self.gyroscope_traces = [np.zeros(self.n) for _ in range(3)]
        self.emg_traces = [np.zeros(self.n) for _ in range(8)]
        self.fig = plt.figure()
        self.axes = [self.fig.add_subplot(4, 1, i+1) for i in range(4)]
        [(ax.set_ylim([-1, 1])) for ax in self.axes[:3]]
        (self.axes[3].set_ylim([-100, 100]))
        self.ographs = [self.axes[0].plot(np.arange(self.n), trace) for trace in self.orientation_traces]
        self.agraphs = [self.axes[1].plot(np.arange(self.n), trace) for trace in self.acceleration_traces]
        self.ggraphs = [self.axes[2].plot(np.arange(self.n), trace) for trace in self.gyroscope_traces]
        self.egraphs = [self.axes[3].plot(np.arange(self.n), trace) for trace in self.emg_traces]
        plt.ion()
        self.first = True

    def output(self):
        data = []
        if self.orientation:

            self.orientation_traces = [np.roll(trace, -1) for trace in self.orientation_traces]
            for i, comp in enumerate(self.orientation):
                self.orientation_traces[i][-1] = comp
                data.append(comp)
            for g, tr in zip(self.ographs, self.orientation_traces):
                g[0].set_ydata(tr)

            self.acceleration_traces = [np.roll(trace, -1) for trace in self.acceleration_traces]
            for i, comp in enumerate(self.acceleration):
                self.acceleration_traces[i][-1] = comp
                data.append(comp)
            for g, tr in zip(self.agraphs, self.acceleration_traces):
                g[0].set_ydata(tr)

        if self.emg:
            self.emg_traces = [np.roll(trace, -1) for trace in self.emg_traces]
            for i, comp in enumerate(self.emg):
                self.emg_traces[i][-1] = comp
                data.append(comp)
            for g, tr in zip(self.egraphs, self.emg_traces):
                g[0].set_ydata(tr)

        plt.draw()
        if self.first:
            plt.pause(1.0 / 1000)

    def on_connected(self, event):
        event.device.request_rssi()
        event.device.stream_emg(True)

    def on_rssi(self, event):
        self.rssi = event.rssi
        self.output()

    def on_orientation(self, event):
        self.orientation = event.orientation
        self.acceleration = event.acceleration
        self.output()

    def on_emg(self, event):
        self.emg = event.emg
        self.output()


if __name__ == '__main__':
    myo.init('C:\\Program Files (x86)\\Thalmic Labs\\myo-sdk-win-0.9.0\\bin\\myo64.dll')
    hub = myo.Hub()
    listener = Listener()
    while hub.run(listener.on_event, 500):
        pass

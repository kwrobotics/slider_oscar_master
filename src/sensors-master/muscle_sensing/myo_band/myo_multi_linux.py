"""
myo_multi_linux.py
A file to process multiple Myo armbands on Linux at a higher level

November 2019
Digby Chappell
"""


if __name__ == '__main__':
    m = MyoRaw(sys.argv[1] if len(sys.argv) >= 2 else None)

    def proc_emg(emg, moving, times=[]):
        print(m.name, emg)

        ## print framerate of received data
        times.append(time.time())
        if len(times) > 20:
            #print((len(times) - 1) / (times[-1] - times[0]))
            times.pop(0)

    m.add_emg_handler(proc_emg)
    m.connect('Robin Lab Myo')
    m.connect('Robin Lab Myo 2')

    try:
        while True:
            m.run(1)

    except KeyboardInterrupt:
        pass
    finally:
        m.disconnect()
        print()

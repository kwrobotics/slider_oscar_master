

# The MIT License (MIT)
#
# Copyright (c) 2017 Niklas Rosenstein
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to
# deal in the Software without restriction, including without limitation the
# rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
# sell copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
# IN THE SOFTWARE.
# libmyo.init('C:\\Program Files (x86)\\Thalmic Labs\\myo-sdk-win-0.9.0\\bin\\myo64.dll')

from matplotlib import pyplot as plt
from collections import deque
from threading import Lock, Thread

import myo
import numpy as np


class EmgCollector(myo.DeviceListener):
  """
  Collects EMG data in a queue with *n* maximum number of elements.
  """

  def __init__(self, n):
    self.n = n
    self.device_handles = []
    self.lock = Lock()
    self.emg_data_queue = [deque(maxlen=n), deque(maxlen=n)]
    self.orientation_data_queue = [deque(maxlen=n), deque(maxlen=n)]

  def get_emg_data(self):
    with self.lock:
      return list(self.emg_data_queue)

  def get_orientation_data(self):
    with self.lock:
      return list(self.orientation_data_queue)

  def on_paired(self, event):
    event.device.stream_emg(True)
    self.device_handles.append(event.device.handle)

  def on_emg(self, event):
    with self.lock:
      if event.device.handle == self.device_handles[0]:
        self.emg_data_queue[0].append((event.timestamp, event.emg))
      else:
        self.emg_data_queue[1].append((event.timestamp, event.emg))

  def on_orientation(self, event):
    with self.lock:
      if event.device.handle == self.device_handles[0]:
        self.orientation_data_queue[0].append((event.timestamp, list(event.orientation)))
      else:
        self.orientation_data_queue[1].append((event.timestamp, list(event.orientation)))


class Plot(object):

  def __init__(self, listener):
    self.n = listener.n
    self.listener = listener
    self.fig = plt.figure()
    self.axes = [self.fig.add_subplot(12, 1, i+1) for i in range(12)]
    [(ax.set_ylim([-100, 100])) for ax in self.axes[:8]]
    [(ax.set_ylim([-1, 1])) for ax in self.axes[8:]]
    self.egraphs0 = [ax.plot(np.arange(self.n), np.zeros(self.n))[0] for ax in self.axes[:8]]
    self.egraphs1 = [ax.plot(np.arange(self.n), np.zeros(self.n))[0] for ax in self.axes[:8]]
    self.ographs0 = [ax.plot(np.arange(self.n), np.zeros(self.n))[0] for ax in self.axes[8:]]
    self.ographs1 = [ax.plot(np.arange(self.n), np.zeros(self.n))[0] for ax in self.axes[8:]]
    plt.ion()

  def update_plot(self):
    emg_data = self.listener.get_emg_data()
    orientation_data = self.listener.get_orientation_data()
    emg_data0 = np.array([x[1] for x in emg_data[0]]).T
    emg_data1 = np.array([x[1] for x in emg_data[1]]).T
    orientation_data0 = np.array([x[1] for x in orientation_data[0]]).T
    orientation_data1 = np.array([x[1] for x in orientation_data[1]]).T
    for g, data in zip(self.egraphs0, emg_data0):
      if len(data) < self.n:
        # Fill the left side with zeroes.
        data = np.concatenate([np.zeros(self.n - len(data)), data])
      g.set_ydata(data)
    for g, data in zip(self.egraphs1, emg_data1):
      if len(data) < self.n:
        # Fill the left side with zeroes.
        data = np.concatenate([np.zeros(self.n - len(data)), data])
      g.set_ydata(data)
    for g, data in zip(self.ographs0, orientation_data0):
      if len(data) < self.n:
        # Fill the left side with zeroes.
        data = np.concatenate([np.zeros(self.n - len(data)), data])
      g.set_ydata(data)
    for g, data in zip(self.ographs1, orientation_data1):
      if len(data) < self.n:
        # Fill the left side with zeroes.
        data = np.concatenate([np.zeros(self.n - len(data)), data])
      g.set_ydata(data)
    plt.draw()

  def main(self):
    while True:
      self.update_plot()
      plt.pause(1.0 / 30)


def main():
  myo.init('C:\\Program Files (x86)\\Thalmic Labs\\myo-sdk-win-0.9.0\\bin\\myo64.dll')
  hub = myo.Hub()
  listener = EmgCollector(512)
  with hub.run_in_background(listener.on_event):
    Plot(listener).main()


if __name__ == '__main__':
  main()
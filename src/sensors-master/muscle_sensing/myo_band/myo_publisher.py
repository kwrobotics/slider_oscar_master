#!/usr/bin/env python3

"""
myo_publisher.py
Publishes myoband data to ROS

November 2019
Digby Chappell
"""


from myo_linux import *

import rospy
from geometry_msgs.msg import PoseStamped


def talker(trackers):
    pubs = []
    for i in range(len(trackers)):  # trackers
        pubs.append(rospy.Publisher('tracker' + str(i+1), PoseStamped, queue_size=10))
    rospy.init_node('vive_trackers', anonymous=True)
    rate = rospy.Rate(250)  # 250hz (4ms)
    while True:
        try:
            for tracker, pub in zip(trackers, pubs):
                tracker.get_pose_quaternion()
                pub.publish(tracker.pose)
                rospy.loginfo(tracker.pose)
            rate.sleep()
        except rospy.ROSInterruptException:
            pass


if __name__ == "__main__":

    m = MyoRaw(sys.argv[1] if len(sys.argv) >= 2 else None)

    m.add_emg_handler(process_emg)
    m.add_pose_handler(process_pose)
    m.connect()

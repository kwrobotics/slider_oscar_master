import rospy
import socket
from geometry_msgs.msg import PoseStamped

# ROS parameters:
rospy.init_node("trackers")
body_pub = rospy.Publisher("/slider/body_tracker", PoseStamped, queue_size=0)
left_pub = rospy.Publisher("/slider/left_tracker", PoseStamped, queue_size=0)
right_pub = rospy.Publisher("/slider/right_tracker", PoseStamped, queue_size=0)
body_pose = PoseStamped()
left_pose = PoseStamped()
right_pose = PoseStamped()

# TCP parameters:
TCP_IP = '192.168.0.103'
TCP_PORT_1 = 5005
TCP_PORT_2 = 5006
TCP_PORT_3 = 5007
BUFFER_SIZE = 1024


def publish_and_receive_data(socket, message, publisher):
    data = socket.recv(BUFFER_SIZE)
    data = filter(None, list(data[data.find('[') + 1:data.find(']') - 1].split(" ")))
    # print "received data:", data
    if len(data) == 7:
        message.header.stamp = rospy.Time.now()
        message.header.frame_id = "my_frame"
        message.pose.position.x = float(data[0])
        message.pose.position.y = float(data[1])
        message.pose.position.z = float(data[2])
        message.pose.orientation.x = float(data[3])
        message.pose.orientation.y = float(data[4])
        message.pose.orientation.z = float(data[5])
        message.pose.orientation.w = float(data[6])
        publisher.publish(message)


if __name__=="__main__":
    s1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s2 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s3 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    s1.connect((TCP_IP, TCP_PORT_1))
    s2.connect((TCP_IP, TCP_PORT_2))
    s3.connect((TCP_IP, TCP_PORT_3))

    while True:
        publish_and_receive_data(s1, body_pose, body_pub)
        publish_and_receive_data(s2, left_pose, left_pub)
        publish_and_receive_data(s3, right_pose, right_pub)

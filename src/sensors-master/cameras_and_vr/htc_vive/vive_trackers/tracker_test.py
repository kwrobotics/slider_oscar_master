import triad_openvr
import time
import sys

v = triad_openvr.triad_openvr()
# v.print_discovered_objects()

if len(sys.argv) == 1:
    interval = 1/250
elif len(sys.argv) == 2:
    interval = 1/float(sys.argv[1])
else:
    print("Invalid number of arguments")
    interval = False
    
if interval:
    while(True):
        start = time.time()
        txt1 = ""
        for each in v.devices["tracker_1"].get_pose_euler():
            txt1 += "%.4f" % each
            txt1 += " "
        txt2 = ""
        for each in v.devices["tracker_2"].get_pose_euler():
            txt2 += "%.4f" % each
            txt2 += " "
        txt3 = ""
        for each in v.devices["tracker_3"].get_pose_euler():
            txt3 += "%.4f" % each
            txt3 += " "
        # print("\r" + txt1 + "\n" + txt2 + "\n" + txt3, end=" ")
        sleep_time = interval-(time.time()-start)
        if sleep_time > 0:
            time.sleep(sleep_time)

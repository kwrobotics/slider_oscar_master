"""
ViveTrackers.py
Contains class to interface with HTC Vive Trackers using openvr are triad_openvr wrapper#

Digby Chappell
October 2019
"""

#!/usr/bin/env python3

from geometry_msgs.msg import PoseStamped
import triad_openvr as vr
import rospy
import sys


class ViveTracker:
    """ class to contain HTC Vive trackers """

    def __init__(self, vr, tracker_number):
        """ initialise tracker, note that tracker numbers start at 1 """
        self.name = "tracker_" + str(tracker_number)
        self.pose = False
        if len(sys.argv) == 1:
            self.interval = 1 / 250
        elif len(sys.argv) == 2:
            self.interval = 1 / float(sys.argv[1])
        else:
            print("Invalid number of arguments")
            self.interval = False
        self.tracker = vr.devices[self.name]

    def get_pose_euler(self):
        """ get the pose as xyz and euler angles"""
        self.pose = self.tracker.get_pose_euler()

    def get_pose_quaternion(self):
        """ get the pose as xyz and quaternion"""
        p = PoseStamped()

        # Make sure the quaternion is valid and normalized
        pose_quaternion = self.tracker.get_pose_quaternion()
        p.header.stamp = rospy.Time.now()
        p.header.frame_id = "map"
        p.pose.position.x, p.pose.position.y, p.pose.position.z, p.pose.orientation.w, \
        p.pose.orientation.x, p.pose.orientation.y, p.pose.orientation.z = pose_quaternion
        self.pose = p


if __name__=="__main__":
    v = vr.triad_openvr()
    trackers = [ViveTracker(v, i+1) for i in range(2)]
    for tracker in trackers:
        tracker.get_pose_euler()

"""
ViveTrackers.py
Contains class to interface with HTC Vive Trackers using openvr are triad_openvr wrapper#

Digby Chappell
October 2019
"""

import sys


class ViveTracker:
    """ class to contain HTC Vive trackers """

    def __init__(self, vr, tracker_number):
        """ initialise tracker, note that tracker numbers start at 1 """
        self.name = "tracker_" + str(tracker_number)
        self.pose = False
        if len(sys.argv) == 1:
            self.interval = 1 / 250
        elif len(sys.argv) == 2:
            self.interval = 1 / float(sys.argv[1])
        else:
            print("Invalid number of arguments")
            self.interval = False
        self.tracker = vr.devices[self.name]

    def get_pose_euler(self):
        """ get the pose as xyz and euler angles"""
        self.pose = self.tracker.get_pose_euler()
        return self.pose

    def get_pose_quaternion(self):
        """ get the pose as xyz and quaternion"""
        self.pose = self.tracker.get_pose_quaternion()
        return self.pose

    def get_twist(self):
        """ get the twist of the vive tracker"""
        self.velocity = self.tracker.get_velocity()
        self.angular_velocity = self.tracker.get_angular_velocity()
        return self.velocity, self.angular_velocity

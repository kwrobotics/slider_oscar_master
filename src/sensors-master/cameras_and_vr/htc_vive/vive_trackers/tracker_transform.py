#!/usr/bin/env python
import roslib
roslib.load_manifest('learning_tf')
import rospy
import tf


def set_world(trackername):
    br = tf.TransformBroadcaster()
    br.sendTransform((0, 0, 0),
                     (0, 0, 0, 1),
                     rospy.Time.now(),
                     turtlename,
                     "vive_trackers")


if __name__ == '__main__':
    rospy.init_node('vive_trackers')
    turtlename = rospy.get_param('~turtle')
    rospy.Subscriber('/%s/pose' % turtlename,
                     turtlesim.msg.Pose,
                     handle_turtle_pose,
                     turtlename)
    rospy.spin()
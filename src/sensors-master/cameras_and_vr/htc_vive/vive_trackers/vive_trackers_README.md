# HTC Vive Trackers

How to set up the HTC Vive Trackers:
1. Set up the HTC Vive with Headset and follow the room set up steps:
https://dgit.com/htc-vive-setup-guide-everything-you-need-to-know-55001/
2. Install openvr for python and triad openvr wrapper, alter some default.vrsettings
files to be able to run without a headset:
http://help.triadsemi.com/en/articles/836917-steamvr-tracking-without-an-hmd
2.b If on Linux, try these steps if it isn't working:
https://github.com/cmbruns/pyopenvr/issues/30#issuecomment-302961948
3. Run the tracker_test.py to check everything works. Note the 
print_discovered_objects() line is commented out because triad openvr has a
problem with null objects.

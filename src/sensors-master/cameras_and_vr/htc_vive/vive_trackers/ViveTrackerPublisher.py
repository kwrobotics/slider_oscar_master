#!/usr/bin/env python3
"""
ViveTrackerPublisher.py
Publishes each tracker detected to a new rostopic

October 2019
Digby Chappell
"""


from ViveTrackers import ViveTracker
import triad_openvr as vr
import rospy
from geometry_msgs.msg import PoseStamped


def talker(trackers):
    pubs = []
    for i in range(len(trackers)):  # trackers
        pubs.append(rospy.Publisher('tracker' + str(i+1), PoseStamped, queue_size=10))
    rospy.init_node('vive_trackers', anonymous=True)
    rate = rospy.Rate(250)  # 250hz (4ms)
    while True:
        try:
            for tracker, pub in zip(trackers, pubs):
                tracker.get_pose_quaternion()
                pub.publish(tracker.pose)
                rospy.loginfo(tracker.pose)
            rate.sleep()
        except rospy.ROSInterruptException:
            pass


if __name__ == "__main__":
    v = vr.triad_openvr()
    detected_trackers = [device for device in v.devices if 'tracker' in device]
    tracker_numbers = [int(tracker.replace("tracker_", "")) for tracker in detected_trackers]
    trackers = [ViveTracker(v, i) for i in tracker_numbers]
    talker(trackers)

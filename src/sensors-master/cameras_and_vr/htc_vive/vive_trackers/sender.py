from ViveTrackers_Windows import ViveTracker
import triad_openvr as vr
import time
import sys
import socket
import numpy as np


if __name__=="__main__":
    v = vr.triad_openvr()
    tracker_names = [int(key.strip('tracker_')) for key, value in v.devices.items() if 'tracker' in key.lower()]
    trackers = [ViveTracker(v, 1)]

    sock = socket.socket(socket.AF_INET,  # Internet
                         socket.SOCK_STREAM)  # TCP
    host = socket.gethostname()
    ip = socket.gethostbyname(host)
    print(ip)
    sock.bind(('', 5005))
    sock.listen(1)
    print("Waiting for client ...")
    connection, client_address = sock.accept()

    running = True
    start = time.time()
    print('running')
    try:
        while running:
            for tracker in trackers:
                try:
                    pose = tracker.get_pose_quaternion()
                    pose = np.around(pose, decimals=3)
                    pose = str(pose)  # convert to string for sending to client
                    print(pose)
                    connection.send(pose.encode())
                except Exception as e:
                    print(e)
            now = time.time()
    except KeyboardInterrupt:
        pass

#!/usr/bin/env python3
from baselines import logger
from baselines.common import set_global_seeds, tf_util as U
from datetime import datetime
from deeprl.wrappers import LogPerf, ObsList, EpisodeRenderer
from deeprl.mlp_policy import MlpPolicy
from deeprl.ppo_agent import PPO_Agent
import gym
from mpi4py import MPI
import os
import argparse
from baselines.common.misc_util import boolean_flag
from gym import spaces

import rospy
import math
import numpy as np

from std_srvs.srv import Empty
from std_msgs.msg import Float64
from control_msgs.msg import JointControllerState
from gazebo_msgs.msg import LinkStates
from slider_gazebo.msg import FloatArray

# from slider_env import SliderEnv

import signal
import sys
def signal_handler(sig, frame):
	print('You pressed Ctrl+C!')
	# saving the policy

	sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)

#Define publishers for each joint position controller commands.
pub_left_hip_pitch    = rospy.Publisher('/slider_gazebo/left_hip_pitch_position_controller/command', Float64, queue_size=1)
pub_left_hip_roll     = rospy.Publisher('/slider_gazebo/left_hip_roll_position_controller/command', Float64, queue_size=1)
pub_left_hip_slide    = rospy.Publisher('/slider_gazebo/left_hip_slide_position_controller/command', Float64, queue_size=1)
pub_left_ankle_roll   = rospy.Publisher('/slider_gazebo/left_ankle_roll_position_controller/command', Float64, queue_size=1)
pub_left_ankle_pitch  = rospy.Publisher('/slider_gazebo/left_ankle_pitch_position_controller/command', Float64, queue_size=1)

pub_right_hip_pitch   = rospy.Publisher('/slider_gazebo/right_hip_pitch_position_controller/command', Float64, queue_size=1)
pub_right_hip_roll    = rospy.Publisher('/slider_gazebo/right_hip_roll_position_controller/command', Float64, queue_size=1)
pub_right_hip_slide   = rospy.Publisher('/slider_gazebo/right_hip_slide_position_controller/command', Float64, queue_size=1)
pub_right_ankle_roll  = rospy.Publisher('/slider_gazebo/right_ankle_roll_position_controller/command', Float64, queue_size=1)
pub_right_ankle_pitch = rospy.Publisher('/slider_gazebo/right_ankle_pitch_position_controller/command', Float64, queue_size=1)

pub_reward = rospy.Publisher('/slider_gazebo/reward', Float64, queue_size=1)
pub_action = rospy.Publisher('/slider_gazebo/action', FloatArray, queue_size=1)

time_before = 0.0
time_now = 0.0
TorsoPosZ_before = 0.0
TorsoPosZ_now = 0.0
joint_state   = np.zeros(10)
joint_vel     = np.zeros(10)
joint_command = np.zeros(10)
joint_effort  = np.zeros(10)

def getState(data):
	# Get the COM state, have to write explicitly 'global'
	global TorsoPosZ_before, TorsoPosZ_now, time_before, time_now
	TorsoPosZ_before = TorsoPosZ_now
	TorsoPosZ_now  = data.pose[1].position.z
	time_before = time_now
	time_now = rospy.get_rostime().nsecs

def lhipPitchCallback(data):
	global joint_state, joint_vel, joint_effort
	joint_state[0]  = data.process_value
	joint_vel[0]    = data.process_value_dot
	joint_effort[0] = data.command

def lhipRollCallback(data):
	global joint_state, joint_vel, joint_effort
	joint_state[1]  = data.process_value
	joint_vel[1]    = data.process_value_dot
	joint_effort[1] = data.command

def lhipSlideCallback(data):
	global joint_state, joint_vel, joint_effort
	joint_state[2]  = data.process_value
	joint_vel[2]    = data.process_value_dot
	joint_effort[2] = data.command

def lanklePitchCallback(data):
	global joint_state, joint_vel, joint_effort
	joint_state[3]  = data.process_value
	joint_vel[3]    = data.process_value_dot
	joint_effort[3] = data.command

def lankleRollCallback(data):
	global joint_state, joint_vel, joint_effort
	joint_state[4]  = data.process_value
	joint_vel[4]    = data.process_value_dot
	joint_effort[4] = data.command

def rhipPitchCallback(data):
	global joint_state, joint_vel, joint_effort
	joint_state[5]  = data.process_value
	joint_vel[5]    = data.process_value_dot
	joint_effort[5] = data.command

def rhipRollCallback(data):
	global joint_state, joint_vel, joint_effort
	joint_state[6]  = data.process_value
	joint_vel[6]    = data.process_value_dot
	joint_effort[6] = data.command

def rhipSlideCallback(data):
	global joint_state, joint_vel, joint_effort
	joint_state[7]  = data.process_value
	joint_vel[7]    = data.process_value_dot
	joint_effort[7] = data.command

def ranklePitchCallback(data):
	global joint_state, joint_vel, joint_effort
	joint_state[8]  = data.process_value
	joint_vel[8]    = data.process_value_dot
	joint_effort[8] = data.command

def rankleRollCallback(data):
	global joint_state, joint_vel, joint_effort
	joint_state[9]  = data.process_value
	joint_vel[9]    = data.process_value_dot
	joint_effort[9] = data.command

def resetWorld():
	rospy.wait_for_service('/gazebo/reset_world')
	resetWorldProxy = rospy.ServiceProxy('/gazebo/reset_world', Empty)
	resetWorldProxy()

def slider_RL_joint_publisher(joint_command):
	pub_left_hip_pitch.publish(joint_command[0])
	pub_left_hip_roll.publish(joint_command[1])
	pub_left_hip_slide.publish(joint_command[2])
	pub_left_ankle_roll.publish(joint_command[3])
	pub_left_ankle_pitch.publish(joint_command[4])
	pub_right_hip_pitch.publish(joint_command[5])
	pub_right_hip_roll.publish(joint_command[6])
	pub_right_hip_slide.publish(joint_command[7])
	pub_right_ankle_roll.publish(joint_command[8])
	pub_right_ankle_pitch.publish(joint_command[9])

def train(gamma, seed, train_T, timesteps): # observation, reward, done
	global joint_command, TorsoPosZ_before, TorsoPosZ_now, joint_state, joint_vel, joint_effort, time_before, time_now

	arguments = sorted(list(locals().keys()))
    # MPI
	rank = MPI.COMM_WORLD.Get_rank()
	seed = seed + 10000 * rank
	sess = U.single_threaded_session()
	sess.__enter__()
	if rank == 0: logger.configure()
	else: logger.configure(format_strs=[])

	utc = datetime.utcnow().strftime('%F_%H-%M-%S-%f')
	name = 'PPO'
	name += '-T'+str(train_T)
	n_procs = MPI.COMM_WORLD.Get_size()
	print('\nrank:', rank)
	if n_procs > 1:
		name += '-d'+str(n_procs)
		utc += '-r'+str(rank)
    # env = LogPerf(env, gamma, 'results/Slider_gamma' + str(gamma) + '/' + name + '/' + utc)
	set_global_seeds(seed)
    # why do i need this? Can I get rid of this?
    # env.seed(seed)

    # agent
	input_shapes = [20] # 10 joint position + 10 joint velocity
	# action_space = spaces.Box(low=-1.57*np.ones(10), high=1.57*np.ones(10))
	action_space = spaces.Box(low=-45/180*math.pi*np.ones(10), high=45/180*math.pi*np.ones(10))

	def policy_fn(name):
		return MlpPolicy(name=name, input_shapes=input_shapes, use_rms=[True], ac_space=action_space, hid_size=64, num_hid_layers=2)
	agent = PPO_Agent(policy_fn, timesteps_per_actorbatch=2048, clip_param=0.2, entcoeff=0.0, optim_epochs=10, optim_stepsize=3e-4,
        optim_batchsize=64, gamma=gamma, lam=0.95, schedule='constant', max_timesteps=timesteps)

	variables = locals()
	for arg_name in arguments: print('{0:20} {1}'.format(arg_name, variables[arg_name]))
	print()

    # training
	resetWorld()
	rate = rospy.Rate(350)
	observation = [np.array(np.concatenate([joint_state, joint_vel]))]    	# GEt the reward
	action = agent.reset(observation)

	############ Env slider wrapper########################
	# env = SliderEnv()
	# observation = env.reset()
	# action = agent.reset(observation)
	ep_t = 0

	for t in range(timesteps+1): # one more for the evaluation

		slider_RL_joint_publisher(action)
		time_elapsed = (time_now-time_before)/1e9
		if time_elapsed == 0:
			reward = 0
		else:
			# for moving forward with certain speed, live_bonus integrated
			reward  = 10 - abs((TorsoPosZ_now-TorsoPosZ_before) / (time_elapsed) - 0.15) 
			reward  -= 1e-5*(abs(np.sum(joint_effort)))

		pub_reward.publish(reward)
		# reward -= 1e-2 * np.dot(np.square(a),weight)

		done = not (np.isfinite(observation).all() and (TorsoPosZ_now < 1.1) and (TorsoPosZ_now > 0.45))

		# observation, reward, done, _ = env.step(action)


		ep_t += 1
		action = agent.step(observation, reward, done)

		if action is None:
			action = np.zeros(10) + np.random.random_sample(10)*0.1
		slider_RL_joint_publisher(action)

		a = FloatArray()
		a.data = action.tolist()
		pub_action.publish(a)

		# print("sum of the effort is ")
		# print(np.sum(joint_effort))
		rate.sleep()

        # reset at the end to log
		if done or ep_t == train_T:
			for i in range(3):
				slider_RL_joint_publisher(np.zeros(10))
				rospy.sleep(0.2)
			resetWorld()
			slider_RL_joint_publisher(np.zeros(10))
			rospy.sleep(0.3)

			observation = [np.array(np.concatenate([joint_state, joint_vel]))]
			action = agent.reset(observation)
			# action = np.zeros(10) + np.random.random_sample(10)*0.05
			# observation = env.reset()
            # action = agent.reset(observation)
			ep_t = 0

#Main section of code that will continuously run unless rospy receives interuption (ie CTRL+C)
if __name__ == '__main__':
	rospy.init_node('slider_joint_positions_node', anonymous=True)
	
	time_before = rospy.get_rostime().nsecs
	time_now = rospy.get_rostime().nsecs

	parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
	parser.add_argument('--gamma', type=float, default=0.99)
	parser.add_argument('--seed', help='RNG seed', type=int, default=10)
	parser.add_argument('--train-T', help='time limit during training', type=int, default=3e3)
	parser.add_argument('--timesteps', help='total timesteps during training', type=int, default=int(9e7))
	args = vars(parser.parse_args())

	# get Pelvis position for determine whether the robot falls or not
	rospy.Subscriber("/gazebo/link_states", LinkStates, getState, queue_size=1)
	# get all joint positions and velocities
	rospy.Subscriber("/slider_gazebo/left_hip_pitch_position_controller/state", JointControllerState,    lhipPitchCallback, queue_size=1)
	rospy.Subscriber("/slider_gazebo/left_hip_roll_position_controller/state", JointControllerState,     lhipRollCallback, queue_size=1)
	rospy.Subscriber("/slider_gazebo/left_hip_slide_position_controller/state", JointControllerState,    lhipSlideCallback, queue_size=1)
	rospy.Subscriber("/slider_gazebo/left_ankle_pitch_position_controller/state", JointControllerState,  lanklePitchCallback, queue_size=1)
	rospy.Subscriber("/slider_gazebo/left_ankle_roll_position_controller/state", JointControllerState,   lankleRollCallback, queue_size=1)
	rospy.Subscriber("/slider_gazebo/right_hip_pitch_position_controller/state", JointControllerState,   rhipPitchCallback, queue_size=1)
	rospy.Subscriber("/slider_gazebo/right_hip_roll_position_controller/state", JointControllerState,    rhipRollCallback, queue_size=1)
	rospy.Subscriber("/slider_gazebo/right_hip_slide_position_controller/state", JointControllerState,   rhipSlideCallback, queue_size=1)
	rospy.Subscriber("/slider_gazebo/right_ankle_pitch_position_controller/state", JointControllerState, ranklePitchCallback, queue_size=1)
	rospy.Subscriber("/slider_gazebo/right_ankle_roll_position_controller/state", JointControllerState,  rankleRollCallback, queue_size=1)

	train(**args)

	# except rospy.ROSInterruptException: pass
from baselines.common import explained_variance, fmt_row, zipsame
from deeprl.dataset import Dataset
from baselines import logger
import baselines.common.tf_util as U
import tensorflow as tf, numpy as np
import time
from baselines.common.mpi_adam import MpiAdam
from baselines.common.mpi_moments import mpi_moments
from mpi4py import MPI
from collections import deque
from deeprl.agent import Agent

# For pausing gazebo
import rospy
from std_srvs.srv import Empty

def add_vtarg_and_adv(news, vpreds, next_vpreds, rews, gamma, lam):
    """
    Compute target value using TD(lambda) estimator, and advantage with GAE(lambda)
    """
    news = np.append(news, 0) # last element is only used for last vtarg, but we already zeroed it if last new = 1
    T = len(rews)
    gaelam = np.empty(T, 'float32')
    lastgaelam = 0
    for t in reversed(range(T)):
        nonterminal = 1-news[t+1]
        delta = rews[t] + gamma * next_vpreds[t] - vpreds[t]
        gaelam[t] = lastgaelam = delta + gamma * lam * nonterminal * lastgaelam
    tdlamret = gaelam + vpreds
    print(tdlamret)
    return gaelam, tdlamret

class PPO_Agent(Agent):
    def __init__(self, policy_fn, *,
        timesteps_per_actorbatch, # timesteps per actor per update
        clip_param, entcoeff, # clipping parameter epsilon, entropy coeff
        optim_epochs, optim_stepsize, optim_batchsize,# optimization hypers
        gamma, lam, # advantage estimation
        adam_epsilon=1e-5,
        schedule='constant', # annealing for stepsize parameters (epsilon and adam)
        max_timesteps=None,
        clip_vf=None,
        callback=None # you can do anything in the callback, since it takes locals(), globals()
    ):
        assert schedule == 'constant' or max_timesteps is not None, 'with a non-constant schedule max_timesteps is required'
        # Setup losses and stuff
        # ----------------------------------------
        self.horizon = timesteps_per_actorbatch
        self.optim_epochs = optim_epochs
        self.optim_stepsize = optim_stepsize
        self.optim_batchsize = optim_batchsize
        self.gamma = gamma
        self.lam = lam
        self.schedule = schedule
        self.max_timesteps = max_timesteps
        self.clip_vf = clip_vf
        self.callback = callback

        self.pi = policy_fn("pi") # Construct network for new policy
        oldpi = policy_fn("oldpi") # Network for old policy
        atarg = tf.placeholder(dtype=tf.float32, shape=[None]) # Target advantage function (if applicable)
        ret = tf.placeholder(dtype=tf.float32, shape=[None]) # Empirical return

        lrmult = tf.placeholder(name='lrmult', dtype=tf.float32, shape=[]) # learning rate multiplier, updated with schedule
        clip_param = clip_param * lrmult # Annealed cliping parameter epislon

        inputs = self.pi.inputs
        ac = self.pi.pdtype.sample_placeholder([None])

        kloldnew = oldpi.pd.kl(self.pi.pd)
        ent = self.pi.pd.entropy()
        meankl = tf.reduce_mean(kloldnew)
        meanent = tf.reduce_mean(ent)
        pol_entpen = (-entcoeff) * meanent

        ratio = tf.exp(self.pi.pd.logp(ac) - oldpi.pd.logp(ac)) # pnew / pold
        surr1 = ratio * atarg # surrogate from conservative policy iteration
        surr2 = tf.clip_by_value(ratio, 1.0 - clip_param, 1.0 + clip_param) * atarg #
        pol_surr = - tf.reduce_mean(tf.minimum(surr1, surr2)) # PPO's pessimistic surrogate (L^CLIP)
        vf_loss = tf.reduce_mean(tf.square(self.pi.vpred - ret))
        total_loss = pol_surr + pol_entpen + vf_loss
        losses = [pol_surr, pol_entpen, vf_loss, meankl, meanent]
        self.loss_names = ["pol_surr", "pol_entpen", "vf_loss", "kl", "ent"]

        var_list = self.pi.get_trainable_variables()
        self.lossandgrad = U.function(inputs + [ac, atarg, ret, lrmult], losses + [U.flatgrad(total_loss, var_list)])
        self.adam = MpiAdam(var_list, epsilon=adam_epsilon)

        self.assign_old_eq_new = U.function([],[], updates=[tf.assign(oldv, newv)
            for (oldv, newv) in zipsame(oldpi.get_variables(), self.pi.get_variables())])
        self.compute_losses = U.function(inputs + [ac, atarg, ret, lrmult], losses)

        U.initialize()
        self.adam.sync()

        # Prepare for rollouts
        # ----------------------------------------
        self.index = 0

        self.ep_rets = [] # returns of completed episodes in this segment
        self.ep_lens = [] # lengths of ...

        # Initialize history arrays
        # TODO: float32 or float64?
        self.rews = np.zeros(self.horizon, 'float32')
        self.vpreds = np.zeros(self.horizon, 'float32')
        self.next_vpreds = np.zeros(self.horizon, 'float32')
        self.news = np.zeros(self.horizon, 'int32')

        self.episodes_so_far = 0
        self.timesteps_so_far = 0
        self.iters_so_far = 0
        self.tstart = time.time()
        self.lenbuffer = deque(maxlen=100) # rolling buffer for episode lengths
        self.rewbuffer = deque(maxlen=100) # rolling buffer for episode rewards

    # resets are allowed before terminations, partial-episode bootstrapping will be used
    # because next vpreds are stored in a separate array
    # https://arxiv.org/abs/1712.00378
    def reset(self, ob):
        ac, vpred = self.pi.act(True, ob)
        if self.clip_vf is not None:
            vpred = np.clip(vpred, self.clip_vf[0], self.clip_vf[1])
        if self.timesteps_so_far == 0:
            self.obs = [np.array([o for _ in range(self.horizon)], dtype='float64') for o in ob] # float64 better?
            self.acs = np.array([ac for _ in range(self.horizon)])
        else:
            self.ep_rets.append(self.cur_ep_ret)
            self.ep_lens.append(self.cur_ep_len)
            self.episodes_so_far += 1
        self.cur_ep_ret = 0
        self.cur_ep_len = 0

        for i, o in enumerate(ob): self.obs[i][self.index] = o
        self.acs[self.index] = ac
        self.vpreds[self.index] = vpred
        self.news[self.index] = True

        return ac

    def step(self, ob, rew, terminal):
        self.rews[self.index] = rew
        self.cur_ep_ret += rew
        self.cur_ep_len += 1
        self.timesteps_so_far += 1
        self.index += 1

        # if not terminal, pi.act before optimizing because the next vpred needs to be known
        if not terminal:
            ac, vpred = self.pi.act(True, ob)
            if self.clip_vf is not None:
                vpred = np.clip(vpred, self.clip_vf[0], self.clip_vf[1])
        else:
            ac, vpred = None, 0

        self.next_vpreds[self.index-1] = vpred

        if self.index == self.horizon:
            rospy.wait_for_service('/gazebo/pause_physics')
            pause = rospy.ServiceProxy('/gazebo/pause_physics', Empty)
            pause()
            self.optimize()

            rospy.wait_for_service('/gazebo/unpause_physics')
            unpause = rospy.ServiceProxy('/gazebo/unpause_physics', Empty)
            unpause()

        if not terminal:
            for i, o in enumerate(ob): self.obs[i][self.index] = o
            self.acs[self.index] = ac
            self.vpreds[self.index] = vpred
            self.news[self.index] = False

        return ac

    # to use during evaluation, doesn't train or change any internal variable
    # warning: if LSTMs are used this function needs to take care of them to avoid interfering with training
    # TODO: separate RNG?
    def test(self, ob, stochastic=True):
        return self.pi.act(stochastic, ob)[0]

    def optimize(self):
        logger.log('[%i] '%MPI.COMM_WORLD.Get_rank() + self.pi.scope + ' iteration %i'%self.iters_so_far)
        # logger.log("********** Iteration %i ************"%self.iters_so_far)
        if self.callback: self.callback(locals(), globals())
        assert self.index == self.horizon

        self.atarg, tdlamret = add_vtarg_and_adv(self.news, self.vpreds, self.next_vpreds, self.rews, self.gamma, self.lam)
        if np.any(self.atarg.std() == 0): logger.log('WARNING: did not standardize advantages to avoid division by 0') # TODO
        else: self.atarg = (self.atarg - self.atarg.mean()) / self.atarg.std() # standardized advantage function estimate
        d = Dataset(dict(ob=self.obs, ac=self.acs, atarg=self.atarg, vtarg=tdlamret), shuffle=not self.pi.recurrent)
        # print([o.sum() for o in self.obs])

        if self.schedule == 'constant':
            cur_lrmult = 1.0
        elif self.schedule == 'linear':
            cur_lrmult =  max(1.0 - float(self.timesteps_so_far) / self.max_timesteps, 0)
        else:
            raise NotImplementedError

        self.optim_batchsize = self.optim_batchsize or self.obs.shape[0]
        vpredbefore = self.vpreds # predicted value function before udpate

        if hasattr(self.pi, "update_rms"): self.pi.update_rms(self.obs) # update running mean/std for policy

        self.assign_old_eq_new() # set old parameter values to new parameter values
        logger.log("Optimizing...")
        logger.log(fmt_row(13, self.loss_names))
        # Here we do a bunch of optimization epochs over the data
        for _ in range(self.optim_epochs):
            losses = [] # list of tuples, each of which gives the loss for a minibatch
            for batch in d.iterate_once(self.optim_batchsize):
                *newlosses, g = self.lossandgrad(*batch["ob"], batch["ac"], batch["atarg"], batch["vtarg"], cur_lrmult)
                self.adam.update(g, self.optim_stepsize * cur_lrmult) 
                losses.append(newlosses)
            logger.log(fmt_row(13, np.mean(losses, axis=0)))

        logger.log("Evaluating losses...")
        losses = []
        for batch in d.iterate_once(self.optim_batchsize):
            newlosses = self.compute_losses(*batch["ob"], batch["ac"], batch["atarg"], batch["vtarg"], cur_lrmult)
            losses.append(newlosses)            
        meanlosses,_,_ = mpi_moments(losses, axis=0)
        logger.log(fmt_row(13, meanlosses))
        for (lossval, name) in zipsame(meanlosses, self.loss_names):
            logger.record_tabular("loss_"+name, lossval)
        logger.record_tabular("ev_tdlam_before", explained_variance(vpredbefore, tdlamret))
        lrlocal = (self.ep_lens, self.ep_rets) # local values
        listoflrpairs = MPI.COMM_WORLD.allgather(lrlocal) # list of tuples
        lens, rews = map(flatten_lists, zip(*listoflrpairs))
        self.lenbuffer.extend(lens)
        self.rewbuffer.extend(rews)
        logger.record_tabular("EpLenMean", np.mean(self.lenbuffer))
        logger.record_tabular("EpRewMean", np.mean(self.rewbuffer))
        logger.record_tabular("EpThisIter", len(lens))
        logger.record_tabular("EpisodesSoFar", self.episodes_so_far)
        logger.record_tabular("TimestepsSoFar", self.timesteps_so_far)
        logger.record_tabular("TimeElapsed", time.time() - self.tstart)
        if MPI.COMM_WORLD.Get_rank()==0:
            logger.dump_tabular()

        logger.log('  done [%i] '%MPI.COMM_WORLD.Get_rank() + self.pi.scope + ' iteration %i'%self.iters_so_far)
        self.iters_so_far += 1
        self.ep_rets = []
        self.ep_lens = []
        self.index = 0

def flatten_lists(listoflists):
    return [el for list_ in listoflists for el in list_]

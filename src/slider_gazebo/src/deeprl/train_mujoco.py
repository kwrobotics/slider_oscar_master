#!/usr/bin/env python3
from baselines import logger
from baselines.common import set_global_seeds, tf_util as U
from datetime import datetime
from envs.wrappers import LogPerf, ObsList, TimeInInput
import gym
from mpi4py import MPI
import os
from mlp_policy import MlpPolicy
from ppo_agent import PPO_Agent

def train(env_id, gamma, seed, train_T, peb, time_aware, timesteps):
    arguments = sorted(list(locals().keys()))

    # MPI
    rank = MPI.COMM_WORLD.Get_rank()
    seed = seed + 10000 * rank
    sess = U.single_threaded_session()
    sess.__enter__()
    if rank == 0: logger.configure()
    else: logger.configure(format_strs=[])

    # environments
    env = gym.make(env_id)
    if train_T is None: train_T = env._max_episode_steps
    env = env.unwrapped
    env = ObsList(env)
    if time_aware:
        env = TimeInInput(env, train_T)
    utc = datetime.utcnow().strftime('%F_%H-%M-%S-%f')
    name = 'PPO'
    name += '-T'+str(train_T)
    if peb: name += 'peb'
    if time_aware: name += 'ta'
    n_procs = MPI.COMM_WORLD.Get_size()
    print('\nrank:', rank)
    if n_procs > 1:
        name += '-d'+str(n_procs)
        utc += '-r'+str(rank)
    env = LogPerf(env, gamma, 'results/' + env_id + '_gamma' + str(gamma) + '/' + name + '/' + utc)
    set_global_seeds(seed)
    env.seed(seed)

    # agent
    input_shapes = [ob_space.shape[0] for ob_space in env.observation_space]
    use_rms = [True]
    if time_aware: use_rms += [False] # no rms for the time
    def policy_fn(name):
        return MlpPolicy(name=name, input_shapes=input_shapes, use_rms=use_rms, ac_space=env.action_space, hid_size=64, num_hid_layers=2)
    agent = PPO_Agent(policy_fn, timesteps_per_actorbatch=2048, clip_param=0.2, entcoeff=0.0, optim_epochs=10, optim_stepsize=3e-4,
        optim_batchsize=64, gamma=gamma, lam=0.95, schedule='constant', max_timesteps=timesteps)

    print('\nPARAMETERS:')
    variables = locals()
    for arg_name in arguments: print('{0:20} {1}'.format(arg_name, variables[arg_name]))
    print()

    # training
    ob = env.reset()
    ac = agent.reset(ob)
    ep_t = 0
    for t in range(timesteps+1): # one more for the evaluation
        ob, rew, done, _ = env.step(ac)
        ep_t += 1
        if not peb and ep_t == train_T: done = True
        ac = agent.step(ob, rew, done)

        # reset at the end to log
        if done or ep_t == train_T:
            ob = env.reset()
            ac = agent.reset(ob)
            ep_t = 0

def main():
    import argparse
    from baselines.common.misc_util import boolean_flag
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--env-id', help='environment ID', default='Hopper-v1')
    parser.add_argument('--gamma', type=float, default=0.99)
    parser.add_argument('--seed', help='RNG seed', type=int, default=0)
    parser.add_argument('--train-T', help='time limit during training', type=int, default=None)
    boolean_flag(parser, 'peb', help='partial-episode bootstrapping', default=False)
    boolean_flag(parser, 'time-aware', help='time-aware agent', default=False)
    parser.add_argument('--timesteps', help='total timesteps during training', type=int, default=int(1e6))
    args = vars(parser.parse_args())
    train(**args)

if __name__ == '__main__':
    main()

from baselines.common.mpi_running_mean_std import RunningMeanStd
import baselines.common.tf_util as U
import tensorflow as tf
import gym
from baselines.common.distributions import make_pdtype
from deeprl.agent import Policy

# simple ANN that concatenates the inputs and creates an actor and a critic
class MlpPolicy(Policy):
    recurrent = False

    # assumes inputs are floats
    def _init(self, input_shapes, use_rms, ac_space, hid_size, num_hid_layers, gaussian_fixed_var=True):
        self.pdtype = pdtype = make_pdtype(ac_space)

        self.inputs = []
        self.input_rms = []
        filtered_inputs = []
        for i, shape in enumerate(input_shapes):
            inp = U.get_placeholder(name="input%i"%(i+1), dtype=tf.float32, shape=[None, shape])
            if use_rms[i]:
                with tf.variable_scope("inputfilter%i"%(i+1)):
                    inp_rms = RunningMeanStd(shape=shape)
                filt_inp = tf.clip_by_value((inp - inp_rms.mean) / inp_rms.std, -5.0, 5.0)
            else:
                inp_rms = None
                filt_inp = inp
            self.inputs.append(inp)
            self.input_rms.append(inp_rms)
            filtered_inputs.append(filt_inp)

        if len(input_shapes) != 1:
            inpz = tf.concat(filtered_inputs, axis=1)
        else:
            inpz = filtered_inputs[0]

        # critic
        with tf.variable_scope('vf'):
            last_out = inpz
            for i in range(num_hid_layers):
                last_out = tf.nn.tanh(tf.layers.dense(last_out, hid_size, name="fc%i"%(i+1), kernel_initializer=U.normc_initializer(1.0)))
            self.vpred = tf.layers.dense(last_out, 1, name='vffinal', kernel_initializer=U.normc_initializer(1.0))[:,0]

        # actor
        with tf.variable_scope('pol'):
            last_out = inpz
            for i in range(num_hid_layers):
                last_out = tf.nn.tanh(tf.layers.dense(last_out, hid_size, name='fc%i'%(i+1), kernel_initializer=U.normc_initializer(1.0)))
            if gaussian_fixed_var and isinstance(ac_space, gym.spaces.Box):
                mean = tf.layers.dense(last_out, pdtype.param_shape()[0]//2, name='final', kernel_initializer=U.normc_initializer(0.01))
                logstd = tf.get_variable(name="logstd", shape=[1, pdtype.param_shape()[0]//2], initializer=tf.zeros_initializer())
                pdparam = tf.concat([mean, mean * 0.0 + logstd], axis=1)
            else:
                pdparam = tf.layers.dense(last_out, pdtype.param_shape()[0], name='final', kernel_initializer=U.normc_initializer(0.01))
            self.pd = pdtype.pdfromflat(pdparam)
            stochastic = tf.placeholder(dtype=tf.bool, shape=())
            ac = U.switch(stochastic, self.pd.sample(), self.pd.mode())

        self._act = U.function([stochastic] + self.inputs, [ac, self.vpred])

    def update_rms(self, obs):
        for i, ob in enumerate(obs):
            rms = self.input_rms[i]
            if rms is not None:
                rms.update(ob)

import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import numpy as np
import os
import csv
plt.rcParams.update({'font.size': 6})

# def smooth_mean_and_std(xs, ys, window_size, freq):
#     assert 0 < window_size and 0 < freq
#     from collections import deque
#     xs = np.concatenate(xs)
#     ys = np.concatenate(ys)
#     idx = np.argsort(xs)
#     x_queue = deque(maxlen=window_size)
#     y_queue = deque(maxlen=window_size)
#     xs_ = []
#     means = []
#     stds = []
#     for i in idx:
#         x_queue.append(xs[i])
#         y_queue.append(ys[i])
#         if len(x_queue) < window_size or i % freq != 0: continue
#         xs_.append(np.mean(x_queue))
#         means.append(np.mean(y_queue))
#         stds.append(np.std(y_queue))
#     return np.array(xs_), np.array(means), np.array(stds)

# training data are asynchronous because episodes start and stop at different steps between agents/workers
def synchronize(steps, lengths, undiscounteds, discounteds, sync_steps):
    indices = []
    sync = sync_steps
    for i in range(len(steps)):
        if steps[i] >= sync:
            indices.append(i-1)
            sync = sync + sync_steps
    steps = np.split(steps, indices)
    lengths = np.split(lengths, indices)
    undiscounteds = np.split(undiscounteds, indices)
    discounteds = np.split(discounteds, indices)
    if len(steps) == len(indices)+1:
        steps = steps[:-1]
        lengths = lengths[:-1]
        undiscounteds = undiscounteds[:-1]
        discounteds = discounteds[:-1]
    steps = [s.mean() for s in steps]
    lengths = [l.mean() for l in lengths]
    undiscounteds = [u.mean() for u in undiscounteds]
    discounteds = [d.mean() for d in discounteds]
    return steps, lengths, undiscounteds, discounteds

def plot_perfs(path, sync_points, max_step, n_best, without, only, std):
    # colors = plt.get_cmap('tab10').colors
    # colors = [(0.12, 0.47, 0.71), (0.17, 0.63, 0.17), (0.84, 0.15, 0.16), (1.00, 0.50, 0.05)]
    # colors = [(0.12, 0.47, 0.71), (0.145, 0.55, 0.44), (0.84, 0.15, 0.16), (0.92, 0.33, 0.11)]
    colors = [plt.get_cmap('tab10').colors[0]] * 2 + [plt.get_cmap('tab10').colors[2]] * 2 + [plt.get_cmap('tab10').colors[1]] * 2
    line_styles = ['--', '-', '--', '-', '--', '-']
    print('Search for performance logs in:', path)
    os.chdir(path)
    for env_id in filter(os.path.isdir, sorted(os.listdir('.'))):
        agent = 0
        lines = []
        os.chdir(env_id)
        print(env_id)
        agent = 0
        for agent_id in filter(os.path.isdir, sorted(os.listdir('.'))):
            if without is not None and without in agent_id: continue
            if only is not None and not only in agent_id: continue
            if 'ta' in agent_id:
                if '-d' in agent_id:
                    agent_idx = 0
                else:
                    agent_idx = 1
            elif 'peb' in agent_id:
                if '-d' in agent_id:
                    agent_idx = 2
                else:
                    agent_idx = 3
            else:
                if '-d' in agent_id:
                    agent_idx = 4
                else:
                    agent_idx = 5
            os.chdir(agent_id)
            print('\t', agent_id)
            all_steps = []
            all_lengths = []
            all_undiscounteds = []
            all_discounteds = []
            for date in filter(os.path.isdir, sorted(os.listdir('.'))):
                os.chdir(date)
                if os.path.isfile('perf.csv'):
                    perf = np.loadtxt('perf.csv')
                    if len(perf.shape) == 1: perf = perf[None,:]
                    if max_step is not None and max_step < int(perf[-1,0]): n = np.argmax(perf[:,0] > max_step)
                    else: n = len(perf[:,0])
                    sync_steps = perf[n-1,0] // sync_points
                    print('sync steps:', sync_steps, n, sync_points)
                    steps, lengths, undiscounteds, discounteds = synchronize(perf[:n,0], perf[:n,2], perf[:n,3], perf[:n,4], sync_steps)
                    print('\t\t', date, len(steps), 'points', '(%i asynchonous points in %i steps)'%(n, perf[n-1,0]))
                    all_steps.append(steps)
                    all_lengths.append(lengths)
                    all_undiscounteds.append(undiscounteds)
                    all_discounteds.append(discounteds)
                os.chdir('..')
            if len(all_steps) != 0:
                print([len(steps) for steps in all_steps])
                n = min([len(steps) for steps in all_steps])
                print(n, 'points will be used')
                steps             = steps[:n]
                all_lengths       = [l[:n] for l in all_lengths]
                all_undiscounteds = [u[:n] for u in all_undiscounteds]
                all_discounteds   = [d[:n] for d in all_discounteds]
                lengths           = np.mean(all_lengths, 0)
                std_lengths       = np.std(all_lengths, 0) / np.sqrt(len(all_steps)) # standard error
                undiscounteds     = np.mean(all_undiscounteds, 0)
                std_undiscounteds = np.std(all_undiscounteds, 0) / np.sqrt(len(all_steps)) # standard error
                discounteds       = np.mean(all_discounteds, 0)
                std_discounteds   = np.std(all_discounteds, 0) / np.sqrt(len(all_steps)) # standard error
                if agent == 0:
                    figure_name = env_id
                    fig = plt.figure(figsize=(6, 9))
                    grid = gridspec.GridSpec(7, 1)
                    axes = [fig.add_subplot(grid[0:2, 0]), fig.add_subplot(grid[2:4, 0]), fig.add_subplot(grid[4:6, 0]), fig.add_subplot(grid[6:7, 0])]
                    fig.suptitle(figure_name)
                    axes[0].set_xlabel('timesteps')
                    axes[0].set_ylabel('sum of rewards')
                    axes[0].grid(color='#dddddd')
                    axes[0].set_axisbelow(True)
                    axes[1].set_xlabel('timesteps')
                    axes[1].set_ylabel('discounted returns')
                    axes[1].grid(color='#dddddd')
                    axes[1].set_axisbelow(True)
                    axes[2].set_xlabel('timesteps')
                    axes[2].set_ylabel('episode lengths')
                    axes[2].grid(color='#dddddd')
                    axes[2].set_axisbelow(True)
                    axes[3].axis('off')
                std_alpha = 0.2 if std else 0.0
                fill_line_0 = axes[0].fill_between(steps, undiscounteds-std_undiscounteds, undiscounteds+std_undiscounteds, color=colors[agent_idx], alpha=std_alpha, lw=0)
                line_0 = axes[0].plot(steps, undiscounteds, color=colors[agent_idx], alpha=1, lw=1, linestyle=line_styles[agent_idx], label=agent_id)[0]
                fill_line_1 = axes[1].fill_between(steps, discounteds-std_discounteds, discounteds+std_discounteds, color=colors[agent_idx], alpha=std_alpha, lw=0)
                line_1 = axes[1].plot(steps, discounteds, color=colors[agent_idx], alpha=1, lw=1, linestyle=line_styles[agent_idx], label=agent_id)[0]
                fill_line_2 = axes[2].fill_between(steps, lengths-std_lengths, lengths+std_lengths, color=colors[agent_idx], alpha=std_alpha, lw=0)
                line_2 = axes[2].plot(steps, lengths, color=colors[agent_idx], alpha=1, lw=1, linestyle=line_styles[agent_idx], label=agent_id)[0]
                lines.append((agent_idx, fill_line_0, line_0, fill_line_1, line_1, fill_line_2, line_2, agent, len(all_steps)))
                agent += 1
            os.chdir('..')
        os.chdir('..')
        if agent == 0: continue
        lines = sorted(lines, reverse=False, key=lambda x: x[0])
        handles, labels = axes[0].get_legend_handles_labels() # Do after ax.plot()
        handles_to_show, labels_to_show = [], []
        n_to_show = min(len(lines), n_best)
        for i in reversed(range(n_to_show)):
            for l in lines[i][1:-2]:
                # l.set_color(colors[i])
                l.set_zorder(len(lines)-i)
            handles_to_show.append(handles[lines[i][-2]])
            label = labels[lines[i][-2]]
            if lines[i][-1] != 1: # average
                label += ' ('+str(lines[i][-1])+')'
            labels_to_show.append(label)
        handles_to_show = list(reversed(handles_to_show))
        labels_to_show = list(reversed(labels_to_show))
        axes[3].legend(handles_to_show, labels_to_show, loc='center')
        fig.tight_layout(rect=[0, 0, 1, 0.95])
        fig.canvas.draw()
        fig.savefig(figure_name+'.pdf')
        print('Figure saved in:', figure_name)
        for i in range(len(lines)):
            print('{:<5} {:<20} {}'.format(i, labels[lines[i][-2]], '('+str(lines[i][-1])+')'))

if __name__ == '__main__':
    import argparse
    from baselines.common.misc_util import boolean_flag
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--sync-points', type=int, default=100)
    parser.add_argument('--max-step', type=int, default=None)
    parser.add_argument('--n-best', type=int, default=5)
    parser.add_argument('--without', default=None)
    parser.add_argument('--only', default=None)
    boolean_flag(parser, 'std', default=True)
    parser.add_argument('--path', default='results/')
    args = parser.parse_args()
    plot_perfs(path=args.path, sync_points=args.sync_points, max_step=args.max_step, n_best=args.n_best, without=args.without, only=args.only, std=args.std)

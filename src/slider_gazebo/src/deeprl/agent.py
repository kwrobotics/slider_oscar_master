import tensorflow as tf

class Agent(object):
    def __new__(cls, *args, **kwargs):
        # We use __new__ since we want the env author to be able to
        # override __init__ without remembering to call super.
        agent = super(Agent, cls).__new__(cls)
        return agent

    def step(self, *args, **kwargs): raise NotImplementedError
    def reset(self, *args, **kwargs): raise NotImplementedError
    def test(self, *args, **kwargs): raise NotImplementedError

    @property
    def unwrapped(self):
        return self

    def __str__(self):
        return '<{} instance>'.format(type(self).__name__)

class Policy(object):
    def __init__(self, name, recurrent=False, **kwargs):
        self.recurrent = recurrent
        with tf.variable_scope(name):
            self.scope = tf.get_variable_scope().name
            self._init(**kwargs)

    def _init(self):
        raise NotImplementedError

    def act(self, stochastic, ob):
        ac1, vpred1 = self._act(stochastic, *[[o] for o in ob])
        return ac1[0], vpred1[0]
    def get_variables(self):
        return tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, self.scope)
    def get_trainable_variables(self):
        return tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, self.scope)
    def get_initial_state(self):
        return []

    def update_rms(self, obs):
        raise NotImplementedError

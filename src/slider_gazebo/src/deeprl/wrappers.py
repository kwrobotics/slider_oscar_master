import os
import gym
import numpy as np

class EpisodeRenderer(gym.Wrapper):
    def __init__(self, env, rendering_freq, finished_only=False, fast=False, viewer_title='simulation'):
        super(EpisodeRenderer, self).__init__(env)
        self.rendering_freq = rendering_freq
        self.finished_only = finished_only
        self.episodes = 0
        self.env.unwrapped.fast_rendering = fast
        self.env.unwrapped.viewer_title = viewer_title

    def _step(self, action):
        observation, reward, done, info = self.env.step(action)
        if self.rendering_freq != 0 and self.episodes%self.rendering_freq == 0:
            if done or not self.finished_only:
                self.env.render()
        return observation, reward, done, info

    def _reset(self):
        self.episodes += 1
        observation = self.env.reset()
        if self.rendering_freq != 0 and self.episodes%self.rendering_freq == 0:
            self.env.render()
        return observation

class LogPerf(gym.Wrapper):
    def __init__(self, env, gamma, path):
        super(LogPerf, self).__init__(env)
        self.gamma = gamma
        self.path = path + '/perf.csv'
        self.episodes = 0
        self.steps = 0
        self.discount = 1
        self.episode_length = 0
        self.needs_reset = True
        directory_path = os.path.dirname(self.path)
        if not os.path.exists(directory_path):
            os.makedirs(directory_path)
        print('Performance saved in:', self.path)

    def _step(self, action):
        assert not self.needs_reset, 'The environment should be reset.'
        self.steps += 1
        self.episode_length += 1
        observation, reward, done, info = self.env.step(action)
        self.undiscounted_return += reward
        self.discounted_return += self.discount * reward
        self.discount *= self.gamma
        if done: self.needs_reset = True
        return observation, reward, done, info

    def _reset(self):
        if self.episode_length != 0: self._log()
        self.needs_reset = False
        self.undiscounted_return = 0
        self.discounted_return = 0
        self.episode_length = 0
        self.episodes += 1
        self.discount = 1
        return self.env.reset()

    def _log(self):
        with open(self.path, 'a') as csv_file:
            csv_file.write('\t'.join([str(self.steps), str(self.episodes), str(self.episode_length),
                str(self.undiscounted_return), str(self.discounted_return)]) + '\n')

class TimeLimit(gym.Wrapper):
    def __init__(self, env, max_episode_steps, output_time=False):
        super(TimeLimit, self).__init__(env)
        if output_time:
            self.observation_space += [gym.spaces.Box(np.array([0.0]), np.array([1.0]))]
        self._max_episode_steps = max_episode_steps
        self._elapsed_steps = 0
        self._output_time = output_time

    def _step(self, action):
        observation, reward, done, info = self.env.step(action)
        self._elapsed_steps += 1
        if self._max_episode_steps <= self._elapsed_steps:
            done = True
        if self._output_time:
            observation = observation + [np.array([(self._max_episode_steps-self._elapsed_steps)/self._max_episode_steps])]
        return observation, reward, done, info

    def _reset(self):
        self._elapsed_steps = 0
        observation = self.env.reset()
        if self._output_time:
            observation += [np.array([1.0])]
        return observation

class TimeInInput(gym.Wrapper):
    def __init__(self, env, max_episode_steps):
        super(TimeInInput, self).__init__(env)
        self.observation_space += [gym.spaces.Box(np.array([-1.0]), np.array([1.0]))]
        self._max_episode_steps = max_episode_steps
        self._elapsed_steps = 0

    def _step(self, action):
        assert self._elapsed_steps < self._max_episode_steps
        observation, reward, done, info = self.env.step(action)
        self._elapsed_steps += 1
        remaining_time = (self._max_episode_steps-self._elapsed_steps)/self._max_episode_steps
        observation = observation + [np.array([2.0 * remaining_time - 1.0])] # TODO: find what's best
        return observation, reward, done, info

    def _reset(self):
        self._elapsed_steps = 0
        observation = self.env.reset()
        observation += [np.array([1.0])]
        return observation

# for compatibility with Gym's environments
class ObsList(gym.Wrapper):
    def __init__(self, env):
        super(ObsList, self).__init__(env)
        self.observation_space = [self.env.observation_space]

    def _step(self, action):
        observation, reward, done, info = self.env.step(action)
        return [observation], reward, done, info

    def _reset(self):
        observation = self.env.reset()
        return [observation]

class RoomsLog(gym.Wrapper):
    def __init__(self, env, path):
        super(RoomsLog, self).__init__(env)
        self.unwrapped_env = self.env.unwrapped
        self.path = path + '/room_log.csv'
        self.episodes = 0
        self.steps = 0
        self.episode_length = 0

        self.targ_loc = None
        self.agent_loc = None
        self.doors = None
        self.buttons = None

        self.buf_log = ""

        directory_path = os.path.dirname(self.path)
        if not os.path.exists(directory_path):
            os.makedirs(directory_path)
        print('Room logs are saved in:', self.path)

    def _step(self, action):
        if self.episode_length == 0:
            self._buffer_log()
        self.steps += 1
        self.episode_length += 1
        observation, reward, done, info = self.env.step(action)
        self._buffer_log()
        return observation, reward, done, info

    def _reset(self):
        self._log()
        self.targ_loc = None
        self.agent_loc = None
        self.doors = None
        self.buttons = None

        self.episode_length = 0
        self.episodes += 1
        return self.env.reset()

    def _get_room(self,coord):
        if np.abs(coord[0]) > 10 or np.abs(coord[1]) > 10:
            return 0
        if coord[0] <= 0:
            if coord[1] <= 0:
                return 3
            else:
                return 4
        else:
            if coord[1] <= 0:
                return 2
            else:
                return 1

    def _grab_pos(self):
        qpos = np.array(self.unwrapped_env.model.data.qpos.flat)
        self.doors = self.unwrapped_env.switches_on
        self.agent_loc = self._get_room(qpos[:2])
        self.targ_loc = self._get_room(qpos[-2:])

    def _buffer_log(self):
        self._grab_pos()
        self.buf_log += '\t'.join([str(self.steps), str(self.episodes), str(self.episode_length),
                 '\t'.join(map(str, self.doors)), str(self.agent_loc), str(self.targ_loc)]) + '\n'

    def _log(self):
        with open(self.path, 'a') as csv_file:
            csv_file.write(self.buf_log)
        self.buf_log = ""

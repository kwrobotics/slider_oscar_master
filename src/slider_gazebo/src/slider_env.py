import gym
from gym import utils
import rospy
from std_srvs.srv import Empty
from std_msgs.msg import Float64
from gazebo_msgs.msg import LinkStates


class SliderEnv(gym.Env):
    def __init__(self):
        utils.EzPickle.__init__(self)

        self.TorsoPos_before      = 0.0
        self.TorsoPos_now         = 0.0
        self.time_before          = 0.0
        self.time_now             = 0.0
        self.self.joint_state     = np.zeros(10)
		self.self.joint_vel       = np.zeros(10)
		self.joint_command        = np.zeros(10)
		self.self.joint_effort    = np.zeros(10)

        rospy.init_node('Slider_Env', anonymous=True)
        # get Pelvis position for determine whether the robot falls or not
		rospy.Subscriber("/gazebo/link_states", LinkStates, self.getState, queue_size=1)
		# get all joint positions and velocities
		rospy.Subscriber("/slider_gazebo/left_hip_pitch_position_controller/state", JointControllerState,    self.lhipPitchCallback, queue_size=1)
		rospy.Subscriber("/slider_gazebo/left_hip_roll_position_controller/state", JointControllerState,     self.lhipRollCallback, queue_size=1)
		rospy.Subscriber("/slider_gazebo/left_hip_slide_position_controller/state", JointControllerState,    self.lhipSlideCallback, queue_size=1)
		rospy.Subscriber("/slider_gazebo/left_ankle_pitch_position_controller/state", JointControllerState,  self.lanklePitchCallback, queue_size=1)
		rospy.Subscriber("/slider_gazebo/left_ankle_roll_position_controller/state", JointControllerState,   self.lankleRollCallback, queue_size=1)
		rospy.Subscriber("/slider_gazebo/right_hip_pitch_position_controller/state", JointControllerState,   self.rhipPitchCallback, queue_size=1)
		rospy.Subscriber("/slider_gazebo/right_hip_roll_position_controller/state", JointControllerState,    self.rhipRollCallback, queue_size=1)
		rospy.Subscriber("/slider_gazebo/right_hip_slide_position_controller/state", JointControllerState,   self.rhipSlideCallback, queue_size=1)
		rospy.Subscriber("/slider_gazebo/right_ankle_pitch_position_controller/state", JointControllerState, self.ranklePitchCallback, queue_size=1)
		rospy.Subscriber("/slider_gazebo/right_ankle_roll_position_controller/state", JointControllerState,  self.rankleRollCallback, queue_size=1)

		self.pub_left_hip_pitch    = rospy.Publisher('/slider_gazebo/left_hip_pitch_position_controller/command', Float64, queue_size=1)
		self.pub_left_hip_roll     = rospy.Publisher('/slider_gazebo/left_hip_roll_position_controller/command', Float64, queue_size=1)
		self.pub_left_hip_slide    = rospy.Publisher('/slider_gazebo/left_hip_slide_position_controller/command', Float64, queue_size=1)
		self.pub_left_ankle_roll   = rospy.Publisher('/slider_gazebo/left_ankle_roll_position_controller/command', Float64, queue_size=1)
		self.pub_left_ankle_pitch  = rospy.Publisher('/slider_gazebo/left_ankle_pitch_position_controller/command', Float64, queue_size=1)

		self.pub_right_hip_pitch   = rospy.Publisher('/slider_gazebo/right_hip_pitch_position_controller/command', Float64, queue_size=1)
		self.pub_right_hip_roll    = rospy.Publisher('/slider_gazebo/right_hip_roll_position_controller/command', Float64, queue_size=1)
		self.pub_right_hip_slide   = rospy.Publisher('/slider_gazebo/right_hip_slide_position_controller/command', Float64, queue_size=1)
		self.pub_right_ankle_roll  = rospy.Publisher('/slider_gazebo/right_ankle_roll_position_controller/command', Float64, queue_size=1)
		self.pub_right_ankle_pitch = rospy.Publisher('/slider_gazebo/right_ankle_pitch_position_controller/command', Float64, queue_size=1)
    
	def slider_RL_joint_publisher(joint_command):
		pub_left_hip_pitch.publish(joint_command[0])
		pub_left_hip_roll.publish(joint_command[1])
		pub_left_hip_slide.publish(joint_command[2])
		pub_left_ankle_roll.publish(joint_command[3])
		pub_left_ankle_pitch.publish(joint_command[4])
		pub_right_hip_pitch.publish(joint_command[5])
		pub_right_hip_roll.publish(joint_command[6])
		pub_right_hip_slide.publish(joint_command[7])
		pub_right_ankle_roll.publish(joint_command[8])
		pub_right_ankle_pitch.publish(joint_command[9])

    def getState(data):
		TorsoPos_before = TorsoPos_now
		TorsoPos_now  = data.pose[1].position.z
		time_before = time_now
		time_now = rospy.get_rostime().nsecs

	def lhipPitchCallback(data):
		self.joint_state[0]  = data.process_value
		self.joint_vel[0]    = data.process_value_dot
		self.joint_effort[0] = data.command

	def lhipRollCallback(data):
		self.joint_state[1]  = data.process_value
		self.joint_vel[1]    = data.process_value_dot
		self.joint_effort[1] = data.command

	def lhipSlideCallback(data):
		self.joint_state[2]  = data.process_value
		self.joint_vel[2]    = data.process_value_dot
		self.joint_effort[2] = data.command

	def lanklePitchCallback(data):
		self.joint_state[3]  = data.process_value
		self.joint_vel[3]    = data.process_value_dot
		self.joint_effort[3] = data.command

	def lankleRollCallback(data):
		self.joint_state[4]  = data.process_value
		self.joint_vel[4]    = data.process_value_dot
		self.joint_effort[4] = data.command

	def rhipPitchCallback(data):
		self.joint_state[5]  = data.process_value
		self.joint_vel[5]    = data.process_value_dot
		self.joint_effort[5] = data.command

	def rhipRollCallback(data):
		self.joint_state[6]  = data.process_value
		self.joint_vel[6]    = data.process_value_dot
		self.joint_effort[6] = data.command

	def rhipSlideCallback(data):
		self.joint_state[7]  = data.process_value
		self.joint_vel[7]    = data.process_value_dot
		self.joint_effort[7] = data.command

	def ranklePitchCallback(data):
		self.joint_state[8]  = data.process_value
		self.joint_vel[8]    = data.process_value_dot
		self.joint_effort[8] = data.command

	def rankleRollCallback(data):
		self.joint_state[9]  = data.process_value
		self.joint_vel[9]    = data.process_value_dot
		self.joint_effort[9] = data.command

	def resetWorld():
		rospy.wait_for_service('/gazebo/reset_world')
		reset_worldProxy = rospy.ServiceProxy('/gazebo/reset_world', Empty)
		reset_worldProxy()

    def step(self, action):
    	# action
    	slider_RL_joint_publisher(action)
    	time_elapsed = (self.time_now-self.time_before)/1e9
    	# reward
		if time_elapsed == 0:
			reward = 0
		else:
			# for moving forward with certain speed, live_bonus integrated
			reward  = 10 - abs((self.TorsoPos_now-self.TorsoPos_before) / (time_elapsed) - 0.15) 
			reward  -= 1e-5*(abs(np.sum(joint_effort)))

		observation = [np.array(np.concatenate([self.joint_state, self.joint_vel]))]

		done = not (np.isfinite(observation).all() and (self.TorsoPos_now < 1.1) and (self.TorsoPos_now > 0.45))

    	return observation, reward, done, {}

	def reset(self):
		for i in range(3):
			slider_RL_joint_publisher(np.zeros(10))
			rospy.sleep(0.2)
		resetWorld()
		slider_RL_joint_publisher(np.zeros(10))
		rospy.sleep(0.3)

		observation = [np.array(np.concatenate([self.joint_state, self.joint_vel]))]
		return observation

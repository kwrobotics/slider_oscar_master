#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 18 14:28:22 2021

@author: robin
"""

import rospy
import numpy as np
import matplotlib.pyplot as plt
import csv

from geometry_msgs.msg import Point, Pose,  Twist, Vector3
from grid_map_msgs.msg import GridMap
from nav_msgs.msg import Odometry
from std_msgs.msg import Float32MultiArray, Float64MultiArray
from shape_msgs.msg import Plane
from matplotlib import colors
from scipy.spatial.transform import Rotation
from numpy.linalg import inv

robot_location = [0, 0, 0]
outer_start_index = 0
inner_start_index = 0
elevation = (0, 0)

column_size = 60
row_size = 60
p_step_0_global = [0, 0]
p_step_1_global = [0, 0]
p_step_2_global = [0, 0]
heights = np.zeros(60)
normals_x = np.zeros(60)
normals_y = np.zeros(60)
normals_z = np.zeros(60)
normal = np.array([0, 0, 1])
d = 0
robot_orientation = [0,0,0,1]
prev_normal = [0,0,0]
prev_heights = [0,0,0]

def plane_callback(msg):

    global outer_start_index, inner_start_index, heights, normals_x, normals_y, normals_z, column_size, row_size
    outer_start_index = msg.outer_start_index
    inner_start_index = msg.inner_start_index
    heights = msg.data[0].data
    normals_x = msg.data[12].data
    normals_y = msg.data[13].data
    normals_z = msg.data[14].data
    column_size = msg.data[0].layout.dim[0].size
    row_size = msg.data[0].layout.dim[1].size

def raw2grid_converter(data, column_size, row_size, outer_start_index, inner_start_index):
    ## Takes tuple or array of data and converts it to a gridded format given a row size and column size 
    ## taking into account start and end indexes 
    ## input data: Nx1 array or tuple or list of data
    ## input column size : int of the column size of array to output
    ## input ros_size : int of the row size of array to output
    ## input outer_start_index: int offset to move start row of array
    ## input inner_start_index: int offset to move start column of array
    ##
    ## output grid: column_size x row_size array of data 
    
    grid = np.zeros((column_size, row_size))

    i, j = 0, 0
    

    for el in data:
        if(i == column_size):
            i = 0
            j += 1
        grid[i,j] = el
        i+=1

    grid = np.concatenate(( grid[outer_start_index:,:], grid[:outer_start_index,:]), axis = 0)
    grid = np.concatenate(( grid[:,:inner_start_index], grid[:,inner_start_index:]), axis = 1)

    
    return grid

def plotter(data2plot, title = ''):
    
    fig, ax = plt.subplots(figsize=(12, 6))
    
    im = ax.imshow(data2plot)

    ax.set_title(title)

    fig.colorbar(im, ax = ax)
    fig.savefig(title)
    
    with open(title+".csv","w+") as my_csv:
        csvWriter = csv.writer(my_csv,delimiter=' ')
        csvWriter.writerows(data2plot)
        
if __name__ == '__main__':

    
    rospy.init_node('heightmap_plotter', anonymous = True)
    sub_elevation = rospy.Subscriber("/elevation_mapping/elevation_map_raw", GridMap, plane_callback)
 

    rate=30
    r = rospy.Rate(rate)


    while not rospy.is_shutdown():
        
        elevation = raw2grid_converter(heights, column_size, row_size, outer_start_index, inner_start_index)
        
        plotter(elevation, 'Height_map')
        
        
        
        
        
        
        
        
        
        
        
        
        
        
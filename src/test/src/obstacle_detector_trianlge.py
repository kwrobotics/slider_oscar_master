#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct 15 17:11:15 2021

@author: robin
"""

import cv2
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Polygon
from matplotlib.collections import PatchCollection
from numpy import genfromtxt
import triangle as tr


seed = 9
np.random.seed(seed)

# image properties
width = 256
height = 256
img = np.zeros((width, height))

# obstacle generation
#minsize = 30
#maxsize = 120
#N_obstacles = 5
#cx = np.random.randint(minsize, width, N_obstacles)
#cy = np.random.randint(minsize, height, N_obstacles)
#wx = np.random.randint(minsize, maxsize, N_obstacles)
#wy = np.random.randint(minsize, maxsize, N_obstacles)
#
#for x, y, w, h in zip(cx, cy, wx, wy):
#    print('Centre: ', (x, y), '\tDimensions: ', (w, h))
#    img[x - int(w/2): x + int(w/2), y - int(h/2): y + int(h/2)] = 1
    
raw_trav = genfromtxt('/home/robin/test_ws/src/test/src/Traversability.csv', delimiter=' ')
raw_trav = np.nan_to_num(raw_trav, nan = 0)
  
raw_trav[raw_trav>0.5] = 1
raw_trav[raw_trav<=0.5] = 0
raw_trav = abs(raw_trav-1)
raw_trav = raw_trav.astype('uint8')

raw_trav = raw_trav[ 320:370,220:250]
#raw_trav = raw_trav[ 200:400,200:300]


img = raw_trav

img = np.pad(img, ((5, 5), (5, 5)), 'constant', constant_values=(0, 0))
plt.imshow(img, cmap='jet')
plt.show()
img = cv2.erode(img.astype(np.float32), np.ones((3, 3), np.uint8))
img = cv2.dilate(img, np.ones((3, 3), np.uint8))

img = cv2.dilate(img, np.ones((5, 5), np.uint8))
img = cv2.erode(img.astype(np.float32), np.ones((5, 5), np.uint8))

#dst = cv2.cornerHarris(img.astype(np.float32), 2, 5, 0.04)
dst = cv2.goodFeaturesToTrack(img, maxCorners = 1000, qualityLevel = 0.01, minDistance = 2 )
dst = dst[:,0,:]

#xcoords, ycoords = np.where(pltdst > 0.01*dst.max())
xcoords, ycoords = dst[:,0], dst[:,1]


coords = np.array([[y, x] for x, y in zip(xcoords, ycoords)])
plt.show()
# pltdst = dst
pltimg = np.copy(img)
#pltimg[pltdst > 0.01*pltdst.max()] = 0.5
for i in range(len(coords)):
    x1, y1 = coords[i]
    pltimg[int(x1), int(y1)] = 0.5

plt.imshow(pltimg, cmap='jet')
plt.show()

edges = []

cimg = cv2.dilate(img, np.ones((5, 5), np.uint8))
print(coords.T)
#cimg = np.copy(img)
plt.imshow(cimg, cmap='jet')
edge_flag = np.zeros((len(coords), len(coords)))
for i in range(len(coords)):
    x1, y1 = coords[i]
    for j in range(len(coords)):
        x2, y2 = coords[j]
        dist = np.sqrt((x1 - x2)**2 + (y1 - y2)**2)
        if dist > 2:
            x_check = np.linspace(x1, x2, int(dist), dtype=np.int)
            y_check = np.linspace(y1, y2, int(dist), dtype=np.int)


            check = np.sum(cimg[x_check, y_check]) / int(dist)
            if check >= 1.0 and edge_flag[i, j] == 0:
                plt.plot([y1, y2], [x1, x2])
                edges.append([[x1, y1], [x2, y2]])
                edge_flag[i, j] = 1
                edge_flag[j, i] = 1


plt.show()

from shapely.geometry import LineString

i_flags = np.zeros(len(edges))
non_intersecting_edges = []
for i in range(len(edges)):
    for j in range(len(edges)):
        if i_flags[i] == 0:
            if i != j:
                A = edges[i][0]
                B = edges[i][1]
                C = edges[j][0]
                D = edges[j][1]

                if A == C or A == D or B == C or B == D:
                    pass
                else:
                    e1 = LineString([A, B])
                    e2 = LineString([C, D])
                    if e1.intersects(e2):
                        try:
                            inter_point = e1.intersection(e2)
                            intercep_point = np.array([inter_point.x, inter_point.y])
                            x_inter, y_inter = inter_point.x, inter_point.y
                            dists = [np.linalg.norm(A-intercep_point),
                                     np.linalg.norm(B-intercep_point),
                                     np.linalg.norm(C-intercep_point),
                                     np.linalg.norm(D-intercep_point)]
                        except:
                            pass
                        
                        if any( el < 1 for el in dists):
                            pass
                        else:
                            #cimg[int(x_inter), int(y_inter)] = 0.5
                            i_flags[i] = 1
                            

                            
    if i_flags[i] == 0:
        non_intersecting_edges.append(edges[i])
        
plt.show()

plt.imshow(cimg, cmap='jet')
segs = []
for i in range(len(non_intersecting_edges)):
    idx1 = (np.sum(np.abs(coords - non_intersecting_edges[i][0]), axis=1)).argmin()
    idx2 = (np.sum(np.abs(coords - non_intersecting_edges[i][1]), axis=1)).argmin()

    A = non_intersecting_edges[i][0]
    B = non_intersecting_edges[i][1]
    segs.append([idx1, idx2])
    plt.plot([A[1], B[1]], [A[0], B[0]])
plt.show()

print(len(non_intersecting_edges))
print(segs)
print(np.array(segs).T)

A = dict(vertices=coords,
         segments=np.array(segs),
         # holes=np.array([[70, 88]])
         )


B = tr.triangulate(A, 'pa65536')
tr.compare(plt, A, B)
plt.show()
# print(B)
patches = []
i = 0
plt.imshow(cimg, cmap='jet')

for a, b, c in B['triangles']:
    x1, y1 = B['vertices'][a]
    x2, y2 = B['vertices'][b]
    x3, y3 = B['vertices'][c]
    area = 0.5* (x1*(y2-y3) + x2*(y3-y1)+ x3*(y1-y2))
    tri = np.array([[y1, x1], [y2, x2], [y3, x3]], dtype=np.int32)
    print(area)
    if area > 5:
        patches.append(Polygon(tri, True))
    i += 1

p = PatchCollection(patches, alpha=0.4, facecolor=None, edgecolor=[1.0, 0.0, 1.0, 1], linewidth=2.0)
ax = plt.gca()
ax.add_collection(p)
plt.show()
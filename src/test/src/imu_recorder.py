#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 25 15:47:15 2021

@author: O.Helander
"""

import rospy
import geometry_msgs.msg as geo_mes
from geometry_msgs.msg import Point, Pose,  Twist, Vector3
from nav_msgs.msg import Odometry
from sensor_msgs.msg import JointState
from scipy.spatial.transform import Rotation as R
from std_msgs.msg import Float32MultiArray, Float64MultiArray
import csv
import numpy as np
from bno055_usb_stick_py import BnoUsbStick


[x,y,z,w] = [0,0,0,1]

pub = rospy.Publisher('/JointState', JointState, queue_size=10)

odom_pub = rospy.Publisher("odom", Odometry, queue_size=10)

euler_pub = rospy.Publisher("/imu/euler", Float64MultiArray, queue_size = 10)

def do_analysis():
    #reads reference values
    #reads measured values
    #compares and comptes scores
    #return scores
    return 

if __name__ == '__main__':
    rospy.init_node("imu_recorder")
    rate=rospy.Rate(30)
    imu = BnoUsbStick(port="/dev/ttyACM0")
    imu.activate_streaming()
    
    pub = rospy.Publisher('imu_quat', geo_mes.Quaternion, queue_size=10)
    
    f = open("/home/robin/catkin_ws/src/test/csv's/ankle_imu", "w") #CHANGE DIS!!!!
    
    writer = csv.writer(f)
    
    writer.writerow(["Time [s]", "pitch [deg.]", "yaw [deg.]", "roll [deg.]"])
    
    rate = 50
    ros_r = rospy.Rate(rate)


    while not rospy.is_shutdown():
        packet = imu.recv_streaming_packet()
        accel = packet.a
        quat = packet.quaternion
        [x,y,z,w] = quat
        
        r = R.from_quat([x,y,z,w])
        r_as_euler= r.as_euler('zxy', degrees=True)
        to_publish = Float64MultiArray()
        to_publish.data = r_as_euler
        euler_pub.publish(to_publish)
        
        current_time = rospy.get_time()
        row = [current_time, r_as_euler[0], r_as_euler[1], r_as_euler[2] ]
        writer.writerow(row)
        
        ros_r.sleep()
    f.close()
    exit()




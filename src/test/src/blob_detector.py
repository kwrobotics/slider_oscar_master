#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Oct  1 13:43:16 2021

@author: oHelander
"""
import numpy as np
import matplotlib.pyplot as plt
import cv2
import rospy 

from grid_map_msgs.msg import GridMap
from nav_msgs.msg import Odometry, OccupancyGrid

from numpy import genfromtxt
from updated_gridmap_listener import raw2grid_converter
from std_msgs.msg import Float32MultiArray, Float64MultiArray
from environment_benchtests import triangle_analysis

# Set up the SimpleBlobdetector with default parameters.
outer_start_index = 0
inner_start_index = 0
column_size = 60
row_size = 60
traversability = np.zeros([column_size,row_size])
robot_orientation = [0,0,0,1]
robot_location = [0, 0, 0]
trav_map = np.zeros([column_size,row_size])
map_width = 60
map_height = 60


def get_ellipses(raw_trav):
   # raw_trav = genfromtxt('traversability.csv', delimiter=',')
    #im_trav = Image.fromarray(raw_trav)
#    fig, ax = plt.subplots()
#    p = plt.imshow(raw_trav) 
#
#    ax.set_xticks(np.arange(0, int(row_size), 10));
#    ax.set_yticks(np.arange(0, int(column_size), 10));
#    fig.colorbar(p, ax = ax)
#    plt.show()
  
    raw_trav = np.nan_to_num(raw_trav, nan = 1)
  
    raw_trav[raw_trav>0.8] = 255
    raw_trav[raw_trav<0.8] = 0
    raw_trav = raw_trav.astype('uint8')

    raw_trav = raw_trav[120:180, 120:180]
    raw_trav = np.pad(raw_trav, pad_width=20, mode='constant', constant_values=255)
    #raw_trav = abs(255-raw_trav)
    
    
    ret, thresh = cv2.threshold(raw_trav, 150, 255, cv2.THRESH_BINARY)
    contours, hierarchy = cv2.findContours(image=thresh, mode=cv2.RETR_TREE, method=cv2.CHAIN_APPROX_NONE)
    # draw contours on the original image
    min_threshold_area = 100
    max_threshold_area = 5000
    ellipses = {"ellipse": [],
                "areas" : []}
    for cnt in contours:        
        area = cv2.contourArea(cnt)         
        if area > min_threshold_area and area < max_threshold_area: 
            cv2.drawContours(image=raw_trav, contours=cnt, contourIdx=-1, color=(135, 0, 0), thickness=2, lineType=cv2.LINE_AA)
            ellipse = cv2.fitEllipse(cnt)
            cv2.ellipse(raw_trav,ellipse,(135,0,0),1)
            ellipses["ellipse"].append(ellipse)
            ellipses["areas"].append(area)
            
#    cv2.imshow("keypoints", raw_trav )
#    cv2.waitKey(0)
    return ellipses 

def odom_callback(data):

    global robot_location, robot_orientation
    odom = data
    robot_location = [odom.pose.pose.position.x, 
                      odom.pose.pose.position.y,
                      odom.pose.pose.position.z]

    robot_orientation = [odom.pose.pose.orientation.x,
                         odom.pose.pose.orientation.y,
                         odom.pose.pose.orientation.z,
                         odom.pose.pose.orientation.w]
    
def traversability_callback(msg):
    
    global outer_start_index, inner_start_index, traversability_slope
    global traversability_step, traversability_roughness, traversability, column_size, row_size
    
    outer_start_index = msg.outer_start_index
    inner_start_index = msg.inner_start_index
    traversability_slope = msg.data[6].data
    traversability_step = msg.data[7].data
    traversability_roughness = msg.data[8].data
    #traversability = msg.data[9].data
    column_size = msg.data[0].layout.dim[0].size
    row_size = msg.data[0].layout.dim[1].size
    
    
def trav_map_callback(msg):
    global trav_map, map_width, map_height, traversability
    trav_map = msg.data
    map_width = msg.info.width
    map_height = msg.info.height
    
    trav_map = np.reshape(np.array(trav_map), (map_width, map_height))
    trav_map[trav_map<0] = 100
    trav_map = abs(trav_map-100)
    trav_map = np.rot90(trav_map)
    traversability = np.fliplr(trav_map)/100

if __name__ == '__main__':

    global traversability_slope2plot, traversability_step2plot, traversability_roughness2plot, traversability2plot
    #listen to traversability map
    rospy.init_node('traversability_listener', anonymous = True)
    traversability_sub = rospy.Subscriber("/traversability_estimation/traversability_map", GridMap, traversability_callback)
    odom_sub = rospy.Subscriber("/odom", Odometry, odom_callback)
    traversability_map_sub = rospy.Subscriber("/traversability_map_visualization/traversability_map", OccupancyGrid , trav_map_callback)
   
    shapes_publisher = rospy.Publisher("/time_slider_gazebo/footstep_plan/shapes", Float64MultiArray, queue_size=1) 

    rate=30
    r = rospy.Rate(rate)
    
    mode = 'triangle'
    while not rospy.is_shutdown():

        #traversability = np.array(traversability).flatten()
        
       # traversability = raw2grid_converter(traversability, column_size, row_size, outer_start_index, inner_start_index)
       shapes_to_publish = Float64MultiArray()
 
       if mode == 'ellipse':
            ellipses = get_ellipses(traversability)
            shapes_to_publish.data = [5,0,0.01,0.01,0,
                                        5,0,0.01,0.01,0,
                                        5,0,0.01,0.01,0,
                                        5,0,0.01,0.01,0,
                                        5,0,0.01,0.01,0
                                        ]
            indexes = np.argsort(ellipses['areas'])
            for index, counter in zip(indexes, range(len(ellipses['areas']))):
                shapes_to_publish.data[counter*5] = -(ellipses['ellipse'][index][0][1]-40)/100 + robot_location[0]
                shapes_to_publish.data[counter*5+1] = (ellipses['ellipse'][index][0][0]-40)/100 + robot_location[1]
                shapes_to_publish.data[counter*5+2] = ellipses['ellipse'][index][1][0]
                shapes_to_publish.data[counter*5+3] = ellipses['ellipse'][index][1][1]           
                shapes_to_publish.data[counter*5+4] = ellipses['ellipse'][index][2]
                
       elif mode == 'triangle':
            triangles = triangle_analysis(traversability)
            shapes_to_publish.data = [5,0,5,0,5,0,
                                     # 5,0,5,0,5,0,
                                     # 5,0,5,0,5,0,
                                     # 5,0,5,0,5,0,
                                     # 5,0,5,0,5,0
                                     ]
            indexes = np.argsort(triangles['area'])
            for index, counter in zip(indexes, range(len(triangles['areas']))):
                shapes_to_publish.data[counter*5] = -(triangles['ellipse'][index][0]-40)/100 + robot_location[0]
                shapes_to_publish.data[counter*5+1] = (triangles['ellipse'][index][1]-40)/100 + robot_location[1]
                shapes_to_publish.data[counter*5+2] = -(triangles['ellipse'][index][2]-40)/100 + robot_location[0]
                shapes_to_publish.data[counter*5+3] = (triangles['ellipse'][index][3]-40)/100 + robot_location[1]        
                shapes_to_publish.data[counter*5+4] = -(triangles['ellipse'][index][4]-40)/100 + robot_location[0]            
                shapes_to_publish.data[counter*5+5] = (triangles['ellipse'][index][5]-40)/100 + robot_location[1]
        

#            
       # print(ellipses_to_publish.data)
       shapes_publisher.publish(shapes_to_publish)
















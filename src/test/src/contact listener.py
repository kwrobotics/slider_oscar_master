#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 18 15:08:51 2021

@author: robin
"""

import numpy as np 
import trollius
from trollius import From

import pygazebo
import pygazebo.msg.contacts_pb2

def publish_loop(topic_name):
    manager = yield From(pygazebo.connect()) 

    def callback(data):
        print(data)
        
    subscriber = manager.subscribe(topic_name,
                     'gazebo.msgs.Contact',
                     callback)

    yield From(subscriber.wait_for_connection())
    while(True):
        yield From(trollius.sleep(1.00))
        print('wait...')
    print(dir(manager))
    import logging

    logging.basicConfig()

    loop = trollius.get_event_loop()
    loop.run_until_complete(publish_loop())
    
    
if __name__ == '__main__':
    
    topic_names=[]
    
    topic_name = "/gazebo/default/time_slider_gazebo/Right_Foot/right_pad1_contact"
    
    publish_loop(topic_name)
    
#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Mon Jul 19 15:57:42 2021

@author: robin
"""
import rospy
import tf 
import geometry_msgs.msg as geo_mes
from geometry_msgs.msg import Point, Pose,  Twist, Vector3
from nav_msgs.msg import Odometry
from sensor_msgs.msg import JointState
from scipy.spatial.transform import Rotation as R
from pyquaternion import Quaternion
from std_msgs.msg import Float32MultiArray, Float64MultiArray


base_to_left_leg = 5
base_to_right_leg = -2


pub = rospy.Publisher('/JointState', JointState, queue_size=10)

odom_pub = rospy.Publisher("odom", Odometry, queue_size=10)

euler_pub = rospy.Publisher("/imu/euler", Float64MultiArray, queue_size = 10)

def callback(data):
    global x, y, z, w
#    global x
#    x= data.x
    [x,y,z,w] = [data.x, data.y, data.z, data.w]
#    print([x,y,z,w] )
    r = R.from_quat([x,y,z,w])
    
    
    
def transform_callback(data):
    
    global quat
    quat = data
    br = tf.TransformBroadcaster()
    [x,y,z,w] = [data.x, data.y, data.z, data.w]
    r = R.from_quat([x,y,z,w])
    #print(r.as_euler('zxy', degrees=True))

   # euler = Vector3
    r_as_euler= r.as_euler('zxy', degrees=True)
  #  [euler.x, euler.y, euler.z] = [r_as_euler[0], r_as_euler[1], r_as_euler[2]]
   # rospy.init_node('IMU_orientation')
    r_as_euler= r.as_euler('zxy', degrees=True)
#    [euler.x, euler.y, euler.z] = [r_as_euler[0], r_as_euler[1], r_as_euler[2]]
    to_publish = Float64MultiArray()
    to_publish.data = r_as_euler
    euler_pub.publish(to_publish)
    
    r=Quaternion([x,y,z,w])
    

    correction = R.from_euler('yxz', [-90,-90,0], degrees=True )
    correction = correction.as_quat()
    correction = Quaternion(correction)
    
    [x,y,z,w] =r*correction
    
    
    br.sendTransform((0, 0, 0),
                      (x,y,z,w),
                     rospy.Time.now(),
                     "base_link",
                     "world")    
    
#    joint_state = JointState()
#    joint_state.name = ['base_to_right_leg', 'base_to_left_leg']
#    joint_state.position = [base_to_right_leg, base_to_left_leg]
#    pub.publish(joint_state)
#    
#    odom = Odometry()
#    odom.header.stamp = rospy.Time.now()
#    odom.header.frame_id = "odom"
#
#    # set the position
#    odom.pose.pose = Pose(Point(0, 0, 0.),Quaternion(x,y,z,w))
#
#    # set the velocity
#    odom.child_frame_id = "base_link"
#    odom.twist.twist = Twist(Vector3(0, 0, 0), Vector3(0, 0, 0))
#
#
#    # publish the message
#    odom_pub.publish(odom)
    
    
    

def joint_space_callback(data):
    return 
    
def IMU_listener():

    # In ROS, nodes are uniquely named. If two nodes with the same
    # name are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
    global quat
    
    rospy.init_node('IMU_listener', anonymous=True)

    rospy.Subscriber("imu_quat", geo_mes.Quaternion, transform_callback)
       

    
#    pos = (0, 0, 0)
#    rot = quat
#    
#    rate = rospy.Rate(5)
#    
#    b = tf.TranformerBroadcaster()
#    
#    while not rospy.is_shutdown():
#        b.sendTransform(pos, rot, rospy.Time.now(), 'slider', '/world')
#        rate.sleep()
#    
#
#    
#    
    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()



if __name__ == '__main__':
    IMU_listener()



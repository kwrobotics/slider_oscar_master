#! /usr/bin/env python3

import rospy
import geometry_msgs.msg as geo_mes
import numpy as np
from bno055_usb_stick_py import BnoUsbStick

rospy.init_node("testPy")
rate=rospy.Rate(30)
imu = BnoUsbStick(port="/dev/ttyACM0")
imu.activate_streaming()

pub = rospy.Publisher('imu_quat', geo_mes.Quaternion, queue_size=10)


while not rospy.is_shutdown():
    packet = imu.recv_streaming_packet()
    accel = packet.a
    quat = packet.quaternion
    Quat = geo_mes.Quaternion(*quat)
    pub.publish(Quat)


    rate.sleep()

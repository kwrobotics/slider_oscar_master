#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 30 10:56:10 2021

@author: Ohelander
"""
from numpy import genfromtxt
import numpy as np

column_size = 5
row_size = 5
traversability = np.zeros([row_size, column_size])
end_state = [3,3]
diag_forward_penalty = -1
side_penalty = -2
diag_back_penalty = -3
back_penalty = -4 

class state:
    def __init__(self, state=[row_size/2, column_size/2]):
        self.map = traversability
        self.state = state
        self.isEnd = False
        self.deterministic = True 
        self.end_state = end_state
        
    def define_map(self, vals):
        self.map = vals
        
    def give_reward(self):
        if self.state == end_state:
            return 100
        else:
            return self.map[self.state[0], self.state[1]]
        
    def isEndfunc(self):
        if self.state == end_state:
            self.isEnd = True
            
    def next_state(self, action):
        if self.deterministic:
            if action == "N":
                nxt_state = [self.state[0]+1, self.state[1]]
            elif action == "S":
                nxt_state = [self.state[0]-1, self.state[1]]
            elif action == "E":
                nxt_state = [self.state[0], self.state[1]+1]
            elif action == "W":
                nxt_state = [self.state[0], self.state[1]-1]            
            elif action == "NE":
                nxt_state = [self.state[0]+1, self.state[1]+1]
            elif action == "NW":
                nxt_state = [self.state[0]+1, self.state[1]-1]
            elif action == "SE":
                nxt_state = [self.state[0]-1, self.state[1]+1]
            elif action == "SW":
                nxt_state = [self.state[0]-1, self.state[1]-1]   
            
            if (nxt_state[0] >= 0) and (nxt_state[0] <= (row_size -1)):
                if (nxt_state[1] >= 0) and (nxt_state[1] <= (column_size -1)):
                    if nxt_state != (1, 1):
                        return nxt_state
            return self.state
        
class agent():
    def __init__(self):
        self.states = []
        self.actions = ["N", "NE", "E", "SE", "S", "SW", "W", "NW"]
        self.action_costs = [0, diag_forward_penalty, side_penalty, diag_back_penalty, 
                             back_penalty, diag_back_penalty, side_penalty, diag_forward_penalty]
        self.state = state()
        self.lr = 0.2
        self.exp_rate = 0.3

        # initial state reward
        self.state_values = {}
        for i in range(row_size):
            for j in range(column_size):
                self.state_values[(i, j)] = 0        
                
        def takeAction(self, action):
            position = self.state.next_state(action)
            return state(state=position)

        def reset(self):
            self.states = []
            self.state = state()

        def play(self, rounds=10):
            i = 0
            while i < rounds:
                # to the end of game back propagate reward
                if self.state.isEnd:
                    # back propagate
                    reward = self.state.give_reward()
                    # explicitly assign end state to reward values
                    self.state_values[self.state.state] = reward  # this is optional
                    print("Game End Reward", reward)
                    for s in reversed(self.states):
                        reward = self.state_values[s] + self.lr * (reward - self.state_values[s])
                        self.state_values[s] = round(reward, 3)
                    self.reset()
                    i += 1
                else:
                    action = self.chooseAction()
                    # append trace
                    self.states.append(self.State.nxtPosition(action))
                    print("current position {} action {}".format(self.State.state, action))
                    # by taking the action, it reaches the next state
                    self.State = self.takeAction(action)
                    # mark is end
                    self.State.isEndFunc()
                    print("nxt state", self.State.state)
                    print("---------------------")
            
if __name__ == "__main__":
    traversability = genfromtxt('traversability.csv', delimiter=',')
    [row_size, column_size] = np.shape(traversability) 
    ag = agent()
    ag.play(50)









#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Sep  5 14:47:50 2021

@author: O.Helander
"""

import rospy
import numpy as np
import matplotlib.pyplot as plt

from geometry_msgs.msg import Point, Pose,  Twist, Vector3
from nav_msgs.msg import Odometry
from std_msgs.msg import Float32MultiArray, Float64MultiArray, Int64MultiArray
from shape_msgs.msg import Plane
from matplotlib import colors
from scipy.spatial.transform import Rotation
from numpy.linalg import inv
pads_R = np.zeros(12)
pads_L = np.zeros(12)
time = []
pads_signal_R = [np.zeros(12),np.zeros(12)]
pads_signal_L = [np.zeros(12),np.zeros(12)]


def pressure_pad_callback(msg):
    global pads_R, pads_L
    
    if len(msg.data) == 24:
  
        pads_R = np.array( msg.data[:12])
        pads_L = np.array( msg.data[12:])
    
    
    
def check_pad_stable(signal):
    mean_thresh = 1
    varaince_thresh = 2
   # print(signal)

    if (np.mean(signal)> mean_thresh): #and (np.std(signal)< varaince_thresh):
        return True
    else:
        return False

def check_foot_stable(signals):
    signals = np.array(signals)
    signals[np.isnan(signals)] = 0
    pad_A_x = signals[:,0]
    pad_A_y = signals[:,1]
    pad_A_z = signals[:,2]

    pad_B_x = signals[:,3]
    pad_B_y = signals[:,4]
    pad_B_z = signals[:,5]
    
    pad_C_x = signals[:,6]
    pad_C_y = signals[:,7]
    pad_C_z = signals[:,8]
    
    pad_D_x = signals[:,9]
    pad_D_y = signals[:,10]
    pad_D_z = signals[:,11]
    
    if check_pad_stable(pad_A_z) and check_pad_stable(pad_B_z) and check_pad_stable(pad_C_z) and check_pad_stable(pad_D_z):
        stable = True
    else:
        stable = False

    
    if check_pad_stable(pad_A_z) or check_pad_stable(pad_B_z) or check_pad_stable(pad_C_z) or check_pad_stable(pad_D_z):
        contact = True
    else:
        contact = False

    return stable, contact
    

if __name__ == '__main__':

    rospy.init_node('contact_detector', anonymous = True)
    pressure_pad_sub = rospy.Subscriber("/slider_gazebo/pad_forces", Float64MultiArray, pressure_pad_callback )
    stability_publisher = rospy.Publisher("/slider_gazebo/stability",
                                          Int64MultiArray, queue_size=1)

    rate=300
    array_lim = 2
    r = rospy.Rate(rate)


    while not rospy.is_shutdown():
        
        current_time = rospy.get_time()
        time.append(current_time)
        pads_signal_R.append(pads_R)
        pads_signal_L.append(pads_L)
        
        if len(pads_signal_R) > array_lim:
            time.pop(0)
            pads_signal_R.pop(0)
            pads_signal_L.pop(0)
            
        stable_R, contact_R = check_foot_stable(pads_signal_R)
        stable_L, contact_L = check_foot_stable(pads_signal_L)
#        if stable_R:
#            print("Right foot stable")
#        elif contact_R:
#            print("Right foot contact")
##        else:
##            print("Right foot off")
#            
#        if stable_L:
#            print("Left foot stable")
#        elif contact_L:
#            print("Left foot contact")
#        else:
#            print("Left foot off")           
#        if check_foot_stable(pads_signal_L):
#            print("Left foot  stable af boiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii")
#        else:
#            print("Left foot not stable boiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii")
        status_to_publish = Int64MultiArray()
        
        status_to_publish.data = 1*np.array([stable_R, contact_R, stable_L, contact_L]).tolist()
        
        stability_publisher.publish(status_to_publish)
       
        r.sleep()
        
        
        
        
        
        
        
        
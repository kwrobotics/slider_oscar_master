#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 15 18:00:30 2021

@author: O.Helander
"""


import rospy
import numpy as np
import matplotlib.pyplot as plt
import csv

from geometry_msgs.msg import Point, Pose,  Twist, Vector3
from grid_map_msgs.msg import GridMap
from nav_msgs.msg import Odometry
from std_msgs.msg import Float32MultiArray, Float64MultiArray
from shape_msgs.msg import Plane
from matplotlib import colors
from scipy.spatial.transform import Rotation
from numpy.linalg import inv
from updated_gridmap_listener import raw2grid_converter, grid_query, plane_callback



outer_start_index = 0
inner_start_index = 0
column_size = 60
row_size = 60
traversability_slope = np.zeros(column_size*row_size)
traversability_step = np.zeros(column_size*row_size)
traversability_roughness = np.zeros(column_size*row_size)
traversability = np.zeros(column_size*row_size)
heights = np.zeros(column_size*row_size)
normals_x = np.zeros(column_size*row_size)
normals_y = np.zeros(column_size*row_size)
normals_z = np.zeros(column_size*row_size)
p_step_0_global = [0, 0]
p_step_1_global = [0, 0]
p_step_2_global = [0, 0]
robot_location = [0, 0, 0]
robot_orientation = [0,0,0,1]

class path_trajectory:
    
    def __init__(self):
        self.trajectory=[] #list of 3x1 np arrays (x,y,theta) x,y being position next point and theta is the heading 
        self.trajectory_cost = 0
        self.start = [0,0]
        self.end = [0,0]
        self.traversability_map=  np.zeros((2,2))
        self.segment_length = 0.1
    
    def update_traversability(self, new_map):
        self.traversability_map = new_map
        
    def create_curve():
        #init with straight path
        # compute cost
        #grad descent until final path found 
        return 
    def get_cost(self):
        p0 = np.array([0,0, 0])
        pcurrent = p0
        for point in self.trajectory:
           segment = point - pcurrent 
            
        
    
#class generated curve
    #end pose, 
    #functions: create curve, get curve, get cost of curve, 
def traversability_callback(msg):
    
    global outer_start_index, inner_start_index, traversability_slope
    global traversability_step, traversability_roughness, traversability, column_size, row_size
    
    outer_start_index = msg.outer_start_index
    inner_start_index = msg.inner_start_index
    traversability_slope = msg.data[6].data
    traversability_step = msg.data[7].data
    traversability_roughness = msg.data[8].data
    traversability = msg.data[9].data
    column_size = msg.data[0].layout.dim[0].size
    row_size = msg.data[0].layout.dim[1].size
    
def traversability_plotter():
    
    fig, ax = plt.subplots(nrows=2, ncols=2, sharex=True,
                                        figsize=(12, 6))
    
    im0 = ax[0,0].imshow(traversability_slope2plot)
    im1 = ax[0,1].imshow(traversability_step2plot)
    im2 = ax[1,0].imshow(traversability_roughness2plot)
    im3 = ax[1,1].imshow(traversability2plot)

    ax[0,0].set_title("Traversability slope")
    ax[0,1].set_title("Traversability step")
    ax[1,0].set_title("Traversability roughness")
    ax[1,1].set_title("Traversability")
    
    
    fig.colorbar(im0, ax = ax[0,0])
    fig.colorbar(im1, ax = ax[0,1])
    fig.colorbar(im2, ax = ax[1,0])
    fig.colorbar(im3, ax = ax[1,1])
    
    fig.savefig("Traversabilities")
    
    with open("traversability.csv","w+") as my_csv:
        csvWriter = csv.writer(my_csv,delimiter=' ')
        csvWriter.writerows(traversability2plot)

def footstep_callback(data):
    
    global p_step_0_global, p_step_1_global, p_step_2_global
    [x_0, y_0, x_1, y_1, x_2, y_2] = np.array(data.data[:6])
    p_step_0_global = [x_0, y_0]
    p_step_1_global = [x_1, y_1]
    p_step_2_global = [x_2, y_2]

def odom_callback(data):

    global robot_location, robot_orientation
    odom = data
    robot_location = [odom.pose.pose.position.x, 
                      odom.pose.pose.position.y,
                      odom.pose.pose.position.z]

    robot_orientation = [odom.pose.pose.orientation.x,
                         odom.pose.pose.orientation.y,
                         odom.pose.pose.orientation.z,
                         odom.pose.pose.orientation.w]   

def adapt_foothold(point):
    ##query_shape
    [x_centered, y_centered] = point

    
    rectangle = [6,9]
    x = np.linspace(int(x_centered)-rectangle[0], int(x_centered)+rectangle[0], 2*rectangle[0]+1)
    y = np.linspace(int(y_centered)-rectangle[1], int(y_centered)+rectangle[1], 2*rectangle[1]+1)
    X,Y = np.meshgrid(x,y)
    positions = np.vstack([ X.ravel(), Y.ravel()])
    travs = []
    costs = []
    for x_point, y_point in zip(positions[0], positions[1]):
        trav = traversability[int(x_point), int(y_point)]
        travs.append(trav)
        cost = get_cost(trav, [x_point, y_point], [x_centered, y_centered])
        costs.append(cost)
    costs = np.array(costs)
    min_idx = np.argmin(costs)
    
    opt_point = [positions[0][min_idx], positions[1][min_idx]]
#    print(costs)
#    print(travs)
    return opt_point
    


def get_cost(trav, point, center_point):
    w_trav, w_dist = 1, 1
    c_trav = 1/trav
    c_dist = np.linalg.norm(np.array(point)-np.array(center_point))
    
    return w_trav*c_trav + w_dist*c_dist 
    


if __name__ == '__main__':
    
    global traversability_slope2plot, traversability_step2plot, traversability_roughness2plot, traversability2plot

    #listen to traversability map
    rospy.init_node('traversability_listener', anonymous = True)

    traversability_sub = rospy.Subscriber("/traversability_estimation/traversability_map", GridMap, traversability_callback)
    sub_plan = rospy.Subscriber("/time_slider_gazebo/footstep_plan", Float64MultiArray, footstep_callback)
    odom_sub = rospy.Subscriber("/odom", Odometry, odom_callback)

    rate=30
    r = rospy.Rate(rate)


    while not rospy.is_shutdown():

        traversability_slope = np.array(traversability_slope).flatten()
        traversability_step = np.array(traversability_step).flatten()
        traversability_roughness = np.array(traversability_roughness).flatten()
        traversability = np.array(traversability).flatten()
        
        traversability_slope = raw2grid_converter(traversability_slope, column_size , row_size, outer_start_index, inner_start_index)
        traversability_step = raw2grid_converter(traversability_step, column_size , row_size, outer_start_index, inner_start_index)
        traversability_roughness = raw2grid_converter(traversability_roughness, column_size, row_size, outer_start_index, inner_start_index)
        traversability = raw2grid_converter(traversability, column_size, row_size, outer_start_index, inner_start_index)
        
        if (np.nanvar(traversability_slope) != 0) or (np.nanmean(traversability_slope) != 0):
             np.reshape(np.array(trav_map), (map_width, map_height))
            traversability_slope2plot = np.rot90(traversability_slope)
        if (np.nanvar(traversability_step) != 0) or (np.nanmean(traversability_step) != 0):
            traversability_step2plot = np.rot90(traversability_step)
        if (np.nanvar(traversability_roughness) != 0) or (np.nanmean(traversability_roughness) != 0):
            traversability_roughness2plot = np.rot90(traversability_roughness)
        if (np.nanvar(traversability) != 0) or (np.nanmean(traversability) != 0):
            traversability2plot = np.rot90(traversability)
 
            
#            
#        p_step_0_local = np.array(p_step_0_global) - np.array(robot_location[:2]) 
#        p_step_1_local = np.array(p_step_1_global) - np.array(robot_location[:2])
#        p_step_2_local = np.array(p_step_2_global) - np.array(robot_location[:2])
        
#        print('-------')
#        print(robot_location)
#        print(p_step_0_global)
#        print(p_step_1_local)

#        unit_conv = 100 #traversabilityconversion from meters to cm
#        p_step_0_local = unit_conv*np.array([-p_step_0_local[1],p_step_0_local[0]])
#        p_step_1_local = unit_conv*np.array([-p_step_1_local[1],p_step_1_local[0]])
#        p_step_2_local = unit_conv*np.array([-p_step_2_local[1],p_step_2_local[0]])
#        
#        p_step_0_local = p_step_0_local + [(row_size/2), (column_size/2)] 
#        p_step_1_local = p_step_1_local + [(row_size/2), (column_size/2)] 
#        p_step_2_local = p_step_2_local + [(row_size/2), (column_size/2)] 
#        
#        p_step_0_local = p_step_0_local.astype(int)
#        p_step_1_local = p_step_1_local.astype(int)
#        p_step_2_local = p_step_2_local.astype(int)       
#        
#        print(p_step_0_global)
#        print(p_step_1_global)
#        print(p_step_1_local)
#        
#        try:    
#            p_step_1_trav = {'trav_slope': traversability_slope[p_step_1_local[0], p_step_1_local[1]] ,
#                             'trav_step': traversability_step[p_step_1_local[0], p_step_1_local[1]] ,
#                             'trav_rough': traversability_roughness[p_step_1_local[0], p_step_1_local[1]] ,
#                             'trav':traversability[p_step_1_local[0], p_step_1_local[1]] }
#            print("adapted foothold is")
#            print(adapt_foothold(p_step_1_local))
#        except:
#            p_step_1_trav = {'trav_slope':1 ,
#                             'trav_step': 1 ,
#                             'trav_rough': 1 ,
#                             'trav':1 }
#            print("--")

   
    #take target position
    #

#    print(p_step_1_trav)
#    print(adapt_foothold(p_step_1_local))

    traversability_plotter()









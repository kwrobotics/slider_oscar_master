#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug  5 11:32:34 2021

@author: Oskar Helander, Peter Tisnikar
"""

import rospy
import numpy as np
import matplotlib.pyplot as plt

from geometry_msgs.msg import Point, Pose,  Twist, Vector3
from grid_map_msgs.msg import GridMap
from nav_msgs.msg import Odometry
from std_msgs.msg import Float32MultiArray, Float64MultiArray
from shape_msgs.msg import Plane
from matplotlib import colors
from scipy.spatial.transform import Rotation
from numpy.linalg import inv
from scipy.ndimage import convolve as sciconvolve 

robot_location = [0, 0, 0]
outer_start_index = 0
inner_start_index = 0
elevation = (0, 0)
column_size = 1
row_size = 1
x_0 = 0
y_0 = 0
x_1 = 0
y_1 = 0
x_2 = 0
y_2 = 0
x_0f = 0
y_0f = 0
x_1f = 0
y_1f = 0
x_2f = 0
y_2f = 0
z_1f = 0
heights = np.zeros(60)
normal = np.array([0, 0, 1])
d = 0
robot_orientation = [0,0,0,1]


   
def odom_callback(data):

    global robot_location, robot_orientation
    odom = data
    robot_location = [odom.pose.pose.position.x, 
                      odom.pose.pose.position.y,
                      odom.pose.pose.position.z]

    robot_orientation = [odom.pose.pose.orientation.x,
                         odom.pose.pose.orientation.y,
                         odom.pose.pose.orientation.z,
                         odom.pose.pose.orientation.w]
    
    
def plane_callback(msg):

    global outer_start_index, inner_start_index, elevation, heights, column_size, row_size
    outer_start_index = msg.outer_start_index
    inner_start_index = msg.inner_start_index
    heights = msg.data[0].data                      # msg is a 15x60x60 Tensor of NP arrays
    column_size = msg.data[0].layout.dim[0].size
    row_size = msg.data[0].layout.dim[1].size


def point_inv_transform(point, translation, quat):
    ## takes point in body frame and returns global frame coorinates given 
    ## translation and rotation from the global frame to the body frame
    ## input point: 3x1 array of position
    ## input translation : 3x1 array of translation from global to body frame
    ## input quat : 4x1 array of quaternions from global to body frame

    R = Rotation.from_quat(quat)
    R = R.as_matrix()

    R_inv = inv(R)
    R_inv = np.array(R_inv)


    trans = np.array(translation)
    trans = np.reshape(trans, (3,1))

    temp = np.concatenate((R_inv, -np.matmul(R_inv,trans)), axis = 1)

    M_inv = np.concatenate((np.array(temp), np.array([[0,0,0,1]])), axis = 0)  

    point_local = np.insert(point, len(point), 1)
    point_global = np.matmul(M_inv, point_local)


    return point_global[:3] 


def convolve2d(slab,kernel,max_missing=0.5,verbose=True):
    '''2D convolution with missings ignored

    <slab>: 2d array. Input array to convolve. Can have np.nan or masked values.
    <kernel>: 2d array, convolution kernel, must have sizes as odd numbers.
    <max_missing>: float in (0,1), max percentage of missing in each convolution
                   window is tolerated before a missing is placed in the result.

    Return <result>: 2d array, convolution result. Missings are represented as
                     np.nans if they are in <slab>, or masked if they are masked
                     in <slab>.

    '''



    assert np.ndim(slab)==2, "<slab> needs to be 2D."
    assert np.ndim(kernel)==2, "<kernel> needs to be 2D."
    assert kernel.shape[0]%2==1 and kernel.shape[1]%2==1, "<kernel> shape needs to be an odd number."
    assert max_missing > 0 and max_missing < 1, "<max_missing> needs to be a float in (0,1)."

    #--------------Get mask for missings--------------
    if not hasattr(slab,'mask') and np.any(np.isnan(slab))==False:
        has_missing=False
        slab2=slab.copy()

    elif not hasattr(slab,'mask') and np.any(np.isnan(slab)):
        has_missing=True
        slabmask=np.where(np.isnan(slab),1,0)
        slab2=slab.copy()
        missing_as='nan'

    elif (slab.mask.size==1 and slab.mask==False) or np.any(slab.mask)==False:
        has_missing=False
        slab2=slab.copy()

    elif not (slab.mask.size==1 and slab.mask==False) and np.any(slab.mask):
        has_missing=True
        slabmask=np.where(slab.mask,1,0)
        slab2=np.where(slabmask==1,np.nan,slab)
        missing_as='mask'

    else:
        has_missing=False
        slab2=slab.copy()

    #--------------------No missing--------------------
    if not has_missing:
        result=sciconvolve(slab2,kernel,mode='constant',cval=0.)
    else:
        H,W=slab.shape
        hh=int((kernel.shape[0]-1)/2)  # half height
        hw=int((kernel.shape[1]-1)/2)  # half width
        min_valid=(1-max_missing)*kernel.shape[0]*kernel.shape[1]

        # dont forget to flip the kernel
        kernel_flip=kernel[::-1,::-1]

        result=sciconvolve(slab2,kernel,mode='constant',cval=0.)
        slab2=np.where(slabmask==1,0,slab2)

        #------------------Get nan holes------------------
        miss_idx=zip(*np.where(slabmask==1))

        if missing_as=='mask':
            mask=np.zeros([H,W])

        for yii,xii in miss_idx:

            #-------Recompute at each new nan in result-------
            hole_ys=range(max(0,yii-hh),min(H,yii+hh+1))
            hole_xs=range(max(0,xii-hw),min(W,xii+hw+1))

            for hi in hole_ys:
                for hj in hole_xs:
                    hi1=max(0,hi-hh)
                    hi2=min(H,hi+hh+1)
                    hj1=max(0,hj-hw)
                    hj2=min(W,hj+hw+1)

                    slab_window=slab2[hi1:hi2,hj1:hj2]
                    mask_window=slabmask[hi1:hi2,hj1:hj2]
                    kernel_ij=kernel_flip[max(0,hh-hi):min(hh*2+1,hh+H-hi), 
                                     max(0,hw-hj):min(hw*2+1,hw+W-hj)]
                    kernel_ij=np.where(mask_window==1,0,kernel_ij)

                    #----Fill with missing if not enough valid data----
                    ksum=np.sum(kernel_ij)
                    if ksum<min_valid:
                        if missing_as=='nan':
                            result[hi,hj]=np.nan
                        elif missing_as=='mask':
                            result[hi,hj]=0.
                            mask[hi,hj]=True
                    else:
                        result[hi,hj]=np.sum(slab_window*kernel_ij)

        if missing_as=='mask':
            result=np.ma.array(result)
            result.mask=mask

    return result


def footstep_callback(data):
    
    global x_0f, y_0f, x_1f, y_1f, x_2f, y_2f
    [x_0f, y_0f, x_1f, y_1f, x_2f, y_2f] = np.array(data.data[:6])
     

if __name__ == '__main__':

    
    rospy.init_node('plane_calculator', anonymous = True)
    # sub_plan = rospy.Subscriber("/time_slider_gazebo/footstep_plan", Float64MultiArray, footstep_callback)
    sub_elevation = rospy.Subscriber("/elevation_mapping/elevation_map_raw", GridMap, plane_callback)
    odom_sub = rospy.Subscriber("/odom", Odometry, odom_callback)

    publisher = rospy.Publisher("plane_eq", Float64MultiArray, queue_size=1)
    

    rate=30
    r = rospy.Rate(rate)


    while not rospy.is_shutdown():

        data2plot = np.zeros((column_size, row_size))

        i, j = 0, 0

        try:
            for el in heights:
                if(i == column_size):
                    i = 0
                    j += 1
                data2plot[i,j] = el
                i+=1

            data2plot = np.concatenate(( data2plot[outer_start_index-0:,:],data2plot[:outer_start_index-0,:]), axis = 0)
            data2plot = np.concatenate(( data2plot[:,:inner_start_index],data2plot[:,inner_start_index:]), axis = 1)
        except:
            pass
            
        

        kernel = np.ones((3,3)) /9
        
        data2plot = convolve2d(data2plot, kernel,max_missing=0.9995)


        # p_0_global = np.array([x_0, y_0])
        # p_1_global = np.array([x_1, y_1])
        # p_2_global = np.array([x_2, y_2])

        x_0f -= robot_location[0] 
        y_0f-= robot_location[1]
        x_1f -= robot_location[0] 
        y_1f -= robot_location[1]
        x_2f -= robot_location[0] 
        y_2f -= robot_location[1]
    
        x_0f, y_0f, x_1f, y_1f, x_2f, y_2f = 100*x_0f, 100*y_0f,100*x_1f, 100*y_1f, 100*x_2f, 100*y_2f
        
        x_0f = (row_size/2)+x_0
        y_0f +=(column_size/2)
        x_1f = (row_size/2)+x_1
        y_1f +=(column_size/2)
        x_2f = (row_size/2)+x_2
        y_2f +=(column_size/2)

  

    
        # x_0 -= robot_location[0] 
        # y_0 -= robot_location[1]
        # x_1 -= robot_location[0] 
        # y_1 -= robot_location[1]
        # x_2 -= robot_location[0] 
        # y_2 -= robot_location[1]
    
        # x_0, y_0, x_1, y_1, x_2, y_2 = 10*x_0, 10*y_0,10*x_1, 10*y_1, 10*x_2, 10*y_2


        x_0, y_0 = 0, -10
        x_1, y_1 = 20,-20
        x_2, y_2 = -20,-20


        p_0_global = np.array([robot_location[0] +y_0/100 , robot_location[1]-x_0/100])
        p_1_global = np.array([robot_location[0] +y_1/100  , robot_location[1]-x_1/100])
        p_2_global = np.array([robot_location[0] +y_2/100, robot_location[1]-x_2/100])

        x_0 = (row_size/2)+x_0
        y_0 +=(column_size/2)
        x_1 = (row_size/2)+x_1
        y_1 +=(column_size/2)
        x_2 = (row_size/2)+x_2
        y_2 +=(column_size/2)


        height_offset = 0.15
        try:
            z_0 = 1*(data2plot[int(x_0),int(y_0)]-height_offset)
            z_1 = 1*(data2plot[int(x_1),int(y_1)]-height_offset)
            z_2 = 1*(data2plot[int(x_2),int(y_2)]-height_offset)
            z_1f = 1*(data2plot[int(x_1f),int(y_1f)]-height_offset)
 
        except:
            z_0 = 0
            z_1 = 0
            z_2 = 0
    


        p_0_global = np.insert(p_0_global, len(p_0_global), z_0)
        p_1_global = np.insert(p_1_global, len(p_1_global), z_1)
        p_2_global = np.insert(p_2_global, len(p_2_global), z_2)


        # print("global points are-----------------------------------------")
        # print(p_0_global)
        # print(p_1_global)
        # print(p_2_global)
        # print("--------------------------------------------------------------------")
  

        ux, uy, uz  = [p_1_global[0]-p_0_global[0], p_1_global[1]-p_0_global[1], z_1-z_0]
        vx, vy, vz  = [p_2_global[0]-p_0_global[0], p_2_global[1]-p_0_global[1], z_2-z_0]
       
        x_0, y_0 = 0, -10
        x_1, y_1 = 20,-20
        x_2, y_2 = -20,-20

        # ux, uy, uz  = [(x_1-x_0)/100, (y_1-y_0)/100, z_1-z_0]
        # vx, vy, vz  = [(x_2-x_0)/100, (y_2-y_0)/100, z_2-z_0]
    
        u_cross_v = [uy*vz-uz*vy, uz*vx-ux*vz, ux*vy-uy*vx]
    
        normal = np.array(u_cross_v)

        if normal[2]<0:
            normal = -normal
    
        print("normal is-----------------------------------------")
        print(normal)
        print(z_1f)
        print("--------------------------------------------------------------------")

        d_0 = np.array(p_0_global).dot(normal)
        d_1 = np.array(p_1_global).dot(normal)
        d_2 = np.array(p_2_global).dot(normal)

        print("Da D's are-----------------------------------------")
        print(d_0)
        print(d_1)
        print(d_2)

        print("--------------------------------------------------------------------")

        if d_0>0:
            d = -d_0
        else:
            d = d_0




    
     #   p = plt.imshow(data2plot) 

    #    ax.set_xticks(np.arange(0, int(row_size), 10));
   #     ax.set_yticks(np.arange(0, int(column_size), 10));
  #      plt.plot(x_0, y_0, 'bo',markersize=5)
 #       plt.plot(x_1, y_1, 'bo',markersize=5)
#        plt.plot(x_2, y_2, 'bo',markersize=5)
    
        to_publish = Float64MultiArray()
        thresh = 0.0005

        if np.isnan(normal).any() == True or normal[2] == 0 or np.isnan(z_1f) == True :
            to_publish.data = [0, 0, 0, 0]

        # elif abs(normal[0]/normal[2])<thresh or abs(normal[1]/normal[2])<thresh:
        #     to_publish.data = [0, 0, 0]

        else:
            to_publish.data = [abs(-normal[0]/normal[2]), -normal[1]/normal[2], -abs(d/normal[2]), z_1f]
            print("Da Normal is ------------------------------------------------------------")
            print( [abs(normal[0]/normal[2]), -normal[1]/normal[2], -abs(d/normal[2]), z_1f])
            print("--------------------------------------------------------------------")

    
        publisher.publish(to_publish)
    
#       plane = Plane()
#
#       plane.coef = [normal[0], normal[1], normal[2], d]
#    
#       publisher.publish(plane)

        r.sleep()
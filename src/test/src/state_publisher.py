#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 20 14:58:35 2021

@author: robin
"""

#listen for imu orientation
#extract orientation from quaternions and make odometry measurements
#publish robot state
from math import sin, cos, pi
import rclpy
from rclpy.node import Node
from rclpy.qos import QoSProfile
from geometry_msgs.msg import Quaternion
from sensor_msgs.msg import JointState
from tf import TransformBroadcaster, TransformStamped
import rospy
import geometry_msgs.msg as geo_mes

base_link_quat = None

class StatePublisher(Node):

    def __init__(self):
        rclpy.init()
        super().__init__('state_publisher')

        qos_profile = QoSProfile(depth=10)
        self.joint_pub = self.create_publisher(JointState, 'joint_states', qos_profile)
        self.broadcaster = TransformBroadcaster(self, qos=qos_profile)
        self.nodeName = self.get_name()
        self.get_logger().info("{0} started".format(self.nodeName))

        degree = pi / 180.0
        loop_rate = self.create_rate(30)

        # robot state
        base_to_left_leg = 0
        base_to_right_leg = 0
        tilt = 0.
        tinc = degree
        swivel = 0.
        angle = 0.
        height = 0.
        hinc = 0.005

        # message declarations
        odom_trans = TransformStamped()
        odom_trans.header.frame_id = 'odom'
        odom_trans.child_frame_id = 'axis'
        joint_state = JointState()

        try:
            while rclpy.ok():
                rclpy.spin_once(self)
                
                rospy.init_node('IMU_listener', anonymous=True)
                
                rospy.Subscriber("imu_quat", geo_mes.Quaternion, transform_callback)


                # update joint_state
                now = self.get_clock().now()
                
                joint_state.header.stamp = now.to_msg()
                joint_state.name = ['base_to_right_leg', 'base_to_left_leg']
                joint_state.position = [base_to_right_leg, base_to_left_leg]

                # update transform
                # (moving in a circle with radius=2)
#                odom_trans.header.stamp = now.to_msg()
#                odom_trans.transform.translation.x = cos(angle)*2
#                odom_trans.transform.translation.y = sin(angle)*2
#                odom_trans.transform.translation.z = 0.7
                odom_trans.transform.rotation = base_link_quat # roll,pitch,yaw

                # send the joint state and transform
                self.joint_pub.publish(joint_state)
                self.broadcaster.sendTransform(odom_trans)

                # Create new robot state
#                tilt += tinc
#                if tilt < -0.5 or tilt > 0.0:
#                    tinc *= -1
#                height += hinc
#                if height > 0.2 or height < 0.0:
#                    hinc *= -1
#                swivel += degree
#                angle += degree/4

                # This will adjust as needed per iteration
                loop_rate.sleep()

        except KeyboardInterrupt:
            pass
        
        
def transform_callback(data):
    global base_link_quat
    base_link_quat = data
        
    
def IMU_listener():

    rospy.init_node('IMU_listener', anonymous=True)

    rospy.Subscriber("imu_quat", geo_mes.Quaternion, transform_callback)
       
    rospy.spin()


def main():
    node = StatePublisher()

if __name__ == '__main__':
    main()

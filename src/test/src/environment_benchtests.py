#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct 19 11:24:06 2021

@author: robin
"""

import numpy as np
import matplotlib.pyplot as plt
import cv2
import triangle as tr

from matplotlib import patches
from matplotlib.collections import PatchCollection
from matplotlib.patches import Polygon
from nav_msgs.msg import Odometry, OccupancyGrid
from numpy import genfromtxt

def triangle_analysis(env, axs):
    img = env
    img = cv2.erode(img.astype(np.float32), np.ones((3, 3), np.uint8))
    img = cv2.dilate(img, np.ones((3, 3), np.uint8))
    
    img = cv2.dilate(img, np.ones((3, 3), np.uint8))
    img = cv2.erode(img.astype(np.float32), np.ones((3, 3), np.uint8))
    
    #dst = cv2.cornerHarris(img.astype(np.float32), 2, 5, 0.04)
    dst = cv2.goodFeaturesToTrack(img, maxCorners = 1000, qualityLevel = 0.01, minDistance = 2 )
    dst = dst[:,0,:]
    xcoords, ycoords = dst[:,0], dst[:,1]
    coords = np.array([[y, x] for x, y in zip(xcoords, ycoords)])
    pltimg = np.copy(img)
    for i in range(len(coords)):
        x1, y1 = coords[i]
        pltimg[int(x1), int(y1)] = 0.5
    edges = []
    
    cimg = cv2.dilate(img, np.ones((3, 3), np.uint8))
    edge_flag = np.zeros((len(coords), len(coords)))
    for i in range(len(coords)):
        x1, y1 = coords[i]
        for j in range(len(coords)):
            x2, y2 = coords[j]
            dist = np.sqrt((x1 - x2)**2 + (y1 - y2)**2)
            if dist > 2:
                x_check = np.linspace(x1, x2, int(dist), dtype=np.int)
                y_check = np.linspace(y1, y2, int(dist), dtype=np.int)
    
                check = np.sum(cimg[x_check, y_check]) / int(dist)
                if check >= 1.0 and edge_flag[i, j] == 0:
                    edges.append([[x1, y1], [x2, y2]])
                    edge_flag[i, j] = 1
                    edge_flag[j, i] = 1
    
    from shapely.geometry import LineString
    
    i_flags = np.zeros(len(edges))
    non_intersecting_edges = []
    for i in range(len(edges)):
        for j in range(len(edges)):
            if i_flags[i] == 0:
                if i != j:
                    A = edges[i][0]
                    B = edges[i][1]
                    C = edges[j][0]
                    D = edges[j][1]
    
                    if A == C or A == D or B == C or B == D:
                        pass
                    else:
                        e1 = LineString([A, B])
                        e2 = LineString([C, D])
                        
#                        xmid = int((e1.coords[0][0]+e1.coords[1][0])/2)
#                        ymid = int((e1.coords[0][1]+e1.coords[1][1])/2)
#                        cimg[int(xmid), int(ymid)] = 0.5
                        
                        if e1.intersects(e2):
                            
                            try:
                                inter_point = e1.intersection(e2)
                                intercep_point = np.array([inter_point.x, inter_point.y])
                                x_inter, y_inter = inter_point.x, inter_point.y
                                dists = [np.linalg.norm(A-intercep_point),
                                         np.linalg.norm(B-intercep_point),
                                         np.linalg.norm(C-intercep_point),
                                         np.linalg.norm(D-intercep_point)]
                            except:
                                pass

                            
#                            print([xmid,ymid])
#                            if env[xmid,ymid] == 0:
#                                i_flags[i] = 1
                                
                            if any( el < 1 for el in dists) :
                                pass
                            else:
#                                cimg[int(x_inter), int(y_inter)] = 0.7
                                i_flags[i] = 1
                                
        if i_flags[i] == 0:
            non_intersecting_edges.append(edges[i])
            
    segs = []
    for i in range(len(non_intersecting_edges)):
        idx1 = (np.sum(np.abs(coords - non_intersecting_edges[i][0]), axis=1)).argmin()
        idx2 = (np.sum(np.abs(coords - non_intersecting_edges[i][1]), axis=1)).argmin()

    
        A = non_intersecting_edges[i][0]
        B = non_intersecting_edges[i][1]

        segs.append([idx1, idx2])
        
    A = dict(vertices=coords,
             segments=np.array(segs),
             # holes=np.array([[70, 88]])
             )
    
    
    B = tr.triangulate(A, 'pa65536')

    patches = []
    i = 0
    axs.imshow(cimg, cmap='jet')
    areas = []
    out = {'vertices':[],
           'area': []
            }
    for a, b, c in B['triangles']:
        x1, y1 = B['vertices'][a]
        x2, y2 = B['vertices'][b]
        x3, y3 = B['vertices'][c]
        area = 0.5* (x1*(y2-y3) + x2*(y3-y1)+ x3*(y1-y2))
        tri = np.array([[y1, x1], [y2, x2], [y3, x3]], dtype=np.int32)
        #print(area)
        center = [int((x1+x2+x3)/3), int((y1+y2+y3)/3)]
        if area > 5 and cimg[center[0], center[1]]==1:
            patches.append(Polygon(tri, True))
            areas.append(area)
            out['vertices'].append([x1, y1, x2, y2, x3, y3])
            out['area'].append(area)
        i += 1
    
    p = PatchCollection(patches, alpha=0.4, facecolor=None, edgecolor=[1.0, 0.0, 1.0, 1], linewidth=2.0)
    axs.add_collection(p)
    return out


def add_obstacle(env, center = [0,0], length = 1, width= 1, shape= 'rec', val = 1.0):
    
    if shape == 'rec':
        env[int(center[0]-length/2):int(center[0]+length/2), int(center[1]-width/2):int(center[1]+width/2)] = val
    if shape == 'circ':
        for i in range (np.shape(env)[0]):
            for j in range(np.shape(env)[1]):
                if ((i-center[0])**2+(j-center[1])**2 )<= length**2:
                    env[i,j] = val


def get_ellipses(raw_trav, axs):
    axs.imshow(raw_trav)
    raw_trav = raw_trav.astype('uint8')

   # raw_trav = np.pad(raw_trav, pad_width=10, mode='constant', constant_values=0)
    
    ret, thresh = cv2.threshold(raw_trav, 0.5, 1, cv2.THRESH_BINARY)
    contours, hierarchy = cv2.findContours(image=thresh, mode=cv2.RETR_TREE, method=cv2.CHAIN_APPROX_NONE)
    # draw contours on the original image

    ellipses = {"ellipse": [],
                "areas" : []}
    for cnt in contours:        
        area = cv2.contourArea(cnt)         
        ellipse = cv2.fitEllipse(cnt)
        ellipses["ellipse"].append(ellipse)
        e_area = (ellipse[1][0]/2.0) * (ellipse[1][1]/2.0) * np.pi
        ellipses["areas"].append(e_area)
    
    for el in ellipses['ellipse']:
        e1=patches.Ellipse(el[0], el[1][0], 
                           el[1][1], el[2],linewidth=2, fill=False)
        axs.add_patch(e1)
    return sum(ellipses['areas'])

row_size = 50
collumn_size = 50

edge_env = np.zeros((row_size, collumn_size))
sqdonut_env = np.zeros((row_size, collumn_size))
corner_env = np.zeros((row_size, collumn_size))
circle_env = np.zeros((row_size, collumn_size))
donut_env = np.zeros((row_size, collumn_size))

raw_trav = genfromtxt('/home/robin/test_ws/src/test/src/Traversability.csv', delimiter=' ')
raw_trav = np.nan_to_num(raw_trav, nan = 0)
  
raw_trav[raw_trav>0.5] = 1
raw_trav[raw_trav<=0.5] = 0
raw_trav = abs(raw_trav-1)
raw_trav = raw_trav.astype('uint8')

#raw_trav = raw_trav[ 320:370,220:250]
raw_trav = raw_trav[ 200:230,230:260]
raw_trav = np.pad(raw_trav, pad_width=5, mode='constant', constant_values=0)

envs = {'envs': [],
        'areas': [],
        'ellipse area': [],
        'triangle area': []}


add_obstacle(edge_env, center = [int(row_size/2),int(collumn_size/2)], length = 20, width = 5)

add_obstacle(sqdonut_env, center = [int(row_size/2),int(collumn_size/2)], length = 20, width =  20)
add_obstacle(sqdonut_env, center = [int(row_size/2),int(collumn_size/2)], length = 10, width = 10, val = 0)

add_obstacle(corner_env, center = [int(row_size/2),int(collumn_size/2)], length = 20, width = 5)
add_obstacle(corner_env, center = [35,32], length = 5, width = 20)

add_obstacle(circle_env, center = [int(row_size/2),int(collumn_size/2)], length = 10, width = 1, shape = 'circ')


add_obstacle(donut_env, center = [int(row_size/2),int(collumn_size/2)], length = 15, width = 1, shape = 'circ')
add_obstacle(donut_env, center = [int(row_size/2),int(collumn_size/2)], length = 7, width = 1, shape = 'circ', val = 0)

fig, ax = plt.subplots(nrows=3, ncols=2, sharex=False,
                                        figsize=(12, 6))
envs['envs'].append(edge_env)
envs['envs'].append(sqdonut_env)
envs['envs'].append(corner_env)
envs['envs'].append(circle_env)
envs['envs'].append(abs(-donut_env))
envs['envs'].append(raw_trav)

for en in envs['envs']:
    envs['areas'].append(np.count_nonzero(en))

ax_list=[]
for aa in ax:
    for a in aa:
        ax_list.append(a)

for ax, env in zip(ax_list, envs['envs']):
    ax.imshow(env)




plt.show()


fig, ax = plt.subplots(nrows=3, ncols=2, sharex=False,
                                        figsize=(12, 6))
ax_list=[]
for aa in ax:
    for a in aa:
        ax_list.append(a)
        
for ax, env in zip(ax_list, envs['envs']):
   ellipse = get_ellipses(env, ax)
   envs['ellipse area'].append(ellipse) 

plt.show()

fig, ax = plt.subplots(nrows=3, ncols=2, sharex=False,
                                        figsize=(12, 6))
ax_list=[]
for aa in ax:
    for a in aa:
        ax_list.append(a)
        
for ax, env in zip(ax_list, envs['envs']):   
    tri = triangle_analysis(env, ax)
    envs['triangle area'].append(sum(tri['area']))
    


fig, ax = plt.subplots()
X = np.arange(len(envs['areas']))
plt.bar(X, envs['areas'], alpha = 0.8, label = 'Area' , width = 0.25)
plt.bar(X+0.25, envs['ellipse area'], alpha = 0.8, label = 'Ellipse Area', width = 0.25)
plt.bar(X+0.5, envs['triangle area'], alpha = 0.8, label = 'Triangle Area', width = 0.25)
ax.legend()


fig, ax = plt.subplots()
ellipse_error = np.divide(abs(np.array(envs['ellipse area'])-np.array(envs['areas'])),np.array(envs['areas']))
triangle_error = np.divide(abs(np.array(envs['triangle area'])-np.array(envs['areas'])),np.array(envs['areas']))

plt.plot(ellipse_error, label = 'ellipse error')
plt.plot(triangle_error, label = 'triangle error')
ax.legend()


plt.show()







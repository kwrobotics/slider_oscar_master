#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 18 11:09:51 2021

@author: robin
"""
import rospy
import numpy as np
import matplotlib.pyplot as plt
import csv
from geometry_msgs.msg import Point, Pose,  Twist, Vector3
from grid_map_msgs.msg import GridMap
from nav_msgs.msg import Odometry, OccupancyGrid
from std_msgs.msg import Float32MultiArray, Float64MultiArray
from shape_msgs.msg import Plane
from matplotlib import colors
from scipy.spatial.transform import Rotation
from numpy.linalg import inv

robot_location = [0, 0, 0]
outer_start_index = 0
inner_start_index = 0
elevation = (0, 0)

column_size = 60
row_size = 60
p_step_0_global = [0, 0]
p_step_1_global = [0, 0]
p_step_2_global = [0, 0]
heights = np.zeros(60)
normals_x = np.zeros(60)
normals_y = np.zeros(60)
normals_z = np.zeros(60)
normal = np.array([0, 0, 1])
traversability_slope = np.zeros(column_size*row_size)
traversability_step = np.zeros(column_size*row_size)
traversability_roughness = np.zeros(column_size*row_size)
traversability = np.zeros((column_size,row_size))
d = 0
robot_orientation = [0,0,0,1]
prev_normal = [0,0,0]
prev_heights = [0,0,0]


def trav_map_callback(msg):
    global trav_map, map_width, map_height, traversability
    trav_map = msg.data
    map_width = msg.info.width
    map_height = msg.info.height
    
    trav_map = np.reshape(np.array(trav_map), (map_width, map_height))
    trav_map[trav_map<0] = 100
    trav_map = abs(trav_map-100)
    trav_map = np.rot90(trav_map)
    traversability = np.fliplr(trav_map)/100
    
def plotter(data2plot, title = ''):
    
    fig, ax = plt.subplots(figsize=(12, 6))
    
    im = ax.imshow(data2plot)

    ax.set_title(title)

    fig.colorbar(im, ax = ax)
    fig.savefig(title)
    
    with open(title+".csv","w+") as my_csv:
        csvWriter = csv.writer(my_csv,delimiter=' ')
        csvWriter.writerows(data2plot)
        
        
if __name__ == '__main__':

    global traversability_slope2plot, traversability_step2plot, traversability_roughness2plot, traversability2plot
    #listen to traversability map
    rospy.init_node('trav_plotter', anonymous = True)
    traversability_map_sub = rospy.Subscriber("/traversability_map_visualization/traversability_map", OccupancyGrid , trav_map_callback)
   
    ellipses_publisher = rospy.Publisher("/time_slider_gazebo/footstep_plan/ellipses", Float64MultiArray, queue_size=1) 

    rate=30
    r = rospy.Rate(rate)
    while not rospy.is_shutdown():

        #traversability = np.array(traversability).flatten()
        
       # traversability = raw2grid_converter(traversability, column_size, row_size, outer_start_index, inner_start_index)
        
        plotter(traversability, 'Traversability')
            
            
        
        
            
            
            
            
            
            
            
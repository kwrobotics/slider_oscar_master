#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug 17 14:22:32 2021

@author: robin
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug  5 11:32:34 2021

@author: Oskar Helander, Peter Tisnikar
"""

import rospy
import numpy as np
import matplotlib.pyplot as plt

from geometry_msgs.msg import Point, Pose,  Twist, Vector3
from grid_map_msgs.msg import GridMap
from nav_msgs.msg import Odometry
from std_msgs.msg import Float32MultiArray, Float64MultiArray
from shape_msgs.msg import Plane
from matplotlib import colors
from scipy.spatial.transform import Rotation
from numpy.linalg import inv

robot_location = [0, 0, 0]
outer_start_index = 0
inner_start_index = 0
elevation = (0, 0)
b = [0,0,1]
p_step_1_normal = [0,0,1]
column_size = 60
row_size = 60
p_step_0_global = [0, 0]
p_step_1_global = [0, 0]
p_step_2_global = [0, 0]
heights = np.zeros(60)
normals_x = np.zeros(60)
normals_y = np.zeros(60)
normals_z = np.zeros(60)
normal = np.array([0, 0, 1])
d = 0
robot_orientation = [0,0,0,1]
prev_normal = [0,0,0]
prev_heights = [0,0,0]

   
def odom_callback(data):

    global robot_location, robot_orientation
    odom = data
    robot_location = [odom.pose.pose.position.x, 
                      odom.pose.pose.position.y,
                      odom.pose.pose.position.z]

    robot_orientation = [odom.pose.pose.orientation.x,
                         odom.pose.pose.orientation.y,
                         odom.pose.pose.orientation.z,
                         odom.pose.pose.orientation.w]
    
    
def plane_callback(msg):

    global outer_start_index, inner_start_index, heights, normals_x, normals_y, normals_z, column_size, row_size
    outer_start_index = msg.outer_start_index
    inner_start_index = msg.inner_start_index
    heights = msg.data[0].data
    normals_x = msg.data[12].data
    normals_y = msg.data[13].data
    normals_z = msg.data[14].data
    column_size = msg.data[0].layout.dim[0].size
    row_size = msg.data[0].layout.dim[1].size


def raw2grid_converter(data, column_size, row_size, outer_start_index, inner_start_index):
    ## Takes tuple or array of data and converts it to a gridded format given a row size and column size 
    ## taking into account start and end indexes 
    ## input data: Nx1 array or tuple or list of data
    ## input column size : int of the column size of array to output
    ## input ros_size : int of the row size of array to output
    ## input outer_start_index: int offset to move start row of array
    ## input inner_start_index: int offset to move start column of array
    ##
    ## output grid: column_size x row_size array of data 
    
    grid = np.zeros((column_size, row_size))

    i, j = 0, 0
    

    for el in data:
        if(i == column_size):
            i = 0
            j += 1
        grid[i,j] = el
        i+=1

    grid = np.concatenate(( grid[outer_start_index:,:], grid[:outer_start_index,:]), axis = 0)
    grid = np.concatenate(( grid[:,:inner_start_index], grid[:,inner_start_index:]), axis = 1)

    
    return grid



def footstep_callback(data):
    
    global p_step_0_global, p_step_1_global, p_step_2_global
    [x_0, y_0, x_1, y_1, x_2, y_2] = np.array(data.data[:6])
    p_step_0_global = [x_0, y_0]
    p_step_1_global = [x_1, y_1]
    p_step_2_global = [x_2, y_2]

def grid_query(point, grid):
    # input point: 2x1 array query point in local coordinates
    # tranform global query point to local coordinates
    [x_local, y_local] = point
    # center point to center
    x_centered = x_local + (row_size/2)
    y_centered =  y_local + (column_size/2)
    
    rectangle = [6,9]
    x = np.linspace(int(x_centered)-rectangle[0], int(x_centered)+rectangle[0], 2*rectangle[0]+1)
    y = np.linspace(int(y_centered)-rectangle[1], int(y_centered)+rectangle[1], 2*rectangle[1]+1)
    X,Y = np.meshgrid(x,y)
    positions = np.vstack([Y.ravel(), X.ravel()])
    out = []
    for x_point, y_point in zip(positions[0], positions[1]):
        #query point
        try: 
            out.append(grid[int(x_point), int(y_point)])
        except:
            out = np.nan
    out = np.nanmean(out)
    
    return out
    
def plane_generator(p_0, p_1, p_2):
    
    ux, uy, uz  = [p_1[0]-p_0[0], p_1[1]-p_0[1], p_1[2]-p_0[2]]
    vx, vy, vz  = [p_2[0]-p_0[0], p_2[1]-p_0[1], p_2[2]-p_0[2]] 
   
    u_cross_v = [uy*vz-uz*vy, uz*vx-ux*vz, ux*vy-uy*vx]

    normal = np.array(u_cross_v)

    if normal[2]<0:
        normal = -normal
    

    return normal

def point_grid_plotter():
    return

if __name__ == '__main__':

    
    rospy.init_node('plane_calculator', anonymous = True)
    sub_plan = rospy.Subscriber("/time_slider_gazebo/footstep_plan", Float64MultiArray, footstep_callback)
    sub_elevation = rospy.Subscriber("/elevation_mapping/elevation_map_raw", GridMap, plane_callback)
    odom_sub = rospy.Subscriber("/odom", Odometry, odom_callback)

    plane_publisher = rospy.Publisher("/time_slider_gazebo/footstep_plan/plane_eq", Float64MultiArray, queue_size=1)
    height_publisher = rospy.Publisher("/time_slider_gazebo/footstep_plan/height_query", Float64MultiArray, queue_size=1) 
    normal_publisher = rospy.Publisher("/time_slider_gazebo/footstep_plan/normal_query", Float64MultiArray, queue_size=1) 
    foot_rot_publisher = rospy.Publisher("/time_slider_gazebo/footstep_plan/foot_rot", Float64MultiArray, queue_size=1) 

    rate=30
    r = rospy.Rate(rate)


    while not rospy.is_shutdown():
        
        elevation = raw2grid_converter(heights, column_size, row_size, outer_start_index, inner_start_index)
        
        normal_x = raw2grid_converter(normals_x, column_size, row_size, outer_start_index, inner_start_index)
        normal_y = raw2grid_converter(normals_y, column_size, row_size, outer_start_index, inner_start_index)
        normal_z = raw2grid_converter(normals_z, column_size, row_size, outer_start_index, inner_start_index)

        p_step_0_local = np.array(p_step_0_global) - np.array(robot_location[:2]) #step in local frame [m]
        p_step_1_local = np.array(p_step_1_global) - np.array(robot_location[:2])
        p_step_2_local = np.array(p_step_2_global) - np.array(robot_location[:2])

        unit_conv = 100 #conversion from meters to cm
        p_step_0_local = unit_conv*np.array([-p_step_0_local[1],p_step_0_local[0]])
        p_step_1_local = unit_conv*np.array([-p_step_1_local[1],p_step_1_local[0]])
        p_step_2_local = unit_conv*np.array([-p_step_2_local[1],p_step_2_local[0]])
        
        p_step_0_z = grid_query(p_step_0_local, elevation) # query heights [m]
        p_step_1_z = grid_query(p_step_1_local, elevation)
        p_step_2_z = grid_query(p_step_2_local, elevation)

        p_step_0_normal = [grid_query(p_step_0_local, normal_x),
                           grid_query(p_step_0_local, normal_y),
                           grid_query(p_step_0_local, normal_z)]
        
        p_step_1_normal = [grid_query(p_step_1_local, normal_x),
                           grid_query(p_step_1_local, normal_y),
                           grid_query(p_step_1_local, normal_z)]
        
        p_step_2_normal = [grid_query(p_step_2_local, normal_x),
                           grid_query(p_step_2_local, normal_y),
                           grid_query(p_step_2_local, normal_z)]        

        footstep_normal = plane_generator([p_step_0_global[0], p_step_0_global[1], p_step_0_z],
                                          [p_step_1_global[0], p_step_1_global[1], p_step_1_z],
                                          [p_step_2_global[0], p_step_2_global[1], p_step_2_z])
        
        d_0 = np.array([p_step_0_global[0], p_step_0_global[1], p_step_0_z]).dot(footstep_normal)
        d_1 = np.array([p_step_1_global[0], p_step_1_global[1], p_step_1_z]).dot(footstep_normal)
        d_2 = np.array([p_step_2_global[0], p_step_2_global[1], p_step_2_z]).dot(footstep_normal)
        
#        print("robot location is-----------------------------------------")        
#        print(robot_location)
#        print("footstep_points are-----------------------------------------")        
#        print([p_step_0_global[0], p_step_0_global[1], p_step_0_z],
#              [p_step_1_global[0], p_step_1_global[1], p_step_1_z],
#              [p_step_2_global[0], p_step_2_global[1], p_step_2_z])
#        print("local footsteps are-----------------------------------------")        
#        print([p_step_0_local[0], p_step_0_local[1], p_step_0_z],
#              [p_step_1_local[0], p_step_1_local[1], p_step_1_z],
#              [p_step_2_local[0], p_step_2_local[1], p_step_2_z])
#        print("footstep normals are-------------------------------------------")
#        print(p_step_0_normal)
#        print(p_step_1_normal)
#        print(p_step_2_normal)
#        
#        print("footstep_normal is-----------------------------------------")
#        print(footstep_normal)
#        print("--------------------------------------------------------------------")
#
#        print("Da D's are-----------------------------------------")
#        print(d_0)
#        print(d_1)
#        print(d_2)
#        print("--------------------------------------------------------------------")
#        
        fixed_p_0 = [0,0] #[cm]
        fixed_p_1 = [0,-30]
        fixed_p_2 = [-20,-25]


        fixed_p_0_global = np.array([robot_location[0] -fixed_p_0[1]/100 , robot_location[1]+fixed_p_0[0]/100])
        fixed_p_1_global = np.array([robot_location[0] -fixed_p_1[1]/100  , robot_location[1]+fixed_p_1[0]/100])
        fixed_p_2_global = np.array([robot_location[0] -fixed_p_2[1]/100, robot_location[1]+fixed_p_2[0]/100])
    
        fixed_p_0_z = grid_query(fixed_p_0, elevation)
        fixed_p_1_z = grid_query(fixed_p_1, elevation)
        fixed_p_2_z = grid_query(fixed_p_2, elevation)
        
        
        fixed_p_0_normal = [grid_query(fixed_p_0, normal_x),
                           grid_query(fixed_p_0, normal_y),
                           grid_query(fixed_p_0, normal_z)]
        
        fixed_p_1_normal = [grid_query(fixed_p_1, normal_x),
                           grid_query(fixed_p_1, normal_y),
                           grid_query(fixed_p_1, normal_z)]
        
        fixed_p_2_normal = [grid_query(fixed_p_2, normal_x),
                           grid_query(fixed_p_2, normal_y),
                           grid_query(fixed_p_2, normal_z)] 
        
        
        fixed_point_normal = plane_generator([fixed_p_0_global[0], fixed_p_0_global[1], fixed_p_0_z],
                                             [fixed_p_1_global[0], fixed_p_1_global[1], fixed_p_1_z],
                                             [fixed_p_2_global[0], fixed_p_2_global[1], fixed_p_2_z])
        
        fixed_point_normal = fixed_p_1_normal

        fixed_d_0 = np.array([fixed_p_0_global[0], fixed_p_0_global[1], fixed_p_0_z]).dot(fixed_point_normal)
        fixed_d_1 = np.array([fixed_p_1_global[0], fixed_p_1_global[1], fixed_p_1_z]).dot(fixed_point_normal)
        fixed_d_2 = np.array([fixed_p_2_global[0], fixed_p_2_global[1], fixed_p_2_z]).dot(fixed_point_normal)
        
#        print("fixed_points are-----------------------------------------")        
#        print([fixed_p_0_global[0], fixed_p_0_global[1], fixed_p_0_z],
#              [fixed_p_1_global[0], fixed_p_1_global[1], fixed_p_1_z],
#              [fixed_p_2_global[0], fixed_p_2_global[1], fixed_p_2_z])
#    
        print("fixed point normal is-----------------------------------------")
        print(fixed_point_normal)
        print("--------------------------------------------------------------------")

        print("Da D's are-----------------------------------------")
#        print(fixed_d_0)
        print(fixed_d_1)
#        print(fixed_d_2)
        print("--------------------------------------------------------------------")
   
        to_publish = Float64MultiArray()

        normal = fixed_point_normal
        d = fixed_d_0
        
#        normal = p_step_1_normal
#        d = d_1       
        
#        if d>0:
#            d = -d
#        else:
#            d = d       
#        or np.linalg.norm(normal-prev_normal)>
        
        if np.isnan(normal).any() == True or np.isnan(d) == True or normal[2] == 0 :
            to_publish.data = prev_normal 
        else:
          #  to_publish.data = [abs(-normal[0]/normal[2]), -normal[1]/normal[2], -abs(d/normal[2])]
            to_publish.data = [-normal[0]/normal[2], -normal[1]/normal[2], (d/normal[2])]
            prev_normal = [-normal[0]/normal[2], -normal[1]/normal[2], (d/(normal[2]))]
            print("Da Normal is ------------------------------------------------------------")
            print( [-normal[0]/normal[2], -normal[1]/normal[2], (d/normal[2])])
            print("--------------------------------------------------------------------")

    
        plane_publisher.publish(to_publish)
        
        
        height_to_publish = Float64MultiArray()
        
        heights_to_publish = [p_step_0_z,
                   p_step_1_z,
                   p_step_2_z]

#        heights_to_publish = [fixed_p_2_z,
#                   fixed_p_2_z,
#                   fixed_p_2_z]
        if np.isnan(heights_to_publish).any() == True or (np.array(heights_to_publish) < 0).any() :
            height_to_publish.data = prev_heights
        else:
            height_to_publish.data = heights_to_publish
            prev_heights = heights_to_publish
        height_publisher.publish(height_to_publish)
            
        normals_to_publish = Float64MultiArray()
        
#        normals = fixed_p_0_normal
#        normals.extend(fixed_p_1_normal)
#        normals.extend(fixed_p_2_normal)
       
        normals = p_step_0_normal
        normals.extend(p_step_1_normal)
        normals.extend(p_step_2_normal)        
        
        if np.isnan(normals).any() == True:
            normals_to_publish.data = [0,0,0,
                                  0,0,0,
                                  0,0,0]
        else:
            normals_to_publish.data = normals 
            
        normal_publisher.publish(normals_to_publish)
        
        a = [0,0,1]
        if np.linalg.norm(p_step_1_normal) != 0:
            b = p_step_1_normal/np.linalg.norm(p_step_1_normal)
        else:
            b = p_step_1_normal
        v = np.cross(a,b)
        s = np.linalg.norm(v)
        c = np.dot(a,b)
        vx = [[0, -v[2], v[1]],
              [v[2], 0, -v[0]],
              [-v[1], v[0], 0]
              ]
        print('vx is')
        print(vx)
        vx = np.array(vx)
        
        foot_rot_matrix = np.identity(3) + vx + np.matmul(vx,vx)*1/(1+c)
        foot_rot_to_publish = Float64MultiArray()
        foot_rot_to_publish.data = foot_rot_matrix.flatten()
        print(foot_rot_matrix)
        foot_rot_publisher.publish(foot_rot_to_publish)

        r.sleep()
        
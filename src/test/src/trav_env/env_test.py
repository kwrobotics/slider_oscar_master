#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct  3 16:33:25 2021

@author: OHelander
"""
import gym 
import numpy as np
import matplotlib.pyplot as plt

from numpy import genfromtxt
from stable_baselines.common.env_checker import check_env

from stable_baselines.common.policies import MlpPolicy
from stable_baselines.common.vec_env import DummyVecEnv
from stable_baselines import PPO2


if __name__ == '__main__':

    env = gym.make("trav_env:traversability_env-v0")
    check_env(env)
    env.render()
    
    model = PPO2(MlpPolicy, env, verbose=1)
    model.learn(total_timesteps=10000)
    
    obs = env.reset()
    actions = []
    for i in range(100):
        action, _states = model.predict(obs)
        actions.append(action)
        obs, rewards, dones, info = env.step(action)
        #env.render()
    print(actions)
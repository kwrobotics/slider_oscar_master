#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct  3 16:27:13 2021

@author: robin
"""

from gym.envs.registration import register

register(
    id='traversability_env-v0',
    entry_point='trav_env.envs:traversabilityEnv',
)

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 30 16:34:52 2021

@author: OHelander
"""

import gym 
import numpy as np
import matplotlib.pyplot as plt

from numpy import genfromtxt


class traversabilityEnv(gym.Env):
    
    def __init__(self, traversabilities = genfromtxt('/home/robin/test_ws/src/test/src/traversability.csv', delimiter=','),
                 start_state = np.array([40,270]), end_state = np.array([150,0])):
        
        self.rows, self.columns = np.shape(traversabilities)
        self.grid = np.nan_to_num(traversabilities, nan = 0)
        self.state = start_state
        self.start_state = start_state
        self.end_state = end_state
        
        self.action_space = gym.spaces.Discrete(8)
        self.actions = ["N", "NE", "E", "SE", "S", "SW", "W", "NW"] 
        
        self.observation_space = gym.spaces.Discrete(int(self.rows*self.columns))
        
        self.diag_forward_penalty = -1
        self.side_penalty = -2
        self.diag_back_penalty = -3
        self.back_penalty = -4 
        
                
    def step(self, action_taken):
        state = self.state
        reward = 0
        done = False
        
        if (self.state == self.end_state).all():
            reward = 100
        else:
            reward = self.grid[self.state[0], self.state[1]]
            
        action = self.actions[action_taken]
        
        if action == "N":
            nxt_state = [self.state[0], self.state[1]-1]
        elif action == "S":
            nxt_state = [self.state[0], self.state[1]+1]
            reward += self.back_penalty
        elif action == "E":
            nxt_state = [self.state[0]+1, self.state[1]]
            reward += self.side_penalty
        elif action == "W":
            nxt_state = [self.state[0]-1, self.state[1]]   
            reward += self.side_penalty
        elif action == "NE":
            nxt_state = [self.state[0]+1, self.state[1]-1]
            reward += self.diag_forward_penalty
        elif action == "NW":
            nxt_state = [self.state[0]-1, self.state[1]-1]
            reward += self.diag_forward_penalty
        elif action == "SE":
            nxt_state = [self.state[0]+1, self.state[1]+1]
            reward += self.diag_back_penalty
        elif action == "SW":
            nxt_state = [self.state[0]-1, self.state[1]+1]   
            reward += self.diag_back_penalty
        
        if (nxt_state[0] >= 0) and (nxt_state[0] <= (self.rows -1)):
            if (nxt_state[1] >= 0) and (nxt_state[1] <= (self.columns -1)):
                if nxt_state != (1, 1):
                    state = nxt_state
        if (state == self.end_state).all():
            done = True
        
        return int(state[0]*self.rows+state[1]), reward, done, {}
    
    def reset(self):
        state = self.start_state
        
        return int(state[0]*self.rows+state[1])
    
    def render(self):
        fig, ax = plt.subplots()
    
        im = ax.imshow(self.grid)
        ax.set_title("Traversability")
        fig.colorbar(im, ax = ax)
        ax.plot(self.start_state[0], self.start_state[1], 'ro', markersize=12)
        ax.plot(self.end_state[0], self.end_state[1], 'bo', markersize=12)
        fig.savefig('test_fig4gym')
        
    


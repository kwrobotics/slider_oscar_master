#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Oct  3 18:03:39 2021

@author: OHelander
"""
import gym 
import numpy as np
import matplotlib.pyplot as plt
import random

from numpy import genfromtxt
from stable_baselines.common.env_checker import check_env

if __name__ == '__main__':

    env = gym.make("trav_env:traversability_env-v0")  

    action_space_size = env.action_space.n
    state_space_size = env.observation_space.n
    
    q_table = np.zeros((state_space_size, action_space_size))
    
    num_episodes = 100000
    max_steps_per_episode = 10 # but it won't go higher than 1
    
    learning_rate = 0.1
    discount_rate = 0.99
    
    exploration_rate = 1
    max_exploration_rate = 1
    min_exploration_rate = 0.01
    
    exploration_decay_rate = 0.01 #if we decrease it, will learn slower
    
    rewards_all_episodes = []
    
    # Q-Learning algorithm
    for episode in range(num_episodes):
        state = env.reset()
        
        done = False
        rewards_current_episode = 0
        
        for step in range(max_steps_per_episode):
            
            # Exploration -exploitation trade-off
            exploration_rate_threshold = random.uniform(0,1)
            if exploration_rate_threshold > exploration_rate: 
                action = np.argmax(q_table[state,:])
            else:
                action = env.action_space.sample()
                
            new_state, reward, done, info = env.step(action)
            
            # Update Q-table for Q(s,a)
            q_table[state, action] = (1 - learning_rate) * q_table[state, action] + \
                learning_rate * (reward + discount_rate * np.max(q_table[new_state,:]))
                
            state = new_state
            rewards_current_episode += reward
            
            if done == True: 
                break
                
        # Exploration rate decay
        exploration_rate = min_exploration_rate + \
            (max_exploration_rate - min_exploration_rate) * np.exp(-exploration_decay_rate * episode)
        
        rewards_all_episodes.append(rewards_current_episode)
        
    # Calculate and print the average reward per 10 episodes
    rewards_per_thousand_episodes = np.split(np.array(rewards_all_episodes), num_episodes / 100)
    count = 100
    print("********** Average  reward per thousand episodes **********\n")
    
    for r in rewards_per_thousand_episodes:
        print(count, ": ", str(sum(r / 100)))
        count += 100
      
        
    print(q_table)
    
    start_state = np.array([150,150])
    end_state = np.array([150,0])
    curr_state = start_state
    row_size = 300
    column_size = 300
    actions = ["N", "NE", "E", "SE", "S", "SW", "W", "NW"]
    fig, ax = plt.subplots()
    grid = np.nan_to_num(genfromtxt('/home/robin/test_ws/src/test/src/traversability.csv', delimiter=','), nan = 0)
    im = ax.imshow(grid)
    ax.set_title("Traversability")
    fig.colorbar(im, ax = ax)
    work = True
    print(q_table[int(4*150+250),:])
    print(actions[np.argmax(q_table[int(4*150+250),:])])
    while work:
        
        action = actions[np.argmax(q_table[int(curr_state[0]*row_size+curr_state[1]),:])]
        if action == "N":
            nxt_state = [curr_state[0], curr_state[1]-1]
        elif action == "S":
            nxt_state = [curr_state[0], curr_state[1]+1]
        elif action == "E":
            nxt_state = [curr_state[0]+1, curr_state[1]]
        elif action == "W":
            nxt_state = [curr_state[0]-1, curr_state[1]]   
        elif action == "NE":
            nxt_state = [curr_state[0]+1, curr_state[1]-1]
        elif action == "NW":
            nxt_state = [curr_state[0]-1, curr_state[1]-1]
        elif action == "SE":
            nxt_state = [curr_state[0]+1, curr_state[1]+1]
        elif action == "SW":
            nxt_state = [curr_state[0]-1, curr_state[1]+1]   
        
        if (nxt_state[0] >= 0) and (nxt_state[0] <= (row_size -1)):
            if (nxt_state[1] >= 0) and (nxt_state[1] <= (column_size -1)):
                if nxt_state != (1, 1):
                    curr_state = nxt_state       
        
        ax.plot(curr_state[0], curr_state[1], 'ro', markersize=12)
        if (curr_state == end_state).all():
            work = False
            
            
        fig.savefig('Qtable_path')
        
        
        
    
    
    
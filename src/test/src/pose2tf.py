#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Mon Oct 11 15:17:58 2021

@author: ohelander
"""
import rospy
import tf 
import numpy as np

from geometry_msgs.msg import PoseStamped
from scipy.spatial.transform import Rotation as R

def callback(data):

    position = data.pose.position
    
    x_final = -position.z
    y_final = -position.x
    z_final = position.y
    
    position.x, position.y, position.z = x_final, y_final, z_final
    
    
    orientation = data.pose.orientation
    #r = R.from_quat([orientation.x, orientation.y, orientation.z, orientation.w])
    r = tf.transformations.quaternion_matrix([-orientation.x, -orientation.z, -orientation.y, -orientation.w])
  #  r[:, 2] *= -1
    trans = np.array([[0,0,1,0],
                      [0,1,0,0],
                      [-1,0,0,0],
                      [0,0,0,1]])

    r = np.matmul( r,trans)

    
    r = tf.transformations.quaternion_from_matrix(r)
    [orientation.x, orientation.y, orientation.z, orientation.w] = r
    
    
    tf_publisher = tf.TransformBroadcaster()
    tf_publisher.sendTransform((position.x, position.y, position.z),
                     (orientation.x, orientation.y, orientation.z, orientation.w),
                     rospy.Time.now(),
                     "world",
                     "odom")
    
    





if __name__ == '__main__':

    rospy.init_node('pose2tf', anonymous=True)
    rate = rospy.Rate(150)
    
    
    while not rospy.is_shutdown():
        
        rospy.Subscriber("pose", PoseStamped, callback)

        rospy.spin()






#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Oct  5 12:38:25 2021

@author: robin
"""

#import triad_openvr as vr
import rospy
import socket
import numpy as np

from geometry_msgs.msg import PoseStamped
from nav_msgs.msg import Odometry
#from ViveTrackers import ViveTracker

HOST = '192.168.0.112'                 # Symbolic name meaning all available interfaces
PORT = 1111              # Arbitrary non-privileged port



if __name__ == "__main__":
#    v = vr.triad_openvr()
#    detected_trackers = [device for device in v.devices if 'tracker' in device]
#    tracker = ViveTracker(v, 1) 
#    talker(tracker)
    rospy.init_node('vive_tracker2odom', anonymous=True)

    rate = rospy.Rate(150)

    pose_pub= rospy.Publisher("pose", PoseStamped, queue_size = 10)


    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.connect((HOST, PORT))
        while True:
            data = s.recv(1024)
            msg = np.frombuffer(data)
            if np.shape(msg) == (7,):
                print(msg)
                
                pose_to_publish = PoseStamped()
                pose_to_publish.pose.position.x = msg[0]
                pose_to_publish.pose.position.y = msg[1]
                pose_to_publish.pose.position.z = msg[2]
                
                pose_to_publish.pose.orientation.x = msg[3]
                pose_to_publish.pose.orientation.y = msg[4]      
                pose_to_publish.pose.orientation.z = msg[5]         
                pose_to_publish.pose.orientation.w = msg[6]
                
                pose_pub.publish(pose_to_publish)
                

                
            
       
                
                
                
                
    
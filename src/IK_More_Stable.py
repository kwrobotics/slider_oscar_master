#!/usr/bin/env python
from control.bdalg import append
import rospy
import math
import numpy as np
import pandas as pd
import pinocchio as pin
import quadprog
import time
import scipy.linalg as la
import control
# from slycot-0.4.0-pp37-pypy37_pp73-win_amd64.whl import slycot
from scipy import signal


from std_msgs.msg import Int8
from std_msgs.msg import Float64
from std_msgs.msg import Float64MultiArray
from gazebo_msgs.msg import LinkStates
from sensor_msgs.msg import JointState
# Lib for reading data from Excel
# from openpyxl import load_workbook
# from openpyxl import Workbook

import csv

#SLIDER urdf model
filePath = '/home/robin-lab/SLIDER_ARTO/src/SLIDER_ROTOGAIT/urdf/'
# NOTE: change to your own urdf location
filename_urdf = filePath + "SLIDER_ROTOGAIT_FOOT_pin.urdf"

#SLIDER state
q_base = np.array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0])
q_joint = np.zeros(10)
q_lower = np.array([-0.7853, -0.7853, 0.0, -0.7853, -
                   0.6, -0.7853, -0.7853, 0.0, -0.7853, -0.6])
q_upper = np.array([0.7853, 0.7853, 1.2, 0.7853, 0.6,
                   0.7853, 0.7853, 1.2, 0.7853, 0.6])
v_base = np.zeros(6)
v_joint = np.zeros(10)

lcontact_force = np.zeros(4)
rcontact_force = np.zeros(4)

zmp_ref = np.zeros(3)
next_zmp_ref = np.zeros(3)
support_foot = 0
foot_ref = np.zeros(9)

dcm_buffer = []

#SLIDER dimensions / properties
m = 14.788
h = 0.69  # constant CoM height
g = 9.8
omega = math.sqrt(g/h)
X = 0.1025  # along x in m
Y = 0.075  # along y in m
mu = 0.75  # ground friction
fzmin = 5


def link_state_callback(msg):
	global q_base, v_base
	q_base = np.array([msg.pose[1].position.x, msg.pose[1].position.y, (msg.pose[1].position.z),
	                  msg.pose[1].orientation.x, msg.pose[1].orientation.y, msg.pose[1].orientation.z, msg.pose[1].orientation.w])
	v_base = np.array([msg.twist[1].linear.x, msg.twist[1].linear.y, msg.twist[1].linear.z,
	                  msg.twist[1].angular.x, msg.twist[1].angular.y, msg.twist[1].angular.z])


def joint_state_callback(msg):
	global q_joint, v_joint
	q_joint = np.array([msg.position[3], msg.position[2], msg.position[4], msg.position[0], msg.position[1],
	                   msg.position[8], msg.position[7], msg.position[9], msg.position[5], msg.position[6]])
	v_joint = np.array([msg.velocity[3], msg.velocity[2], msg.velocity[4], msg.velocity[0], msg.velocity[1],
	                   msg.velocity[8], msg.velocity[7], msg.velocity[9], msg.velocity[5], msg.velocity[6]])


def lcontact_pads_callback(msg):
	global lcontact_force
	lcontact_force = msg.data


def rcontact_pads_callback(msg):
	global rcontact_force
	rcontact_force = msg.data


def zmp_ref_callback(msg):
	global zmp_ref, next_zmp_ref, support_foot, remaining_time
	zmp_ref[0:2] = np.asarray(msg.data[0:2])
	next_zmp_ref[0:2] = np.asarray(msg.data[2:4])
	support_foot = msg.data[4]


def foot_ref_callback(msg):
	global foot_ref
	foot_ref = np.asarray(msg.data)


def compute_CoM(time_passed, com_stacked, zmp):
	global omega
	I = np.eye(2)

	#compute CoM position and velocity given ZMP trajectory
	cosh_value = np.cosh(omega*min([time_passed, 0.4]))
	sinh_value = np.sinh(omega*min([time_passed, 0.4]))
	com_matrix = np.vstack((np.hstack((cosh_value*I, sinh_value*I/omega)),
       	                     np.hstack((omega*sinh_value*I, cosh_value*I))))
	zmp_matrix = np.vstack(((1-cosh_value)*I, -omega*sinh_value*I))

	com_ref = com_matrix.dot(com_stacked.reshape(4, 1)) + \
	                         zmp_matrix.dot(zmp.reshape(2, 1))
	return com_ref.flatten()


def compute_ZMP(left_pos, right_pos):
	global lcontact_force, rcontact_force
	alpha = 0.09  # along x in m to contact pad
	beta = 0.065  # along y in m to contact pad
	# lzmp = [0,0.2,0]
	# rzmp = [0,-0.2,0]

	lforce_sum = np.sum(lcontact_force)
	rforce_sum = np.sum(rcontact_force)

    #NOTE: may need to be local for walking
	if (rforce_sum > 0):
		#Calculate local CoP for right foot
		rx = alpha*(rcontact_force[2]+rcontact_force[3] -
		            rcontact_force[0]-rcontact_force[1]) / rforce_sum
		ry = beta*(rcontact_force[0]+rcontact_force[3] -
		           rcontact_force[1]-rcontact_force[2]) / rforce_sum
		rx = np.clip(rx, -alpha, alpha)
		ry = np.clip(ry, -beta, beta)
	 	#Transform to global coordinates: 	  R.T * [x; y; 0.0] + p
		rR = right_pos.rotation[0:3, 0:3]
		rp = right_pos.translation[0:3]
		rzmp = np.dot(rR.T, np.array([rx, ry, 0.0]).reshape(3, 1)) + rp.reshape(3, 1)

	if (lforce_sum > 0):
		#Calculate local CoP for left foot
		lx = alpha*(lcontact_force[2]+lcontact_force[3] -
		            lcontact_force[0]-lcontact_force[1]) / lforce_sum
		ly = beta*(lcontact_force[0]+lcontact_force[3] -
		           lcontact_force[1]-lcontact_force[2]) / lforce_sum
		lx = np.clip(lx, -alpha, alpha)
		ly = np.clip(ly, -beta, beta)
		#Transform to global coordinates: 	  R.T * [x; y; 0.0] + p
		lR = left_pos.rotation[0:3, 0:3]
		lp = left_pos.translation[0:3]
		lzmp = np.dot(lR.T, np.array([lx, ly, 0.0]).reshape(3, 1)) + lp.reshape(3, 1)

	if (lforce_sum > fzmin and rforce_sum > fzmin):  # double support
		zmp = (rzmp + lzmp) / 2
		return zmp.flatten()
	elif (rforce_sum > fzmin and lforce_sum <= fzmin):  # right single support
		return rzmp.flatten()
	elif (lforce_sum > fzmin and rforce_sum <= fzmin):  # left single support
		return lzmp.flatten()
	else:
		return np.zeros(3)  # should never happen


# def compute_force_torque():
# 	global lcontact_force, rcontact_force

# 	alpha = 0.09  # along x in m to contact pad
# 	beta = 0.065  # along y in m to contact pad

# 	lforce_measure = 0.25*np.sum(lcontact_force)
# 	rforce_measure = 0.25*np.sum(rcontact_force)

# 	ltorque_measure = np.hstack(alpha*(lcontact_force[2] + lcontact_force[3] - lcontact_force[0] - lcontact_force[1]), beta*(
# 		lcontact_force[0] + lcontact_force[3] - lcontact_force[1] - lcontact_force[2]))
# 	rtorque_measure = np.hstack(alpha*(rcontact_force[2] + rcontact_force[3] - rcontact_force[0] - rcontact_force[1]), beta*(
#         rcontact_force[0] + rcontact_force[3] - rcontact_force[1] - rcontact_force[2]))

#     return lforce_measure, rforce_measure, ltorque_measure, rtorque_measure


def qp_solver(P, q, G=None, h=None, A=None, b=None):
	# add to make positive definite
	qp_G = .5 * (P + P.T) + np.eye(P.shape[0])*(1e-10)
	qp_a = -q
	#assume no equality constraint (i.e. A=None)
	if A is not None:
		qp_C = -numpy.vstack([A, G]).T
		qp_b = -numpy.hstack([b, h])
		meq = A.shape[0]
	if G is None:   # no equality constraint
		qp_C = None
		qp_b = None
		meq = 0
	else:
		qp_C = -G.T
		qp_b = -h
		meq = 0

	return quadprog.solve_qp(qp_G, qp_a, qp_C, qp_b, meq)[0]


def LQR_Solver(A, B, Q, R):

    K, S, E = control.lqr(A, B, Q, R)

    return K


def compute_desired_wrench(com, com_ref, vcom, vcom_ref, zmp, zmp_ref, dt):
	global dcm_buffer, omega, g
	com_error = com_ref - com
	vcom_error = vcom_ref - vcom
	zmp_error = zmp_ref - zmp

	#Calculate dcm error and integral
	dcm_error = com_error + vcom_error/omega
	dcm_buffer.append(dcm_error)
	# TODO: change to exponential moving average?
	dcm_error_integral = sum(dcm_buffer)
	if (len(dcm_buffer) == 1000):
		dcm_buffer = dcm_buffer[1:]

	#Calculate zmp_feedback with DCM control law
	kp = 5
	ki = 10
	kz = 1
	zmp_feedback = zmp_ref - (1+(kp/omega))*(dcm_error) - \
	                          (ki/omega)*(dcm_error_integral) + \
	                           (kz/omega)*(zmp_error)

	#Calculate desired contact wrench
	force = m*(pow(omega, 2)*(com-zmp_feedback)-np.array([0.0, 0.0, -g]))
	return dcm_error, np.hstack((force, np.cross(com, force)))


# def compute_desired_wrench_Tianhu(com, com_ref, vcom, vcom_ref, zmp, zmp_ref, dt):
# 	global dcm_buffer, omega, g, h

# 	zmp_ref = [zmp_ref[1], zmp_ref[2], h]
# 	acom_ref = pow(omega, 2)*(com_ref - zmp_ref)
# 	dcm = com + vcom/omega
# 	vdcm = omega*(dcm - zmp)

# 	com_error = com_ref - com
# 	vcom_error = vcom_ref - vcom
# 	zmp_error = zmp_ref - zmp
# 	dcm_error = com_error + vcom_error/omega
# 	acom_error = pow(omega, 2)*(com_error - zmp_error)

# 	A = np.array([[0, 1, 0], [0, 0, 1], [0, 0, 0]])
# 	B = np.array([[0], [0], [1]])
# 	C = np.array([[1, 0, -pow(omega, 2)], [1, 1/omega, 0],
# 	             [1, 0, 0], [0, 1, 0], [0, 0, 1]])
# 	D = 0
# 	Q = np.diag([1, 1, 1])
# 	R = 1
# 	#sys = signal.StateSpace(A,B,C,D)
#     # sys = control.ss(A,B,C,D)
#     # X = np.matrix(np.zeros(3,1)) # X = [CoM,VCoM,ACoM]
#     # X[1] = com; X[2] = vcom; X[3] = acom;
#     # Y = np.matrix(np.zeros(5,1)) # Y = [VRP, DCM, CoM, VCoM, ACoM]
#     # U = np.matrix(np.zeros(1)) # U = [JCoM(CoM_dotdotdot)]
#     # X_Dot = np.matrix(np.zeros(3,1)) # X_Dot = [VCoM,ACoM,JCoM]
#     # X_Dot = A.*X + B.*U
#     # Y = C.*X
# 	K, _, _ = LQR_Solver(A, B, Q, R)
# 	Y_error = np.array([[zmp_error], [dcm_error], [com_error],
# 	                   [vcom_error], [acom_error]])
# 	CoM_Jerk_error = np.dot(K, Y_error)
# 	acom_error = acom_error + CoM_Jerk_error*dt

# 	#Calculate desired contact wrench
# 	#force = m*(pow(omega,2)*(com-zmp_feedback)-np.array([0.0,0.0,-g]))
# 	force = m*(acom_ref + acom_error - np.array([0.0, 0.0, -g]))
# 	return dcm_error, np.hstack((force, np.cross(com, force)))


def distribute_wrench(w):
	U = np.matrix([[1, 0, -mu, 0, 0, 0],
				   [-1, 0, -mu, 0, 0, 0],
				   [0, 1, -mu, 0, 0, 0],
				   [0, -1, -mu, 0, 0, 0],
				   [0, 0, -Y, 1, 0, 0],
				   [0, 0, -Y, -1, 0, 0],
				   [0, 0, -X, 1, 0, 0],
				   [0, 0, -X, -1, 0, 0],
				   [Y, X, -mu*(X+Y), -mu, -mu, -1],
				   [-Y, X, -mu*(X+Y), -mu, -mu, -1],
				   [Y, -X, -mu*(X+Y), -mu, mu, -1],
				   [-Y, -X, -mu*(X+Y), mu, mu, -1],
				   [Y, X, -mu*(X+Y), mu, mu, 1],
				   [-Y, X, -mu*(X+Y), -mu, mu, 1],
				   [Y, -X, -mu*(X+Y), mu, -mu, 1],
				   [-Y, -X, -mu*(X+Y), -mu, -mu, 1],
				   [0, 0, -1, 0, 0, 0]])

	zeros = np.zeros((17, 6))
	G = np.vstack((np.hstack((U, zeros)), np.hstack((zeros, U))))  # 34 x 12
	h = np.zeros(34).reshape((34,))  # (34,)
	h[16] = -fzmin
	h[33] = -fzmin
	#TODO: include minimization of ankle torques and foot ratios
	M = np.hstack((np.eye(6, 6), np.eye(6, 6)))  # 6 x 12
	b = w  # (6,)

	P = np.dot(M.T, M)
	q = -np.dot(M.T, b)

	return qp_solver(P, q, G, h)


def saturate_wrench(w):
	G = np.matrix([[1, 0, -mu, 0, 0, 0],
				  [-1, 0, -mu, 0, 0, 0],
				  [0, 1, -mu, 0, 0, 0],
				  [0, -1, -mu, 0, 0, 0]])
	h = np.zeros(4).reshape((4,))
	M = np.eye(6, 6)
	b = w

	P = np.dot(M.T, M)
	q = -np.dot(M.T, b)

	return qp_solver(P, q, G, h)


def compute_zmp_qp(w, com):
	global g, omega
	return com - ((np.array([0.0, 0.0, -g]) + w[0:3]/m)/pow(omega, 2))


def admittance_control(zmp_desired, zmp, com_ref, vcom, vcom_ref, dt):
	A = np.array([[0.02, 0, 0],
				  [0, 0.01, 0],
				  [0, 0, 0]])

	vcom_cmd = vcom_ref + A.dot(zmp-zmp_desired)
	com_cmd = com_ref + (vcom_cmd*dt)  # integrate vcom
	#acom_cmd = (vcom_cmd - vcom)/dt #differentiate vcom
	return com_cmd


# def foot_control(support_foot, left_pos, right_pos, L_position_ref, R_position_ref, L_orientation_ref, R_orientation_ref, com_ref, dt):

# 	k1 = 1  # stiffness of parallel spring
# 	k2 = 2  # stiffness of series springe
# 	b = 0.1  # viscosity of parallel damper

#     # State Space Control
# 	A = np.matrix(np.zeros(4, 4))
# 	A[1, 1] = -(k1 + k2)/b
# 	A[1, 3] = -(k1*k2)/b
# 	A[1, 4] = -k2
# 	A[2, 2] = A[1, 1]
# 	A[2, 4] = A[1, 3]
# 	A[3, 4] = 1
# 	B = np.matrix(np.zeros(4, 1))
# 	B[2] = -k2
# 	B[4] = 1

#     # LQR

# 	Q_torque_R = np.diag([1, 1, 1, 1])
# 	R_torque_R = 45
# 	Q_torque_P = np.diag([1, 1, 1, 1])
# 	R_torque_P = 45
# 	Q_force = np.diag([1, 1, 1, 1])
# 	R_force = 45

#     # LQR Gain
#     #Roll
# 	K_torque_R, _, _ = LQR_Solver(A, B, Q_torque_R, R_torque_R)
#     #Pitch
# 	K_torque_P, _, _ = LQR_Solver(A, B, Q_torque_P, R_torque_P)
#     #Vertical Z-direction
# 	K_force, _, _ = LQR_Solver(A, B, Q_force, R_force)

#     #Desired foot force/torque
# 	lforce_ref, ltorque_ref, rforce_ref, rtorque_ref = compute_desired_force_torque(
# 	    support_foot, L_position_ref, R_position_ref, com_ref)
#     #Measured foot force/torque
# 	lforce_measure, rforce_measure, ltorque_measure, rtorque_measure = compute_force_torque()
# 	ltorque_measure = [ltorque_measure[0], 0, ltorque_measure[1]]
# 	rtorque_measure = [rtorque_measure[0], 0, rtorque_measure[1]]
    
# 	lforce_error = lforce_ref - lforce_measure
# 	rforce_error = rforce_ref - rforce_measure
# 	ltorque_error = ltorque_ref - ltorque_measure
# 	rtorque_error = rtorque_ref - rtorque_measure
    
#     # l_P_error = L_position_ref - L_position
#     # r_P_error = R_position_ref - R_position
#     # Force - Position
# 	# foot_err =  foot_ref - foot_measure # 
# 	# P_err = foot_err[0:3]
# 	# V_err = foot_err[3:6]
# 	l_P_err = foot_ref[0:3]-(left_pos.translation-np.array([0.0, 0.0, 0.04]))
# 	r_P_err = foot_ref[0:3]-(right_pos.translation-np.array([0.0, 0.0, 0.04]))
# 	l_V_err = (foot_ref[0:3] - (left_pos.translation-np.array([0.0, 0.0, 0.04])))/dt
# 	r_V_err = (foot_ref[0:3] - (right_pos.translation-np.array([0.0, 0.0, 0.04])))/dt
	
    
    
# 	l_theta_error = L_orientation_ref - left_pos.orientation
# 	r_theta_error = R_orientation_ref - right_pos.orientation
# 	Y_lforce = [lforce_error, lforcedot_error, l_P_err, l_V_err]
# 	Y_rforce = [rforce_error, rforcedot_error, r_P_err, r_V_err]
# 	Y_ltorque = [ltorque_error, ltorquedot_error, l_theta_error, l_thetadot_error]
# 	Y_rtorque = [rtorque_error, rtorquedot_error, r_theta_error, r_thetadot_error]

#     #Acceleration_error
#     #Vertical Z-direction
# 	l_A_err = np.multiply(K_force, Y_lforce)
# 	r_A_err = np.multiply(K_force, Y_rforce)
#     #Roll
# 	l_theta_dotdot_err_R = np.multiply(K_torque_R, Y_ltorque)
# 	r_theta_dotdot_err_R = np.multiply(K_torque_R, Y_rtorque)
#     #Pitch
# 	l_theta_dotdot_err_P = np.multiply(K_torque_P, Y_ltorque)
# 	r_theta_dotdot_err_P = np.multiply(K_torque_P, Y_rtorque)
    
#     #Velocity_error
# 	l_theta_dot_err_R = l_theta_dot_err_R + l_theta_dotdot_err_R*dt
# 	r_theta_dot_err_R = r_theta_dot_err_R + r_theta_dotdot_err_R*dt
# 	l_theta_dot_err_P = l_theta_dot_err_P + l_theta_dotdot_err_P*dt
# 	r_theta_dot_err_P = r_theta_dot_err_P + r_theta_dotdot_err_P*dt
# 	l_V_err = l_V_err + l_A_err*dt
# 	r_V_err = r_V_err + r_A_err*dt
    
#     # Vertical direction Z- direction
# 	vrfoot_desired = (foot_ref[0:3]-(right_pos.translation-np.array([0.0, 0.0, 0.04])))/dt
# 	vlfoot_desired = (foot_ref[0:3]-(left_pos.translation-np.array([0.0, 0.0, 0.04])))/dt
# 	vrfoot_desired = foot_ref[3:6] +[vrfoot_desired[0], vrfoot_desired[1], l_V_err[2]]
# 	vlfoot_desired = foot_ref[3:6] +[vlfoot_desired[0], vlfoot_desired[1], r_V_err[2]]
    
#     # Orientation Roll and Pitch
# 	olfoot_desired = foot_ref[3:6] +[l_theta_dot_err_R[0], 0, l_theta_dot_err_P[2]]
# 	orfoot_desired = foot_ref[3:6] +[r_theta_dot_err_R[0], 0, r_theta_dot_err_P[2]]


# 	return vrfoot_desired, vlfoot_desired, orfoot_desired, orfoot_desired

# def compute_desired_force_torque(support_foot, L_position_ref, R_position_ref, com_ref):
# 	global com_ref_buffer
    
#     # J = #Moment of Inertia
    
# 	I = np.matrix(np.eye(3))
# 	O = np.matrix(np.zeros((3,3)))
# 	D_L1 = np.hstack((I, O)) 
# 	D_L2 = np.dot((np.matrix(L_position_ref - com_ref)), I)
# 	D_L = np.vstack(D_L1, D_L2)
# 	D_R1 = np.hstack((I, O))
# 	D_R2 = np.dot((np.matrix(R_position_ref - com_ref)), I)
# 	D_R = np.vstack(D_R1, D_R2)    
# 	D_LR_1 = np.hstack((I,I,O,O))
# 	D_LR_2 = np.hstack(np.dot((np.matrix(L_position_ref - com_ref)), (np.matrix(R_position_ref - com_ref)), I),I)
# 	D_LR = np.vstack(D_LR_1, D_LR_2)
# 	np.append(com_ref_buffer,com_ref)
# 	vcom_ref_pre = com_ref_buffer[(len(com_ref_buffer) - len(com_ref)+2):(len(com_ref_buffer) - len(com_ref)+4)]
# 	acom_ref = (com_ref[2:4] - vcom_ref_pre)/dt
# 	H1 = m*(np.append(acom_ref, 0) + [0, 0, -g])
# 	# H2 = J*acom_desired
# 	H2 = np.cross(com_ref, H1)
# 	H = np.vstack(H1,H2)
    


    
# 	if (support_foot == 1): #left foot - left foot velocity should be zero
    
# 			A = np.multiply(np.linalg.inv(D_L), H)
# 			lforce_ref = A[0:3]
# 			ltorque_ref = A[3:6]
# 			rforce_ref = np.zeros(3)
# 			rtorque_ref = np.zeros(3)

# 	elif (support_foot == -1): #right foot - right foot velocity should be zero
        
# 			A = np.multiply(np.linalg.inv(D_R), H)
# 			rforce_ref = A[0:3]
# 			rtorque_ref = A[3:6]
# 			lforce_ref = np.zeros(3)
# 			ltorque_ref = np.zeros(3)            
            
# 	else:
# 			#double support - both feet velocities should be zero
            
# 			A = np.multiply(np.linalg.inv(D_LR), H)
# 			lforce_ref = A[0:3]
# 			rforce_ref = A[3:6]
# 			ltorque_ref = A[6:9]
# 			rtorque_ref = A[9:12]
        
# 	return lforce_ref, ltorque_ref, rforce_ref, rtorque_ref
    
def inverse_kinematics_acc(Jcom, dJcom, acom_desired, Jlfoot, dJlfoot, alfoot_desired, Jrfoot, dJrfoot, arfoot_desired, q_prev, qdot_prev):
	global q_lower, q_upper
	# Klim = 0.1 #proportional gain for joint limits
	# #Set up constraints matrices
	# G = np.hstack(( np.zeros((20, 6)), np.kron(np.eye(10, dtype=int), np.array([[-1],[1]])) ))

	# h = np.zeros(20).reshape((20,))
	# for i in range(10):
	# 	#qdot_lower = (q_lower[i]-q_prev[i+7])*1000 
	# 	#qdot_upper = (q_upper[i]-q_prev[i+7])*1000
	# 	#https://hal.archives-ouvertes.fr/hal-01356989v3/document
	# 	h[i*2] = -(2/(0.001**2))*(q_lower[i]-q_prev[i+7]-0.001*qdot_prev[i+6])#-Klim*(qdot_lower-qdot_prev[i+6])*1000
	# 	h[i*2+1] = (2/(0.001**2))*(q_upper[i]-q_prev[i+7]-0.001*qdot_prev[i+6])#Klim*(qdot_upper-qdot_prev[i+6])*1000

	#Set up cost matrix for CoM
	Pcom = np.dot((dJcom*0.001 + Jcom).T, dJcom*0.001 + Jcom) #(dJ/dt)*dt + J
	rcom = -np.dot(acom_desired-np.dot(dJcom, qdot_prev), dJcom*0.001 + Jcom)

	#Set up cost matrices for foot trajectories
	Plfoot = np.dot((dJlfoot*0.001 + Jlfoot).T, dJlfoot*0.001 + Jlfoot)
	rlfoot = -np.dot(alfoot_desired-np.dot(dJlfoot, qdot_prev), dJlfoot*0.001 + Jlfoot)
	Prfoot = np.dot((dJrfoot*0.001 + Jrfoot).T, dJrfoot*0.001 + Jrfoot)
	rrfoot = -np.dot(arfoot_desired-np.dot(dJrfoot, qdot_prev), dJrfoot*0.001 + Jrfoot)

	Wcom = 1000
	Wfoot = 500
	P = Wcom*Pcom + Wfoot*(Plfoot + Prfoot)
	r = Wcom*rcom + Wfoot*(rlfoot + rrfoot)

	qdotdot_cmd = qp_solver(P, r)#, G, h)
	qdot_cmd = qdot_prev[6:16] + (qdotdot_cmd[6:16]*0.001) #Integrate to get qdot
	q_cmd = q_prev[7:17] + (qdot_cmd*0.001) #Integrate to get q
	return q_cmd

def inverse_kinematics_vel(Jcom, vcom_desired, Jrfoot, Jlfoot, vrfoot_desired, vlfoot_desired, q_prev, dt):
	global q_lower, q_upper
	Klim = 0.2 #proportional gain for joint limits
	#Set up constraints matrices
	G = np.hstack(( np.zeros((20, 6)), np.kron(np.eye(10, dtype=int), np.array([[-1],[1]])) ))

	h = np.zeros(20).reshape((20,))
	for i in range(10):
		h[i*2] = -Klim*(q_lower[i]-q_prev[i+7])*1000
		h[i*2+1] = Klim*(q_upper[i]-q_prev[i+7])*1000
	#G = np.zeros((1, 6))
	#h = 0

	Kcom = np.array([-0.16, 0.16, 0.01]) #np.array([-0.04, 0.03, 0.01])
	Kfoot = np.array([0.01, 0.1, 0.1]) #np.array([0.01, 0.01, 0.01])

	#Set up cost matrix for vCoM
	Pcom = np.matmul(Jcom.T, Jcom)
	rcom = -np.matmul(np.multiply(Kcom, vcom_desired), Jcom)

	#Set up cost matrix for vfoot
	Prfoot = np.matmul(Jrfoot.T, Jrfoot)
	Plfoot = np.matmul(Jlfoot.T, Jlfoot)
	rrfoot = -np.matmul(np.multiply(Kfoot, vrfoot_desired), Jrfoot)
	rlfoot = -np.matmul(np.multiply(Kfoot, vlfoot_desired), Jlfoot)

	Wcom = 1000
	Wfoot = 50
	P = Wcom*Pcom + Wfoot*(Prfoot + Plfoot)
	r = Wcom*rcom + Wfoot*(rrfoot + rlfoot)

	qdot_cmd = qp_solver(P, r) #, G, h)
	q_cmd = q_prev[7:17] + (qdot_cmd[6:16]*dt) #Integrate to get q command
	return q_cmd
	
def smoothing(value, buffer, window):
	buffer.append(value)
	output = sum(buffer) / len(buffer)
	if (len(buffer) == window):
		buffer = buffer[1:]
	return output, buffer

#Define a RRBot joint positions publisher for joint controllers.
def slider_joint_positions_publisher():
	global q_base, q_joint, v_base, v_joint, support_foot, zmp_ref, next_zmp_ref

	#Initiate node for controlling 4 joints positions.
	rospy.init_node('slider_joint_positions_node', anonymous=True)

	#Define publishers for each joint position controller commands.
	pub_left_hip_pitch   = rospy.Publisher('/time_slider_gazebo/left_hip_pitch_position_controller/command', Float64, queue_size=1)#None)
	pub_left_hip_roll    = rospy.Publisher('/time_slider_gazebo/left_hip_roll_position_controller/command', Float64, queue_size=1)#None)
	pub_left_hip_slide   = rospy.Publisher('/time_slider_gazebo/left_hip_slide_position_controller/command', Float64, queue_size=1)#None)
	pub_left_ankle_roll  = rospy.Publisher('/time_slider_gazebo/left_ankle_roll_position_controller/command', Float64, queue_size=1)#None)
	pub_left_ankle_pitch = rospy.Publisher('/time_slider_gazebo/left_ankle_pitch_position_controller/command', Float64, queue_size=1)#None)

	pub_right_hip_pitch   = rospy.Publisher('/time_slider_gazebo/right_hip_pitch_position_controller/command', Float64, queue_size=1)#None)
	pub_right_hip_roll    = rospy.Publisher('/time_slider_gazebo/right_hip_roll_position_controller/command', Float64, queue_size=1)#None)
	pub_right_hip_slide   = rospy.Publisher('/time_slider_gazebo/right_hip_slide_position_controller/command', Float64, queue_size=1)#None)
	pub_right_ankle_roll  = rospy.Publisher('/time_slider_gazebo/right_ankle_roll_position_controller/command', Float64, queue_size=1)#None)
	pub_right_ankle_pitch = rospy.Publisher('/time_slider_gazebo/right_ankle_pitch_position_controller/command', Float64, queue_size=1)#None)

	#Define subscribers for measured joint and link states
	sub_link = rospy.Subscriber("/gazebo/link_states", LinkStates, link_state_callback)
	sub_joint = rospy.Subscriber("/time_slider_gazebo/joint_states", JointState, joint_state_callback)

	#Define subscribers for contact sensors to measure ZMP
	sub_lcontact_pads = rospy.Subscriber("/time_slider_gazebo/left_foot_contactForcePads", Float64MultiArray, lcontact_pads_callback)
	sub_rcontact_pads = rospy.Subscriber("/time_slider_gazebo/right_foot_contactForcePads", Float64MultiArray, rcontact_pads_callback)

	#Define subscriber for reference ZMP and foot trajectory
	sub_zmp_ref = rospy.Subscriber('/time_slider_gazebo/zmp_foothold', Float64MultiArray, zmp_ref_callback)
	sub_foot_ref = rospy.Subscriber('/time_slider_gazebo/foot_trajectory', Float64MultiArray, foot_ref_callback)

	#Define publisher for planner input
	pub_planner = rospy.Publisher('/time_slider_gazebo/planner_input', Float64MultiArray, queue_size=1)
	planner_msg = Float64MultiArray()

	#Define publishers for testing / analyzing
	pub_com = rospy.Publisher('/time_slider_gazebo/com', Float64MultiArray, queue_size=1)
	pub_com_ref = rospy.Publisher('/time_slider_gazebo/com_reference', Float64MultiArray, queue_size=1)
	pub_foot = rospy.Publisher('/time_slider_gazebo/foot', Float64MultiArray, queue_size=1)
	pub_foot_ref = rospy.Publisher('/time_slider_gazebo/foot_reference', Float64MultiArray, queue_size=1)
	pub_zmp = rospy.Publisher('/time_slider_gazebo/zmp_error', Float64MultiArray, queue_size=1)
	pub_dcm = rospy.Publisher('/time_slider_gazebo/dcm_error', Float64MultiArray, queue_size=1)
	pub_wrench = rospy.Publisher('time_slider_gazebo/zmp_wrench', Float64MultiArray, queue_size=1)
	pub_wrench_opt = rospy.Publisher('time_slider_gazebo/zmp_wrench_opt', Float64MultiArray, queue_size=1)
	com_msg = Float64MultiArray()
	com_ref_msg = Float64MultiArray()
	foot_msg = Float64MultiArray()
	foot_ref_msg = Float64MultiArray()
	zmp_msg = Float64MultiArray()
	dcm_msg = Float64MultiArray()
	wrench_msg = Float64MultiArray()
	wrench_opt_msg = Float64MultiArray()

	#Load the urdf model
	model = pin.buildModelFromUrdf(filename_urdf)
	data = model.createData()
	q = pin.neutral(model)

	dt = 0.01
	rate = rospy.Rate(1/dt) #1000 Hz
	#While loop to have joints follow a certain position, while rospy is not shutdown.
	i = 0
	time_delay = 2000
	intervel = 2000
	traj_len = 20000

	zmp_buffer = []
	vcom_buffer = []

	start_time = 0.0
	Jcom_prev = np.zeros((3, 16))

	# Publish the joint trajectory
	time_start = time.time()

	while not rospy.is_shutdown():
		if(i <= time_delay):
			index = 0
		elif(time_delay < i and i < traj_len + time_delay):
			index = i-time_delay
		elif(i >= traj_len + time_delay):
			index = traj_len - 1

		#Obtain current joint configuration
		q = np.concatenate((q_base, q_joint))
		v = np.concatenate((v_base, v_joint))#数据拼接
		
		#Locate CoM
		com = pin.centerOfMass(model, data, q, v)
		pin.forwardKinematics(model, data, q, v) 
		pin.updateFramePlacements(model, data)
		vcom = data.vcom[0]
		#Locate ZMP
		left_pos = data.oMf[model.getFrameId("Left_Foot")]
		right_pos = data.oMf[model.getFrameId("Right_Foot")]
		zmp = compute_ZMP(left_pos, right_pos)
			#Smooth measured ZMP
		zmp_msg.data, zmp_buffer = smoothing(zmp, zmp_buffer, 20)
		pub_zmp.publish(zmp_msg)

		#SET UP PLANNER FOR WALKING
		#When remaining_time ends, switch support foot, move to the next desired ZMP, and reset remaining time
		time_passed = rospy.get_time() - start_time
		if ((0.4 - time_passed) < dt):
			support_foot = -support_foot
			start_time = rospy.get_time()
			new_zmp_ref = next_zmp_ref
		else:
			new_zmp_ref = zmp_ref

		if (support_foot == 1): #left foot
			planner_msg.data = [com[0], vcom[0], com[1], vcom[1], rospy.get_time()-start_time, left_pos.translation[0], left_pos.translation[1], support_foot]
		elif (support_foot == -1): #right foot
			planner_msg.data = [com[0], vcom[0], com[1], vcom[1], rospy.get_time()-start_time, right_pos.translation[0], right_pos.translation[1], support_foot]
		
		pub_planner.publish(planner_msg)

		#COMPUTE COM_REF
		new_vcom, vcom_buffer = smoothing(vcom, vcom_buffer, 10) #smooth vcom
		com_ref = compute_CoM(time_passed, np.append(com[0:2], new_vcom[0:2]), new_zmp_ref[0:2])
		#NOTE: for push recovery; remove lines 436-7 for walking
		com_ref = np.array([0.0, 0.0, 0.0, 0.0])
		new_zmp_ref = np.array([0.0, 0.0, 0.0])
		

		#COMPUTE WRENCH
		dcm_error, w = compute_desired_wrench(com, np.append(com_ref[0:2], [h]), new_vcom, np.append(com_ref[2:4], [0.0]), zmp, new_zmp_ref, dt)
		# dcm_error, w = compute_desired_wrench_Tianhu(com, np.append(com_ref[0:2], [h]), new_vcom, np.append(com_ref[2:4], [0.0]), zmp, new_zmp_ref, dt)
		#Publish DCM error and wrench
		dcm_msg.data = dcm_error
		pub_dcm.publish(dcm_msg)
		wrench_msg.data = compute_zmp_qp(w, com) 
		pub_wrench.publish(wrench_msg)

		#DISTRIBUTE WRENCH
		w_dist = distribute_wrench(w)
		#Calculation zmp_qp from distributed wrench
		lzmp_w = compute_zmp_qp(w_dist[0:6], com) 
		rzmp_w = compute_zmp_qp(w_dist[6:12], com) 
		#Averaging left and right zmp_qp
		w_opt = (lzmp_w + rzmp_w)/2
		wrench_opt_msg.data = w_opt
		pub_wrench_opt.publish(wrench_opt_msg)

		#ADMITTANCE CONTROL - obtain vCoM
		com_cmd = admittance_control(w_opt, zmp, np.append(com_ref[0:2], [h]), vcom, np.append(com_ref[2:4], [0.0]), dt)
		#TODO: add foor damping control here
		#NOTE: for push recovery, uncomment line 460 for purely inverse kinematics control
		d = 0.05*math.sin(1*(time.time()-time_start))
		com_cmd = np.array([0.0, 0.0, h])
		# com_cmd = [2.47016227e-03, 9.55641467e-05, 5.81906342e-01]
		# com_cmd = [2.52296894e-03, 9.73108385e-05, 5.92153965e-01]
		# com_cmd = np.array([2.47016227e-03, 9.55641467e-05, 5.81906342e-01])
		#Publish CoM
		com_msg.data = np.append(com, vcom)
		pub_com.publish(com_msg)
		com_ref_msg.data = com_ref
		pub_com_ref.publish(com_ref_msg)

		#INVERSE KINEMATICS
		#Obtain jacobians and jacobian time derivatives
		pin.forwardKinematics(model, data, q, v) 
		pin.computeJointJacobians(model, data, q) 
		pin.computeJointJacobiansTimeVariation(model, data, q, v) 
		pin.updateFramePlacements(model, data)

		Jcom = pin.jacobianCenterOfMass(model, data, q)
		Jlfoot = pin.getFrameJacobian(model, data, model.getFrameId("Left_Foot"), pin.ReferenceFrame.LOCAL)#_WORLD_ALIGNED)
		Jrfoot = pin.getFrameJacobian(model, data, model.getFrameId("Right_Foot"), pin.ReferenceFrame.LOCAL)#_WORLD_ALIGNED)
		dJlfoot = pin.getFrameJacobianTimeVariation(model, data, model.getFrameId("Left_Foot"), pin.ReferenceFrame.LOCAL)
		dJrfoot = pin.getFrameJacobianTimeVariation(model, data, model.getFrameId("Right_Foot"), pin.ReferenceFrame.LOCAL)
		if (np.all(Jcom_prev==0)):
			dJcom = Jcom_prev
		else:
			dJcom = (Jcom-Jcom_prev)*1000
		Jcom_prev = Jcom

		#CoM acceleration / velocity-based tracking law
		#acom_desired =  np.multiply(Kp_com, com_cmd-com) + np.multiply(Kd_com, vcom_cmd-vcom) + acom_cmd
		vcom_desired = (com_cmd-com)/dt
		# vcom_desired = [0, 0, 0]
		com_error = com_cmd - com
		# print('com:' , com)
		# print('com_error: ', com_error)
		# print('com_cmd:, ', com_cmd)
		# print('vcom desired: ', vcom_desired)

		#Foot acceleration/velocity-based tracking law (FFDC)
		#vlfoot = np.dot(Jlfoot[0:3], v)
		#vrfoot = np.dot(Jrfoot[0:3], v)
		if (support_foot == 1): #left foot - left foot velocity should be zero
			#arfoot_desired = np.multiply(Kp_foot, foot_ref[0:3]-(right_pos.translation-np.array([0.0, 0.0, 0.04]))) + np.multiply(Kd_foot, foot_ref[3:6]-vrfoot) + foot_ref[6:9]
			#alfoot_desired = np.multiply(Kd_foot, np.zeros(3)-vlfoot)

			vrfoot_desired = foot_ref[3:6] + (foot_ref[0:3]-(right_pos.translation-np.array([0.0, 0.0, 0.04])))/dt
			vlfoot_desired = np.zeros(3) 
		elif (support_foot == -1): #right foot - right foot velocity should be zero
			#arfoot_desired = np.multiply(Kd_foot,np.zeros(3)-vrfoot)
			#alfoot_desired = np.multiply(Kp_foot, foot_ref[0:3]-(left_pos.translation-np.array([0.0, 0.0, 0.04]))) + np.multiply(Kd_foot, foot_ref[3:6]-vlfoot) + foot_ref[6:9]

			vrfoot_desired = np.zeros(3) 
			vlfoot_desired = foot_ref[3:6] + (foot_ref[0:3]-(left_pos.translation-np.array([0.0, 0.0, 0.04])))/dt
		else:
			#double support - both feet velocities should be zero
			#arfoot_desired = np.multiply(Kd_foot, np.zeros(3)-vrfoot)
			#alfoot_desired = np.multiply(Kd_foot, np.zeros(3)-vlfoot)

			vrfoot_desired = np.zeros(3)
			vlfoot_desired = np.zeros(3) 
			
		#NOTE: for push recovery; remove lines 526-7 for walking 
		vrfoot_desired = (np.array([0.0, -0.273, 0.0])-(right_pos.translation-np.array([0.0, 0.0, 0.04])))/dt
		vlfoot_desired = (np.array([0.0, 0.273, 0.0])-(left_pos.translation-np.array([0.0, 0.0, 0.04])))/dt
		# print('vrfoot_desired:', vrfoot_desired)
		# vrfoot_desired, vlfoot_desired = [0, 0, 0], [0, 0, 0]
		#q_command = inverse_kinematics_acc(Jcom, dJcom[0:3], acom_desired, Jlfoot[0:3], dJlfoot[0:3], alfoot_desired, Jrfoot[0:3], dJrfoot[0:3], arfoot_desired, q, v)
		# q_command = inverse_kinematics_vel(Jcom, vcom_desired, Jrfoot[0:3], Jlfoot[0:3], vrfoot_desired, vlfoot_desired, q, dt)
		# print('foot_ref: ', foot_ref)
		# vrfoot_desired = np.zeros(3)
		# vlfoot_desired = np.zeros(3)
		print(q_joint)
		# print('com_ref: ', com_ref)
		q_command = inverse_kinematics_vel(Jcom, vcom_desired, Jrfoot[0:3], Jlfoot[0:3], vrfoot_desired, vlfoot_desired, q, dt)
		# print('q_command: ', q_command)
		# q_command = np.array(np.zeros(10))
		# print('q_command: ', q_command)

		#Publish old joint commands
		# urdf_compensation = 0.7
		# left_hip_slide_compensation = 0.5*0.106*math.tan(-left_hip_roll_position[index])# only compensate for 50%, if 1 will be too much
		# right_hip_slide_compensation = 0.5*0.106*math.tan(right_hip_roll_position[index])# only compensate for 50%, if 1 will be too much
		# pub_left_hip_pitch.publish(left_hip_pitch_position[index])
		# pub_left_hip_roll.publish(left_hip_roll_position[index])
		# pub_left_hip_slide.publish(left_hip_slide_position[index]-left_hip_slide_compensation-0.025-urdf_compensation) # 0.025: offset from ankle pitch axis to carbon leg bottom 
		# pub_left_ankle_roll.publish(left_ankle_roll_position[index])
		# pub_left_ankle_pitch.publish(left_ankle_pitch_position[index])
		# pub_right_hip_pitch.publish(right_hip_pitch_position[index])
		# pub_right_hip_roll.publish(right_hip_roll_position[index])
		# pub_right_hip_slide.publish(right_hip_slide_position[index]-right_hip_slide_compensation-0.025-urdf_compensation) # 0.025: offset from ankle pitch axis to carbon leg bottom  
		# pub_right_ankle_roll.publish(right_ankle_roll_position[index])
		# pub_right_ankle_pitch.publish(right_ankle_pitch_position[index])
		#print('zmp: ', np.array([zmpx[index], zmpy[index]]))


		#Publish new joint commands
		d = 0.2*math.sin(1*(time.time()-time_start))
		# q_command[3] = d
		# q_command[8] = d
		# pub_left_hip_roll.publish(q_command[0])
		# pub_left_hip_pitch.publish(q_command[1])
		pub_left_hip_slide.publish(q_command[2]) 
		pub_left_ankle_pitch.publish(q_command[3])
		# pub_left_ankle_roll.publish(q_command[4])

		# pub_right_hip_roll.publish(q_command[5])
		# pub_right_hip_pitch.publish(q_command[6])
		pub_right_hip_slide.publish(q_command[7])
		pub_right_ankle_pitch.publish(q_command[8])
		# pub_right_ankle_roll.publish(q_command[9])
		pub_left_hip_roll.publish(q_command[0])
		pub_left_hip_pitch.publish(q_command[1])
		# pub_left_hip_slide.publish(0) 
		# pub_left_ankle_pitch.publish(0)
		pub_left_ankle_roll.publish(q_command[4])

		pub_right_hip_roll.publish(q_command[5])
		pub_right_hip_pitch.publish(q_command[6])
		# pub_right_hip_slide.publish(0)
		# pub_right_ankle_pitch.publish(0)
		pub_right_ankle_roll.publish(q_command[9])

		i = i+1 

		if(i > traj_len + time_delay - 5):
			i = 0

		rate.sleep() #sleep for rest of rospy.Rate(100)
		# rospy.spin()


#Main section of code that will continuously run unless rospy receives interuption (ie CTRL+C)
if __name__ == '__main__':

	right_hip_pitch_position   = []
	right_hip_roll_position    = []
	right_hip_slide_position   = []
	right_ankle_roll_position  = []
	right_ankle_pitch_position = []
	left_hip_pitch_position    = []
	left_hip_roll_position     = []
	left_hip_slide_position    = []
	left_ankle_roll_position   = []
	left_ankle_pitch_position  = []
	zmpx = []
	zmpy = []

	# j = 0
	# filePath = '/home/robinlab/SLIDER_ARTO/src/slider_gazebo/data/'

	# with open(filePath + 'joint_trajectory.csv') as csvfile:
	# 	readCSV = csv.reader(csvfile, delimiter=',')
	# 	for row in readCSV:
	# 		if j != 0:
	# 			right_hip_pitch_position.append(float(row[0]))
	# 			right_hip_roll_position.append(float(row[1]))
	# 			right_hip_slide_position.append(float(row[2]))
	# 			right_ankle_roll_position.append(float(row[3]))
	# 			right_ankle_pitch_position.append(float(row[4]))
	# 			left_hip_pitch_position.append(float(row[5]))
	# 			left_hip_roll_position.append(float(row[6]))
	# 			left_hip_slide_position.append(float(row[7]))
	# 			left_ankle_roll_position.append(float(row[8]))
	# 			left_ankle_pitch_position.append(float(row[9]))
	# 			zmpx.append(float(row[10]))
	# 			zmpy.append(float(row[11]))
	# 		j = j+1

	# traj_len = j-1

	try: 
		slider_joint_positions_publisher()

	except rospy.ROSInterruptException: pass




